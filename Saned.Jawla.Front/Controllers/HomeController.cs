﻿using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Front.Models;

namespace Saned.Jawla.Front.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public HomeController()
        {
            _unitOfWork = new UnitOfWorkAsync();


        }


        public async Task<ActionResult> Index(long? id)
        {
            if (id != null)
            {
                var bo = await _unitOfWork.EventDetails.GetAsync().FirstAsync(u => u.EventId == id);


                EventViewModel result = new EventViewModel
                {
                    StartTime = Convert.ToDateTime(bo.StartTime).ToShortTimeString(),
                    EndTime = Convert.ToDateTime(bo.EndTime).ToShortTimeString(),
                    Latitude = bo.CategoryDetails.Latitude,
                    Longitude = bo.CategoryDetails.Longitude,
                    CountryId = bo.CategoryDetails.CountryId,
                    Name = bo.CategoryDetails.Name,
                    CityId = bo.CategoryDetails.CityId,
                    Description = bo.CategoryDetails.Description,
                    EndDate = bo.EndDate,
                    Id = bo.EventId,
                    EventStartDate = bo.CategoryDetails.EventStartDate.Value,
                    Owner = bo.Owner,


                };

                var imgs = await
                    _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(),
                        RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");

                result.ImagePath = imgs.FirstOrDefault(x => x.IsDefault) != null ?
                    "http://jawlaapi.saned-projects.com/uploads/" + imgs.FirstOrDefault(x => x.IsDefault).PhotoUrl
                    : "http://jawlaapi.saned-projects.com/img/logo.png";

                return View(result);
            }
            else
            {
                return View();
            }


        }
    }
}