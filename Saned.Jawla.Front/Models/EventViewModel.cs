﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saned.Jawla.Front.Models
{
    public class EventViewModel
    {
        public string Owner { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EventStartDate { get; set; }
        public long Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
    }
}