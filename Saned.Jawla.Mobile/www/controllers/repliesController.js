﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('repliesController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isReplyOwner = false;
    var userLoggedIn;

    function GetRepliesForComment(comment) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        appServices.CallService('replies', 'GET', "api/Comments/GetCommentsByParentId/" + comment.id, '', function (replies) {
            SpinnerPlugin.activityStop();
            if (replies && replies.length > 0) {
                angular.forEach(replies, function (reply) {
                    if (JSON.parse(CookieService.getCookie('userLoggedIn'))) {
                        userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        if (CookieService.getCookie('userLoggedIn')) {
                            isReplyOwner = userLoggedIn.id == reply.userId ? true : false;
                        }

                        if (typeof reply.photoUrl != 'undefined' && reply.photoUrl != null && reply.photoUrl != '' && reply.photoUrl != ' ') {
                            reply.photoUrl = reply.photoUrl;
                        }
                        else {
                            reply.photoUrl = 'img/profile.png';
                        }

                        reply.isReplyOwner = isReplyOwner;
                    }

                    $scope.replies = replies;
                    $scope.CommentContainsReplies = true;
                    SpinnerPlugin.activityStop();

                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                });
                           
            }
            else {
                $scope.replies = [];
                $scope.CommentContainsReplies = false;
                SpinnerPlugin.activityStop();
                setTimeout(function () {
                    $scope.$digest();
                }, fw7.DelayBeforeScopeApply);
            }
        });
    }

    InitService.addEventListener('ready', function () {
        $scope.form = {};
        $scope.eventRepliesForm = {};
        $scope.CommentContainsReplies = false;
        $scope.isUserVisitor = true;

        app.onPageInit('replies', function (page) {
            if ($rootScope.currentOpeningPage != 'replies') return;
            $rootScope.currentOpeningPage = 'replies';

        });

        app.onPageBeforeAnimation('replies', function (page) {
            if ($rootScope.currentOpeningPage != 'replies') return;
            $rootScope.currentOpeningPage = 'replies';
            resetRepliesForm();
        });

        app.onPageAfterAnimation('replies', function (page) {
            if ($rootScope.currentOpeningPage != 'replies') return;
            $rootScope.currentOpeningPage = 'replies';
            

            var comment = JSON.parse(CookieService.getCookie('requiredComment'));
            var requiredEvent = JSON.parse(CookieService.getCookie('requiredEvent'));

            if (typeof requiredEvent.eventId == 'undefined' || requiredEvent.eventId == null ) {
                requiredEvent.eventId = requiredEvent.categoryDetailsId
            }

            GetRepliesForComment(comment);
            if (CookieService.getCookie('userLoggedIn')) {
                $scope.isUserVisitor = false;
            }
            else {
                $scope.isUserVisitor = true;
            }
            resetRepliesForm();
        });

       var resetRepliesForm = function () {
            $scope.EventReplyReset = false;
            $scope.eventRepliesForm.replyText = null;
            if (typeof $scope.EventRepliesForm != 'undefined' && $scope.EventRepliesForm != null) {
                $scope.EventRepliesForm.$setPristine(true);
                $scope.EventRepliesForm.$setUntouched();
            }
        };

        $scope.submitReplyForm = function (isValid) {
            $scope.EventReplyReset = true;
            var requiredComment = JSON.parse(CookieService.getCookie('requiredComment'));
            var requiredEvent = JSON.parse(CookieService.getCookie('requiredEvent'));

            if (typeof requiredEvent.eventId == 'undefined' || requiredEvent.eventId == null) {
                if (requiredEvent.categoryDetailsId) {
                    requiredEvent.eventId = requiredEvent.categoryDetailsId;
                }
                else {
                    requiredEvent.eventId = requiredEvent.placeId;
                }
             
            }

            if (isValid) {
                var params = {
                    CommentText: $scope.eventRepliesForm.replyText,
                    CommentTypeId: 1,
                    ParentId: requiredComment.id,
                    RelatedId: requiredEvent.eventId
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('replies', 'POST', "api/Comments/SaveComment", params, function (result) {
                    SpinnerPlugin.activityStop();
                    if (result) {
                        resetRepliesForm();
                                    
                        $scope.CommentContainsReplies = true;
                        if (JSON.parse(CookieService.getCookie('userLoggedIn'))) {
                            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                            isReplyOwner = userLoggedIn.id == result.userId ? true : false;
                            result.isReplyOwner = isReplyOwner;
                            if (typeof userLoggedIn.photoUrl != 'undefined' && userLoggedIn.photoUrl != null && userLoggedIn.photoUrl != '' && userLoggedIn.photoUrl != ' ') {
                                result.photoUrl = userLoggedIn.photoUrl;
                            }
                            else {
                                result.photoUrl = result.photoUrl ? result.photoUrl : 'img/profile.png';
                            }
                        }
                        else {
                            result.photoUrl = result.photoUrl ? result.photoUrl : 'img/profile.png';
                        }
                        
                        $scope.replies.splice(0, 0, result);                       
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            }
        };

        $scope.DeleteReply = function (reply) {
            language.openFrameworkModal('تأكيد', 'هل انت متأكد من حذف التعليق ؟', 'confrim', function () {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('replies', 'POST', "api/Comments/DeleteComment/" + reply.id, '', function (result) {
                    if (result != null) {
                        SpinnerPlugin.activityStop();
                        $scope.replies = $scope.replies.filter(function (item) {
                            return item.id !== reply.id;
                        });

                        $scope.CommentContainsReplies = $scope.replies != null && $scope.replies.length > 0 ? true : false;
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });

            });
            
        };

        $scope.GoBack = function () {
            CookieService.removeCookie('requiredComment');
            CookieService.removeCookie('requiredEvent');
            helpers.GoBack();
        };

        app.init();
    });

}]);

