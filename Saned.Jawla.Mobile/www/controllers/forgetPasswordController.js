﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('forgetPasswordController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    InitService.addEventListener('ready', function () {
        app.onPageInit('forgetPass', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPass') return;
            $rootScope.currentOpeningPage = 'forgetPass';

        });

        app.onPageBeforeAnimation('forgetPass', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPass') return;
            $rootScope.currentOpeningPage = 'forgetPass';
            resetForgetPasswordForm();

        });

        app.onPageAfterAnimation('forgetPass', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPass') return;
            $rootScope.currentOpeningPage = 'forgetPass';
            resetForgetPasswordForm();
        });

        var resetForgetPasswordForm = function () {
            $scope.forgetPassReset = false;
            $scope.forgetPassForm.email = null;
            if (typeof $scope.ForgetPasswordForm != 'undefined' && $scope.ForgetPasswordForm != null) {
                $scope.ForgetPasswordForm.$setPristine(true);
                $scope.ForgetPasswordForm.$setUntouched();
            }
        }

        $scope.form = {};
        $scope.forgetPassForm = {};

        $scope.submitForm = function (isValid) {
            $scope.forgetPassReset = true;
            if (isValid) {
                var params = {
                    'email': $scope.forgetPassForm.email
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('forgetPass', "POST", "api/Account/ForgetPassword", params, function (res) {
                    if (res != null) {
                        SpinnerPlugin.activityStop();
                        CookieService.setCookie('confirmationMail', $scope.forgetPassForm.email);
                        language.openFrameworkModal('نجاح', 'تم إرسال الكود لبريدك الإليكتروني بنجاح .', 'alert', function () {               
                            helpers.GoToPage('resetPassword', null);
                        });
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        app.init();
    });

}]);

