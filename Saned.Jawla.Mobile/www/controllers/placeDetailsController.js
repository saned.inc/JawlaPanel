﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('placeDetailsController', ['$scope', '$rootScope', '$http', '$filter', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', 'SidePanelService', function ($scope, $rootScope, $http, $filter, InitService, $log, appServices, CookieService, helpers, SidePanelService) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var photos;
    var markerObj;
    var PlaceDefautImage;
    var photoObj;
    var placeId;
    var placeMarkers;
    var placeDetails;
    var placesIdsArray = [];
    var branchesPlaceMarkers = [];
    var PlaceImagesArray = [];
    var placePhotosArray = [];
    var allAmbassadors = [];
    var myPhotoBrowserPopupDark;
    var mySwiper;
    $rootScope.Advertisements = null;
   


    InitService.addEventListener('ready', function () {
        app.onPageInit('placeDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'placeDetails') return;
            $rootScope.currentOpeningPage = 'placeDetails';
            

            $$('#divInfinitePlaceDetails').on('ptr:refresh', function (e) {
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#PlaceDetailsSwiper');
                    }
                });
                $scope.placeImages = null;
                placePhotosArray = [];
                PlaceImagesArray = [];
                getPlaceDetails();
                app.pullToRefreshDone();
            });
        });

        app.onPageBeforeAnimation('placeDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'placeDetails') return;

            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#PlaceDetailsSwiper');
            $rootScope.currentOpeningPage = 'placeDetails';
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            placeId = page.query.placeId;
            $rootScope.load = true;
            $scope.placeImages = null;
            placePhotosArray = [];
            PlaceImagesArray = [];
            
            $scope.cites = myApp.fw7.Cities;

            //getPlaceDetails();

        });

        app.onPageAfterAnimation('placeDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'placeDetails') return;
            $rootScope.currentOpeningPage = 'placeDetails';
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            getPlaceDetails();
           setTimeout(function () {
                $scope.$digest();
            }, fw7.DelayBeforeScopeApply);
 
        });
        app.onPageReinit('placeDetails', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#PlaceDetailsSwiper');

        });

        var getPlaceDetails = function () {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            $scope.ambassadors = null
            $scope.placeContainsAmbassadors = false;
            allAmbassadors = [];
            if (userLoggedIn) {
                userId = userLoggedIn.id;
            }
            else {
                userId = "";    
            }
           
            appServices.CallService('placeDetails', 'GET', "api/VisitorCover/GetVisitorCoversByCategoryDetailsId/"+placeId,"", function (ambassadors) {
                if (ambassadors&&ambassadors.length > 0) {
                    angular.forEach(ambassadors, function (ambassador) {
                        ambassador.name = ambassador.name != null ? ambassador.name : ambassador.userName;
                        allAmbassadors.push(ambassador);
                    })
                    $scope.ambassadors = allAmbassadors;
                   
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.placeContainsAmbassadors = true;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }

            });
                var placeParamter = {
                    PageNumber: "1",
                    PageSize: "10",
                    Id: placeId,
                    userId: userId
                };

                appServices.CallService('placeDetails', 'POST', "api/CategoryDetails/ViewPlace", placeParamter, function (placeDetailsResult) {
                    SpinnerPlugin.activityStop();
                    if (placeDetailsResult) {                     
                        placeDetails = placeDetailsResult;
                        CookieService.setCookie(placeId, JSON.stringify(placeDetailsResult));
                        placeDetails = placeDetailsResult;
                        $scope.placeContainsImages = typeof placeDetailsResult.images != 'undefined' && placeDetailsResult.images.length > 0 ? true : false;
                        $scope.placeContainsbranches = typeof placeDetailsResult.branches != 'undefined' && placeDetailsResult.branches.length > 0 ? true : false;
                        $scope.placeContainsFaqs = typeof placeDetailsResult.faqs != 'undefined' && placeDetailsResult.faqs.length > 0 ? true : false;
                        $scope.isAmbassadorActive = typeof placeDetailsResult.ambassadorRequestId != 'undefined' && placeDetailsResult.ambassadorRequestId != 0 ? true : false;
                        $scope.placeDescription = typeof placeDetailsResult.description != 'null' && placeDetailsResult.description != "" ? true : false;
                        $scope.workingHours = typeof placeDetailsResult.workHours != 'null' && placeDetailsResult.workHours != "" ? true : false;

                        PlaceDefautImage = $filter("filter")(placeDetailsResult.images, { isDefault: true })[0];
                        $scope.placeImage = typeof PlaceDefautImage != 'undefined' && PlaceDefautImage != null ? PlaceDefautImage.imageUrl : 'img/pic.jpg';                   
                        if (typeof placeDetailsResult.images != 'undefined' && placeDetailsResult.images.length > 0) {
                            for (var i = 1; i <= placeDetailsResult.images.length; i++) {
                                var photoObj = { url: hostUrl + 'Uploads/' + placeDetailsResult.images[placeDetailsResult.images.length - i].imageUrl };
                                placePhotosArray.push(photoObj);
                            }

                            if (placePhotosArray.length > 0) {
                                myPhotoBrowserPopupDark = app.photoBrowser({
                                    photos: placePhotosArray,
                                    theme: 'dark',
                                    backLinkText: 'إغلاق',
                                    lazyLoading: true,
                                    maxZoom: 4,
                                    ofText: 'من',
                                    type: 'popup'
                                });
                            }

                            if (placeDetailsResult.images.length >= 4) {
                                for (var i = 1; i < 5 ; i++) {
                                    PlaceImagesArray.push(placeDetailsResult.images[placeDetailsResult.images.length - i]);
                                }
                                $scope.placeImages = PlaceImagesArray;
                                app.pullToRefreshDone();
                            }
                            else {
                                $scope.placeImages = placeDetailsResult.images;
                                app.pullToRefreshDone();
                            }

                        }

                        placeMarkers = [{ "title": placeDetailsResult.name, "lat": placeDetailsResult.latitude, "lng": placeDetailsResult.longitude, "description": placeDetailsResult.description }];
                        helpers.initMap('mapPlace', placeMarkers, 'placeDetails', '', function (map) {
                        });


                        branchesPlaceMarkers = [];
                        if (typeof placeDetailsResult.branches != 'undefined' && placeDetailsResult.branches.length > 0) {
                            angular.forEach(placeDetailsResult.branches, function (branche) {
                                if ((branche.latitude != "" && branche.longitude != "") || (branche.latitude != null && branche.longitude != null)) {
                                    $scope.branchesMap = true;
                                    markerObj = { "title": branche.city, "lat": branche.latitude, "lng": branche.longitude, "description": "" };
                                    branchesPlaceMarkers.push(markerObj);
                                }
                            });
                            helpers.initMap('mapPlaceBransh', branchesPlaceMarkers, 'placeDetails', '', function (map) {
                            });
                        }

                        $scope.place = placeDetailsResult;
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        language.openFrameworkModal('خطأ', 'فشل في استرجاع تفاصيل المكان', 'alert', function () { });
                        helpers.GoToPage('places', null);
                    }
                });
            
          
        }

        $scope.openEventGallery = function (index) {
            myPhotoBrowserPopupDark.open(index);
        };

        var matchSocial = function (socialLink) {
            if ((socialLink).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/))
                cordova.InAppBrowser.open(socialLink, '_system', 'location=no');
            else
                language.openFrameworkModal('خطأ', "اللينك غير صحيح", 'alert', function () { });
        }

        $scope.placeContact = function (contactType, object) {
            if (contactType == "call") {
                window.plugins.CallNumber.callNumber(
               function onSuccess(successResult) {
                   console.log("Success:" + successResult);
               }, function onError(errorResult) {
                   console.log("Error:" + errorResult);
               }, object, true);
            }
            else if (contactType == "whatsapp") {
                window.open('whatsapp://send?= مرحبا اخى&phone=+966' + object, "_system");
            }
            else if (contactType == "facebok") {
                matchSocial(object);
            }
            else if (contactType == "twitter") {
                matchSocial(object);
            }
            else if (contactType == "googleplus") {
                matchSocial(object);
            }
        }

        $scope.goToVisitorOpinion = function (rate,name,id,userRating) {

            helpers.GoToPage('visitorsOpinion', { rate: rate, name: name, id: id, userRating: userRating, placeDetails: placeDetails });
        }

        $scope.goToAmbassadorDetails = function (ambassadorRequestId,isAmbassadoor) {
            if (isAmbassadoor) {
                helpers.GoToPage('ambassadorDetails', { ambassadorId: ambassadorRequestId });
            }
            else {             
                    //language.openFrameworkModal('خطأ', ' انت لست سفيرا لهذا المكان .', 'alert', function () {
                    //   // helpers.GoToPage('contact', null);
                    //});                
            }
            
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {
            });
        };

        $scope.GoToPreviousPage = function () {
            window.history.back();
            history.go(-1);
            navigator.app.backHistory();      
           // myApp.fw7.views[0].router.back();
            //helpers.GoBack();
        };

         $scope.goToSearchResult = function (selectedCity) {
              $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
              $('#linkHomeBannerCity .item-after').html('');
              $rootScope.CurrentuserCity = selectedCity;
              $rootScope.cityName = selectedCity.name;
              $rootScope.cityId = selectedCity;
              helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });


}])




