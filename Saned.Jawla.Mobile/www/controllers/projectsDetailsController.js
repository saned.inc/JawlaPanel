﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('projectsDetailsController', ['$scope', '$rootScope', '$http', '$filter', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, $filter, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var projectId;
    var projectMarkers;
    var myPhotoBrowserPopupDark;
    var ProjectDefautImage;

    InitService.addEventListener('ready', function () {
        app.onPageInit('projectsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'projectsDetails') return;
            $rootScope.currentOpeningPage = 'projectsDetails';
            $$('#divInfiniteProjectDetails').on('ptr:refresh', function (e) {
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#ProjectDetailsSwiper');
                        app.pullToRefreshDone();
                    }
                });
            

            });
        });

        app.onPageReinit('projectsDetails', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#ProjectDetailsSwiper');
        });

        app.onPageBeforeAnimation('projectsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'projectsDetails') return;
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;


            $rootScope.currentOpeningPage = 'projectsDetails';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#ProjectDetailsSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;

        });

        app.onPageAfterAnimation('projectsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'projectsDetails') return;
            $rootScope.currentOpeningPage = 'projectsDetails';

           
             projectId = page.query.projectId;
            var projectParamter = {
                PageNumber: "1",
                PageSize: "10",
                Id: projectId
            }

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('projectsDetails', 'POST', "api/CategoryDetails/ViewProject", projectParamter, function (projectDetails) {
                SpinnerPlugin.activityStop();
                if (projectDetails) {
                    $scope.project = projectDetails;
                    $scope.projectContainImages = typeof projectDetails.images != 'undefined' && projectDetails.images.length > 0 ? true : false;

                    ProjectDefautImage = $filter("filter")(projectDetails.images, { isDefault: true })[0];
                    $scope.projectImage = typeof ProjectDefautImage != 'undefined' && ProjectDefautImage != null ? ProjectDefautImage.imageUrl : 'img/pic.jpg';
           
                     projectMarkers = [{ "title": projectDetails.name, "lat": projectDetails.latitude, "lng": projectDetails.longitude, "description": projectDetails.description }];
                    helpers.initMap('mapProject', projectMarkers, 'projectsDetails', '', function (map) {
                    });

                    if (typeof projectDetails.images != 'undefined' && projectDetails.images.length > 0) {
                        var photosArray = [];
                        angular.forEach(projectDetails.images, function (photo) {
                            photo.imageUrl = photo.imageUrl ? photo.imageUrl : 'img/pic.jpg';
                            var photoObj = { url: hostUrl + 'Uploads/' + photo.imageUrl };                        
                            photosArray.push(photoObj);
                        });

                        if (photosArray.length > 0) {
                            myPhotoBrowserPopupDark = app.photoBrowser({
                                photos: photosArray,
                                theme: 'dark',
                                backLinkText: 'إغلاق',
                                lazyLoading:true,
                                ofText: 'من',
                                type: 'popup'
                            });
                        }
                    }
      
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });

        });

        $scope.openEventGallery = function (index) {
            myPhotoBrowserPopupDark.open(index);
        };

        $scope.matchSocial = function (socialLink) {
            if ((socialLink).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/))
                cordova.InAppBrowser.open(socialLink, '_system', 'location=no');
            else
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
        }

        $scope.projectOwnerContact = function (contactType, object) {
            if (contactType == "call") {
                window.plugins.CallNumber.callNumber(
               function onSuccess(successResult) {
                   console.log("Success:" + successResult);
               }, function onError(errorResult) {
                   console.log("Error:" + errorResult);
               }, object, true);
            }
            else if (contactType == "whatsapp") {
                window.open('whatsapp://send?= مرحبا اخى&phone=+966' + object, "_system");
            }
            else if (contactType == "facebok") {
                $scope.matchSocial(object);
            }
            else if (contactType == "twitter") {
                $scope.matchSocial(object);
            }
            else if (contactType == "googleplus") {
                $scope.matchSocial(object);
            }
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

          $scope.goToSearchResult = function (selectedCity) {
              $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
              $('#linkHomeBannerCity .item-after').html('');
              $rootScope.CurrentuserCity = selectedCity;
              $rootScope.cityName = selectedCity.name;
              $rootScope.cityId = selectedCity;
              helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

