﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('favoritesController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var initFavorites = true;
    var allFavorites = [];

    function convertDate(inputFormat) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getMonth() + 1), pad(d.getDate()), d.getFullYear()].join('-');
    }

    function ClearListData() {
        allFavorites = [];
        loading = false;
        CookieService.setCookie('favorite-page-number', 1);
        $scope.favorites = null;
        $scope.favoriteAlert = false;
    }

    InitService.addEventListener('ready', function () {
        app.onPageInit('favourites', function (page) {
            if ($rootScope.currentOpeningPage != 'favourites') return;
            $rootScope.currentOpeningPage = 'favourites';

            $scope.txtFavoriteFilter = { value: null };

            $$('#divInfiniteFavorites').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.favoritesInfiniteLoader = true;

                var params = {
                    'PageNumber': parseInt(CookieService.getCookie('favorite-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('favourites', 'POST', "api/Favourite/GetFavourite", params, function (response) {
                    if (response && response.length > 0) {
                        SpinnerPlugin.activityStop();
                        loading = false;

                        CookieService.setCookie('favorite-page-number', parseInt(CookieService.getCookie('favorite-page-number')) + 1);
                        angular.forEach(response, function (favorite) {
                            favorite.imageUrl = favorite.imageUrl ? favorite.imageUrl : 'img/pic.jpg';
                            allFavorites.push(favorite);
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                        if (response && response.length < $rootScope.pageSize) {
                            $scope.favoritesInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteFavorites');
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                            return;
                        }
                    }
                    else {
                        SpinnerPlugin.activityStop();
                        $scope.favoritesInfiniteLoader = false;
                    }
                });
            });

            $$('#divInfiniteFavorites').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#favouriteswiper');
                        app.pullToRefreshDone();
                    }
                });
                app.pullToRefreshDone();
            });

        });

        app.onPageReinit('favourites', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#favouriteswiper');        
        });

        app.onPageBeforeAnimation('favourites', function (page) {
            if ($rootScope.currentOpeningPage != 'favourites') return;
            $rootScope.currentOpeningPage = 'favourites';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#favouriteswiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('favourites', function (page) {
            if ($rootScope.currentOpeningPage != 'favourites') return;
            $rootScope.currentOpeningPage = 'favourites';
            
            $scope.favoriteAlert = false;
            $scope.favoritesInfiniteLoader = true;
            $scope.txtFavoriteFilter = { value: null };

            var params = {
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('favourites', 'POST', "api/Favourite/GetFavourite", params, function (response) {
                SpinnerPlugin.activityStop();
                ClearListData();
                if (response && response.length > 0) {
                    app.attachInfiniteScroll('#divInfiniteFavorites');
                    for (var i = 0; i < response.length; i++) {
                        allFavorites.push(response[i]);
                    }
                    $scope.favorites = allFavorites;
                    $scope.favoritesInfiniteLoader = response.length > 2 ? true : false;
                    $scope.favoriteAlert = false;
                    angular.forEach($scope.favorites, function (favorite) {
                        favorite.imageUrl = favorite.imageUrl ? favorite.imageUrl : 'img/pic.jpg';
                    });
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.favorites = null;
                    $scope.favoriteAlert = true;
                    $scope.favoritesInfiniteLoader = false;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }

            });


          
        });

        $scope.FilterFavorites = function () {
            if (typeof $scope.txtFavoriteFilter.value != 'undefined') {
                ClearListData();

                var params = {};

                if ($scope.txtFavoriteFilter.value == null || $scope.txtFavoriteFilter.value == '' || $scope.txtFavoriteFilter.value == ' ') {
                    params = {
                        PageNumber: "1",
                        PageSize: $rootScope.pageSize
                    };
                }
                else {
                    var selectedDate = convertDate($scope.txtFavoriteFilter.value);

                    params = {
                        PageNumber: "1",
                        PageSize: $rootScope.pageSize,
                        EventStatyDate: selectedDate
                    };
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('favourites', 'POST', "api/Favourite/GetFavourite", params, function (response) {
                    $scope.favorites = null;
                    SpinnerPlugin.activityStop();
                    if (response && response.length > 0) {
                        loading = false;
                        SpinnerPlugin.activityStop();
                        $scope.favorites = response;
                        $scope.favoriteAlert = false;
                        $scope.favoritesInfiniteLoader = response.length > 2 ? true : false;
                        angular.forEach($scope.favorites, function (favorite) {
                            favorite.imageUrl = favorite.imageUrl ? favorite.imageUrl : 'img/pic.jpg';
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        SpinnerPlugin.activityStop();
                        $scope.favoriteAlert = true;
                        $scope.favoritesInfiniteLoader = false;
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            }
        };

        $scope.goToEventDetails = function (eventId) {
            helpers.GoToPage('activitiesDetails', { eventId: eventId });
        };

        $scope.DeleteFavorit = function (favoriteId) {
            language.openFrameworkModal('تأكيد', 'هل انت متأكد من حذف الفاعلية ؟', 'confrim', function () {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('favourites', 'POST', "api/Favourite/RemoveFavourite/" + favoriteId, {}, function (response) {
                    SpinnerPlugin.activityStop();
                    if (response) {
                        SpinnerPlugin.activityStop();
                        $scope.favorites = $scope.favorites.filter(function (item) {
                            return item.id !== favoriteId;
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            });
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

