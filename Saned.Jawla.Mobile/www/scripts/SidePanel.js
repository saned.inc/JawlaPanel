﻿myApp.angular.factory('SidePanelService', ['$rootScope', 'CookieService', 'appServices', function ($rootScope, CookieService, appServices) {
    'use strict';

    function DrawMenu() {
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var IsSocialLoggedIn = CookieService.getCookie('loginUsingSocial') ? CookieService.getCookie('loginUsingSocial') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? CookieService.getCookie('userLoggedIn') : null;
        $rootScope.IsVisitor = IsVisitor;

        var fw7 = myApp.fw7;

        setTimeout(function () {
            $scope.$apply();
            $rootScope.$apply();
        }, fw7.DelayBeforeScopeApply);

        if (IsVisitor == 'true') {
            $('#linkMenuToHome').show();
            $('#linkMenuToFavorites').hide();
            $('#linkMenuToSearch').show();
            $('#linkMenuToContact').show();
            if ($rootScope.isAmbassador) {
                $rootScope.isAmbassador = false;
                $('#linkMenuToAddAmbassador').hide();
            }
            else {
                $('#linkMenuToAddAmbassador').show();
            }
          
            $('#linkMenuToAmbassadorList').show();
            $('#linkMenuToUserProfile').hide();
            $('#linkMenuToChangePassword').hide();
            $('#linkMenuToLogin').show();

            $('#lblMenuLoginText').html('تسجيل دخول');
        }
        else {
            $('#linkMenuToHome').show();
            $('#linkMenuToFavorites').show();
            $('#linkMenuToSearch').show();
            $('#linkMenuToContact').show();
            if ($rootScope.isAmbassador) {
                $rootScope.isAmbassador = false;
                $('#linkMenuToAddAmbassador').hide();
            }
            else {
                $('#linkMenuToAddAmbassador').show();
            }
            $('#linkMenuToAmbassadorList').show();
            $('#linkMenuToLogin').show();
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToUserProfile').show();
                if (IsSocialLoggedIn == 'true') {
                    $('#linkMenuToChangePassword').hide();
                }
                else {
                    $('#linkMenuToChangePassword').show();
                }
                $('#lblMenuLoginText').html('تسجيل خروج');
            }
            else {
                $('#linkMenuToFavorites').hide();
                $('#linkMenuToUserProfile').hide();
                $('#linkMenuToChangePassword').hide();
                $('#lblMenuLoginText').html('تسجيل دخول');
            }
        }
    }

    function loadAdvertisments(callBack) {
        appServices.CallService('search', 'POST', "api/Advertisement/GetAdvertisementlist", { "PageNumber": 1, "PageSize": 10 }, function (ads) {
            if (ads) {
                myApp.fw7.Advertisements = ads;
                $rootScope.Advertisements = ads;
                callBack(ads);
            }
        });
    }

    return {
        DrawMenu: DrawMenu,
        loadAdvertisments: loadAdvertisments
    }

}]);