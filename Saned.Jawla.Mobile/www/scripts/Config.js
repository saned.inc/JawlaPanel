﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />
/// <reference path="../js/jquery-2.1.0.js" />

var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;

Template7.global = {
    android: isAndroid,
    ios: isIos
};

var $$ = Dom7;

if (isAndroid) {
    $$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');
    $$('.view .navbar').prependTo('.view .page');
}

if (isIos) {
    $$('.view.navbar-fixed').removeClass('navbar-fixed').addClass('navbar-through');
    $$('.view .navbar').prependTo('.view .page');
}

var myApp = {};

myApp.config = {
};

var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

myApp.angular = angular.module('myApp', ['ErrorCatcher', 'jkAngularRatingStars']).config(function ($provide) {
    $provide.decorator("$exceptionHandler", function ($delegate, $injector, $log) {
        return function (exception, cause) {
            $log.error(exception)
            $delegate(exception, cause);
        };
    });
}).directive('homePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'homeController',
        scope: {},
        templateUrl: 'pages/home.html'
    };
}).directive('placesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'placesController',
        scope: {},
        templateUrl: 'pages/places.html'
    };
}).directive('placesDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'placeDetailsController',
        scope: {},
        templateUrl: 'pages/placeDetails.html'
    };
}).directive('landingPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'landingController',
        scope: {},
        templateUrl: 'pages/landing.html'
    };
}).directive('loginPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'loginController',
        scope: {},
        templateUrl: 'pages/login.html'
    };
}).directive('signupPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'signupController',
        scope: {},
        templateUrl: 'pages/signup.html'
    };
}).directive('addAmbassadorPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'addAmbassadorController',
        scope: {},
        templateUrl: 'pages/addAmbassador.html'
    };
}).directive('forgetPassPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'forgetPasswordController',
        scope: {},
        templateUrl: 'pages/forgetPassword.html'
    };
}).directive('resetPasswordPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'resetPasswordController',
        scope: {},
        templateUrl: 'pages/resetPassword.html'
    };
}).directive('changePassPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'changePasswordController',
        scope: {},
        templateUrl: 'pages/changePassword.html'
    };
}).directive('activationPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'activationController',
        scope: {},
        templateUrl: 'pages/activation.html'
    };
}).directive('visitorsOpinionPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'visitorsOpinionController',
        scope: {},
        templateUrl: 'pages/visitorsOpinion.html'
    };
}).directive('ambassadorListPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'ambassadorListController',
        scope: {},
        templateUrl: 'pages/ambassadorList.html'
    };
}).directive('ambassadorDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'ambassadorDetailsController',
        scope: {},
        templateUrl: 'pages/ambassadorDetails.html'
    };
}).directive('ambassadorProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'ambassadorProfileController',
        scope: {},
        templateUrl: 'pages/ambassadorProfile.html'
    };
}).directive('userProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userProfileController',
        scope: {},
        templateUrl: 'pages/userProfile.html'
    };
}).directive('userPlacePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userPlaceController',
        scope: {},
        templateUrl: 'pages/userPlace.html'
    };
}).directive('userOpinionPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userOpinionController',
        scope: {},
        templateUrl: 'pages/userOpinion.html'
    };
}).directive('searchPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'searchController',
        scope: {},
        templateUrl: 'pages/search.html'
    };
}).directive('searchResultsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'searchResultsController',
        scope: {},
        templateUrl: 'pages/searchResults.html'
    };
}).directive('projectsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'projectsController',
        scope: {},
        templateUrl: 'pages/projects.html'
    };
}).directive('projectsDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'projectsDetailsController',
        scope: {},
        templateUrl: 'pages/projectsDetails.html'
    };
}).directive('eventsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'eventsController',
        scope: {},
        templateUrl: 'pages/events.html'
    };
}).directive('favouritesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'favoritesController',
        scope: {},
        templateUrl: 'pages/favorites.html'
    };
}).directive('activitiesDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'activitiesDetailsController',
        scope: {},
        templateUrl: 'pages/activitiesDetails.html'
    };
}).directive('contactPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'contactController',
        scope: {},
        templateUrl: 'pages/contact.html'
    };
}).directive('repliesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'repliesController',
        scope: {},
        templateUrl: 'pages/replies.html'
    };
}).directive('userDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userDetailsController',
        scope: {},
        templateUrl: 'pages/userDetails.html'
    };
});

myApp.angular.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    }
    return fallbackSrc;
});

myApp.fw7 = {
    app: new Framework7({
        swipeBackPage: false,
        swipePanel: false,
        panelsCloseByOutside: true,
        animateNavBackIcon: true,
        material: isAndroid ? true : false,
        materialRipple: false,
        modalButtonOk: 'تم',
        modalButtonCancel: 'إلغاء'
    }),
    options: {
        dynamicNavbar: true,
        domCache: true
    },
    views: [],
    Countries: [],
    Cities: [],
    Categories: [],
    Advertisements: [],
    DelayBeforeScopeApply: 500
};