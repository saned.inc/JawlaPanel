﻿myApp.angular.factory('CookieService', [function () {
    'use strict';

    var getCookie = function getCookie(cookieName) {
        return localStorage.getItem('Jawla_' + cookieName) ? localStorage.getItem('Jawla_' + cookieName) : null;
    }

    var setCookie = function setCookie(cookieName, cookieValue) {
        localStorage.setItem('Jawla_' + cookieName, cookieValue);
    }

    var removeCookie = function (cookieName) {
        localStorage.removeItem('Jawla_' + cookieName);
    }

    return {
        getCookie: getCookie,
        setCookie: setCookie,
        removeCookie: removeCookie
    };
}]);