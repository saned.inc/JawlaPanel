﻿/// <reference path="../js/jquery-2.1.0.js" />
/// <reference path="../js/framework7.min.js" />
/// <reference path="../js/angular.js" />

var calendarAddTransportArriveDate;

myApp.angular.controller('mainController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'helpers', 'CookieService', 'SidePanelService', function ($scope, $rootScope, $http, InitService, $log, helpers, CookieService, SidePanelService) {
    'use strict';

    var fw7 = myApp.fw7;
    var mainView = fw7.views['.view-main'];
    var app = myApp.fw7.app;
    $rootScope.currentOpeningPage = 'landing';

    $scope.GoToHome = function () {
        helpers.GoToPage('home', null);
    };
    $scope.GoToFavorites = function () {
        helpers.GoToPage('favourites', null);
    };
    $scope.GoToSearch = function () {
        helpers.GoToPage('search', null);
    };
    $scope.GoToContact = function () {
        helpers.GoToPage('contact', null);
    };
    $scope.GoToAddAmbassador = function () {
        helpers.GoToPage('addAmbassador', null);
    };
    $scope.GoToUserProfile = function () {
        helpers.GoToPage('userProfile', null);
    };
    $scope.GoToAmbassadorList = function () {
        helpers.GoToPage('ambassadorList', null);
    };
    $scope.GoToChangePassword = function () {
        helpers.GoToPage('changePass', null);
    };
    $scope.GoToLogin = function () {
        CookieService.removeCookie('appToken');
        CookieService.removeCookie('USName');
        CookieService.removeCookie('refreshToken');
        CookieService.removeCookie('userLoggedIn');
        CookieService.removeCookie('loginUsingSocial');
        CookieService.removeCookie('UserID');
        CookieService.setCookie('Visitor', false);
        helpers.GoToPage('login', null);
    };

    
    SidePanelService.DrawMenu();




    InitService.addEventListener('ready', function () {

    });

}]);


