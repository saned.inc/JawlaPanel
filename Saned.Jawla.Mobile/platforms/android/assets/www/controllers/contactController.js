﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('contactController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $scope.nameRegex = /^[A-Za-z0-9]*$/;
    $scope.contactUsForm = {};

    InitService.addEventListener('ready', function () {
        app.onPageBeforeAnimation('contact', function (page) {
            if ($rootScope.currentOpeningPage != 'contact') return;
            $rootScope.currentOpeningPage = 'contact';
            resetContactForm(); 
        });

        app.onPageAfterAnimation('contact', function (page) {
            if ($rootScope.currentOpeningPage != 'contact') return;
            $rootScope.currentOpeningPage = 'contact';           
            $scope.contactSubmitted = false;
            resetContactForm();
            
        });

        app.onPageInit('contact', function (page) {
            if ($rootScope.currentOpeningPage != 'contact') return;
            $rootScope.currentOpeningPage = 'contact';
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('contact', 'GET', 'api/Contact/GetApplicationInforamtion', "", function (contactUsSocilaResult) {
                SpinnerPlugin.activityStop();
                if (contactUsSocilaResult) {
                    $scope.contact = contactUsSocilaResult;
                    $scope.facebook = contactUsSocilaResult.facebookUrl;
                    $scope.twitter = contactUsSocilaResult.twitterUrl;
                    $scope.instgram = contactUsSocilaResult.instagrameUrl;
                    $scope.youtube = contactUsSocilaResult.youtubeUrl;
                    $scope.googleplus = contactUsSocilaResult.googlePlusUrl;
                    $scope.linkedin = contactUsSocilaResult.linkedInUrl;
                    $scope.callShow = contactUsSocilaResult.phoneNumber != "" && contactUsSocilaResult.phoneNumber != null ? true : false;
                    $scope.emailShow = contactUsSocilaResult.email != "" && contactUsSocilaResult.email != null ? true : false;
                    $scope.facebookShow = contactUsSocilaResult.facebookUrl != "" && contactUsSocilaResult.facebookUrl != null ? true : false;
                    $scope.twitterShow = contactUsSocilaResult.twitterUrl != "" && contactUsSocilaResult.twitterUrl != null ? true : false;
                    $scope.InstgramShow = contactUsSocilaResult.instagrameUrl != "" && contactUsSocilaResult.instagrameUrl != null ? true : false;
                    $scope.youtubeShow = contactUsSocilaResult.googlePlusUrl != "" && contactUsSocilaResult.googlePlusUrl != null ? true : false;
                    $scope.googlePlusShow = contactUsSocilaResult.googlePlusShow != "" && contactUsSocilaResult.googlePlusShow != null ? true : false;
                    $scope.LinkedInShow = contactUsSocilaResult.linkedInUrl != "" && contactUsSocilaResult.linkedInUrl != null ? true : false;
                }
            });
         
        });

        $scope.contactUsSubmitForm = function (isValid) {
            $scope.contectReset = true;
            if (isValid) {

                var contactVariables = {
                    "PhoneNumber": $scope.contactUsForm.userMobileContactUs,
                    "Name": $scope.contactUsForm.userNameContactUs,
                    "message": $scope.contactUsForm.userMessageContactUs,
                    "Email": $scope.contactUsForm.userEmailContactUs
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('contact', 'POST', 'api/ContactUs/AddMessage', contactVariables, function (contactUsResult) {
                    SpinnerPlugin.activityStop();
                    if (contactUsResult == "1") {
                        resetContactForm();
                        language.openFrameworkModal('تنبيه', 'تم ارسال الرسالة بنجاح', 'alert', function () { });
                    }
                    else {
                        resetContactForm();
                        language.openFrameworkModal('تنبيه', 'خطا اثناء ارسال رسالتك', 'alert', function () { });
                    }
                });
            }
        }

        $scope.matchSocial = function (socialLink) {
            if ((socialLink).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) 
                cordova.InAppBrowser.open(socialLink, '_system', 'location=no');
            else
                app.alert("اللينك غير صحيح");  
        }

        $scope.btnCallWithUs = function (contactPhone) {
            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {
                  console.log("Success:" + successResult);
            },  function onError(errorResult) {
                console.log("Error:" + errorResult);
            }, contactPhone, true);
        }

        $scope.btnEmailWithUs = function (contactEmail) {
            cordova.InAppBrowser.open("mailto:" + contactEmail + '?subject=' + 'جولة' + '&body=' + 'محتوي الرسالة', '_system', 'location=no,toolbar=no,zoom=no');
        }

        $scope.contactUsWithSocial = function (socialType) {
            if (socialType) {                
                if (socialType == 'facebook') {
                    $scope.matchSocial($scope.facebook);
                }
                else if (socialType == 'twitter') {
                    $scope.matchSocial($scope.twitter);                                   
                }
                else if (socialType =='instgram') {
                    $scope.matchSocial($scope.instgram);
                }
                else if (socialType == 'youtube') {
                    $scope.matchSocial($scope.youtube);                                   
                }
                else if (socialType == 'googleplus') {
                    $scope.matchSocial($scope.googleplus);          
                }
                else if (socialType == 'linkedin') {
                    $scope.matchSocial($scope.linkedin);                 
                }                              
            }
        }

        var resetContactForm = function () {
            $scope.contectReset = false;
            $scope.contactUsForm.userMobileContactUs = null;
            $scope.contactUsForm.userEmailContactUs = null;
            $scope.contactUsForm.userNameContactUs = null;
            $scope.contactUsForm.userMessageContactUs = null;
            if (typeof $scope.ContactUsForm != 'undefined' && $scope.ContactUsForm != null) {
                $scope.ContactUsForm.$setPristine(true);
                $scope.ContactUsForm.$setUntouched();
            }
        }

        $scope.contactUsBack = function () {
            helpers.GoBack();
        }
      
        app.init();
    });

}]);

