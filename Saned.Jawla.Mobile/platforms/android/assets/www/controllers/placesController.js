﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('placesController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';
    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading = false;
    var categoryId;
    var initPlacesInfinite;
    var allPlaces = [];

    InitService.addEventListener('ready', function () {
        app.onPageInit('places', function (page) {
            if ($rootScope.currentOpeningPage != 'places') return;
            $rootScope.currentOpeningPage = 'places';

            $$('#divInfinitePlaces').on('infinite', function () {
                $rootScope.load = true;
                    if (loading) return;
                    loading = true;
                    $scope.placesInfiniteLoader = true;
                    var placesParamter = {
                        'categoryId': categoryId,
                        'PageNumber': parseInt(CookieService.getCookie('places-page-number')) + 1,
                        'PageSize': $rootScope.pageSize
                    }

                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    appServices.CallService('places', 'POST', "api/CategoryDetails/SearchCategoryResult",
                        placesParamter
                    , function (placesResult) {
                        SpinnerPlugin.activityStop();
                        if (placesResult && placesResult.length > 0) {
                            $scope.categoryName = placesResult[0].category;
                            loading = false;
                            CookieService.setCookie('places-page-number', parseInt(CookieService.getCookie('places-page-number')) + 1);
                            angular.forEach(placesResult, function (place) {
                                place.rating = parseInt(place.rating / 2);
                                place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                                allPlaces.push(place);
                            });
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                            if (placesResult && placesResult.length <= $rootScope.pageSize) {
                                $scope.placesInfiniteLoader = false;
                                setTimeout(function () {
                                    $scope.$digest();
                                }, fw7.DelayBeforeScopeApply);
                                app.detachInfiniteScroll('#divInfinitePlaces');
                                return;
                            }
                        }
                        else {
                          $scope.placesInfiniteLoader = false;
                        }
                    });
            });

            $$('#divInfinitePlaces').on('ptr:refresh', function (e) {
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#homeSwiper');
                        
                    }
                });
                $rootScope.load = true;
                updatePlaces();

            });
        });

        app.onPageBeforeAnimation('places', function (page) {
            if ($rootScope.currentOpeningPage != 'places') return;
            $rootScope.currentOpeningPage = 'places';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#Placeswiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');           
            $scope.selectedItem = $rootScope.CurrentuserCity;
            resetPlacesScope();
            $scope.places = "";
            $scope.cites = myApp.fw7.Cities;
          
        });

        app.onPageReinit('places', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#Placeswiper');
            if ($rootScope.cityId) {
                $scope.selectedItem = $rootScope.cityId;
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });

        app.onPageAfterAnimation('places', function (page) {
            if ($rootScope.currentOpeningPage != 'places') return;
            $rootScope.currentOpeningPage = 'places';
         
            categoryId = page.query.categoryId;
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            updatePlaces();
  
        });

        var updatePlaces = function () {
            resetPlacesScope();
            var placesParamter = {
                'categoryId': categoryId,
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize
            }
            appServices.CallService('places', 'POST', "api/CategoryDetails/SearchCategoryResult", placesParamter
                , function (placesResult) {
                    SpinnerPlugin.activityStop();
                    if (placesResult && placesResult.length > 0) {
                     
                        $scope.categoryName = placesResult[0].category;
                        $scope.placesInfiniteLoader = false;
                        app.attachInfiniteScroll('#divInfinitePlaces');
                        angular.forEach(placesResult, function (place) {
                            place.rating = parseInt(place.rating / 2);
                            place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                            allPlaces.push(place);
                        });
                        $scope.places = allPlaces;
                        app.pullToRefreshDone();
                    }
                    else {
                        $scope.placeAlert = true;
                        app.pullToRefreshDone();
                        $scope.placesInfiniteLoader = false;
                        if (categoryId == 1) {
                            $rootScope.tourismPlaces = true;
                            $scope.categoryType = "لايوجد مطاعم";
                            $scope.categoryName = "مطاعم";
                        }
                        if (categoryId == 2) {
                        
                            $scope.categoryType = "لايوجد اماكن سياحية";
                            $scope.categoryName = "اماكن سياحية";
                        }
                        if (categoryId == 5) {
                     
                            $scope.categoryType = "لايوجد اماكن تاريخية";
                            $scope.categoryName = "اماكن تاريخية";
                        }
                        if (categoryId == 6) {                       
                            $scope.categoryType = "لايوجد فنادق";
                            $scope.categoryName = " فنادق";
                        }
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });

        }

        var resetPlacesScope = function () {
            CookieService.setCookie('places-page-number', 1);
            $scope.placeAlert = false;
            $scope.places = null;
            loading = false;
            allPlaces = [];
        }

        $scope.goToPlaceDetails = function (placeId) {
            helpers.GoToPage('placeDetails', { placeId: placeId });
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
           
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });      
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

