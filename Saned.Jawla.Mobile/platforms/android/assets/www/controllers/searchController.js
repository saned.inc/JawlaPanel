﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('searchController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var currentPlaceLatitude;
    var currentPlaceLongitude;
    var initciriesLoades = false;
    var initCountriesLoaded = false;
    var initCitiesLoaded = false;
    var initMunicipalities = false;  
    var countryId;
    var cityId;
    var municipalityId;
    var categoryId;
    var placeId;
    $scope.searchListForm = {};
    $scope.searchForm = {};
   


    InitService.addEventListener('ready', function () {
        app.onPageBeforeAnimation('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';
            $scope.countries = myApp.fw7.Countries;
            $scope.categories = myApp.fw7.Categories;
            $rootScope.resetSearchForm();

        });

        app.onPageAfterAnimation('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';
            $rootScope.resetSearchForm();
            initciriesLoades = false;      
            $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.nameRegex = /^[A-Za-z0-9]*$/;
            $scope.IsCityDisabled = true;
            $scope.IsMunicipalityDisabled = true;
            $scope.IsPlacesDisabled = true;
            $scope.IsCityLoaded = false;
            $scope.IsMunicipalityLoaded = false;
            $scope.IsPlacesLoaded = false;
         
        });

        app.onPageInit('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';
        });

        $scope.searchSubmitForm = function () {
            $scope.IsCityDisabled = true;
            $scope.IsMunicipalityDisabled = true;
            $scope.IsPlacesDisabled = true;
            $scope.IsCityLoaded = false;
            $scope.IsMunicipalityLoaded = false;
            $scope.IsPlacesLoaded = false;
            $scope.municipalitiesList = null;
            $scope.placesList = null;
            $scope.cities = null;
            setTimeout(function () {
                $scope.$digest();
            }, fw7.DelayBeforeScopeApply);
            countryId = $scope.searchForm.countryId
            cityId = $scope.searchForm.cityId
            municipalityId = $scope.searchForm.municipalityId
            categoryId = $scope.searchForm.categoryId
            placeId = $scope.searchForm.placeId
            helpers.GoToPage('searchResults', { country: countryId, city: cityId, municipality: municipalityId, category: categoryId, place: placeId, currentPlaceLatitude: currentPlaceLatitude, currentPlaceLongitude: currentPlaceLongitude });
        }

        $rootScope.resetSearchForm = function () {
            $scope.searchForm.countryId = "";
            $('#linkSearchCountry .item-after').html('اختر دولة');
            $scope.searchForm.cityId = "";
            $('#linkSearchCity .item-after').html('اختر مدينة');
            $scope.searchForm.municipalityId = "";
            $('#linkSearchMunicipality .item-after').html('اختر الحي');
            $scope.searchForm.categoryId = "";
            $('#linkSearchCategory .item-after').html('اختر التصنيف');
            $scope.searchForm.placeId = "";
            $('#linkSearchPlace .item-after').html('اختر المكان');          
            $scope.searchForm.nearBy = "";
            $scope.IsCityDisabled = true;
            $scope.IsMunicipalityDisabled = true;
            $scope.IsPlacesDisabled = true;
            $scope.locationDisabled = false;
        }

        $scope.loadCitiesInCountry = function (categoryId, countryId, cityId, municipalityId) {
            initCountriesLoaded = true;
            $('#linkSearchPlace .item-after').html('اختر المكان');
            $scope.searchForm.placeId = "";
            $('#linkSearchCity .item-after').html('اختر مدينة');
            $scope.searchForm.cityId = "";
            $('#linkSearchMunicipality .item-after').html('اختر الحي');
            $scope.searchForm.municipalityId = "";
            $scope.IsMunicipalityDisabled = true;
            $scope.IsMunicipalityLoaded = false;
            if (initCitiesLoaded == false || initMunicipalities == false) {
                cityId = "";
                municipalityId = ""
            }
            $scope.loadPlacesInCategory(categoryId, countryId,municipalityId,cityId);
            $scope.IsCityDisabled = true;
            $scope.IsCityLoaded = true;
            appServices.CallService('search', 'GET', "api/City/FindCities/" + countryId, "", function (cities) {
                if (cities && cities.length > 0) {
                    $scope.IsCityDisabled = false;
                    $scope.IsCityLoaded = false;
                    $scope.cities = cities;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.IsCityDisabled = false;
                    $scope.IsCityLoaded = false;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }

            });
        }

        $scope.loadMunicipalitiesInCity = function (categoryId, countryId, cityId, municipalityId) {
            initCitiesLoaded = true;
            $scope.searchForm.placeId = "";
            $('#linkSearchPlace .item-after').html('اختر المكان');
            if (cityId == null) {
                $scope.IsMunicipalityDisabled = true;
                $scope.IsMunicipalityLoaded = false;
                $('#linkSearchMunicipality .item-after').html('اختر الحي');
                $scope.searchForm.municipalityId = "";
            }
            else {
                $scope.IsMunicipalityDisabled = true;
                $scope.IsMunicipalityLoaded = true;
                $scope.loadPlacesInCategory(categoryId, countryId,municipalityId,cityId);
                appServices.CallService('search', 'GET', "api/Municipality/FindMunicipalities/" + cityId, "", function (municipalities) {
                    if (municipalities && municipalities.length > 0) {
                        $scope.IsMunicipalityLoaded = false;
                        $scope.IsMunicipalityDisabled = false;
                        $scope.municipalitiesList = municipalities;
                        $('#linkSearchMunicipality .item-after').html('اختر الحي');
                        $scope.searchForm.municipalityId = "";
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        $scope.IsMunicipalityDisabled = false;
                        $scope.IsMunicipalityLoaded = false;
                        $('#linkSearchMunicipality .item-after').html('اختر الحي');
                        $scope.searchForm.municipalityId = "";
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            }
        };

        $scope.loadPlacesInCategory = function (categoryId, countryId, municipalityId, cityId) {
            $scope.searchForm.placeId = "";
            $('#linkSearchPlace .item-after').html('اختر المكان');
            if (initCountriesLoaded == true || initCitiesLoaded == true || initMunicipalities) {
                $scope.resetInit();
                $scope.IsPlacesDisabled = true;
                $scope.IsPlacesLoaded = false;
            }
            else if (initCountriesLoaded == false || initCitiesLoaded == false) {
                $scope.IsPlacesDisabled = true;
                $scope.IsPlacesLoaded = true;
            }
            var categoryParams = {
                CategoryId: categoryId,
                CountryId: countryId,
                CityId: cityId,
                MunicipalityId: municipalityId
            }

            appServices.CallService('search', 'POST', "api/CategoryDetails/GetCategoryDetails", categoryParams, function (places) {
                if (places && places.length > 0) {
                    $scope.disablePlace();
                    $scope.placesList = places;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else if (places.length == 0) {
                    $scope.disablePlace();
                    $scope.placesList = "";
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });
        };

        $scope.refreshPlacesAfterMunicipalities = function (categoryId, countryId, cityId, municipalityId) {
            initMunicipalities = true;
            $scope.loadPlacesInCategory(categoryId, countryId,municipalityId,cityId);
        }

        $scope.getCurrentLocation = function () {
            $scope.IslocationLoaded = true;
            var onSuccess = function (position) {
                $scope.IslocationLoaded = false;
                currentPlaceLatitude = position.coords.latitude;
                currentPlaceLongitude = position.coords.longitude;
                helpers.geocodeLatLng(position.coords.latitude, position.coords.longitude,
                    function (address) {
                        $scope.searchForm.nearBy = address;
                        $scope.locationDisabled = true;
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    });
            };
            var onError = function (positionError) {
                language.openFrameworkModal('خطأ', 'خطا في تحديد الموقع', 'alert', function () { });
            };
            navigator.geolocation.getCurrentPosition(onSuccess, onError);
        }

        $scope.disablePlace = function () {
            $scope.IsPlacesDisabled = false;
            $scope.IsPlacesLoaded = false;
        }

        $scope.resetInit = function () {
            initCountriesLoaded = false;
            initCitiesLoaded = false;
            initMunicipalities = false;
          
        }

        $scope.GoBack = function () {
            helpers.GoBack();
        }
        app.init();
    });

}]);

