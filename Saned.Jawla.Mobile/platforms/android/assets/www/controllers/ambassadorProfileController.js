﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('ambassadorProfileController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var ambassadorId;

    InitService.addEventListener('ready', function () {
        app.onPageInit('ambassadorProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorProfile') return;
            $rootScope.currentOpeningPage = 'ambassadorProfile';

        });

        app.onPageBeforeAnimation('ambassadorProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorProfile') return;
            $rootScope.currentOpeningPage = 'ambassadorProfile';        
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('ambassadorProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorProfile') return;
            $rootScope.currentOpeningPage = 'ambassadorProfile';
            
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            if (advertisments !== undefined && advertisments.length > 0) {
                var counter = 0;
                var max = advertisments.length;

                var mySwiper = app.swiper('#AmbassadoProfileswiper', {
                    pagination: '.swiper-pagination',
                    loop: true,
                    lazyLoading: true,
                    preloadImages: false,
                    autoplayDisableOnInteraction: false,
                    onSlideNextStart: function (swiper) {
                        var time = parseInt(advertisments[counter].secondNumber * 1000);
                        setTimeout(function () {
                            counter++;
                            if (counter >= parseInt(max)) {
                                counter = 0;
                            }
                            swiper.slideNext();
                        }, time);
                    }
                });
            }
        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

          $scope.goToSearchResult = function (selectedCity) {
            $scope.selectedItem = '';
            $('#linkHomeBannerCity .item-after').html('المدينة');
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedId.id, municipality: "", category: "", place: "", currentPlaceLatitude: "", currentPlaceLongitude: "" });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

