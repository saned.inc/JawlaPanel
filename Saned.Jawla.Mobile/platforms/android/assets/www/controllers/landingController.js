﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('landingController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', '$interval', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, $interval) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function EnterApplication() {
        if (CookieService.getCookie('userLoggedIn') || CookieService.getCookie('UserID')) {
            if (CookieService.getCookie('UserEntersCode') == "false") {
                if (CookieService.getCookie('loginUsingSocial') == 'true') {
                    helpers.GoToPage('home', null);
                } else {
                    helpers.GoToPage('activation', null);
                }
            }
            else {
                helpers.GoToPage('home', null);
            }
        }
        else {
            if (CookieService.getCookie('UID')) {
                helpers.GoToPage('signup', null);
            }
            else {
                helpers.GoToPage('login', null);
            }
        }
    }

     function getCurrentLocation () {
        var onSuccess = function (position) {
            var currentPlaceLatitude = position.coords.latitude;
            var currentPlaceLongitude = position.coords.longitude;
            helpers.geocodeLatLng(position.coords.latitude, position.coords.longitude,
                function (address) {
                    $rootScope.currentLocation = address.split(',')[2];
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                });
        };
        var onError = function (positionError) {
            app.alert('خطا في تحديد الموقع');
        };
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }

    var GetAdvertisements = function () {
        appServices.CallService('search', 'POST', "api/Advertisement/GetAdvertisementlist", { "PageNumber": 1, "PageSize": 10 }, function (ads) {
            if (ads) {
                myApp.fw7.Advertisements = ads;
            }
        });
    }

    $document.ready(function () {
        console.log('ready');
        getCurrentLocation();

        EnterApplication();
        
        $rootScope.pageSize = 20;
        $rootScope.load = false;

        app.onPageInit('landing', function (page) {
            console.log('Landing OnPageInit');
            if ($rootScope.currentOpeningPage != 'landing') return;
            GetAdvertisements();
        });

        app.onPageAfterAnimation('landing', function (page) {
            console.log('Landing onPageAfterAnimation');
            if ($rootScope.currentOpeningPage != 'landing') return;
        });

        

        app.init();
    });

}]);