﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('addAmbassadorController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var ambassadorImage;
    $scope.form = {};
    $scope.ambassadorForm = {};
   
    $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $scope.nameRegex = /^[A-Za-z0-9]*$/;
    $scope.ageRegex = /^\d+$/;

    InitService.addEventListener('ready', function () {

        app.onPageBeforeAnimation('addAmbassador', function (page) {
            $scope.userLoged = false;
            $scope.emailDisabled = false;
            resetEmbassadorForm();
            if (CookieService.getCookie('userLoggedIn') != null) {
                var userInformation = JSON.parse(CookieService.getCookie('userLoggedIn'));
                $scope.ambassadorForm.addAmbassadorMobile = userInformation.phoneNumber;
                $scope.ambassadorForm.addAmbassadorFullname = userInformation.name;
                $scope.ambassadorForm.addAmbassadorAddress = userInformation.address;
                if (parseInt(userInformation.age) > 0) {
                    $scope.ambassadorForm.addAmbassadorAge = userInformation.age;
                }
                $scope.ambassadorForm.addAmbassadorAccounts = userInformation.soicalLinks;
                if (userInformation.email != null && userInformation.email != "") {
                    $scope.ambassadorForm.addAmbassadorEmail = userInformation.email;
                    $scope.emailDisabled = true;
                }
                else {
                    $scope.emailDisabled = false;
                }
                $scope.userLoged = true;

            }
        });

        app.onPageAfterAnimation('addAmbassador', function (page) {
            if ($rootScope.currentOpeningPage != 'addAmbassador') return;
            $rootScope.currentOpeningPage = 'addAmbassador';          
            $scope.AmbassadorPicture = "img/profile.png";                
        });

        app.onPageInit('addAmbassador', function (page) {           
            if ($rootScope.currentOpeningPage != 'addAmbassador') return;
            $rootScope.currentOpeningPage = 'addAmbassador';
           
        });

        $scope.addAmbassadorSubmittedForm = function (isValid) {
            $scope.ambassdorReset = true;
            if (isValid) {             
                var dataVariables = {
                    "UserName": "ahmed",
                    "PhoneNumber": $scope.ambassadorForm.addAmbassadorMobile,
                    "Name": $scope.ambassadorForm.addAmbassadorFullname,
                    "Role": "User",                                   
                    'Age': $scope.ambassadorForm.addAmbassadorAge,
                    'ImageFilename':'image.jpg',
                    "SoicalLinks": $scope.ambassadorForm.addAmbassadorAccounts,
                    "Email": $scope.ambassadorForm.addAmbassadorEmail,
                    'Address': $scope.ambassadorForm.addAmbassadorAddress,
                    'ImageBase64': ambassadorImage,
                  
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('addAmbassador', 'POST', 'Api/Ambassador/CreateAmbassador', dataVariables, function (addAmbassadorResult) {
                    if (addAmbassadorResult && addAmbassadorResult == 1) {
                        SpinnerPlugin.activityStop();
                        $rootScope.isAmbassador = true;
                        language.openFrameworkModal('نجاح', 'تم اضافة السفير بنجاح', 'alert', function () {
                            if (CookieService.getCookie('userLoggedIn')) {
                                var userInformation = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                userInformation.name = $scope.ambassadorForm.addAmbassadorFullname;
                                userInformation.age = $scope.ambassadorForm.addAmbassadorAge;
                                userInformation.address = $scope.ambassadorForm.addAmbassadorAddress;
                                userInformation.phoneNumber = $scope.ambassadorForm.addAmbassadorMobile;
                                userInformation.email = $scope.ambassadorForm.addAmbassadorEmail;
                                userInformation.soicalLinks = $scope.ambassadorForm.addAmbassadorAccounts;
                                CookieService.setCookie('userLoggedIn', JSON.stringify(userInformation));
                                resetEmbassadorForm();
                                helpers.GoToPage('home', null);
                            }
                            else {
                                resetEmbassadorForm();
                                CookieService.removeCookie('appToken');
                                CookieService.removeCookie('USName');
                                CookieService.removeCookie('refreshToken');
                                CookieService.removeCookie('userLoggedIn');
                                CookieService.removeCookie('loginUsingSocial');
                                CookieService.removeCookie('UserID');
                                CookieService.setCookie('Visitor', false);
                                helpers.GoToPage('login', null);
                            }
                        });
                    }
                    else {                   
                        SpinnerPlugin.activityStop();
                    }
                });
            }
        }

        $scope.addAmbassadorPicture = function () {
            navigator.camera.getPicture(function (userImageUri) {
                ambassadorImage = userImageUri
                $scope.AmbassadorPicture =  userImageUri;
                $scope.$digest();
            },
            function (message) {
                language.openFrameworkModal('خطأ', 'خطا اثناء تحميل الصورة', 'alert', function () { });
            },
            {
                quality: 100,
                destinationType: navigator.camera.DestinationType.DATA_URL,
                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            });
        }

        var resetEmbassadorForm = function () {
            $scope.addAmbassadorSubmitted = false;
            $scope.ambassdorReset = false;
            $scope.ambassadorForm.addAmbassadorMobile = "";
            $scope.ambassadorForm.addAmbassadorFullname = "";
            $scope.ambassadorForm.addAmbassadorAddress = "";
            $scope.ambassadorForm.addAmbassadorAge = "";
            $scope.ambassadorForm.addAmbassadorAccounts = "";
            $scope.ambassadorForm.addAmbassadorEmail = "";
            if (typeof $scope.AmbassadorForm != 'undefined' && $scope.AmbassadorForm != null) {
                $scope.AmbassadorForm.$setPristine(true);
                $scope.AmbassadorForm.$setUntouched();
            }
        }

        $scope.addAmbassadorBack = function () {
            helpers.GoBack();
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };


        app.init();
    });
}]);

