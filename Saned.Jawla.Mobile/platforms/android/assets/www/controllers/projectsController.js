﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('projectsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', 'SidePanelService', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, SidePanelService) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var categoryId;    
    var allProjects = [];

    InitService.addEventListener('ready', function () {
        app.onPageInit('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;
            $rootScope.currentOpeningPage = 'projects';          
            $$('#divInfiniteProjects').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.projectsInfiniteLoader = true;
                var params = {
                    'categoryId': categoryId,
                    'PageNumber': parseInt(CookieService.getCookie('projects-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('projects', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (projectsResults) {
                    SpinnerPlugin.activityStop();
                    if (projectsResults && projectsResults.length > 0) {
                        $scope.projectsInfiniteLoader = false;
                        loading = false;
                        CookieService.setCookie('projects-page-number', parseInt(CookieService.getCookie('projects-page-number')) + 1);
                        angular.forEach(projectsResults, function (project) {
                            project.rating = parseInt(project.rating / 2);
                            project.imageUrl = project.imageUrl ? project.imageUrl : 'img/pic.jpg';
                            allProjects.push(project);
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                        if (projectsResults && projectsResults.length < $rootScope.PageSize) {                         
                            $scope.projectsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteProjects');
                            return;
                        }
                    }
                    else {
                        $scope.projectsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteProjects');
                        return;
                    }
                });
            });

            $$('#divInfiniteProjects').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#homeSwiper');
                     }
                });

                updateProjects();
            });
        });

        app.onPageReinit('projects', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#ProjectSwiper');
           
        });
        app.onPageBeforeAnimation('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;

            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;

            $rootScope.currentOpeningPage = 'projects';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#ProjectSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('projects', function (page) {
            if ($rootScope.currentOpeningPage != 'projects') return;
            $rootScope.currentOpeningPage = 'projects';           
            $scope.projectsInfiniteLoader = true;
             categoryId = page.query.categoryId;
             $scope.resetProjectsScope();
             SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
               updateProjects();

      
           
        });

        $scope.gotoProjectDetails = function (projectId) {
            helpers.GoToPage('projectsDetails', { projectId: projectId });

        }
        
        var updateProjects = function () {
            $scope.resetProjectsScope();
                var projectsParamter = {
                    'categoryId': categoryId,
                    'PageNumber': 1,
                    'PageSize': $rootScope.pageSize
                }
                appServices.CallService('projects', 'POST', "api/CategoryDetails/SearchCategoryResult", projectsParamter, function (projectResult) {
                    SpinnerPlugin.activityStop();
                    if (projectResult && projectResult.length > 0) {
                        $scope.ProjectsAlert = false;
                        $scope.projectsInfiniteLoader = false;
                        angular.forEach(projectResult, function (project) {
                            project.rating = parseInt(project.rating / 2);
                            project.imageUrl = project.imageUrl ? project.imageUrl : 'img/pic.jpg';
                            allProjects.push(project);
                            app.attachInfiniteScroll('#divInfiniteProjects');
                        });
                        app.pullToRefreshDone();
                        $scope.projects = allProjects;
                    }
                    else {
                        $scope.projectsInfiniteLoader = false;
                        $scope.ProjectsAlert = true;
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
        }

        $scope.resetProjectsScope = function () {
            CookieService.setCookie('projects-page-number', 1);
            $scope.projects = null;
            allProjects = [];
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

