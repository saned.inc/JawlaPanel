﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('searchResultsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var country;
    var city;
    var cityName;
    var municipality;
    var category ;
    var place;
    var currentPlaceLatitude;
    var currentPlaceLongitude;
    var selectCityId;
    var initSearchResult = false;
    var allSearchPlaces = [];

    InitService.addEventListener('ready', function () {
        app.onPageInit('searchResults', function (page) {
            if ($rootScope.currentOpeningPage != 'searchResults') return;
            $rootScope.currentOpeningPage = 'searchResults';
          
            $scope.selectedItem = page.query.currentCity;

            $$('#divInfiniteSearchResults').on('infinite', function () {               
                if (loading) return;
                loading = true;
                $scope.searchInfiniteLoader = true;
                var searchVariable = {
                    PageNumber: parseInt(CookieService.getCookie('searchResults-page-number')) + 1,
                    PageSize: $rootScope.pageSize,
                    CountryId: country,
                    CityId: city,
                    MunicipalityId: municipality,
                    CategoryId: category,
                    CategoryDetailsId: place,
                    Longitude: currentPlaceLatitude,
                    Latitude: currentPlaceLongitude,
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('searchResults', 'POST', "api/CategoryDetails/SearchCategoryResult", searchVariable, function (searchResults) {
                    SpinnerPlugin.activityStop();
                    if (searchResults && searchResults.length > 0) {                       
                        loading = false;
                        CookieService.setCookie('searchResults-page-number', parseInt(CookieService.getCookie('searchResults-page-number')) + 1);
                        angular.forEach(searchResults, function (place) {
                            place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                            allSearchPlaces.push(place);
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                        if (searchResults && searchResults.length <= $rootScope.pageSize) {
                            $scope.searchInfiniteLoader = false;
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                            app.detachInfiniteScroll('#divInfiniteSearchResults');
                            return;
                        }
                    }
                    else {
                        $scope.searchInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteSearchResults');
                        return;
                    }
                });
            });

            $$('#divInfiniteSearchResults').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#SearchResultsSwiper');
                        app.pullToRefreshDone();
                    }
                });

            });
        });

        app.onPageReinit('searchResults', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#SearchResultsSwiper');
        });

        app.onPageBeforeAnimation('searchResults', function (page) {
            if ($rootScope.currentOpeningPage != 'searchResults') return;
            $rootScope.currentOpeningPage = 'searchResults';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#SearchResultsSwiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $scope.cites = myApp.fw7.Cities;           
  
        });

        app.onPageAfterAnimation('searchResults', function (page) {
            if ($rootScope.currentOpeningPage != 'searchResults') return;
            $rootScope.currentOpeningPage = 'searchResults';
            $scope.searchInfiniteLoader = true;
            $scope.resetSearchResults();
             country = page.query.country;
             city = page.query.city;
             cityName = page.query.cityName;
             municipality = page.query.municipality;
             category = page.query.category;
             place = page.query.place;
             currentPlaceLatitude = page.query.currentPlaceLatitude;
             currentPlaceLongitude = page.query.currentPlaceLongitude;
             $scope.CurrentuserCityId = city;
             //$scope.CurrentuserCityName = cityName;
             $scope.selectedItem = page.query.currentCity;

             getSearchResult(country, city);
            
        });

        var getSearchResult = function (country, city) {
            if (initSearchResult) {
                initSearchResult = false;
                $scope.resetSearchResults();
      
            }
            var serachVariable = {
                PageNumber: "1",
                PageSize: $rootScope.pageSize,
                CountryId: country,
                CityId: city,
                MunicipalityId: municipality,
                CategoryId: category,
                CategoryDetailsId: place,
                Longitude: currentPlaceLatitude,
                Latitude: currentPlaceLongitude,
            }

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('searchResults', 'POST', "api/CategoryDetails/SearchCategoryResult", serachVariable, function (searchResults) {
                SpinnerPlugin.activityStop();
                if (searchResults && searchResults.length > 0) {                 
                    $scope.searchInfiniteLoader = false;
                    app.attachInfiniteScroll('#divInfiniteSearchResults');
                    angular.forEach(searchResults, function (place) {
                        place.imageUrl = place.imageUrl ? place.imageUrl : 'img/pic.jpg';
                        allSearchPlaces.push(place);
                    });
                    $scope.PlacesResult = allSearchPlaces;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.PlacesResultAlert = true;
                    $scope.searchInfiniteLoader = false;
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
            });



        }

        $scope.goToplaceSearchedDetails = function (placeId, categoryId) {
            if (categoryId < 7) {
                helpers.GoToPage('placeDetails', { placeId: placeId });
            }
            else if (categoryId == 7) {
                helpers.GoToPage('activitiesDetails', { eventId: placeId });
            }
            else if (categoryId == 8) {
                helpers.GoToPage('projectsDetails', { projectId: placeId });
            }
            
        }

        $scope.resetSearchResults = function () {
            $scope.PlacesResult = null;
            $scope.PlacesResultAlert = false;
            allSearchPlaces = [];
            CookieService.setCookie('searchResults-page-number', '1');
        }

        $scope.goToSearchPage = function () {
            $rootScope.resetSearchForm();
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            initSearchResult = true;
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;         
            getSearchResult($rootScope.soadiaId, selectedCity.id);
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

