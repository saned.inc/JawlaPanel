﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('ambassadorListController', ['$scope', '$rootScope', '$http','InitService', '$log', 'appServices', 'CookieService', 'helpers', function ($scope, $rootScope, $http,InitService, $log, appServices, CookieService, helpers) {
    'use strict';
    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading=false; 
    var allAmbassador= []; 
    InitService.addEventListener('ready', function () {
        app.onPageBeforeAnimation('ambassadorList', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorList') return;
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;


            $rootScope.currentOpeningPage = 'ambassadorList';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#AmbassadorListswiper');
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');         
            $scope.selectedItem = $rootScope.CurrentuserCity;

            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageInit('ambassadorList', function (page) {
        
            $$('#divInfiniteAmbassadorList').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.ambassadorsInfiniteLoader = true;
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('ambassadorList', 'POST', "Api/Ambassador/GetAmbassadors", {
                    'PageNumber': parseInt(CookieService.getCookie('ambassadorList-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                }, function (ambassadorListResult) {
                    SpinnerPlugin.activityStop();
                    if (ambassadorListResult && ambassadorListResult.length > 0) {
                        loading = false;
                        CookieService.setCookie('places-page-number', parseInt(CookieService.getCookie('places-page-number')) + 1);
                        angular.forEach(ambassadorListResult, function (ambassador) {
                            ambassador.imageUrl = ambassador.imageUrl ? ambassador.imageUrl : 'img/profile.jpg';
                            allAmbassador.push(ambassador);
                        });
                        setTimeout(function () {
                            $scope.$digest();
                        }, fw7.DelayBeforeScopeApply);
                        if (ambassadorListResult && ambassadorListResult.length <= $rootScope.pageSize) {
                            $scope.ambassadorsInfiniteLoader = false;
                            setTimeout(function () {
                                $scope.$digest();
                            }, fw7.DelayBeforeScopeApply);
                            app.detachInfiniteScroll('#divInfiniteAmbassadorList');
                            return;
                        }
                    }
                    else {
                        $scope.ambassadorsInfiniteLoader = false;
                    }
                });
            });

            $$('#divInfiniteAmbassadorList').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#AmbassadorListswiper');
                    }
                });
                ubDateAmbassadors();
            });
        });

        app.onPageAfterAnimation('ambassadorList', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorList') return;
            $rootScope.currentOpeningPage = 'ambassadorList';
            resetAmbassadorScope();
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            ubDateAmbassadors();

        });

        app.onPageReinit('ambassadorList', function (page) {
            if ($rootScope.currentOpeningPage != 'ambassadorList') return;
            $rootScope.currentOpeningPage = 'ambassadorList';
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#AmbassadorListswiper');
            if ($rootScope.cityName) {
                $('#linkHomeBannerCity .item-inner').html($rootScope.cityName);
                $('#linkHomeBannerCity .item-after').html('');
                //$scope.selectedItem = null;
                //$scope.selectedItem = $rootScope.cityId;
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }
            else {
                $('#linkHomeBannerCity .item-inner').html($rootScope.cityBeforeChange);
                $('#linkHomeBannerCity .item-after').html('');
            }
        });

        var ubDateAmbassadors = function () {
            resetAmbassadorScope();
            $scope.ambassadorsInfiniteLoader = true;
            appServices.CallService('ambassadorList', 'POST', "Api/Ambassador/GetAmbassadors", {
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize
            }, function (ambassadorListResult) {
                SpinnerPlugin.activityStop();
                if (ambassadorListResult && ambassadorListResult.length > 0) {             
                    angular.forEach(ambassadorListResult, function (ambassador) {
                        ambassador.photoUrl = ambassador.photoUrl ? ambassador.photoUrl : "img/logo.png";
                        allAmbassador.push(ambassador);
                    });
                    app.attachInfiniteScroll('#infiniteLoaderAmbassador');
                    $scope.ambassadorList = allAmbassador;
                    $scope.ambassadorListAlert = false;
                    $scope.ambassadorsInfiniteLoader = false;
                    app.pullToRefreshDone();
                    setTimeout(function () {
                        $scope.$digest();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.ambassadorsInfiniteLoader = false;
                    $scope.ambassadorListAlert = true;
                    app.pullToRefreshDone();
                }
            });
        }

        var resetAmbassadorScope = function () {
            CookieService.setCookie('ambassadorList-page-number', '1');
            $scope.ambassadorList = null;
            allAmbassador = [];
            loading = false;
        }

        $scope.goToAmbassadorProfile = function (ambassadorId) {
            helpers.GoToPage('ambassadorDetails', { ambassadorId: ambassadorId });
        }

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });
        };

        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };
       app.init();
    });
}]);

