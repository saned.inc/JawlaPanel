﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('eventsController', ['$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', 'SidePanelService', function ($scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, SidePanelService) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loading;
    var initEvents = true;
    var allEvents = [];
    var mySwiper;

    function convertDate(inputFormat) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getMonth() + 1), pad(d.getDate()), d.getFullYear()].join('-');
    }

    function ClearListData() {
        allEvents = [];
        loading = false;
        CookieService.setCookie('events-page-number', 1);
        $scope.events = null;
        $scope.eventAlert = false;
    }

    InitService.addEventListener('ready', function () {
        app.onPageInit('events', function (page) {
            if ($rootScope.currentOpeningPage != 'events') return;
            $rootScope.currentOpeningPage = 'events';
            var categoryId = page.query.categoryId;
            $scope.categoryId = categoryId;
            $scope.txtEventFilter = { value: null };

            $$('#divInfiniteEvents').on('infinite', function () {
                $rootScope.load = true;
                if (loading) return;
                loading = true;
                $scope.eventsInfiniteLoader = true;

                var params = {
                    'categoryId': categoryId,
                    'PageNumber': parseInt(CookieService.getCookie('events-page-number')) + 1,
                    'PageSize': $rootScope.pageSize
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                    SpinnerPlugin.activityStop();
                    if (response && response.length > 0) {
                        loading = false;

                        CookieService.setCookie('events-page-number', parseInt(CookieService.getCookie('events-page-number')) + 1);
                        angular.forEach(response, function (event) {
                            event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                            allEvents.push(event);
                        });
                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                        if (response && response.length < $rootScope.pageSize) {
                            $scope.eventsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteEvents');
                            setTimeout(function () {
                                $scope.$apply();
                            }, fw7.DelayBeforeScopeApply);
                            return;
                        }
                    }
                    else {
                        $scope.eventsInfiniteLoader = false;
                    }
                });
            });

            $$('#divInfiniteEvents').on('ptr:refresh', function (e) {
                $rootScope.load = true;
                $rootScope.LoadAdvertisements(function (IsAdvertisementsLoaded) {
                    if (IsAdvertisementsLoaded) {
                        $rootScope.swiper.destroy(true, true);
                        $rootScope.initalSwiper('#Eventswiper');
                    }
                });
                var params = {
                    'categoryId': categoryId,
                    'PageNumber': 1,
                    'PageSize': $rootScope.pageSize
                };

                appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                    $scope.events = null;
                    ClearListData();
                    if (response && response.length > 0) {
                        $scope.eventAlert = false;
                        $scope.events = response;
                        angular.forEach($scope.events, function (event) {
                            event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                        });

                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);

                        app.pullToRefreshDone();
                    }
                    else {
                        $scope.eventAlert = true;
                        app.pullToRefreshDone();
                    }
                });
            });
        });

        app.onPageReinit('events', function (page) {
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#Eventswiper');         

        });

        app.onPageBeforeAnimation('events', function (page) {
            if ($rootScope.currentOpeningPage != 'events') return;
            $rootScope.currentOpeningPage = 'events';
            var advertisments = myApp.fw7.Advertisements;
            $scope.Advertisements = advertisments;
            $rootScope.swiper.destroy(true, true);
            $rootScope.initalSwiper('#Eventswiper');
            $scope.selectedItem = $rootScope.CurrentuserCity;
            $('#linkHomeBannerCity .item-inner').html($rootScope.CurrentuserCity.name);
            $('#linkHomeBannerCity .item-after').html('');                  
            $scope.cites = myApp.fw7.Cities;
        });

        app.onPageAfterAnimation('events', function (page) {
            if ($rootScope.currentOpeningPage != 'events') return;
            $rootScope.currentOpeningPage = 'events';
            var categoryId = page.query.categoryId;
            $scope.categoryId = page.query.categoryId;
            $scope.eventAlert = false;
            $scope.eventsInfiniteLoader = true;
            $scope.txtEventFilter = { value: null };

            var params = {
                'categoryId': categoryId,
                'PageNumber': 1,
                'PageSize': $rootScope.pageSize
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                $scope.events = null;
                ClearListData();
                SpinnerPlugin.activityStop();
                if (response && response.length > 0) {
                    app.attachInfiniteScroll('#divInfiniteEvents');
                    $scope.eventsInfiniteLoader = response.length > 2 ? true : false;

                    angular.forEach(response, function (event) {
                        allEvents.push(event);
                    });
                    $scope.events = allEvents;
                    $scope.eventAlert = false;
                    angular.forEach($scope.events, function (event) {
                        event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                    });
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    $scope.events = null;
                    $scope.eventAlert = true;
                    $scope.eventsInfiniteLoader = false;
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
                
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);        
        });
        

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.FilterEvents = function () {
            if (typeof $scope.txtEventFilter.value != 'undefined') {
                ClearListData()

                var params = {};

                if ($scope.txtEventFilter.value == null || $scope.txtEventFilter.value == '' || $scope.txtEventFilter.value == ' ') {
                    params = {
                        CategoryId: $scope.categoryId,
                        PageNumber: "1",
                        PageSize: $rootScope.pageSize
                    };
                }
                else {
                    var selectedDate = convertDate($scope.txtEventFilter.value);

                    params = {
                        CategoryId: $scope.categoryId,
                        PageNumber: "1",
                        PageSize: $rootScope.pageSize,
                        EventStatyDate: selectedDate
                    };
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('events', 'POST', "api/CategoryDetails/SearchCategoryResult", params, function (response) {
                    $scope.events = null;
                    SpinnerPlugin.activityStop();
                    if (response && response.length > 0) {
                        loading = false;
                        SpinnerPlugin.activityStop();
                        $scope.events = response;
                        $scope.eventAlert = false;
                        $scope.eventsInfiniteLoader = response.length > 2 ? true : false;
                        angular.forEach($scope.events, function (event) {
                            event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                        });

                        var txtEventDateFilter = document.getElementById('txtEventDateFilter');
                        //txtEventDateFilter.setAttribute('type', 'text');
                        //txtEventDateFilter.focus();
                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        SpinnerPlugin.activityStop();
                        $scope.eventAlert = true;
                        $scope.eventsInfiniteLoader = false;
                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    }
                });
            }
        };

        $scope.goToEventDetails = function (eventId) {
            helpers.GoToPage('activitiesDetails', { eventId: eventId, categoryId: $scope.categoryId });
        };

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', {});
        };

        $scope.goToSearchResult = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.CurrentuserCity = selectedCity;
            $rootScope.cityName = selectedCity.name;
            $rootScope.cityId = selectedCity;
            helpers.GoToPage('searchResults', { country: $rootScope.soadiaId, city: selectedCity.id, cityName: selectedCity.name, currentCity: selectedCity, municipality: "", category: "", place: "", currentPlaceLatitude: null, currentPlaceLongitude: null });
        };


        $scope.goToAdvertisementUrl = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('خطأ', 'الرابط غير صحيح', 'alert', function () { });
            }
        };

        app.init();
    });

}]);

