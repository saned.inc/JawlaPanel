﻿using System.Web.Http;
using System.Web.Mvc;
using Saned.Jawla.Api.App_Start;
using System.Web.Optimization;

namespace Saned.Jawla.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutoMapperConfiguration.Configure();
            AreaRegistration.RegisterAllAreas();

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configure(WebApiConfig.Register);
          
        }
    }
}
