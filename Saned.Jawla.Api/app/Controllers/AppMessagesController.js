﻿(function () {
   
    var appMessagesController = function ($rootScope, $scope, messageSvc, $window, $routeParams, notify, $bootbox) {

        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 5;
        $scope.totalData = {};
        $scope.showResult = [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
        $scope.isDataLoading = true;
        $scope.data = [];
        $rootScope.messages = [];
        /*---------------AllCountNotSeen------------------------*/
        messageSvc.getAllNotSeenCount().then(function (response) {
            $rootScope.unseenMsgCount = response.data;
        });

        /*---------------TopTen------------------------*/
        messageSvc.getTopTen().then(function (response) {
            $rootScope.messages = response.data;
        });


        /*---------------Set Object Seen------------------------*/
        $rootScope.setRead = function (id) {
            $scope.isDataLoading = true;
            messageSvc.read(id).then(function (response) {
                $scope.data.push(response.data);
                $scope.isDataLoading = false;
                $window.location = '#/Views/message?id=' + response.data.contactId;
            });
        }

        /*---------------Paging------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            $scope.isDataLoading = true;
            messageSvc.getAll(pageSize, currentPage).then(function (response) {
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }

        $scope.pageChanged = function (currentPage, pageSize) {
            $scope.getData(currentPage, pageSize);
        };

        /*---------------Load Data------------------------*/
        if ($routeParams.id)
            $rootScope.setRead($routeParams.id);
        else
            $scope.getData($scope.currentPage, $scope.pageSize);

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    messageSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;
                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        if ($routeParams.id)
                            $window.location = '#/';
                        else {
                            $scope.data.splice(index, 1);
                            $rootScope.messages.filter(function (item) {
                                if (item.contactId === id) {
                                    $rootScope.messages.splice($rootScope.messages.indexOf(item), 1);
                                    if (item.isRead === false)
                                        $rootScope.unseenMsgCount -= 1;
                                }
                            });
                            if ($scope.data.length === 0)
                                $scope.getData($scope.currentPage, $scope.pageSize);
                        }
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };

    };

    var module = angular.module("App");
    module.controller("appMessagesController", ["$rootScope", "$scope", "messageSvc", "$window", "$routeParams", "notify", "$bootbox", appMessagesController]);

}());