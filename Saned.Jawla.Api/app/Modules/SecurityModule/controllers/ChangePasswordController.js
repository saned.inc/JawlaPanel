﻿(function () {
    var changePasswordController = function ($rootScope, $scope, notify, userSvc, authService) {
        $scope.save = function (form) {

            userSvc.changePassword($scope.model).then(function (response) {

                var status = response.status;
                var data = response.data;

                if (status === 200) {
                    notify.success("تم الحفظ");
                    authService.logOut();
                }

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 400) {
                    notify.error(data.message);
                }
                else if (status === 409) {
                    alert(JSON.stringify(data));
                }
                else if (status === 500)
                    alert(JSON.stringify(data));
            });

        }
    };

    var module = angular.module("SecurityModule");
    module.controller("changePasswordController", ["$rootScope", "$scope", "notify", "userSvc", "authService", changePasswordController]);

}());