﻿
(function () {
    var manageCategoryController = function ($rootScope, $scope, $routeParams, notify, categorySvc, ngAuthSettings, $location) {

        /*---------------Init------------------------*/
        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        debugger;
        $scope.delteimage = function () {
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
        }

        $scope.uploadedImageWidth = 0;
        $scope.uploadedImageHeight = 0;
        $scope.imageChange = function (e) {

            var file, img, adsFU;
            adsFU = $('#adsFU');
            var _URL = window.URL || window.webkitURL;
            if ((file = adsFU[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    debugger;
                    //alert(this.width + " " + this.height);
                    $scope.uploadedImageWidth = this.width;
                    $scope.uploadedImageHeight = this.height;
                    $scope.$apply();
                    $scope.$apply();
                };
                img.onerror = function () {
                    //alert("not a valid file: " + file.type);
                    $scope.uploadedImageWidth = 0;
                    $scope.uploadedImageHeight = 0
                    $scope.$apply()
                };
                img.src = _URL.createObjectURL(file);


            }

        }

        $scope.removeImageUrl = function () {
            $scope.model.attachmentUrl = '';
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
            $scope.model.coverId = 0;
            $scope.$apply();
        }

        categorySvc.getAllParent().then(function (response) {
            debugger;
            $scope.allParentCategories = response.data;
        });

        $scope.model = {
            id: $scope.routeParam
        }


        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0
            }

        } else {

            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            categorySvc.get($routeParams.id).then(function (response) {
                debugger;
                var data = response.data;
                $scope.model = data;
                $scope.model.photoUrl = ngAuthSettings.imagesFolderUri + $scope.model.photoUrl;

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /*---------------Save------------------------*/
        $scope.save = function (form) {

            if ($scope.isAdd) {
                categorySvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {

                        $scope.model = {

                        };
                        form.$setUntouched();
                        form.$setPristine();

                        notify.success('تمت الاضافة بنجاح');
                        $location.url('/Category');
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        alert($scope.toasterWarning);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                categorySvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success("تم التعديل بنجاح");
                    }

                    $location.url('/Category');

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        notify.error("not updated there is an error");
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });

        }

        $scope.StringToNumber = function (value) {
            debugger;
            if (!angular.isUndefined(value)) {
                var intValue = parseInt(value);

                return intValue;
            }
            return 0;

        }

    };

    var module = angular.module("AppModule");
    module.controller("manageCategoryController", ["$rootScope", "$scope", "$routeParams", "notify", "categorySvc", "ngAuthSettings","$location", manageCategoryController]);

}());