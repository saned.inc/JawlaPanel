﻿
(function () {
    var manageAmbassadorController = function ($rootScope, $scope, $routeParams, $window, notify, ngAuthSettings, ambassadorSvc) {

        /*---------------Init------------------------*/
        $scope.routeParam = $routeParams.id;

        $scope.isAdd = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        $scope.model = {
            id: $scope.routeParam
        }

        ambassadorSvc.get($routeParams.id).then(function (response) {
            var status = response.status;
            var data = response.data;
            $scope.model = data;
            $scope.model.statusName = $scope.model.status == 1 ? 'جديد' : $scope.model.status == 2 ? 'موافقة' : 'رفض';
            $scope.model.photoUrl = ngAuthSettings.imagesFolderUri + $scope.model.photoUrl;
            if ($scope.model.attachmentUrl)
                $scope.deletehidebutton = true;

        }, function (response) {
            var status = response.status;
            var data = response.data;

            if (status === 500)
                alert(JSON.stringify(data));
        });


        /*---------------Approve------------------------*/
        $scope.approve = function (form, duplicatedUserId) {
            debugger;
            ambassadorSvc.approve($scope.model.id, duplicatedUserId).then(function (response) {
                debugger;

                if (response.data.duplicatedUsers) {
                    $scope.duplicatedUsers = response.data.duplicatedUsers;


                } else {
                    var status = response.status;

                    if (status === 200) {
                        notify.success('تم الموافقة على الطلب بنجاح');
                    }

                    $window.location.href = '/#/Ambassador';
                }



            }, function (response) {

                var status = response.status;
                var data = response.data;

                if (status === 409) {
                    alert($scope.toasterWarning);
                }
                else if (status === 500)
                    alert(JSON.stringify(data));
            });

        }

        /*---------------Reject------------------------*/
        $scope.reject = function (form) {

            ambassadorSvc.reject($scope.model.id).then(function (response) {
                var status = response.status;

                if (status === 200) {
                    notify.success('تم رفض الطلب');
                }

                $window.location.href = '/#/Ambassador';

            }, function (response) {

                var status = response.status;
                var data = response.data;

                if (status === 409) {
                    alert($scope.toasterWarning);
                }
                else if (status === 500)
                    alert(JSON.stringify(data));
            });

        }



    };

    var module = angular.module("AppModule");
    module.controller("manageAmbassadorController", ["$rootScope", "$scope", "$routeParams", "$window", "notify", "ngAuthSettings", "ambassadorSvc", manageAmbassadorController]);

}());