﻿
(function () {
    var manageEventController = function ($rootScope,
        $scope,
        $routeParams,
        notify,
        eventsSvc,
        countrySvc,
        citySvc,
        municipalitySvc, $filter, datashare, $location, $q , categorySvc) {

        categorySvc.getCategoryByParentId(5).then(function (response) {
            $scope.childCategories = response.data;
        });


        $scope.removeImageUrl = function () {
            debugger;
            $scope.model.attachmentUrl = '';
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
            $scope.model.coverId = 0;
            $scope.$apply();
        }



        $scope.uploadedImageWidth = 0;
        $scope.uploadedImageHeight = 0;
        $scope.imageChange = function (e) {

            var file, img, adsFU;
            adsFU = $('#adsFU');
            var _URL = window.URL || window.webkitURL;
            if ((file = adsFU[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    debugger;
                    //alert(this.width + " " + this.height);
                    $scope.uploadedImageWidth = this.width;
                    $scope.uploadedImageHeight = this.height;
                    $scope.$apply();
                    $scope.$apply();
                };
                img.onerror = function () {
                    //alert("not a valid file: " + file.type);
                    $scope.uploadedImageWidth = 0;
                    $scope.uploadedImageHeight = 0
                    $scope.$apply()
                };
                img.src = _URL.createObjectURL(file);


            }

        }

        /*---------------Init------------------------*/
        $scope.title = "اضافة / تعديل فاعلية";
        $scope.FormDetails = {};
        $scope.model = {};
        $scope.FormContacts = {};
        $scope.Contact = {};
        $scope.model.latitude = 24.713552;
        $scope.model.longitude = 46.675296;
        $scope.FormImages = {};
        $scope.images = {};
        $scope.Map = {};
        $scope.FormMap = {};

        // #region loading_DropList

        $scope.onCityChange = function (id) {
            if (angular.isUndefined(id))
                return;

            municipalitySvc.returnAllByCityId(id).then(function (data) {

                $scope.disabledMunicipalityDrp = false;
                $scope.Municipalities = [];
                $scope.Municipalities = data.data;
                $scope.model.municipality = $filter('filter')($scope.Municipalities, { id: $scope.model.municipalityId })[0];
            });
        }
        $scope.onCountryChange = function (id) {
            if (angular.isUndefined(id))
                return;


            citySvc.returnAllByCountryId(id).then(function (data) {

                $scope.disabledCityDrp = false;
                $scope.cities = [];
                $scope.cities = data.data;
                $scope.onCityChange($scope.model.cityId);
                $scope.model.city = $filter('filter')($scope.cities, { id: $scope.model.cityId })[0];
            });
        }

        $scope.disabledCityDrp = true;
        $scope.disabledMunicipalityDrp = true;

        $scope.loading_DropList = function () {
            countrySvc.returnAll().then(function (data) {
                $scope.countries = data.data;

                $scope.LoadData();


            });

        }






        // #endregion

        // #region DatePickers
        $scope.today = function () {
            $scope.dt = new Date();
        };


        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.openStart = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();
            $scope.model["openedStart"] = !$scope.model["openedStart"];
        };
        $scope.openEnd = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();
            $scope.model["openedEnd"] = !$scope.model["openedEnd"];
        };

        //$scope.disabled = function (date, mode) {
        //    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        //};

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        // $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


        $scope.OnChangeEndDate = function () {
            var endDate = new Date($scope.model.endDate);
            var startDate = new Date($scope.model.eventStartDate);
            $scope.FormDetails.model.endDate.$setValidity("endBeforeStart", endDate >= startDate);
            $scope.FormDetails.model.eventStartDate.$setValidity("startbeforeEnd", startDate <= endDate);

        }

        $scope.OnChangeStartDate = function () {

            var endDate = new Date($scope.model.endDate);
            var startDate = new Date($scope.model.eventStartDate);


            if (endDate == 'Invalid Date')
                $scope.FormDetails.model.eventStartDate.$setValidity("startbeforeEnd", true);
            else
                $scope.FormDetails.model.eventStartDate.$setValidity("startbeforeEnd", startDate <= endDate);

            $scope.FormDetails.model.endDate.$setValidity("endBeforeStart", endDate >= startDate);
            $scope.FormDetails.model.eventStartDate.$setValidity("startbeforeToday", startDate >= $scope.dt);


        }



        // #endregion


        // #region SaveVsUpdate





        $scope.isAdd = true;
        $scope.LoadData = function () {
            eventsSvc.get($routeParams.id)
                      .then(function (response) {
                          var data = response.data;
                          $scope.model = data;
                          $scope.model.startTime = new Date($scope.model.startTime);
                          $scope.model.endTime = new Date($scope.model.endTime);
                          $scope.onCountryChange(data.countryId);
                          if (data.contacts) {
                              $scope.model.PhoneNumber = $filter('filter')(data.contacts, { soicalType: 'PhoneNumber' })[0].contactInfo;
                              $scope.model.WhatsApp = $filter('filter')(data.contacts, { soicalType: 'WhatsApp' })[0].contactInfo;
                              $scope.model.Facebook = $filter('filter')(data.contacts, { soicalType: 'Google' })[0].contactInfo;
                              $scope.model.Twitter = $filter('filter')(data.contacts, { soicalType: 'Twitter' })[0].contactInfo;
                              $scope.model.Google = $filter('filter')(data.contacts, { soicalType: 'Facebook' })[0].contactInfo;
                          }
                          $scope.model.country = $filter('filter')($scope.countries, { id: data.countryId })[0];
                      }, function (response) {
                          var status = response.status;
                          var data = response.data;

                          if (status === 500)
                              alert(JSON.stringify(data));
                      });
        }

        $scope.routeParam = $routeParams.id;
        if ($scope.routeParam) {
            $scope.isAdd = false;
            //  $scope.loading_DropList();

            $q.all([countrySvc.returnAll(), eventsSvc.get($routeParams.id)]).then(function (values) {
                $scope.countries = values[0].data;

                $scope.model = values[1].data;
                $scope.model.startTime = new Date($scope.model.startTime);
                $scope.model.endTime = new Date($scope.model.endTime);
                if (values[1].data.contacts) {
                    $scope.model.PhoneNumber = $filter('filter')(values[1].data.contacts, { soicalType: 'PhoneNumber' })[0].contactInfo;
                    $scope.model.WhatsApp = $filter('filter')(values[1].data.contacts, { soicalType: 'WhatsApp' })[0].contactInfo;
                    $scope.model.Facebook = $filter('filter')(values[1].data.contacts, { soicalType: 'Google' })[0].contactInfo;
                    $scope.model.Twitter = $filter('filter')(values[1].data.contacts, { soicalType: 'Twitter' })[0].contactInfo;
                    $scope.model.Google = $filter('filter')(values[1].data.contacts, { soicalType: 'Facebook' })[0].contactInfo;
                }
                $scope.onCountryChange(values[1].data.countryId);

                $scope.model.country = $filter('filter')($scope.countries, { id: values[1].data.countryId })[0];


                $scope.model.workHours = parseInt($scope.model.workHours);
            }, function (response) {
                notify.success("حدث خطأ أثناء الاتصال بالخادم . اعد تحميل الصفحة.");
            });



        }

        $scope.save = function () {

            $scope.model["imageFilename"] = angular.isUndefined($scope.model.attachmentViewModel) ? "" : $scope.model.attachmentViewModel.filename;
            $scope.model["imageBase64"] = angular.isUndefined($scope.model.attachmentViewModel) ? "" : $scope.model.attachmentViewModel.base64;

            debugger;


            $scope.model.latitude = $('#hdnLatitude').val();
            $scope.model.longitude = $('#hdnLongitude').val();

            $scope.model.cityId = $scope.model.city.id;
            $scope.model.countryId = $scope.model.country.id;
            

            if ($scope.model.municipality) {
                $scope.model.municipalityId = $scope.model.municipality.id;
            } else {
                $scope.model.municipalityId = null;
            }



            $scope.model.contacts = [
                            {
                                SoicalType: "PhoneNumber",
                                ContactInfo: $scope.model.PhoneNumber,
                                ContactTypeId: 1,
                                Icon: "ionicons ion-android-call"
                            },
                            {
                                SoicalType: "WhatsApp",
                                ContactInfo: $scope.model.WhatsApp,
                                ContactTypeId: 1,
                                Icon: "ionicons ion-social-whatsapp"
                            },
                            {
                                SoicalType: "Google",
                                ContactInfo: $scope.model.Google,
                                ContactTypeId: 1,
                                Icon: "ionicons ion-social-googleplus"
                            },
                            {
                                SoicalType: "Twitter",
                                ContactInfo: $scope.model.Twitter,
                                ContactTypeId: 1,
                                Icon: "ionicons ion-social-twitter"
                            },
                            {
                                SoicalType: "Facebook",
                                ContactInfo: $scope.model.Facebook,
                                ContactTypeId: 1,
                                Icon: "ionicons ion-social-facebook"
                            }
            ];


            if ($scope.isAdd) {
                eventsSvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        $scope.clearFormsVsGotofirststep();
                        $scope.gotoStep(1);
                        notify.success($rootScope.transaction.NotifySuccess);
                        $location.url('/Event');
                    }

                },
                    function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            alert(JSON.stringify(data));
                        }


                        if (status === 409) {
                            alert($scope.toasterWarning);
                        } else if (status === 500)
                            alert(JSON.stringify(data));
                    });
            } else
                eventsSvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.gotoStep(1);
                        $location.url('/Event');

                    }

                },
                    function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 409) {
                            notify.error("not updated there is an error");
                        } else if (status === 500)
                            alert(JSON.stringify(data));
                    });

        }



        if ($scope.isAdd) {
            countrySvc.returnAll().then(function (data) {
                $scope.countries = data.data;

            });
        }

        $scope.clearFormsVsGotofirststep = function () {

            $scope.model = {};
            $scope.images = {};
            $scope.Map = {};
            $scope.gotoStep(1);
        };

        $scope.function = function () {
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
        }



        // #endregion


        //Model
        $scope.currentStep = 1;
        $scope.steps = [
            {
                step: 1,
                name: "البيانات الاساسية",
                template: "/app/Modules/AppModule/views/Event/Edit.html"

            },
            {
                step: 2,
                name: "الصور",
                template: "/app/Modules/AppModule/views/Event/images.html"
            },
            {
                step: 3,
                name: "الموقع",
                template: "/app/Modules/AppModule/views/Event/Map.html"
            },
             {
                 step: 4,
                 name: "حسابات التواصل",
                 template: "/app/Modules/AppModule/views/Event/Contacts.html"
             }
        ];
        $scope.user = {};
        //step 1 validate Edit

        $scope.getFormTemplateValidation = function () {

            //|| (model.attachmentViewModel == undefined && model.photoUrl == undefined) 
            if ($scope.currentStep === 1) {
                if (!angular.isUndefined($scope.FormDetails.model)) {
                    if (!($scope.FormDetails.model.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormDetails.model.$invalid;
                    } else return true;
                } else {
                    return true;
                }
            }
            else if ($scope.currentStep === 2) {
                //if (!angular.isUndefined($scope.model.imagesList)) {
                //    if (!($scope.model.imagesList.length>0 && $scope.currentStep >= $scope.steps.length)) {
                //        return $scope.model.imagesList.length <= 0;
                //    }             
                //}
                //else
                //    return true;

                return false;

            }

            else if ($scope.currentStep === 3) {

                if (!angular.isUndefined($scope.FormMap.Map)) {
                    if (!($scope.FormMap.Map.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormMap.Map.$invalid;
                    }
                    else return true;;
                }

                else {
                    return true;
                }
            }
            else
                return true;
        }
        //Functions
        $scope.gotoStep = function (newStep) {


            $scope.currentStep = newStep;


        }



        $scope.getStepTemplate = function () {
            for (var i = 0; i < $scope.steps.length; i++) {
                if ($scope.currentStep == $scope.steps[i].step) {
                    return $scope.steps[i].template;
                }
            }
        }

        $scope.delteimage = function () {
            //if ($scope.model.attachmentViewModel) {
            //    $scope.model.attachmentViewModel.attachmentUrl = undefined;
            //    $scope.model.attachmentId = 0;
            //    $scope.model.attachmentViewModel = undefined;
            //}
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
        }
    };

    var module = angular.module("AppModule");
    module.controller("manageEventController", ["$rootScope", "$scope", "$routeParams", "notify", "eventsSvc", "countrySvc", "citySvc", "municipalitySvc", "$filter", "datashare", "$location", "$q", "categorySvc", manageEventController]);

}());





