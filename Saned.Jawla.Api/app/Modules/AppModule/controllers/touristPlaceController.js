﻿(function () {
    var touristPlaceController = function ($rootScope, $scope, $route, $bootbox, notify, $window, touristPlaceSvc, countrySvc, citySvc, municipalitySvc, appSettings, ngAuthSettings) {

        $scope.showResult = $rootScope.pageSizes;
        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.model = {};
        //  $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        /*---------------Search------------------------*/

        $scope.disabledMunicipalityDrp = true;
        $scope.disabledCityDrp = true;
        countrySvc.returnAll().then(function (data) {
            $scope.countries = data.data;
        });

        $scope.onCityChange = function (id) {
            municipalitySvc.returnAllByCityId(id).then(function (data) {
                $scope.disabledMunicipalityDrp = false;
                $scope.Municipalities = [];
                $scope.Municipalities = data.data;
            });
        }
        $scope.onCountryChange = function (id) {
            citySvc.returnAllByCountryId(id).then(function (data) {
                $scope.disabledCityDrp = false;
                $scope.cities = [];
                $scope.cities = data.data;
            });
        }
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {
            if ($scope.model)
                $scope.getData($scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.ContactUsSearch = function (pageWithsearch) {
            debugger;
            $scope.isDataLoading = true;
            $scope.currentPage = pageWithsearch !== true ? 1 : $scope.currentPage;
            $scope.model.countryId = angular.isUndefined($scope.model.countryId) ? null : $scope.model.countryId;
            $scope.model.cityId = (angular.isUndefined($scope.model.cityId) || $scope.model.cityId == null) ? null : $scope.model.cityId;
            $scope.model.municipalityId = (angular.isUndefined($scope.model.municipalityId) || $scope.model.municipalityId == null) ? null : $scope.model.municipalityId;

            touristPlaceSvc.search($scope.model, $scope.currentPage, $scope.pageSize).then(function (response) {
                $scope.data = response.data;
                if (response.data.length > 0)
                    $scope.totalData = response.data.length <= 0 ? 0 : response.data[0].overAllCount;
                $scope.isDataLoading = false;

            });

        }
        /*---------------Paging------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            touristPlaceSvc.postAll(pageSize, currentPage).then(function (response) {
                debugger;
                $scope.data = response.data;
                if ($scope.data.length > 0) {
                    $scope.totalData = angular.isUndefined(response.data[0].overAllCount) ? 0 : $scope.data[0].overAllCount;

                }
                $scope.isDataLoading = false;
            });
        }

        $scope.getData($scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {
            $scope.getData(currentPage, pageSize);
        };



        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {

            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    touristPlaceSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;

                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);

                        for (var i = 0; i < $scope.data.length; i++) {
                            if ($scope.data[i].id == id) {
                                index = i;
                                break;
                            }
                        }

                        $scope.data.splice(index, 1);

                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };


        /* ------------- Sort function ------------ */
        $scope.sortKey = 'name';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("touristPlaceController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "touristPlaceSvc", "countrySvc", "citySvc", "municipalitySvc", "translation", "appSettings", "ngAuthSettings", touristPlaceController]);

}());