﻿
(function () {
    var manageCountryController = function ($rootScope, $scope, $routeParams, notify, countrySvc) {

        /*---------------Init------------------------*/
        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        $scope.model = {
            id: $scope.routeParam
        }

        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0
            }

        } else {

            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            countrySvc.get($routeParams.id).then(function (response) {
                debugger;
                var data = response.data;
                $scope.model = data;

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /*---------------Save------------------------*/
        $scope.save = function (form) {

            if ($scope.isAdd) {
                countrySvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {

                        $scope.model = {

                        };
                        form.$setUntouched();
                        form.$setPristine();

                        notify.success($rootScope.transaction.NotifySuccess);
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        alert($scope.toasterWarning);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                countrySvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success("لقد تمت عملية التعديل بنجاح ");
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        notify.error("خطا لم تتم عملية التعديل");
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });

        }

        $scope.StringToNumber = function (value) {
            debugger;
            if (!angular.isUndefined(value)) {
                var intValue = parseInt(value);

                return intValue;
            }
            return 0;

        }

    };

    var module = angular.module("AppModule");
    module.controller("manageCountryController", ["$rootScope", "$scope", "$routeParams", "notify", "countrySvc", manageCountryController]);

}());