﻿(function () {
    var servicesController = function ($rootScope, $scope, $routeParams, appSettings, $bootbox, notify, servicesSvc) {


        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        $scope.model = {};
        $scope.isAllCommentsLoaded = false;
        $scope.isAllRatesLoaded = false;

        $scope.messages = [
               { "Id": 1, "Name": "Hassan", "PhoneNumber": "0123456789", "Email": "hassan@khaled.com", "Message": "this is the message" },
               { "Id": 2, "Name": "Hassan", "PhoneNumber": "0123456789", "Email": "hassan@khaled.com", "Message": "this is the message" },
               { "Id": 3, "Name": "Hassan", "PhoneNumber": "0123456789", "Email": "hassan@khaled.com", "Message": "this is the message" },
               { "Id": 4, "Name": "Hassan", "PhoneNumber": "0123456789", "Email": "hassan@khaled.com", "Message": "this is the message" },
        ];

   

        $scope.cancelSearch = function () {

            if ($routeParams.userId && $scope.model.name)
                $scope.getDataByUserId($scope.currentPage, $scope.pageSize, $routeParams.userId);
            else if ($scope.model.name)
                $scope.getData($scope.currentPage, $scope.pageSize);

            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.search = function () {
            $scope.isDataLoading = true;
            $scope.currentPage = 1;
            if ($routeParams.userId) {
                servicesSvc.search($scope.model, $scope.currentPage, $scope.pageSize, $routeParams.userId)
                    .then(function (response) {
                        $scope.data = response.data.data;
                        $scope.totalData = response.data.totalCount;
                        $scope.isDataLoading = false;

                    });
            } else {
                servicesSvc.searchForAll($scope.model, $scope.currentPage, $scope.pageSize)
                    .then(function (response) {
                        $scope.data = response.data.data;
                        $scope.totalData = response.data.totalCount;
                        $scope.isDataLoading = false;

                    });
            }

        }

        /*---------------Paging ByUserId------------------------*/
        $scope.getDataByUserId = function (currentPage, pageSize, serviceId) {
            servicesSvc.getAllByUserId(pageSize, currentPage, serviceId).then(function (response) {
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }


        /*---------------Paging for all------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            servicesSvc.getAll(pageSize, currentPage).then(function (response) {
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    servicesSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;

                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };

        if ($routeParams.serviceId) {
            servicesSvc.get($routeParams.serviceId).then(function (response) {
                $scope.model = response.data;
                $scope.isDataLoading = false;

                //for comments 
                $scope.groups = _.groupBy($scope.model.comments, 'id');
                for (var j = 0; j < Object.keys($scope.groups).length; j++) {
                    if ($scope.groups.hasOwnProperty(Object.keys($scope.groups)[j])) {
                        for (var i = 0; i < $scope.groups[Object.keys($scope.groups)[j]].length; i++) {
                            if (j % 2 === 0)
                            { $scope.groups[Object.keys($scope.groups)[j]][i]["cls"] = "even"; }
                            else
                                $scope.groups[Object.keys($scope.groups)[j]][i]["cls"] = "odd";
                        }
                    }
                }

                if ($scope.model.comments.length < 6)
                    $scope.isAllCommentsLoaded = true;

                if ($scope.model.userServiceRates.length < 6)
                    $scope.isAllRatesLoaded = true;
            });

        }
        else {
            // check if it from userid orfrom here 
            if ($routeParams.userId) {
                $scope.getDataByUserId($scope.currentPage, $scope.pageSize, $routeParams.userId);

                $scope.pageChanged = function (currentPage, pageSize) {
                    $scope.getDataByUserId(currentPage, pageSize, $routeParams.userId);
                };
            } else {
                $scope.getData($scope.currentPage, $scope.pageSize);

                $scope.pageChanged = function (currentPage, pageSize) {
                    $scope.getData(currentPage, pageSize);
                };
            }
        }

        /* ------------- Show More In Comments ------------ */
        var currentPageForComments = 1;
        $scope.showMore = function (pageSize) {
            currentPageForComments += 1;
            servicesSvc.getNextPage(pageSize, currentPageForComments, $routeParams.serviceId).then(function (response) {
                $scope.secondMorecomments = response.data.data;
                //for comments 
                $scope.groupsSecondMorecomments = _.groupBy($scope.secondMorecomments, 'id');
                for (var j = 0; j < Object.keys($scope.groupsSecondMorecomments).length; j++) {
                    if ($scope.groupsSecondMorecomments.hasOwnProperty(Object.keys($scope.groupsSecondMorecomments)[j])) {
                        for (var i = 0; i < $scope.groupsSecondMorecomments[Object.keys($scope.groupsSecondMorecomments)[j]].length; i++) {
                            if (j % 2 === 0)
                            { $scope.groupsSecondMorecomments[Object.keys($scope.groupsSecondMorecomments)[j]][i]["cls"] = "even"; }
                            else
                                $scope.groupsSecondMorecomments[Object.keys($scope.groupsSecondMorecomments)[j]][i]["cls"] = "odd";
                        }
                    }
                }
                for (var k = 0; k < $scope.secondMorecomments.length; k++) {
                    $scope.model.comments.push($scope.secondMorecomments[k]);
                }
                if (response.data.totalCount === $scope.model.comments.length)
                    $scope.isAllCommentsLoaded = true;
                $scope.isDataLoading = false;
            });
        }

        /* ------------- Show More In Rates ------------ */
        var currentPageForRates = 1;
        $scope.showRatesMore = function (pageSize) {

            currentPageForRates += 1;
            servicesSvc.getNextRatePage(pageSize, currentPageForRates, $routeParams.serviceId).then(function (response) {
                $scope.secondMoreRates = response.data.data;
                debugger;
                for (var k = 0; k < $scope.secondMoreRates.length; k++) {
                    $scope.model.userServiceRates.push($scope.secondMoreRates[k]);
                }
                if (response.data.totalCount === $scope.model.userServiceRates.length)
                    $scope.isAllRatesLoaded = true;
                $scope.isDataLoading = false;
            });
        }

        /* ------------- Sort function ------------ */
        $scope.sortKey = 'name';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("servicesController", ["$rootScope", "$scope", "$routeParams", "appSettings", "$bootbox", "notify", "servicesSvc", servicesController]);

}());