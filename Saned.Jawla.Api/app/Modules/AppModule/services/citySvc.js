﻿(function () {
    
    var citySvc = function ($http, $q, $rootScope,ngAuthSettings) {
      //  var apiUri = ngAuthSettings.apiServiceBaseUri;
        var apiUri = ngAuthSettings.apiServiceBaseUri;
        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/City/GetCities").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }
        var returnAllByCountryId = function (countryId) {
            debugger;
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/City/FindCities/" + countryId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }

        var returnAllByCountryIdwithArabic = function (countryId) {
            debugger;
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/City/FindCitiesWithArabic/" + countryId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }
        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
             var model = { PageNumber: currentPage, PageSize: pageSize };
             $http.post(apiUri + "/api/City/GetAllCitiesCompleteWithPaging", model).then(function (successResponse) {
               
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getAllCon= function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            var model = { PageNumber: currentPage, PageSize: pageSize };
            $http.post(apiUri + "/api/Country/GetAllCountries", model).then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (countryId, currentPage, pageSize) {
            debugger;
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            var model = {CountryId:countryId, PageNumber: currentPage, PageSize: pageSize };

            $http.get(apiUri + "/api/City/GetCityByCountryId/" + countryId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {
        
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
           
            $http.get(apiUri +"/api/City/GetById/"+ id).then(function (successResponse) {
           
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post
           
            $http.post(apiUri + "/api/City/CreateCity", model).then(function (successResponse) {
                deferred.resolve(successResponse);
               
            }, function (failureResponse) {
                debugger;
                deferred.reject(failureResponse);
               
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/City/UpdateCity", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            
            $http.get(apiUri + "/api//City/DeleteById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            returnAll: returnAll,
            getAllCon: getAllCon,
            returnAllByCountryIdwithArabic:returnAllByCountryIdwithArabic,
            returnAllByCountryId:returnAllByCountryId
        }


    };

    var module = angular.module("AppModule");
    module.factory("citySvc", ["$http", "$q", "$rootScope", "ngAuthSettings", citySvc]);

}());