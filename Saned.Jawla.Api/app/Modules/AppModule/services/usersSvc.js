﻿var usersSvc = function ($http, $q, ngAuthSettings) {
    var api = ngAuthSettings.apiServiceBaseUri;
    var getAll = function (keyword, pageSize, currentPage) {
        debugger;
        var deferred = $q.defer();
        var model =
            {
                PageNumber: currentPage,
                PageSize: pageSize,
                Keyword: keyword
            };
        $http.post(api + "/api/Account/GetUsers", model).then(function (successResponse) {

            deferred.resolve(successResponse);
        }, function (failureResponse) {

            deferred.reject(failureResponse);
        });
        return deferred.promise;
    };
    var search = function (keyword, currentPage, pageSize) {

        var deferred = $q.defer();
        var model =
            {
                PageNumber: currentPage,
                PageSize: pageSize,
                Keyword: keyword
            };
        $http.post(api + "api/Account/GetUsers", model).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    };

    var getUserRating = function (id) {
        var deferred = $q.defer();

        $http.get(api + "api/User/getUserRating/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var getUserCommnets = function (id) {
        var deferred = $q.defer();

        $http.get(api + "api/User/GetUserComments/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var getUserPhotos = function (id) {
        var deferred = $q.defer();
        $http.get(api + "api/User/GetUserPhotos/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var getUserFavourites = function (id) {
        var deferred = $q.defer();
        $http.get(api + "api/User/getUserFavourites/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }


    var deleteUserComment = function (id) {
        var deferred = $q.defer();
        $http.post(api + "api/Comments/DeleteComment/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }


    var deleteUserRating = function (id) {
        var deferred = $q.defer();
        $http.post(api + "api/User/DeleteUserRating/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var deleteUserPhoto = function (id) {
        var deferred = $q.defer();
        $http.post(api + "api/Photo/DeletePhoto/" + id).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var changePassword = function (changePasswordModel) {
        var deferred = $q.defer();
        var model =
          {
              NewPassword: changePasswordModel.newPassword,
              ConfirmNewPassword: changePasswordModel.confirmNewPassword,
              UserId: changePasswordModel.userId
          };

        $http.post(api + "api/User/ChangePassword/", model).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var activateUser = function (userid) {
        var deferred = $q.defer();

        $http.post(api + "api/User/ActivateUser/" + userid).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }

    var deActivateUser = function (userid) {
        var deferred = $q.defer();

        $http.post(api + "api/User/DeactivateUser/" + userid).then(function (successResponse) {
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });
        return deferred.promise;
    }


    var deleteUser = function (userid) {
        var deferred = $q.defer();

        debugger;
        $http.post(api + "api/User/Delete/" + userid).then(function (successResponse) {
            debugger;
            deferred.resolve(successResponse);
        }, function (failureResponse) {
            deferred.reject(failureResponse);
        });

        return deferred.promise;

    };

    return {
        getAll: getAll,
        search: search,
        getUserCommnets: getUserCommnets,
        deleteUserComment: deleteUserComment,
        getUserPhotos: getUserPhotos,
        deleteUserPhoto: deleteUserPhoto,
        getUserFavourites: getUserFavourites,
        getUserRating: getUserRating,
        deleteUserRating: deleteUserRating,
        changePassword: changePassword,
        activateUser: activateUser,
        deActivateUser: deActivateUser,
        deleteUser: deleteUser
    }


};

var module = angular.module("AppModule");
module.factory("usersSvc", ["$http", "$q", "ngAuthSettings", "$rootScope", usersSvc]);