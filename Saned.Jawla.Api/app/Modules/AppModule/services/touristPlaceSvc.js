﻿(function () {
    var touristPlaceSvc = function ($http, $q, ngAuthSettings) {
        var apiUri = ngAuthSettings.apiServiceBaseUri;
        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/Places").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }

        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            var model = { PageNumber: currentPage, PageSize: pageSize, CategoryId:2 , ParentCategoryId: 2 };

            // Initiates the AJAX call
            $http.post(apiUri + "/api/Places", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var getAllPost = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            debugger;
            var model = { PageNumber: currentPage, PageSize: pageSize, CategoryId: 2 , ParentCategoryId: 2 };
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Places", model).then(function (successResponse) {
                debugger;
                deferred.resolve(successResponse);
            }, function (failureResponse) {
            debugger;
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var search = function (model, currentPage, pageSize) {
            debugger;
            model["PageNumber"] = currentPage;
            model["PageSize"] = pageSize;
            model["CategoryId"] = 2;
            model["ParentCategoryId"] = 2;

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Places", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(apiUri + "/api/Places/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getambassador = function () {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(apiUri + "/api/Ambassador/GetAllAmbassadors").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(apiUri + "/api/Places/CreatePlaces", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Places/UpdatePlaces", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            debugger;
            $http.post(apiUri + "/api/Places/Delete/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            postAll: getAllPost,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            getambassador: getambassador,
            returnAll: returnAll
        }


    };


    var module = angular.module("AppModule");
    module.factory("touristPlaceSvc", ["$http", "$q", "ngAuthSettings", "$rootScope", touristPlaceSvc]);

}());