﻿angular.module("AppModule")
    .factory("datashare",
        function($window, $rootScope) {
            angular.element($window).on('storage',
                function(event) {
                    //if (event.key === 'userId' || event.key === 'chatpeople') {
                        $rootScope.$apply();
                    //}
                });
            return {
                setData: function(prop, val) {
                    $window.localStorage && $window.localStorage.setItem(prop, val);
                    return this;
                },
                getData: function(prop) {
                    return $window.localStorage && $window.localStorage.getItem(prop);
                }
            };
        });

