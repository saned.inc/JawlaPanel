﻿(function () {

    var rolesSvc = function ($http, $q,ngAuthSettings) {
        var api = ngAuthSettings.apiServiceBaseUri;

        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Permission/PermissionView").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }
        var getRoles = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Account/AllRols").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }
        var addRoleAll = function (model) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
           
            $http.post(api + "/api/Permission/AddUserInRole", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var removeRoleAll = function (model) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.post(api + "/api/Permission/RemoveUserFromRole", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var getUsers = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Ambassador/GetAllAmbassadors").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }
        var getAll = function (model) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.post(api + "/api/Permission/PermissionView", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var search = function (rolesId, currentPage, pageSize) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Roles/GetById/" + rolesId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(api + "/api/Roles/GetById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(api + "/api/Roles/CreateRoles", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "/api/Roles/UpdateRoles", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Roles/DeleteById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            getRoles:getRoles,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            returnAll: returnAll,
            addRoleAll: addRoleAll,
            getUsers:getUsers,
            removeRoleAll: removeRoleAll
        }


    };

    var module = angular.module("AppModule");
    module.factory("rolesSvc", ["$http", "$q", "$rootScope", "ngAuthSettings", rolesSvc]);

}());