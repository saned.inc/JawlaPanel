﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;

namespace Saned.Jawla.Api.Attributes
{
    

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {

        private readonly AuthRepository _repo = null;

        public AuthorizeAttribute()
        {
            _repo = new AuthRepository();
        }
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext.RequestContext.Principal.Identity.IsAuthenticated)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
        
            }
            else
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
        }


        public override void OnAuthorization(HttpActionContext actionContext)
        {

            if (actionContext.RequestContext.Principal.Identity.IsAuthenticated)
            {
                string name = actionContext.RequestContext.Principal.Identity.Name;
                if (!string.IsNullOrEmpty(name))
                {
                    ApplicationUser u = _repo.FindUserByName(name);
                    if (u == null)
                    {
                        actionContext.ModelState.AddModelError("", "User Not found");
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, "DeletedUser");
                    }
                    if (u.IsDeleted.Value)
                    {
                        actionContext.ModelState.AddModelError("", "User Is NotActive");
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, "DeactiveUser");
                    }


                }
                else
                    base.HandleUnauthorizedRequest(actionContext);

            }
            else
            {
                base.HandleUnauthorizedRequest(actionContext);

            }



        }




    }




}