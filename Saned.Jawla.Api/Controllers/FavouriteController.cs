using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Favourite")]
    [Authorize]
    public class FavouriteController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public FavouriteController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }

        [Route("CreateFavourite")]
        public async Task<IHttpActionResult> CreateFavourite(FavouriteViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var userName = User.Identity.GetUserName();
                var u = await GetApplicationUser(userName);
                viewModel.UserId = u.Id;

                var checkIn = await _unitOfWork.Favourite.FindAsync(viewModel.UserId, viewModel.CategoryDetailsId);
                if (checkIn != null)
                {
                    var errors = new List<string>();
                    var error = ValidateMessage(errors, "Item In Favourites");
                    IHttpActionResult errorResult = GetErrorResult(error);
                    return errorResult;
                }

                var favouriteViewModel = Mapper.Map<FavouriteViewModel, Favourite>(viewModel);
                _unitOfWork.Favourite.CreateAsync(favouriteViewModel);
                var res = await _unitOfWork.Complete();
                IHttpActionResult response = Ok(res);

                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        [HttpPost]
        [Route("RemoveFavourite/{id}")]
        public async Task<IHttpActionResult> RemoveFavourite(long id)
        {
            try
            {
                var userName = User.Identity.GetUserName();
                if (string.IsNullOrEmpty(userName))
                {
                    List<string> errors = new List<string>();
                    var error = ValidateMessage(errors, "Can't Get User");
                    IHttpActionResult errorResult = GetErrorResult(error);

                    return errorResult;
                }
                var u = await GetApplicationUser(userName);


                var favourite = await _unitOfWork.Favourite.FindAsync(u.Id, id);
              
                if (favourite != null)
                {
                    _unitOfWork.Favourite.DeleteAsync(favourite);
                 
                    return Ok(await _unitOfWork.Complete());
                }
                else
                {
                    List<string> errors = new List<string>();
                    var error = ValidateMessage(errors, "Can't Find Favourite");
                    IHttpActionResult errorResult = GetErrorResult(error);
                    return errorResult;
                }

               
            }
            catch (Exception e)
            {

                return BadRequest(e.GetaAllMessages());
            }

        }


      

        [Route("GetFavourite")]
        [HttpPost]
        public async Task<IHttpActionResult> SearchResult(CategoryDetailsSearchViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                var u = await GetApplicationUser(userName);
                var list = await _unitOfWork.Favourite.FindAllAsync(model.PageNumber, model.PageSize, u.Id, model.CountryId, model.CityId, model.MunicipalityId, model.CategoryId, model.CategoryDetailsId, model.Longitude, model.Latitude, model.EventStatyDate);
                return Ok(list);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                    msg = ex.InnerException.Message;

                return BadRequest(msg);
            }

        }
    }
}