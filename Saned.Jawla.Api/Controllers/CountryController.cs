using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Country")]
    public class CountryController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public CountryController()
        {

            _unitOfWork = new UnitOfWorkAsync();
        }

        //[AllowAnonymous]

        [Attributes.Authorize(Roles = "Administrator")]
        [Route("CreateCountry")]
        public async Task<IHttpActionResult> CreateCountry(CountryViewModel viewModel)
        {
            try
            {

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                //if (string.IsNullOrEmpty(userName))
                //    return BadRequest("401");
                //else
                {
                    var input = new Country
                    {
                        Id = viewModel.CountryId,
                        ArabicName = viewModel.ArabicName,
                        EnglishName = viewModel.EnglishName,
                        Longitude = viewModel.Longitude,
                        Latitude = viewModel.Latitude
                    };

                    //var exist = await _unitOfWork.Country.GetById(viewModel.CountryId);


                    //if (exist==null)
                    //{
                    _unitOfWork.Country.CreateAsync(input);
                    //}
                    //else {

                    //    exist.Update(viewModel.ArabicName, viewModel.EnglishName, viewModel.Latitude, viewModel.Longitude);
                    //}


                    var result = await _unitOfWork.Complete();

                    return Ok(result);
                }


            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("CreateCountry " + msg);
            }

        }

        [HttpGet]
        [Route("GetCountries")]
        public async Task<IHttpActionResult> GetCountries()
        {
            try
            {
                return Ok(await _unitOfWork.Country.GetAsync());
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetCountries " + msg);
            }
        }

        [HttpGet]
        [Route("GetCountrieswithArabicName")]
        public async Task<IHttpActionResult> GetCountrieswithArabic()
        {
            try
            {
                return Ok(await _unitOfWork.Country.GetAsyncWithArabic());
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetCountries " + msg);
            }
        }


        [HttpGet]
        [Route("GetAllCountries")]
        public async Task<IHttpActionResult> GetAllCountries()
        {
            try
            {
                return Ok(await _unitOfWork.Country.GetAllAsync());
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetCountries " + msg);
            }
        }
        [HttpPost]
        [Route("GetCountries")]
        public async Task<IHttpActionResult> GetCountries(CountryPagingViewModel model)
        {
            try
            {
                return Ok(await _unitOfWork.Country.FindAllAsync(model.pageIndex, model.pageSize));
            }
            catch (Exception ex)
            {

                return BadRequest("GetCountries with pageindex & pagesize ----" + ex.GetaAllMessages());
            }

        }

        [HttpPost]
        [Route("GetAllCountries")]

        public async Task<IHttpActionResult> GetAllCountries(CountryPagingViewModel model)
        {

            try
            {
                return Ok(await _unitOfWork.Country.GetAllComplete(model.pageIndex, model.pageSize));
            }
            catch (Exception e)
            {
                return BadRequest("GetAllCountries ----" + e.GetaAllMessages());

            }

        }

        [HttpPost]
        [Route("GetAllCountriesWithPaging")]
        public async Task<IHttpActionResult> GetAllCountriesWithPaging(CountryPagingViewModel model)
        {

            try
            {
                //return Ok(await _unitOfWork.Country.GetAllCompleteWithPaging(model.pageIndex, model.pageSize));
                var lst = await
                    _unitOfWork.Country.GetAllCompleteWithPaging(model.pageIndex, model.pageSize);

                var totalCount = await
                    _unitOfWork.Country.GetAllComplete(model.pageIndex, model.pageSize);
                var result = new
                {
                    TotalCount = totalCount.Count,
                    Data = lst
                };
                return Ok(result);

            }
            catch (Exception e)
            {
                return BadRequest("GetAllCountries With Paging----" + e.GetaAllMessages());

            }

        }

        [HttpPost]
        [Route("SearchCountriesWithPaging")]
        public async Task<IHttpActionResult> SearchCountriesWithPaging(CountryPagingViewModel model)
        {

            try
            {
                //return Ok(await _unitOfWork.Country.GetAllCompleteWithPaging(model.pageIndex, model.pageSize));
                var lst = await
                    _unitOfWork.Country.SearchCountryWithPaging(model.pageIndex, model.pageSize, model.Keyword);

                var totalCount = await
                    _unitOfWork.Country.SearchAllComplete(model.pageIndex, model.pageSize, model.Keyword);
                var result = new
                {
                    TotalCount = totalCount,
                    Data = lst
                };
                return Ok(result);

            }
            catch (Exception e)
            {
                return BadRequest("GetAllCountries With Paging----" + e.GetaAllMessages());

            }

        }



        [HttpGet]
        [Attributes.Authorize(Roles = "Administrator")]
        [Route("DeleteById/{id}")]

        public async Task<IHttpActionResult> DeleteById(int id)
        {

            try
            {
                _unitOfWork.Country.DeleteById(id);
                await _unitOfWork.Complete();
                return Ok(await _unitOfWork.Country.GetAllAsync());
            }
            catch (Exception e)
            {
                return BadRequest("Delete country by id ----" + e.GetaAllMessages());

            }

        }

        [HttpPost]
        [Route("UpdateCountry")]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> UpdateCountry(Country c)
        {

            var q = await _unitOfWork.Country.GetById(c.Id);
            if (q == null)
            {
                return BadRequest("Error id not exist ");
            }
            else
            {
                _unitOfWork.Country.Update(new Country
                {
                    Id = c.Id,
                    ArabicName = c.ArabicName,
                    EnglishName = c.EnglishName,
                    Longitude = c.Longitude,
                    Latitude = c.Latitude
                });

            }
            return Ok(await _unitOfWork.Complete());
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IHttpActionResult> GetById(int id)
        {

            try
            {
                return Ok(await _unitOfWork.Country.GetById(id));
            }
            catch (Exception e)
            {

                return BadRequest("Select Country By Id -----" + e.GetaAllMessages());
            }


        }


    }
}