using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/City")]
    public class CityController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public CityController()
        {

            _unitOfWork = new UnitOfWorkAsync();
        }

        [AllowAnonymous]
        [Route("CreateCity")]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> CreateCity(CountryViewModel viewModel)
        {
            try
            {

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                //if (string.IsNullOrEmpty(userName))
                //    return BadRequest("401");
                //else
                {
                    _unitOfWork.City.CreateAsync(new City()
                    {
                        ArabicName = viewModel.ArabicName,
                        EnglishName = viewModel.ArabicName,
                        Latitude = viewModel.Latitude,
                        Longitude = viewModel.Longitude,
                        CountryId = viewModel.CountryId

                    });
                    var result = await _unitOfWork.Complete();

                    return Ok(result);
                }


            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("CreateCity ---->>>>>>" + msg);
            }

        }

        [HttpGet]
        [Route("GetCities")]
        public async Task<IHttpActionResult> GetCities()
        {
            try
            {
                return Ok(await _unitOfWork.City.GetAsync());
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetCities " + msg);
            }
        }

        [HttpGet]
        [Route("FindCitiesWithArabic/{id?}")]
        public async Task<IHttpActionResult> FindCitiesWithArabic(int id)
        {
            try
            {
                return Ok(await _unitOfWork.City.FindAsyncWitharabicName(id));
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("FindCities " + msg);
            }
        }


        [HttpGet]
        [Route("FindCities/{id?}")]
        public async Task<IHttpActionResult> FindCities(int id)
        {
            try
            {
                return Ok(await _unitOfWork.City.FindAsync(id));
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("FindCities " + msg);
            }
        }
        [HttpGet]
        [Route("GetCityByCountryId/{CountryId}")]
        public async Task<IHttpActionResult> GetCityByCountryId(int CountryId)
        {
            try
            {
                return Ok(await _unitOfWork.City.GetCityByCountryId(CountryId));
            }
            catch (Exception msg)
            {

                return BadRequest("GetCityByCountryId---->>" + msg.GetaAllMessages());
            }


        }

        [HttpPost]
        [Route("FindCities")]
        public async Task<IHttpActionResult> FindCities(CityPagingViewModel model)
        {
            try
            {
                return Ok(await _unitOfWork.City.FindAllAsync(model.pageIndex, model.pageSize, model.countryId));
            }
            catch (Exception ex)
            {

                return BadRequest("Find Cities with pageIndex PagesSize countryId paramteters" + ex.GetaAllMessages());
            }

        }

        [HttpGet]
        [Route("DeleteById/{id}")]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> DeleteById(int id)
        {

            try
            {
                _unitOfWork.City.DeleteById(id);
                await _unitOfWork.Complete();
                return Ok(await _unitOfWork.City.GetAllCitiesComplete(1, 10));

            }
            catch (Exception e)
            {

                return BadRequest("Delete City By Id -----" + e.GetaAllMessages());
            }

        }

        [HttpPost]
        [Route("UpdateCity")]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> UpdateCountry(City c)
        {

            var q = await _unitOfWork.City.GetById(c.Id);
            if (q == null)
            {
                return BadRequest("Error id not exist ");
            }
            else
            {
                _unitOfWork.City.Update(new City
                {
                    Id = c.Id,
                    ArabicName = c.ArabicName,
                    EnglishName = c.EnglishName,
                    Longitude = c.Longitude,
                    Latitude = c.Latitude,
                    CountryId = c.CountryId
                });

            }
            return Ok(await _unitOfWork.Complete());
        }

        [HttpPost]
        [Route("GetAllCitiesComplete")]

        public async Task<IHttpActionResult> GetAllCitiesComplete(PagingViewModel model)
        {

            try
            {
                return Ok(await _unitOfWork.City.GetAllCitiesComplete(model.PageNumber, model.PageSize));
            }
            catch (Exception e)
            {
                return BadRequest("GetAllCitiesComplete ------" + e.GetaAllMessages());

            }


        }

        [HttpPost]
        [Route("GetAllCitiesCompleteWithPaging")]
        public async Task<IHttpActionResult> GetAllCitiesCompleteWithPaging(PagingViewModel model)
        {
            var lst = await
                _unitOfWork.City.GetAllCitiesCompleteWithPaging(model.PageNumber, model.PageSize);


            var totalCount = await
                _unitOfWork.City.GetAllCitiesComplete(model.PageNumber, model.PageSize);

            var result = new
            {
                TotalCount = totalCount.Count,
                Data = lst
            };

            return Ok(result);

        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IHttpActionResult> GetById(int id)
        {

            try
            {
                return Ok(await _unitOfWork.City.GetById(id));
            }
            catch (Exception e)
            {

                return BadRequest("Select Country By Id -----" + e.GetaAllMessages());
            }


        }

        //[HttpPost]
        //[Route("GetByIdWithPaging")]
        //public async Task<IHttpActionResult> GetByIdWithPaging(CitySearchWithCountryIdViewModel model) {
        //    var lst = await
        //       _unitOfWork.City.GetCityByCountryIdWithPaging(model.CountryId, model.PageNumber, model.PageSize);


        //    var totalCount = await
        //        _unitOfWork.City.GetCityByCountryId(model.CountryId);

        //    var result = new
        //    {
        //        TotalCount = totalCount.Count,
        //        Data = lst
        //    };

        //    return Ok(result);


        //}


    }
}