﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Persistence;
namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : BasicController
    {




        [HttpGet]
        [Route("GetUserComments/{userId}")]
        public async Task<IHttpActionResult> GetUserComments(string userId)
        {
            var result = await _unitOfWork.UserRepository.GetUserComment(userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUserPhotos/{userId}")]
        public async Task<IHttpActionResult> GetUserPhotos(string userId)
        {
            var result = await _unitOfWork.UserRepository.GetUserPhoto(userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUserFavourites/{userId}")]
        public async Task<IHttpActionResult> GetUserFavourites(string userId)
        {
            var result = await _unitOfWork.UserRepository.GetUserFavourites(userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUserRating/{userId}")]
        public async Task<IHttpActionResult> GetUserRating(string userId)
        {
            var result = await _unitOfWork.UserRepository.GetUserRating(userId);
            return Ok(result);
        }

        [HttpPost]
        [Route("DeleteUserRating/{id}")]
        public async Task<IHttpActionResult> DeleteUserRating(int id)
        {
            await _unitOfWork.UserRepository.DeleteUserRating(id);
            return Ok();

        }

        [HttpPost]
        [Route("ActivateUser/{userId}")]
        public async Task<IHttpActionResult> ActivateUser(string userId)
        {
            await _unitOfWork.UserRepository.SetUserIsDeleted(false, userId);
            return Ok();
        }

        [HttpPost]
        [Route("DeactivateUser/{userId}")]
        public async Task<IHttpActionResult> DeactivateUser(string userId)
        {
            await _unitOfWork.UserRepository.SetUserIsDeleted(true, userId);
            return Ok();
        }

        [HttpPost]
        [Route("Delete/{userId}")]
        public async Task<IHttpActionResult> Delete(string userId)
        {
            try
            {
                await _unitOfWork.UserRepository.DeleteUser(userId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<IHttpActionResult> ChangePassword(AdminChangePasswordViewModel adminChangePasswordViewModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUserManagerImpl UserManager = new ApplicationUserManagerImpl();
                string code = UserManager.GeneratePasswordResetToken(adminChangePasswordViewModel.UserId);
                var result = await UserManager.ResetPasswordAsync(adminChangePasswordViewModel.UserId, code, adminChangePasswordViewModel.NewPassword);
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }
        }

    }
}