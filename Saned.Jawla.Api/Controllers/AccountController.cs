﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using Saned.Core.Security.Core;
using Saned.Core.Security.Core.Dtos;
using Saned.Core.Security.Persistence;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Core.Security.Persistence.Tools;
using Saned.Jawla.Api.Attributes;
using Saned.Jawla.Api.Results;
using Saned.Jawla.Api.ViewModels;


namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly AuthRepository _repo = null;
        public AccountController()
        {
            _repo = new AuthRepository();
            _unitOfWork = new UnitOfWorkAsync();
        }

        private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;


        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                IdentityResult result =
                    await _repo.RegisterUser(viewModel.Name, viewModel.PhoneNumber, viewModel.Email, viewModel.UserName, viewModel.Password, viewModel.Role);

                IHttpActionResult errorResult = GetErrorResult(result);

                if (errorResult != null)
                    return errorResult;

                var u = await _repo.FindUser(viewModel.UserName, viewModel.Password);
                return Ok(u.Id);
            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("Register " + msg);
            }

        }



        [AllowAnonymous]
        [Route("SendEmail")]
        public async Task<IHttpActionResult> SendEmail()
        {
            string str = "";
            try
            {
                string userName = User.Identity.GetUserName();
                var u = await GetApplicationUser(userName);
                EmailManager mngMail = new EmailManager();
                str = mngMail.SendActivationEmail(EmailType.EmailConfirmation.GetHashCode().ToString(), u.Email, "ok");
                await _repo.SendEmail(u, "Test For Send", EmailType.EmailConfirmation.GetHashCode().ToString());
            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("SendEmail --- " + msg);
            }
            return Ok(str);
        }

        [AllowAnonymous]
        [Route("ConfirmEmail")]
        public async Task<IHttpActionResult> ConfirmEmail(ConfirmViewModel confirmViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var user = await GetApplicationUser(confirmViewModel.UserName);

                IdentityResult result = await _repo.ConfirmEmail(user.Id, confirmViewModel.Code);

                IHttpActionResult errorResult = GetErrorResult(result);

                if (errorResult != null)
                {
                    return errorResult;
                }
                

                if (user != null)
                {
                    var isAmbassedor = IsInRole(user.Id, "Safeer");
                    return Ok(new { User = user, IsAmbassador = isAmbassedor });
                }
                else
                    return BadRequest("Can't Get User");
            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("SendEmail --- " + msg);
            }

        }

        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordViewModel resetPasswordView)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var user = await _repo.FindUser(resetPasswordView.Email);
                if (user == null)
                {
                    ModelState.AddModelError("Email", "Not Found");
                    return BadRequest(ModelState);
                }



                IdentityResult result = await _repo.ResetPassword(user.Id, resetPasswordView.Code, resetPasswordView.Password);

                IHttpActionResult errorResult = GetErrorResult(result);

                if (errorResult != null)
                {
                    return errorResult;
                }
                return Ok();
            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("ResetPassword --- " + msg);
            }
        }


        [AllowAnonymous]
        [Route("ForgetPassword")]
        [HttpPost]
        public async Task<IHttpActionResult> ForgetPassword(ForgotPasswordViewModel forgotPasswordViewModel)
        {
            try
            {
                if (!ModelState.IsValid || forgotPasswordViewModel == null)
                {
                    return BadRequest(ModelState);
                }
                var user = await _repo.FindUser(forgotPasswordViewModel.Email);
                if (user == null)
                {
                    ModelState.AddModelError("Email", "Not Found");
                    return BadRequest(ModelState);
                }
                await _repo.ForgetPassword(forgotPasswordViewModel.Email);
                return Ok();
            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("ForgetPassword --- " + msg);
            }


        }


        #region Attributes
        [Attributes.Authorize(Roles = "User")]
        [Route("ChangeInfo")]
        [HttpPost]
        #endregion

        public async Task<IHttpActionResult> ChangeInfo(UserProfileViewModel viewProfile)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                ;
                if (u == null)
                {
                    ModelState.AddModelError("", "You Need To Login");
                    return BadRequest(ModelState);
                }

                if (viewProfile.PhoneNumber != null && _repo.CheckifPhoneAvailable(viewProfile.PhoneNumber, u.Id))
                {
                    ModelState.AddModelError("PhoneNumber", "Phone Number already Exists");
                    return BadRequest(ModelState);

                }

                int result = _repo.UpdateUser(u.Id, viewProfile.Name, viewProfile.PhoneNumber, viewProfile.Age, viewProfile.Address);
                return Ok(result);
            }
            catch (Exception ex)
            {
                if (viewProfile != null)
                {
                    ModelState.AddModelError("Name", viewProfile.Name);

                }
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }

        }

        #region Attributes
        [Attributes.Authorize(Roles = "User")]
        [Route("ChangeImage")]
        [HttpPost]
        #endregion
        public async Task<IHttpActionResult> ChangeImage(UserPhotoViewModel viewProfile)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                if (u == null)
                {
                    ModelState.AddModelError("", "You Need To Login");
                    return BadRequest(ModelState);
                }
                string imagePath = "none";
                if (viewProfile.Picture.ToLower() == "none")
                {
                    int result = _repo.UpdateUserImage(u.Id, null);

                }
                else
                {
                    imagePath = ImageSaver.SaveImage(viewProfile.Picture);
                    int result = _repo.UpdateUserImage(u.Id, imagePath);
                }


                return Ok(imagePath);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }


        }

        [Route("GetUsers")]
        [HttpPost]
        public async Task<IHttpActionResult> GetUsers(PagingViewModel model)
        {
            var result =
                await _repo.FindAllUser(model.PageNumber, model.PageSize, model.keyword);
            int totalCount = (await _repo.FindAllUser(model.keyword)).Count;
            PaginationSet<ApplicationUserDto> pagedSet = new PaginationSet<ApplicationUserDto>()
            {
                Items = result,
                Page = model.PageNumber,
                TotalCount = totalCount,
                TotalPages = (int)Math.Ceiling((decimal)totalCount / model.PageNumber)
            };

            return Ok(pagedSet);
        }



        [Route("Users/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            var result = await _repo.FindUserById(id);

            return Ok(result);
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> Delete(string id)
        {
            _repo.DeleteById(id);

            return Ok();
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);

                IdentityResult result = await _repo.ChangePassword(u.Id, changePasswordViewModel.OldPassword, changePasswordViewModel.NewPassword);

                IHttpActionResult errorResult = GetErrorResult(result);

                if (errorResult != null)
                {
                    return errorResult;
                }
                return Ok();
            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("ChangePassword --- " + msg);
            }

        }


        [Attributes.Authorize(Roles = "User,Safeer")]
        [Route("GetUserInfo")]
        [HttpPost]
        public async Task<IHttpActionResult> GetUserInfo()
        {
            var userName = User.Identity.GetUserName();

            if (!string.IsNullOrEmpty(userName))
            {
                var user = await GetApplicationUser(userName);



                if (user != null)
                {
                    var isAmbassedor = IsInRole(user.Id, "Safeer");
                   
                    return Ok(new { User = user, IsAmbassador = isAmbassedor });

                }


                else
                    return BadRequest("Can't Get User");
            }
            return BadRequest("You Need To Login ");


        }

        [Route("UserRols")]
        public async Task<IHttpActionResult> GetCurrentUserRoles()
        {
            var userName = User.Identity.GetUserName();

            if (!string.IsNullOrEmpty(userName))
            {
                var user = await GetApplicationUser(userName);
                if (user != null)
                {
                    var rols = await _repo.GetUserRoles(user.Id);
                    return Ok(rols);
                }

                else
                    return BadRequest("Can't Get User");
            }

            return BadRequest("You Need To Login ");
        }
        [Route("AllRols")]
        public async Task<IHttpActionResult> GetRoles()
        {
            try
            {
                var rols = await _repo.GetRoles();
                if (rols.Any())
                    return Ok(rols);
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetRoles --- " + msg);
            }



        }

        #region Soical

        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            string redirectUri = string.Empty;

            if (error != null)
            {
                return BadRequest(Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            var redirectUriValidationResult = ValidateClientAndRedirectUri(this.Request, ref redirectUri);

            if (!string.IsNullOrWhiteSpace(redirectUriValidationResult))
            {
                return BadRequest(redirectUriValidationResult);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            IdentityUser user = await _repo.FindAsync(new UserLoginInfo(externalLogin.LoginProvider, externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            redirectUri = string.Format("{0}#external_access_token={1}&provider={2}&haslocalaccount={3}&external_user_name={4}",
                                            redirectUri,
                                            externalLogin.ExternalAccessToken,
                                            externalLogin.LoginProvider,
                                            hasRegistered.ToString(),
                                            externalLogin.UserName);

            return Redirect(redirectUri);

        }
        [AllowAnonymous]
        [Route("ReSendConfirmationCode/{username}")]
        public async Task<IHttpActionResult> ReSendConfirmationCode(string username)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return BadRequest("User name Not Send");

                var user = await _repo.FindUserByUserName(username);
                if (user == null)
                    return BadRequest("User Not Found");
                if (user.EmailConfirmed == true)
                    return Ok("Email Is Confirmed");
                await _repo.ReSendEmailConfirmation(user);
                return Ok("Email Is SendConfrim");

            }
            catch (Exception ex)
            {

                string msg = ex.GetaAllMessages();
                return BadRequest("ReSendConfirmationCode " + msg);
            }
        }

        // POST api/Account/RegisterExternal
        [AllowAnonymous]
        [Route("RegisterExternal2")]
        public async Task<IHttpActionResult> RegisterExternalbck(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var verifiedAccessToken = await VerifyExternalAccessToken(model.Provider, model.ExternalAccessToken);
            if (verifiedAccessToken == null)
            {
                return BadRequest("Invalid Provider or External Access Token");
            }

            ApplicationUser user = await _repo.FindAsync(new UserLoginInfo(model.Provider, verifiedAccessToken.user_id));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                return BadRequest("External user is already registered");
            }

            user = new ApplicationUser() { UserName = model.UserName };

            IdentityResult result = await _repo.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            var info = new ExternalLoginInfo()
            {
                DefaultUserName = model.UserName,
                Login = new UserLoginInfo(model.Provider, verifiedAccessToken.user_id)
            };

            result = await _repo.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            //generate access token response
            var accessTokenResponse = GenerateLocalAccessTokenResponse(model.UserName);

            return Ok(accessTokenResponse);
        }


        [AllowAnonymous]
        [Route("RegisterExternal3")]
        public async Task<IHttpActionResult> RegisterExternal2(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUser user;
            if (model.Provider != "Twitter")
            {
                var verifiedAccessToken = await VerifyExternalAccessToken(model.Provider, model.ExternalAccessToken);

                if (verifiedAccessToken == null)
                {
                    return BadRequest("Invalid Provider or External Access Token");
                }
                model.UserId = verifiedAccessToken.user_id;
                user = await _repo.FindAsync(new UserLoginInfo(model.Provider, model.UserId));
            }
            else
            {
                user = await _repo.FindAsync(new UserLoginInfo(model.Provider, model.UserId));
            }




            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                // return BadRequest("External user is already registered");
                var token = GenerateLocalAccessTokenResponseUpdate(user);
                return Ok(token);
            }

            if (string.IsNullOrWhiteSpace(model.Name))
                model.Name = model.Provider + "User";
            user = new ApplicationUser()
            {
                UserName = model.UserName,
                Name = model.Name

            };

            IdentityResult result = await _repo.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            var info = new ExternalLoginInfo()
            {
                DefaultUserName = model.UserName,
                Login = new UserLoginInfo(model.Provider, model.UserId)
            };

            result = await _repo.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            //generate access token response
            user = await _repo.FindAsync(new UserLoginInfo(model.Provider, model.UserId));
            var accessTokenResponse = GenerateLocalAccessTokenResponseUpdate(user);

            return Ok(accessTokenResponse);
        }

        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUser user;
            //if (model.Provider != "Twitter")
            //{
            //    var verifiedAccessToken = await VerifyExternalAccessToken(model.Provider, model.ExternalAccessToken);

            //    if (verifiedAccessToken == null)
            //    {
            //        return BadRequest("Invalid Provider or External Access Token"+model.ExternalAccessToken +"provider"+ model.Provider );
            //    }
            //    model.UserId = verifiedAccessToken.user_id;
            //    user = await _repo.FindAsync(new UserLoginInfo(model.Provider, model.UserId));
            //}
            //else
            {
                user = await _repo.FindAsync(new UserLoginInfo(model.Provider, model.UserId));
            }




            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                // return BadRequest("External user is already registered");
                var token = GenerateLocalAccessTokenResponseUpdate(user);
                return Ok(token);
            }

            if (string.IsNullOrWhiteSpace(model.Name))
                model.Name = model.Provider + "User";
            user = new ApplicationUser()
            {
                UserName = model.UserName,
                Name = model.Name,
                Email = model.Email

            };

            IdentityResult result = await _repo.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            var info = new ExternalLoginInfo()
            {
                DefaultUserName = model.UserName,
                Login = new UserLoginInfo(model.Provider, model.UserId)
            };

            result = await _repo.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            //generate access token response
            user = await _repo.FindAsync(new UserLoginInfo(model.Provider, model.UserId));






            var accessTokenResponse = GenerateLocalAccessTokenResponseUpdate(user);

            return Ok(accessTokenResponse);
        }
        private JObject GenerateLocalAccessTokenResponseUpdate(ApplicationUser user)
        {

            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim("sub", user.UserName));
            using (AuthRepository repo = new AuthRepository())
            {
                var userRoles = repo.GetRoles(user.Id);
                foreach (var role in userRoles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role));
                }
            }
            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
            var isAmbassedor = IsInRole(user.Id, "Safeer");
            JObject tokenResponse = new JObject(
                new JProperty("userName", user.UserName),
                new JProperty("access_token", accessToken),
                new JProperty("token_type", "bearer"),
                //new JProperty("PhotoUrl", user.PhotoUrl ?? string.Empty),
                new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
                new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString()),
                new JProperty("IsAmbassedor", isAmbassedor.ToString())
                );

            return tokenResponse;
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("ObtainLocalAccessToken")]
        public async Task<IHttpActionResult> ObtainLocalAccessToken(string provider, string externalAccessToken)
        {

            if (string.IsNullOrWhiteSpace(provider) || string.IsNullOrWhiteSpace(externalAccessToken))
            {
                return BadRequest("Provider or external access token is not sent");
            }

            var verifiedAccessToken = await VerifyExternalAccessToken(provider, externalAccessToken);
            if (verifiedAccessToken == null)
            {
                return BadRequest("Invalid Provider or External Access Token");
            }

            IdentityUser user = await _repo.FindAsync(new UserLoginInfo(provider, verifiedAccessToken.user_id));

            bool hasRegistered = user != null;

            if (!hasRegistered)
            {
                return BadRequest("External user is not registered");
            }

            //generate access token response
            var accessTokenResponse = GenerateLocalAccessTokenResponse(user.UserName);

            return Ok(accessTokenResponse);

        }

        #endregion

        #region Helpers



        private string ValidateClientAndRedirectUri(HttpRequestMessage request, ref string redirectUriOutput)
        {

            Uri redirectUri;

            var redirectUriString = GetQueryString(Request, "redirect_uri");

            if (string.IsNullOrWhiteSpace(redirectUriString))
            {
                return "redirect_uri is required";
            }

            bool validUri = Uri.TryCreate(redirectUriString, UriKind.Absolute, out redirectUri);

            if (!validUri)
            {
                return "redirect_uri is invalid";
            }

            var clientId = GetQueryString(Request, "client_id");

            if (string.IsNullOrWhiteSpace(clientId))
            {
                return "client_Id is required";
            }

            var client = _repo.FindClient(clientId);

            if (client == null)
            {
                return string.Format("Client_id '{0}' is not registered in the system.", clientId);
            }

            if (!string.Equals(client.AllowedOrigin, redirectUri.GetLeftPart(UriPartial.Authority), StringComparison.OrdinalIgnoreCase))
            {
                return string.Format("The given URL is not allowed by Client_id '{0}' configuration.", clientId);
            }

            redirectUriOutput = redirectUri.AbsoluteUri;

            return string.Empty;

        }

        private string GetQueryString(HttpRequestMessage request, string key)
        {
            var queryStrings = request.GetQueryNameValuePairs();

            if (queryStrings == null) return null;

            var match = queryStrings.FirstOrDefault(keyValue => string.Compare(keyValue.Key, key, true) == 0);

            if (string.IsNullOrEmpty(match.Value)) return null;

            return match.Value;
        }

        private async Task<ParsedExternalAccessToken> VerifyExternalAccessToken(string provider, string accessToken)
        {
            ParsedExternalAccessToken parsedToken = null;

            var verifyTokenEndPoint = "";

            if (provider == "Facebook")
            {
                //You can get it from here: https://developers.facebook.com/tools/accesstoken/
                //More about debug_tokn here: http://stackoverflow.com/questions/16641083/how-does-one-get-the-app-access-token-for-debug-token-inspection-on-facebook
                var appToken = "276971536031476|lPmkKAidu0_m9fY3DnOTsVs5lyw";
                verifyTokenEndPoint = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", accessToken, appToken);
            }
            else if (provider == "Google")
            {
                verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}", accessToken);
            }
            else
            {
                return null;
            }

            var client = new HttpClient();
            var uri = new Uri(verifyTokenEndPoint);
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                dynamic jObj = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(content);

                parsedToken = new ParsedExternalAccessToken();

                if (provider == "Facebook")
                {
                    parsedToken.user_id = jObj["data"]["user_id"];
                    parsedToken.app_id = jObj["data"]["app_id"];

                    if (!string.Equals(Startup.FacebookAuthOptions.AppId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                }
                else if (provider == "Google")
                {
                    parsedToken.user_id = jObj["user_id"];
                    parsedToken.app_id = jObj["audience"];

                    if (!string.Equals(Startup.GoogleAuthOptions.ClientId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }

                }

            }

            return parsedToken;
        }

        private JObject GenerateLocalAccessTokenResponse(string userName)
        {

            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, userName));
            identity.AddClaim(new Claim("role", "user"));

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);

            JObject tokenResponse = new JObject(
                new JProperty("userName", userName),
                new JProperty("access_token", accessToken),
                new JProperty("token_type", "bearer"),
                new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
                new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
                );

            return tokenResponse;
        }



        #endregion
    }
}
