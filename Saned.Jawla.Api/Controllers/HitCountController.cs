using System;
using System.Threading.Tasks;
using System.Web.Http;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Views;

namespace Saned.Jawla.Api.Controllers
{
    public class HitCountController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public HitCountController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }
        [HttpPost]
        public async Task<IHttpActionResult> AddViews(HitCountDetails viewModel)
        {
            try
            {

                return Ok(await _unitOfWork.HitCountDetailsRepository.CreateAsync(viewModel));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("AddViews..." + msg);
            }


        }
    }
}