﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Jawla.Api.Properties;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;
using Saned.Jawla.Data.Core.Enum;


namespace Saned.Jawla.Api.Controllers
{


    //HotelAdd,
    //    ,
    //    HotelAddedWithoutAdminApprove,
    //    HotelViewAny,

    [RoutePrefix("api/Hotels")]
    public class HotelsController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public HotelsController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }
        #region Admin_HotelsController

        [Route("")]
        [HttpPost]
        [Attributes.Authorize(Roles = "HotelViewAny,Administrator")]
        public async Task<IHttpActionResult> Hotels(CategoryDetailsSearchViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);


                int? status = CategoryDetailsStatusEnum.Approve.GetHashCode();
                string ambassadorId = null;
                //bool hotelViewByCreatorRole = IsInRole(u.Id, PremisionEnum.HotelViewByCreator.ToString());
                bool hotelViewAnyRole = IsInRole(u.Id, PremisionEnum.HotelViewAny.ToString());
                bool administratorRole = IsInRole(u.Id, RolesEnum.Administrator.ToString());
                if (!administratorRole && !hotelViewAnyRole)
                {
                    ambassadorId = u.Id;
                }


                var list = await _unitOfWork.CategoryDetails.FindAllAsync(model.PageNumber, model.PageSize, model.CountryId, model.CityId, model.MunicipalityId, CategoryTypeEnum.Hotels.GetHashCode(), model.CategoryDetailsId, model.Longitude, model.Latitude, status, 4, model.EventStatyDate, ambassadorId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                {
                    msg = ex.InnerException.Message;

                }
                return BadRequest(msg);
            }

        }



        [HttpGet]
        [Route("{id:long}")]
        public async Task<IHttpActionResult> GetHotelById(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = await _unitOfWork.CategoryDetails.GetById(id);


                if (bo == null)
                {
                    return NotFound();
                }


                #region RestaurantsDetails
                RestaurantViewModel result = new RestaurantViewModel
                {
                    Id = bo.Id,
                    Name = bo.Name,
                    CountryId = bo.CountryId,
                    CityId = bo.CityId,
                    MunicipalityId = bo.MunicipalityId,
                    Latitude = bo.Latitude,
                    Longitude = bo.Longitude,
                    IsBin = bo.IsBin,
                    Status = bo.Status,
                    AmbassadorId = bo.AmbassadorId,
                    Description = bo.Description,
                    WorkHours = bo.Hotels.WorkHours,
                    CategoryId = bo.CategoryId

                };

                #endregion


                #region QuestionsVsAnswers

                var questions = await _unitOfWork.Faq.GetAllAsync(id);
                if (questions != null)
                {
                    result.Questions = Mapper.Map<List<CategoryDetailsFaq>, List<QuestionViewModel>>(questions);
                }

                #endregion

                #region Braches

                var branch = await _unitOfWork.Branche.FindAllAsyncWithInclude(id);
                if (branch != null)
                {
                    result.Branches = Mapper.Map<List<CategoryDetailsBranche>, List<BranchViewModel>>(branch);
                }

                #endregion

                #region ProjectContact
                int typeProject = ContactTypeEnum.ProjectContact.GetHashCode();
                var contactProject = await _unitOfWork.Contact.FindAllAsync(id, typeProject);
                if (contactProject != null)
                {

                    foreach (var contactRecord in contactProject)
                    {
                        if (contactRecord.SoicalType == SoicalTypeEnum.PhoneNumber.ToString())
                            result.ProjectPhoneNumber = contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Facebook.ToString())
                            result.ProjectFacebook = contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Google.ToString())
                            result.ProjectGoogle = contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.WhatsApp.ToString())
                            result.ProjectWhatsapp = contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Twitter.ToString())
                            result.ProjectTwitter = contactRecord.ContactInfo;



                    }
                }

                #endregion

                #region images
                string uploadPath = Settings.Default.UploadFullPath;

                List<Uploader> imagesList = new List<Uploader>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");
                foreach (var image in imgs)
                {
                    if (!image.IsDefault)
                    {
                        Uploader photo = new Uploader()
                        {
                            Url = uploadPath + image.PhotoUrl,
                            IsDefault = image.IsDefault,
                            Id = image.Id
                        };
                        imagesList.Add(photo);
                    }
                    else
                    {
                        result.AttachmentUrl = uploadPath + image.PhotoUrl;
                        result.CoverId = image.Id;
                    }
                }
                result.ImagesList = imagesList;


                #endregion


                return Ok(result);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetHotelById " + msg);
            }
        }




        [Authorize(Roles = "HotelDeleteAny,Administrator")]
        [HttpPost]
        [Route("Delete/{id:long}")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();

                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);

                var restaurant = await _unitOfWork.CategoryDetails.GetAsync(id);
                if (restaurant == null)
                {
                    return NotFound();
                }

                bool hotelDeleteAnyRole = IsInRole(u.Id, PremisionEnum.HotelDeleteAny.ToString());
                //bool hotelDeleteByCreatorRole = IsInRole(u.Id, PremisionEnum.HotelDeleteByCreator.ToString());
                bool administratorRole = IsInRole(u.Id, RolesEnum.Administrator.ToString());
                if (!administratorRole && !hotelDeleteAnyRole &&
                    u.Id == restaurant.AmbassadorId)
                    return Unauthorized();


                _unitOfWork.CategoryDetails.DeleteAsync(restaurant);
                _unitOfWork.CategoryDetails.RemoveImagesbyCategoryId(id);
                await _unitOfWork.Complete();
                return Ok();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Delete " + msg);
            }
        }


        [Route("Create")]
        [HttpPost]

        [Authorize(Roles = "HotelAdd,Administrator")]
        public async Task<IHttpActionResult> Create(RestaurantViewModel viewModel)
        {

            try
            {
                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);

                //   int status = IsInRole(u.Id, "HotelAdd") ? CategoryDetailsStatusEnum.New.GetHashCode() : CategoryDetailsStatusEnum.Approve.GetHashCode();
                int status = CategoryDetailsStatusEnum.Approve.GetHashCode();//IsInRole(u.Id, "HotelAddedWithoutAdminApprove") || IsInRole(u.Id, "Administrator") ? CategoryDetailsStatusEnum.Approve.GetHashCode() : CategoryDetailsStatusEnum.New.GetHashCode();
                viewModel.AmbassadorId = u.Id;

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);






                var categoryDetails = new CategoryDetails()
                {
                    CategoryId = viewModel.CategoryId,
                    Name = viewModel.Name,
                    CountryId = viewModel.CountryId,
                    CityId = viewModel.CityId,
                    MunicipalityId = viewModel.MunicipalityId,
                    Latitude = viewModel.Latitude,
                    Longitude = viewModel.Longitude,
                    IsBin = viewModel.IsBin,
                    Status = status.ToString(),
                    AmbassadorId = viewModel.AmbassadorId,
                    Description = viewModel.Description,

                };
                categoryDetails.Hotels = new HotelsDetails()
                {
                    WorkHours = viewModel.WorkHours

                };

                if (viewModel.Questions.Count > 0)
                {
                    categoryDetails.CategoryDetailsFaqs = viewModel.Questions
                                                      .Select(a => new CategoryDetailsFaq
                                                      {
                                                          Answer = a.Answer,
                                                          Question = a.Question,
                                                      }).ToList();
                }

                var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);


                #region Contact


                categoryDetails.CategoryDetailsContacts = new List<CategoryDetailsContact>();
                if (viewModel.ProjectPhoneNumber != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.ProjectWhatsapp != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()

                    });
                if (viewModel.ProjectFacebook != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.ProjectTwitter != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.ProjectGoogle != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion




                #region Branches

                if (viewModel.Branches.Count > 0)
                {
                    _unitOfWork.Branche.AddRange(viewModel.Branches.Select(a => new CategoryDetailsBranche()
                    {
                        CategoryDetailsId = result.Id,
                        CityId = a.City.Id,
                        CountryId = a.Country.Id,
                        MunicipalityId = a.Municipality != null ? a.Municipality.Id : (int?)null,
                        Name = a.Name,
                        Longitude = a.Longitude,
                        Latitude = a.Latitude
                    }).ToList());
                }

                #endregion
                IHttpActionResult response = Ok(await _unitOfWork.Complete());





                #region SaveImages
                if (viewModel.ImagesList != null)
                {
                    foreach (var image in viewModel.ImagesList)
                    {
                        if (!string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                        {
                            var randomImage = SaveImage(image.Filename, image.Base64);
                            await CreatePhoto(new Photo()
                            {
                                IsDefault = false,
                                Lang = "ar",
                                ModifiedDate = DateTime.Now,
                                PhotoUrl = randomImage,
                                RelatedId = result.Id.ToString(),
                                RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                                Title = viewModel.Name

                            });
                        }
                    }
                }

                if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                {
                    string defaultImge = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = result.Id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = viewModel.Name

                    });

                }


                #endregion






                return response;
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }




        [Route("Update")]
        [HttpPost]
        [Authorize(Roles = "HotelEditAny,Administrator")]

        public async Task<IHttpActionResult> Update(RestaurantViewModel viewModel)
        {
            try
            {



                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);



                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;



                var selected = await _unitOfWork.CategoryDetails.GetById(viewModel.Id);
                if (selected == null)
                    return NotFound();


                bool hotelEditAnyRole = IsInRole(u.Id, PremisionEnum.HotelEditAny.ToString());
                // bool hotelEditByCreatorRole = IsInRole(u.Id, PremisionEnum.HotelEditByCreator.ToString());
                bool administratorRole = IsInRole(u.Id, "Administrator");
                if (!administratorRole && !hotelEditAnyRole &&
                    u.Id == selected.AmbassadorId)
                    return Unauthorized();






                #region SaveEventDetails

                selected.CategoryId = viewModel.CategoryId;
                selected.Name = viewModel.Name;
                selected.CountryId = viewModel.CountryId;
                selected.CityId = viewModel.CityId;
                selected.MunicipalityId = viewModel.MunicipalityId;
                selected.Latitude = viewModel.Latitude;
                selected.Longitude = viewModel.Longitude;
                selected.IsBin = viewModel.IsBin;
                selected.Status = viewModel.Status;
                selected.AmbassadorId = viewModel.AmbassadorId;
                selected.Description = viewModel.Description;
                selected.Hotels.WorkHours = viewModel.WorkHours;

                //var res = await _unitOfWork.Complete();
                #endregion

                #region saveQuestionVsAnswers

                if (viewModel.Questions.Count > 0)
                {
                    _unitOfWork.Faq.RemoveRange(selected.CategoryDetailsFaqs);
                    selected.CategoryDetailsFaqs = viewModel.Questions
                                                       .Select(a => new CategoryDetailsFaq
                                                       {
                                                           Answer = a.Answer,
                                                           Question = a.Question,
                                                       }).ToList();
                }


                #endregion

                #region Contact
                _unitOfWork.Contact.RemoveRange(viewModel.Id);
                selected.CategoryDetailsContacts = new List<CategoryDetailsContact>();
                if (viewModel.ProjectPhoneNumber != null)
                    selected.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.ProjectWhatsapp != null)
                    selected.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()

                    });
                if (viewModel.ProjectFacebook != null)
                    selected.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.ProjectTwitter != null)
                    selected.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.ProjectGoogle != null)
                    selected.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion


                #region Branches

                if (viewModel.Branches.Count > 0)
                {
                    _unitOfWork.Branche.RemoveRangeByCategoryDetailsId(viewModel.Id);
                    _unitOfWork.Branche.AddRange(viewModel.Branches.Select(a => new CategoryDetailsBranche()
                    {
                        CategoryDetailsId = viewModel.Id,
                        CityId = a.City.Id,
                        CountryId = a.Country.Id,
                        MunicipalityId = a.Municipality != null ? a.Municipality.Id : (int?)null,
                        Name = a.Name,
                        Longitude = a.Longitude,
                        Latitude = a.Latitude
                    }).ToList());
                }

                #endregion

                #region SaveImages
                await UpdateImages(viewModel.ImagesList, viewModel.Id, viewModel.Name, viewModel.CoverId, viewModel.ImageFilename, viewModel.ImageBase64);


                #endregion






                response = Ok(await _unitOfWork.Complete());
                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }


        [HttpPost]
        [Route("Approve/{id:long}")]
        public async Task<IHttpActionResult> Approve(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = await _unitOfWork.CategoryDetails.GetAsync(id);
                if (bo == null)
                {
                    return NotFound();
                }
                bo.Approve();
                await _unitOfWork.Complete();
                return Ok();

            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Approve" + msg);
            }
        }



        private async Task UpdateImages(List<Uploader> imagesList, long id, string name, int coverId, string imageFilename, string imageBase64)
        {
            if (coverId == 0)
            {
                _unitOfWork.PhotoRepository.DeleteMainImage(id);
            }


            if (!string.IsNullOrEmpty(imageFilename) && !string.IsNullOrEmpty(imageBase64))
            {
                string defaultImge = SaveImage(imageFilename, imageBase64);

                Photo selected = _unitOfWork.PhotoRepository.GetPhoto(coverId);
                if (selected != null)
                {
                    selected.IsDefault = true;
                    selected.Lang = "ar";
                    selected.ModifiedDate = DateTime.Now;
                    selected.PhotoUrl = defaultImge;
                    selected.RelatedId = id.ToString();
                    selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                    selected.Title = name;
                    await _unitOfWork.Complete();
                }
                else
                {
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = name

                    });
                }


            }
            if (imagesList != null && imagesList.Count > 0)
            {
                List<int> editedImages = imagesList.Select(uploader => uploader.Id).ToList();
                if (coverId != 0)
                {
                    editedImages.Add(coverId);
                }
                _unitOfWork.PhotoRepository.RemoveImages(editedImages, id);
                await _unitOfWork.Complete();


                foreach (var image in imagesList)
                {
                    if (image.Id == 0 && !string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = false,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = randomImage,
                            RelatedId = id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = name
                        });
                    }
                    else if (image.Id != 0 && !string.IsNullOrEmpty(image.Filename) &&
                             !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        Photo selected = _unitOfWork.PhotoRepository.GetPhoto(image.Id);
                        if (selected != null)
                        {
                            selected.IsDefault = false;
                            selected.Lang = "ar";
                            selected.ModifiedDate = DateTime.Now;
                            selected.PhotoUrl = randomImage;
                            selected.RelatedId = id.ToString();
                            selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                            selected.Title = name;
                            await _unitOfWork.Complete();
                        }

                    }
                }
            }
        }

        #endregion





    }


}