﻿using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using Saned.Jawla.Api.ViewModels;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Contact")]
    public class ApplicationContactInformationController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public ApplicationContactInformationController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }

        [HttpGet]
        [Route("GetApplicationInforamtion")]
        public async Task<IHttpActionResult> GetApplicationInforamtion()
        {

            try
            {

                return Ok(await _unitOfWork.ApplicationContact.GetAsync());
            }
            catch (Exception e)
            {
                return BadRequest("GetApplicationInforamtion ---" + e.GetaAllMessages());
            }
        }


        [HttpPost]
        [Route("CreateContact")]
        public async Task<IHttpActionResult> CreateContact(ApplicationContactInformationViewModel viewModel)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);


                var information = new ApplicationContactInformation()
                {
                    Id = viewModel.Id,
                    AboutUs = viewModel.AboutUs,
                    FacebookUrl = viewModel.FacebookUrl,
                    TwitterUrl = viewModel.TwitterUrl,
                    Email = viewModel.Email,
                    GooglePlusUrl = viewModel.GooglePlusUrl,
                    InstagrameUrl = viewModel.InstagrameUrl,
                    LinkedInUrl = viewModel.LinkedInUrl,
                    PhoneNumber = viewModel.PhoneNumber,
                    YoutubeUrl = viewModel.YoutubeUrl,
                };

                var appInfo = await _unitOfWork.ApplicationContact.GetAsync();
                if (appInfo == null)
                {
                    _unitOfWork.ApplicationContact.CreateAsync(information);
                }
                else
                {

                    appInfo.Update(
                        viewModel.PhoneNumber,
                        viewModel.Email,
                        viewModel.FacebookUrl,
                        viewModel.TwitterUrl,
                        viewModel.InstagrameUrl,
                        viewModel.YoutubeUrl,
                        viewModel.GooglePlusUrl,
                        viewModel.LinkedInUrl,
                        viewModel.AboutUs
                        );
                }
             
                return Ok(await _unitOfWork.Complete());
            }
            catch (Exception e)
            {

                return BadRequest("CreateContact ---" + e.GetaAllMessages());
            }



        }


        [HttpPost]
        [Route("RemoveContact")]
        public IHttpActionResult RemoveContact(ApplicationContactInformation contact)
        {

            try
            {
                _unitOfWork.ApplicationContact.DeleteAsync(contact);
                return Ok(_unitOfWork.Complete());
            }
            catch (Exception e)
            {

                return BadRequest("RemoveContact ---" + e.GetaAllMessages());
            }
        }

        [HttpPost]
        [Route("DeleteById/{id}")]

        public IHttpActionResult RemoveConatct(int id ) {
            try
            {
                _unitOfWork.ApplicationContact.DeleteById(id);
                return Ok(_unitOfWork.Complete());

            }
            catch (Exception e)
            {
                return BadRequest("Delete Contact By id " + e.GetaAllMessages());
                
            }
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IHttpActionResult> GetById(int id) {
            try
            {
                return Ok(await _unitOfWork.ApplicationContact.GetById(id));
            }
            catch (Exception e)
            {

                return BadRequest("Get Contact By id " + e.GetaAllMessages());
            }


        }





    }
}
