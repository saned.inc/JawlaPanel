﻿using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/ContactUs")]
    public class ApplicationContactUsController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public ApplicationContactUsController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }


        [HttpGet]
        [Route("GetAll")]
        public async Task<IHttpActionResult> GetAllContactUS() {

            try
            {

                return Ok(await _unitOfWork.ApplicationContactUsRepository.GetAsync());
            }
            catch (Exception e)
            {
                return BadRequest("GetAllContactUs ---" + e.GetaAllMessages());

            }

        }

        [HttpGet]
        [Route("Search/{keyword}")]
        public async Task<IHttpActionResult> Search(string keyword)
        {

            try
            {
                var result = await _unitOfWork.ApplicationContactUsRepository.GetAsync();
                result = result.Where(x => x.Name.ToLower().Contains(keyword.ToLower())).ToList();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest("GetAllContactUs ---" + e.GetaAllMessages());

            }

        }

        [HttpPost]
        [Route("AddMessage")]
        public async Task<IHttpActionResult> CreateMessage(ApplicationContactUsViewModel viewModel) {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                

                var message = new ApplicationContactUs()
                {
                  
                    Name=viewModel.Name,
                    Message = viewModel.Message,
                    Email = viewModel.Email,
                    PhoneNumber = viewModel.PhoneNumber
                };

                _unitOfWork.ApplicationContactUsRepository.CreateAsync(message);
                return Ok( await _unitOfWork.Complete());
            }
            catch (Exception e)
            {
                return BadRequest("CreateContactUs ---" + e.GetaAllMessages());
              
            }
        }

        [HttpPost]
        [Route("RemoveContactUs/{id}")]
        public async Task<IHttpActionResult> RemoveConctactUs(int id)
        {
            try
            {
               var contact= await _unitOfWork.ApplicationContactUsRepository.FindAsync(id);
                var result = 0;
                if (contact != null)
                {
                    _unitOfWork.ApplicationContactUsRepository.DeleteAsync(contact);
                    await _unitOfWork.Complete();
                }
               
                return Ok(result);
            }
            catch (Exception e)
            {

                return BadRequest(e.GetaAllMessages());
            }

        }

        [HttpGet]
        [Route("FindContactUs/{id?}")]
        public async Task<IHttpActionResult> FindContactUs(int id)
        {
            try
            {
                return Ok(await _unitOfWork.ApplicationContactUsRepository.FindAsync(id));
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }



    }
}
