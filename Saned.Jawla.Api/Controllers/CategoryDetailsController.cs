﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Persistence.Repositories;
using Saned.Modules.Views;
namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/CategoryDetails")]
    public class CategoryDetailsController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public CategoryDetailsController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }

        [Route("SearchCategoryResult")]
        [HttpPost]
        public async Task<IHttpActionResult> SearchResult(CategoryDetailsSearchViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)    
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                var list =
                    await
                        _unitOfWork.CategoryDetails.FindAllAsync(model.PageNumber, model.PageSize, model.CountryId,
                            model.CityId, model.MunicipalityId, model.SubCategoryId, model.CategoryDetailsId,
                            model.Longitude, model.Latitude, model.CategoryId, model.EventStatyDate);

                if (string.IsNullOrEmpty(userName)) return Ok(list);
                var u = await GetApplicationUser(userName);
                foreach (var item in list)
                {
                    if (item.Id == null) continue;
                    var checkIn = await _unitOfWork.Favourite.FindAsync(u.Id, item.Id.Value);
                    item.IsFavourite = checkIn != null;
                }

                return Ok(list);
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                    msg = ex.InnerException.Message;
                return BadRequest(msg);
            }

        }

        [Route("CreateEvent")]
        public async Task<IHttpActionResult> CreateEvent(EventViewModel viewModel)
        {
            try
            {
                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var userName = User.Identity.GetUserName();

                //  if (viewModel.Id == 0)
                {
                    var categoryDetails = new CategoryDetails()
                    {
                        CategoryId = viewModel.CategoryId,
                        Name = viewModel.Name,
                        CountryId = viewModel.CountryId,
                        CityId = viewModel.CityId,
                        MunicipalityId = viewModel.MunicipalityId,
                        Latitude = viewModel.Latitude,
                        Longitude = viewModel.Longitude,
                        IsBin = viewModel.IsBin,
                        Status = viewModel.Status,
                        AmbassadorId = viewModel.AmbassadorId,
                        EventStartDate = viewModel.EventStartDate,
                        Description = viewModel.Description
                    };
                    var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);
                    //await _unitOfWork.Complete();

                    categoryDetails.Events = new EventDetails()
                    {

                        Owner = viewModel.Owner,
                        EndDate = viewModel.EndDate,
                        StartTime = viewModel.StartTime,
                        EndTime = viewModel.EndTime,
                    };


                    var res = await _unitOfWork.Complete();
                    if (viewModel.Images != null)
                    {
                        foreach (var image in viewModel.Images)
                        {
                            if (!string.IsNullOrEmpty(image.Key) && !string.IsNullOrEmpty(image.Value))
                            {
                                var randomImage = SaveImage(image.Key, image.Value);
                                await CreatePhoto(new Photo()
                                {
                                    IsDefault = false,
                                    Lang = "ar",
                                    ModifiedDate = DateTime.Now,
                                    PhotoUrl = randomImage,
                                    RelatedId = result.Id.ToString(),
                                    RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                                    Title = viewModel.Name

                                });
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                    {
                        string defaultImge = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = true,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = defaultImge,
                            RelatedId = result.Id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = viewModel.Name

                        });

                    }

                    response = Ok(res);

                }
                //else
                //{
                //    var eventDetails = await (_unitOfWork.EventDetails.GetAsync(viewModel.Id));
                //    eventDetails.Update(viewModel.Owner, viewModel.EndDate);
                //    var result = await _unitOfWork.Complete();
                //    response = Ok(result);
                //}

                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        [Route("ViewEvent")]
        public async Task<IHttpActionResult> ViewEvent(GetDetailsViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                // Load Main Data

                var userName = User.Identity.GetUserName();
                ApplicationUser user = null;
                if (!string.IsNullOrEmpty(userName))
                {
                    user = await GetApplicationUser(userName);
                    viewModel.UserId = user.Id;
                }
                var bo = await _unitOfWork.EventDetails.GetAsync(viewModel.Id, viewModel.UserId);
                if (bo == null)
                {
                    return NotFound();
                }
                EventDetailsViewModel result = new EventDetailsViewModel
                {
                    StartTime = Convert.ToDateTime(bo.StartTime).ToShortTimeString(),
                    EndTime = Convert.ToDateTime(bo.EndTime).ToShortTimeString(),
                    Latitude = bo.Latitude,
                    Longitude = bo.Longitude,
                    Category = bo.Category,
                    Country = bo.Country,
                    Name = bo.Name,
                    AllHit = bo.AllHit,
                    City = bo.City,
                    Description = bo.Description,
                    EventEndDate = bo.EventEndDate,
                    EventId = bo.EventId,
                    EventStartDate = bo.EventStartDate,
                    IsBin = bo.IsBin,
                    Municipality = bo.Municipality,
                    Owner = bo.Owner,
                    Rating = bo.Rating,
                    UserRating = bo.UserRating,
                    Ambassador = bo.Ambassador,

                };
                //var userName = User.Identity.GetUserName();
                if (!string.IsNullOrEmpty(userName))
                {
                    //  var u = await GetApplicationUser(userName);
                    if (user != null)
                    {
                        var checkIn = await _unitOfWork.Favourite.FindAsync(user.Id, viewModel.Id);
                        result.IsFavourite = checkIn != null;
                        if (user.Id == bo.AmbassadorId)
                            result.IsOwner = true;
                    }

                    //--TODO Check Ambassador For Event.
                    var mbassadorRequest = await _unitOfWork.AmbassadorRequest.FindAsyncByUserId(bo.AmbassadorId);
                    if (mbassadorRequest != null)
                    {
                        result.IsAmbassador = true;
                        result.AmbassadorRequestId = mbassadorRequest.Id;
                    }


                }

                #region ProjectContact
                int typeProject = ContactTypeEnum.ProjectContact.GetHashCode();
                var contactProject = await _unitOfWork.Contact.FindAllAsync(viewModel.Id, typeProject);
                if (contactProject != null)
                {

                    foreach (var contactRecord in contactProject)
                    {
                        if (contactRecord.SoicalType == SoicalTypeEnum.PhoneNumber.ToString())
                            result.ContactPhoneNumber = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Facebook.ToString())
                            result.ContactFaceBook = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Google.ToString())
                            result.ContactGoogle = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.WhatsApp.ToString())
                            result.ContactWhatsUp = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Twitter.ToString())
                            result.ContactTwitter = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                    }
                }

                #endregion

                // Get Image Need TO add Migration for Photo
                var coverPhotosList = await _unitOfWork.VisitorCover.FindAllAsync(viewModel.Id);


                var imgs =
                    await
                        _unitOfWork.PhotoRepository.FindAllAsync(viewModel.Id.ToString(),
                            RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");

                if (imgs.Count <= 0) return Ok(result);
                foreach (var image in imgs.Select(t => new CategoryDetailsImage
                {
                    ImageUrl = t.PhotoUrl,
                    IsDefault = t.IsDefault
                }))
                {
                    result.Images.Add(image);
                }

                if (coverPhotosList.Count <= 0) return Ok(result);
                foreach (var image in coverPhotosList.Select(t => new CategoryDetailsImage
                {
                    ImageUrl = t.PhotoUrl,
                    IsDefault = t.IsDefault
                }))
                {
                    result.Images.Add(image);
                }




                #region View

                if (string.IsNullOrEmpty(viewModel.UserId))
                    viewModel.UserId = "0";

                var hit = new HitCountDetails()
                {

                    UserId = viewModel.UserId,
                    DeviceId = viewModel.DeviceId,
                    ModifiedDate = DateTime.Now,
                    Hits = 1,
                    RelatedId = viewModel.Id.ToString(),
                    RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),


                };
                await _unitOfWork.HitCountDetailsRepository.CreateAsync(hit);

                #endregion



                return Ok(result);


            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        [Route("CreateProject")]
        public async Task<IHttpActionResult> CreateProject(ProjectViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var userName = User.Identity.GetUserName();

                // if (viewModel.Id == 0)
                // {
                var categoryDetails = new CategoryDetails()
                {
                    CategoryId = viewModel.CategoryId,
                    Name = viewModel.Name,
                    CountryId = viewModel.CountryId,
                    CityId = viewModel.CityId,
                    MunicipalityId = viewModel.MunicipalityId,
                    Latitude = viewModel.Latitude,
                    Longitude = viewModel.Longitude,
                    IsBin = viewModel.IsBin,
                    Status = viewModel.Status,
                    AmbassadorId = viewModel.AmbassadorId,
                    Description = viewModel.Description
                };
                var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);


                categoryDetails.Projects = new ProjectDetails()
                {
                    ProjectId = result.Id,
                    Owner = viewModel.Owner,

                };
                categoryDetails.CategoryDetailsContacts = new List<CategoryDetailsContact>();

                #region OwerContact

                if (viewModel.OwnerPhoneNumber != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.OwnerPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.OwerContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.OwnerWhatsapp != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.OwnerWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.OwerContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()
                    });
                if (viewModel.OwnerFacebook != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.OwnerFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.OwerContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.OwnerTwitter != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.OwnerTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.OwerContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.OwnerGoogle != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.OwnerGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.OwerContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion

                #region ProjectContact

                if (viewModel.ProjectPhoneNumber != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.ProjectWhatsapp != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()
                    });
                if (viewModel.ProjectFacebook != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.ProjectTwitter != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.ProjectGoogle != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion

                IHttpActionResult response = Ok(await _unitOfWork.Complete());
                //}


                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        [HttpPost]
        [Route("Approve/{id}")]
        public async Task<IHttpActionResult> ApproveViewCategoryDetails(long id)
        {
            try
            {
                var bo = await _unitOfWork.CategoryDetails.FindAsync(id);
                if (bo != null)
                {
                    bo.Approve();
                    return Ok(await _unitOfWork.Complete());

                }
                return NotFound();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [HttpPost]
        [Route("GetCategoryDetails")]
        public async Task<IHttpActionResult> GetCategoryDetails(CategoryNameViewModel viewModel)
        {
            try
            {
                return Ok(await _unitOfWork.CategoryDetails.FindAsync(viewModel.CategoryId, viewModel.SubCategoryId, viewModel.CountryId, viewModel.CityId, viewModel.MunicipalityId));
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetCategoryDetails " + msg);
            }
        }

        [HttpPost]

        [Route("CreatePlace")]
        public async Task<IHttpActionResult> CreatePlace(HotelViewModel viewModel)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var categoryDetails = new CategoryDetails()
                {
                    CategoryId = viewModel.CategoryId,
                    Name = viewModel.Name,
                    CountryId = viewModel.CountryId,
                    CityId = viewModel.CityId,
                    MunicipalityId = viewModel.MunicipalityId,
                    Latitude = viewModel.Latitude,
                    Longitude = viewModel.Longitude,
                    IsBin = viewModel.IsBin,
                    Status = viewModel.Status,
                    AmbassadorId = viewModel.AmbassadorId,
                    Description = viewModel.Description
                };
                var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);

                categoryDetails.Hotels = new HotelsDetails()
                {
                    WorkHours = viewModel.WorkHours

                };

                #region Contact


                categoryDetails.CategoryDetailsContacts = new List<CategoryDetailsContact>();
                if (viewModel.ProjectPhoneNumber != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.ProjectWhatsapp != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()

                    });
                if (viewModel.ProjectFacebook != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.ProjectTwitter != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.ProjectGoogle != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion

                IHttpActionResult response = Ok(await _unitOfWork.Complete());

                #region Image List

                if (viewModel.Images != null)
                {
                    foreach (var image in viewModel.Images)
                    {
                        if (!string.IsNullOrEmpty(image.Key) && !string.IsNullOrEmpty(image.Value))
                        {
                            var randomImage = SaveImage(image.Key, image.Value);
                            await CreatePhoto(new Photo()
                            {
                                IsDefault = false,
                                Lang = "ar",
                                ModifiedDate = DateTime.Now,
                                PhotoUrl = randomImage,
                                RelatedId = result.Id.ToString(),
                                RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                                Title = viewModel.Name

                            });
                        }
                    }
                }

                #endregion

                #region DefaultImage

                if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                {
                    string defaultImge = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = result.Id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = viewModel.Name

                    });

                }

                #endregion





                return response;
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> CreatePlaceBranch(BrancheViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var getCategoryDetails = await _unitOfWork.CategoryDetails.GetAsync(viewModel.CategoryDetailsId);
                if (getCategoryDetails == null)
                {
                    var errors = new List<string>();
                    var error = ValidateMessage(errors, "Place Number " + viewModel.CategoryDetailsId + " Not Found");
                    IHttpActionResult errorResult = GetErrorResult(error);
                    return errorResult;
                }
                else
                {
                    _unitOfWork.Branche.CreateAsync(new CategoryDetailsBranche()
                    {
                        CategoryDetailsId = viewModel.CategoryDetailsId,
                        PhoneNumber = viewModel.PhoneNumber,
                        CountryId = viewModel.CountryId,
                        CityId = viewModel.CityId,
                        MunicipalityId = viewModel.MunicipalityId,
                        Latitude = viewModel.Latitude,
                        Longitude = viewModel.Longitude
                    });
                    return Ok(await _unitOfWork.Complete());
                }
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreatePlaceFaq(FaqViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var getCategoryDetails = await _unitOfWork.CategoryDetails.GetAsync(viewModel.CategoryDetailsId);
                if (getCategoryDetails == null)
                {
                    List<string> errors = new List<string>();
                    var error = ValidateMessage(errors, "Place Number " + viewModel.CategoryDetailsId + " Not Found");
                    IHttpActionResult errorResult = GetErrorResult(error);
                    return errorResult;
                }
                else
                {
                    _unitOfWork.Faq.CreateAsync(new CategoryDetailsFaq()
                    {
                        CategoryDetailsId = viewModel.CategoryDetailsId,
                        Question = viewModel.Question,
                        Answer = viewModel.Answer,
                    });
                    return Ok(await _unitOfWork.Complete());
                }
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [Route("ViewPlace")]
        public async Task<IHttpActionResult> ViewPlace(GetDetailsViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                // Load Main Data

                var userName = User.Identity.GetUserName();
                ApplicationUser user = null;
                if (!string.IsNullOrEmpty(userName))
                {
                    user = await GetApplicationUser(userName);
                    viewModel.UserId = user.Id;
                }
                var bo = await _unitOfWork.PlaceDetails.GetAsync(viewModel.Id, viewModel.UserId);
                if (bo == null)
                {
                    return NotFound();
                }


                #region Load Main Data

                PlaceDetailsViewModel result = new PlaceDetailsViewModel
                {
                    Latitude = bo.Latitude,
                    Longitude = bo.Longitude,
                    Category = bo.Category,
                    Country = bo.Country,
                    Name = bo.Name,
                    AllHit = bo.AllHit,
                    City = bo.City,
                    Description = bo.Description,
                    IsBin = bo.IsBin,
                    Municipality = bo.Municipality,
                    Rating = bo.Rating,
                    UserRating = bo.UserRating,
                    PlaceId = bo.HotelId,
                    WorkHours = bo.WorkHours,
                    Ambassador = bo.Ambassador
                };

                #endregion


                #region  In Favourite , Is Owner

                if (!string.IsNullOrEmpty(userName))
                {
                    if (user != null)
                    {
                        var checkIn = await _unitOfWork.Favourite.FindAsync(user.Id, viewModel.Id);
                        result.IsFavourite = checkIn != null;
                        if (user.Id == bo.AmbassadorId)
                            result.IsOwner = true;
                    }
                }
                var mbassadorRequest = await _unitOfWork.AmbassadorRequest.FindAsyncByUserId(bo.AmbassadorId);
                if (mbassadorRequest != null)
                {
                    result.IsAmbassador = true;
                    result.AmbassadorRequestId = mbassadorRequest.Id;
                }
                #endregion


                #region Images

                var imgs =
                    await
                        _unitOfWork.PhotoRepository.FindAllAsync(viewModel.Id.ToString(),
                            RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");

                if (imgs.Count > 0)
                {
                    foreach (var image in imgs.Select(t => new CategoryDetailsImage
                    {
                        ImageUrl = t.PhotoUrl,
                        IsDefault = t.IsDefault
                    }))
                    {
                        result.Images.Add(image);
                    }
                }
                #endregion


                #region Braches

                var branch = await _unitOfWork.Branche.FindAllAsync(viewModel.Id);
                if (branch != null)
                {
                    result.Branches.AddRange((IEnumerable<CategoryDetailsBrancheDto>)branch);
                }

                #endregion


                #region Contact
                int type = ContactTypeEnum.ProjectContact.GetHashCode();
                var contact = await _unitOfWork.Contact.FindAllAsync(viewModel.Id, type);
                if (contact != null)
                {

                    foreach (var contactRecord in contact)
                    {
                        if (contactRecord.SoicalType == SoicalTypeEnum.PhoneNumber.ToString())
                            result.ProjectPhoneNumber = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Facebook.ToString())
                            result.ProjectFacebook = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Google.ToString())
                            result.ProjectGoogle = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.WhatsApp.ToString())
                            result.ProjectWhatsapp = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Twitter.ToString())
                            result.ProjectTwitter = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                    }
                }

                #endregion


                var faq = await _unitOfWork.Faq.FindAllAsync(viewModel.PageNumber, viewModel.PageSize, viewModel.Id);
                if (faq != null)
                {
                    result.Faqs.AddRange(faq);
                }
                #region View

                if (string.IsNullOrEmpty(viewModel.UserId))
                    viewModel.UserId = "0";

                var hit = new HitCountDetails()
                {

                    UserId = viewModel.UserId,
                    DeviceId = viewModel.DeviceId,
                    ModifiedDate = DateTime.Now,
                    Hits = 1,
                    RelatedId = viewModel.Id.ToString(),
                    RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),


                };
                await _unitOfWork.HitCountDetailsRepository.CreateAsync(hit);

                #endregion
                return Ok(result);


            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }


        [Route("ViewProject")]
        public async Task<IHttpActionResult> ViewProject(GetDetailsViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                // Load Main Data

                var userName = User.Identity.GetUserName();
                ApplicationUser user = null;
                if (!string.IsNullOrEmpty(userName))
                {
                    user = await GetApplicationUser(userName);
                    viewModel.UserId = user.Id;
                }
                var bo = await _unitOfWork.ProjectDetails.GetAsync(viewModel.Id, viewModel.UserId);
                if (bo == null)
                {
                    return NotFound();
                }

                #region Load Main Data

                ProjectDetailsViewModel result = new ProjectDetailsViewModel
                {
                    Latitude = bo.Latitude,
                    Longitude = bo.Longitude,
                    Category = bo.Category,
                    Country = bo.Country,
                    Name = bo.Name,
                    AllHit = bo.AllHit,
                    City = bo.City,
                    Description = bo.Description,
                    IsBin = bo.IsBin,
                    Municipality = bo.Municipality,
                    Rating = bo.Rating,
                    UserRating = bo.UserRating,
                    ProjectId = bo.ProjectId,
                    Owner = bo.Owner,
                    Ambassador = bo.Ambassador,
                    Idea = bo.Idea
                };

                #endregion

                #region  In Favourite , Is Owner

                if (!string.IsNullOrEmpty(userName))
                {
                    if (user != null)
                    {
                        var checkIn = await _unitOfWork.Favourite.FindAsync(user.Id, viewModel.Id);
                        result.IsFavourite = checkIn != null;
                        if (user.Id == bo.AmbassadorId)
                            result.IsOwner = true;
                    }
                }
                var mbassadorRequest = await _unitOfWork.AmbassadorRequest.FindAsyncByUserId(bo.AmbassadorId);
                if (mbassadorRequest != null)
                {
                    result.IsAmbassador = true;
                    result.AmbassadorRequestId = mbassadorRequest.Id;
                }
                #endregion

                #region Images

                var imgs =
                    await
                        _unitOfWork.PhotoRepository.FindAllAsync(viewModel.Id.ToString(),
                            RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");

                if (imgs.Count > 0)
                {
                    foreach (var image in imgs.Select(t => new CategoryDetailsImage
                    {
                        ImageUrl = t.PhotoUrl,
                        IsDefault = t.IsDefault
                    }))
                    {
                        result.Images.Add(image);
                    }
                }
                #endregion

                #region ProjectContact
                int typeProject = ContactTypeEnum.ProjectContact.GetHashCode();
                var contactProject = await _unitOfWork.Contact.FindAllAsync(viewModel.Id, typeProject);
                if (contactProject != null)
                {

                    foreach (var contactRecord in contactProject)
                    {
                        if (contactRecord.SoicalType == SoicalTypeEnum.PhoneNumber.ToString())
                            result.ProjectPhoneNumber = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Facebook.ToString())
                            result.ProjectFacebook = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Google.ToString())
                            result.ProjectGoogle = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.WhatsApp.ToString())
                            result.ProjectWhatsapp = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Twitter.ToString())
                            result.ProjectTwitter = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;



                    }
                }

                #endregion

                #region OwerContact
                int typeOwer = ContactTypeEnum.OwerContact.GetHashCode();
                var contactOwer = await _unitOfWork.Contact.FindAllAsync(viewModel.Id, typeOwer);
                if (contactOwer != null)
                {

                    foreach (var contactRecord in contactOwer)
                    {
                        if (contactRecord.SoicalType == SoicalTypeEnum.PhoneNumber.ToString())
                            result.OwnerPhoneNumber = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Facebook.ToString())
                            result.OwnerFacebook = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Google.ToString())
                            result.OwnerGoogle = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.WhatsApp.ToString())
                            result.OwnerWhatsapp = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;

                        else if (contactRecord.SoicalType == SoicalTypeEnum.Twitter.ToString())
                            result.OwnerTwitter = string.IsNullOrWhiteSpace(contactRecord.ContactInfo) ? null : contactRecord.ContactInfo;



                    }
                }

                #endregion


                #region View

                if (string.IsNullOrEmpty(viewModel.UserId))
                    viewModel.UserId = "0";

                var hit = new HitCountDetails()
                {

                    UserId = viewModel.UserId,
                    DeviceId = viewModel.DeviceId,
                    ModifiedDate = DateTime.Now,
                    Hits = 1,
                    RelatedId = viewModel.Id.ToString(),
                    RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),


                };
                await _unitOfWork.HitCountDetailsRepository.CreateAsync(hit);

                #endregion
                return Ok(result);


            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

    }
}