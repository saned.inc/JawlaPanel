﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Jawla.Api.Properties;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;
using Saned.Jawla.Data.Core.Enum;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/FeaturedProjects")]
    public class FeaturedProjectsController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public FeaturedProjectsController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }
        #region Admin_FeaturedProjects
        [Route("")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,ProjectViewAny")]
        public async Task<IHttpActionResult> FeaturedProjects(CategoryDetailsSearchViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                int? status = null;
                string ambassadorId = null;
                // bool viewByCreatorRole = IsInRole(u.Id, PremisionEnum.ProjectByCreator.ToString());
                bool viewAnyRole = IsInRole(u.Id, PremisionEnum.ProjectViewAny.ToString());
                bool administratorRole = IsInRole(u.Id, RolesEnum.Administrator.ToString());


                if (!administratorRole && !viewAnyRole)
                    ambassadorId = u.Id;



                var list = await _unitOfWork.CategoryDetails.FindAllAsync(model.PageNumber, model.PageSize, model.CountryId, model.CityId, model.MunicipalityId, 8, model.CategoryDetailsId, model.Longitude, model.Latitude, status, 6, model.EventStatyDate, ambassadorId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                {
                    msg = ex.InnerException.Message;

                }
                return BadRequest(msg);
            }

        }



        [HttpGet]
        [Route("{id:long}")]
        public async Task<IHttpActionResult> GetFeaturedProjectById(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = _unitOfWork.ProjectDetails.GetAsync().FirstOrDefault(u => u.ProjectId == id);
                if (bo == null)
                {
                    return NotFound();
                }
                FeatureProjectViewModel result = new FeatureProjectViewModel
                {

                    Latitude = bo.CategoryDetails.Latitude,
                    Longitude = bo.CategoryDetails.Longitude,
                    CountryId = bo.CategoryDetails.CountryId,
                    Name = bo.CategoryDetails.Name,
                    CityId = bo.CategoryDetails.CityId,
                    Description = bo.CategoryDetails.Description,
                    IsBin = bo.CategoryDetails.IsBin,
                    MunicipalityId = bo.CategoryDetails.MunicipalityId,
                    Owner = bo.Owner,
                    Id = bo.ProjectId,
                    Idea = bo.Idea,
                    CategoryId = bo.CategoryDetails.CategoryId,
                };

                List<CategoryDetailsContact> contacts = await _unitOfWork.Contact.FindAllAsync(id, 1);

                result.Contacts = contacts;


                List<CategoryDetailsContact> ownerContacts = await _unitOfWork.Contact.FindAllAsync(id, 2);
                result.OwnerContacts = ownerContacts;
                //PhoneNumber
                //    WhatsApp
                //    Facebook
                //    Twitter
                //    Google


                //result.Images
                string uploadPath = Settings.Default.UploadFullPath;

                List<Uploader> imagesList = new List<Uploader>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");
                foreach (var image in imgs)
                {
                    if (!image.IsDefault)
                    {
                        Uploader photo = new Uploader()
                        {
                            Url = uploadPath + image.PhotoUrl,
                            IsDefault = image.IsDefault,
                            Id = image.Id
                        };
                        imagesList.Add(photo);
                    }
                    else
                    {
                        result.AttachmentUrl = uploadPath + image.PhotoUrl;
                        result.CoverId = image.Id;
                    }
                }
                result.ImagesList = imagesList;

                return Ok(result);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetFeaturedProjectById " + msg);
            }
        }



        [HttpPost]
        [Route("{id:long}/images")]
        public async Task<IHttpActionResult> GetImagesByFeaturedProjectId(long id, Pager page)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                List<CategoryDetailsImage> images = new List<CategoryDetailsImage>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");

                foreach (var image in imgs.Select(t => new CategoryDetailsImage
                {
                    ImageUrl = t.PhotoUrl,
                    IsDefault = t.IsDefault
                }))
                {
                    images.Add(image);
                }

                return Ok(images);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetImagesByFeaturedProjectId " + msg);
            }
        }




        [HttpPost]
        [Route("Delete/{id:long}")]
        [Attributes.Authorize(Roles = "Administrator,ProjectDeleteAny")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var projects = _unitOfWork.ProjectDetails.GetFeatureProjectById(id);
                if (projects.CategoryDetails == null)
                {
                    return NotFound();
                }
                _unitOfWork.CategoryDetails.DeleteAsync(projects.CategoryDetails);
                await _unitOfWork.Complete();
                return Ok();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Delete " + msg);
            }
        }


        [Route("CreateFeaturedProject")]
        [Attributes.Authorize(Roles = "Administrator,ProjectAdd")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateFeaturedProject(FeatureProjectViewModel viewModel)
        {
            try
            {



                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);



                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;
                int status = CategoryDetailsStatusEnum.Approve.GetHashCode(); //IsInRole(u.Id, "ProjectAddedWithoutAdminApprove") || IsInRole(u.Id, "Administrator") ? CategoryDetailsStatusEnum.Approve.GetHashCode() : CategoryDetailsStatusEnum.New.GetHashCode();


                //  if (viewModel.Id == 0)
                {
                    var categoryDetails = new CategoryDetails()
                    {
                        CategoryId = viewModel.CategoryId,
                        Name = viewModel.Name,
                        CountryId = viewModel.CountryId,
                        CityId = viewModel.CityId,
                        MunicipalityId = viewModel.MunicipalityId,
                        Latitude = viewModel.Latitude,
                        Longitude = viewModel.Longitude,
                        IsBin = viewModel.IsBin,
                        Status = status.ToString(),
                        AmbassadorId = viewModel.AmbassadorId,
                        Description = viewModel.Description
                    };
                    var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);
                    //await _unitOfWork.Complete();


                    categoryDetails.Projects = new ProjectDetails()
                    {

                        Owner = viewModel.Owner,
                        Idea = viewModel.Idea

                    };
                    var res = await _unitOfWork.Complete();

                    viewModel.Contacts.ForEach(s => s.CategoryDetailsId = result.Id);

                    viewModel.OwnerContacts.ForEach(s => s.CategoryDetailsId = result.Id);
                    //add contacts

                    _unitOfWork.Contact.AddRange(viewModel.Contacts);

                    _unitOfWork.Contact.AddRange(viewModel.OwnerContacts);
                    await _unitOfWork.Complete();

                    if (viewModel.ImagesList != null)
                    {
                        foreach (var image in viewModel.ImagesList)
                        {
                            if (!string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                            {
                                var randomImage = SaveImage(image.Filename, image.Base64);
                                await CreatePhoto(new Photo()
                                {
                                    IsDefault = false,
                                    Lang = "ar",
                                    ModifiedDate = DateTime.Now,
                                    PhotoUrl = randomImage,
                                    RelatedId = result.Id.ToString(),
                                    RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                                    Title = viewModel.Name

                                });
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                    {
                        string defaultImge = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = true,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = defaultImge,
                            RelatedId = result.Id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = viewModel.Name

                        });

                    }

                    response = Ok(res);

                }
                //else
                //{
                //    var eventDetails = await (_unitOfWork.EventDetails.GetAsync(viewModel.Id));
                //    eventDetails.Update(viewModel.Owner, viewModel.EndDate);
                //    var result = await _unitOfWork.Complete();
                //    response = Ok(result);
                //}

                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }



        [Route("UpdateFeaturedProject")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,ProjectlEditAny")]
        public async Task<IHttpActionResult> UpdateFeaturedProject(FeatureProjectViewModel viewModel)
        {
            try
            {



                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);








                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;

                var selectedFeatureProject = _unitOfWork.ProjectDetails.GetFeatureProjectById(viewModel.Id);
                if (selectedFeatureProject == null)
                    return NotFound();




                bool editAnyRole = IsInRole(u.Id, PremisionEnum.ProjectlEditAny.ToString());
                // bool editByCreatorRole = IsInRole(u.Id, PremisionEnum.ProjectEditByCreator.ToString());
                bool administratorRole = IsInRole(u.Id, "Administrator");
                if (!administratorRole && !editAnyRole &&
                    u.Id == selectedFeatureProject.CategoryDetails.AmbassadorId)
                    return Unauthorized();

                #region SaveEventDetails

                selectedFeatureProject.CategoryDetails.CategoryId = viewModel.CategoryId;
                selectedFeatureProject.CategoryDetails.Name = viewModel.Name;
                selectedFeatureProject.CategoryDetails.CountryId = viewModel.CountryId;
                selectedFeatureProject.CategoryDetails.CityId = viewModel.CityId;
                selectedFeatureProject.CategoryDetails.MunicipalityId = viewModel.MunicipalityId;
                selectedFeatureProject.CategoryDetails.Latitude = viewModel.Latitude;
                selectedFeatureProject.CategoryDetails.Longitude = viewModel.Longitude;
                selectedFeatureProject.CategoryDetails.IsBin = viewModel.IsBin;
                //selectedFeatureProject.CategoryDetails.Status = viewModel.Status;
                selectedFeatureProject.CategoryDetails.AmbassadorId = viewModel.AmbassadorId;
                selectedFeatureProject.CategoryDetails.Description = viewModel.Description;
                selectedFeatureProject.Owner = viewModel.Owner;
                selectedFeatureProject.Idea = viewModel.Idea;
                var res = await _unitOfWork.Complete();
                #endregion

                #region SaveImages
                await UpdateFeatureProjectsImages(viewModel.ImagesList, viewModel.Id, viewModel.Name, viewModel.CoverId, viewModel.ImageFilename, viewModel.ImageBase64);


                #endregion


                #region SaveContacts

                _unitOfWork.Contact.RemoveRange(viewModel.Id);
                viewModel.Contacts.ForEach(s => s.CategoryDetailsId = viewModel.Id);
                _unitOfWork.Contact.AddRange(viewModel.Contacts);

                viewModel.OwnerContacts.ForEach(s => s.CategoryDetailsId = viewModel.Id);
                _unitOfWork.Contact.AddRange(viewModel.OwnerContacts);


                await _unitOfWork.Complete();


                #endregion


                response = Ok(res);
                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }




        [HttpPost]
        [Route("Approve/{id:long}")]
        public async Task<IHttpActionResult> Approve(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = await _unitOfWork.CategoryDetails.GetAsync(id);
                if (bo == null)
                {
                    return NotFound();
                }
                bo.Approve();
                await _unitOfWork.Complete();
                return Ok();

            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Approve" + msg);
            }
        }

        private async Task UpdateFeatureProjectsImages(List<Uploader> imagesList, long id, string name, int coverId, string imageFilename, string imageBase64)
        {

            if (coverId == 0)
            {
                _unitOfWork.PhotoRepository.DeleteMainImage(id);
            }

            if (!string.IsNullOrEmpty(imageFilename) && !string.IsNullOrEmpty(imageBase64))
            {
                string defaultImge = SaveImage(imageFilename, imageBase64);
                Photo selected = _unitOfWork.PhotoRepository.GetPhoto(coverId);

                if (selected != null)
                {
                    selected.IsDefault = true;
                    selected.Lang = "ar";
                    selected.ModifiedDate = DateTime.Now;
                    selected.PhotoUrl = defaultImge;
                    selected.RelatedId = id.ToString();
                    selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                    selected.Title = name;
                }
                else
                {
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = name

                    });
                }


                await _unitOfWork.Complete();

            }
            if (imagesList != null)
            {
                List<int> editedImages = imagesList.Select(uploader => uploader.Id).ToList();
                if (coverId != 0)
                {
                    editedImages.Add(coverId);
                }

                _unitOfWork.PhotoRepository.RemoveImages(editedImages, id);
                await _unitOfWork.Complete();


                foreach (var image in imagesList)
                {
                    if (image.Id == 0 && !string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = false,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = randomImage,
                            RelatedId = id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = name
                        });
                    }
                    else if (image.Id != 0 && !string.IsNullOrEmpty(image.Filename) &&
                             !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        Photo selected = _unitOfWork.PhotoRepository.GetPhoto(image.Id);
                        if (selected != null)
                        {
                            selected.IsDefault = false;
                            selected.Lang = "ar";
                            selected.ModifiedDate = DateTime.Now;
                            selected.PhotoUrl = randomImage;
                            selected.RelatedId = id.ToString();
                            selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                            selected.Title = name;
                            await _unitOfWork.Complete();
                        }

                    }
                }
            }
        }

        #endregion





    }

}