using System;
using System.Threading.Tasks;
using System.Web.Http;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;

namespace Saned.Jawla.Api.Controllers
{
    public class PhotoController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public PhotoController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddPhoto(Photo viewModel)
        {
            return await CreatePhoto(viewModel);
        }

    

        [HttpPost]
        public async Task<IHttpActionResult> ModifiyPhoto(Photo viewModel)
        {
            try
            {
                Photo bo = new Photo
                {
                    Id = viewModel.Id,
                    IsDefault = viewModel.IsDefault,
                    Lang = viewModel.Lang,
                    PhotoUrl = viewModel.PhotoUrl,
                    RelatedId = viewModel.RelatedId,
                    RelatedTypeId = viewModel.RelatedTypeId,
                    Title = viewModel.Title,
                };

                return Ok(await _unitOfWork.PhotoRepository.UpdateAsync(bo));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("ModifiyPhoto..." + msg);
            }


        }
        [HttpGet]
        public async Task<IHttpActionResult> GetPhoto(int id)
        {
            try
            {
                return Ok(await _unitOfWork.PhotoRepository.GetAsync(id));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("GetPhoto..." + msg);
            }

        }
        [HttpPost]
        public async Task<IHttpActionResult> GetPhotoList(PhotoParamViewModel viewModel)
        {
            try
            {
                return Ok(await _unitOfWork.PhotoRepository.FindAllAsync(viewModel.RelatedId, viewModel.RelatedType, viewModel.Lang));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("GetPhotoList..." + msg);
            }

        }
        [HttpPost]
        public async Task<IHttpActionResult> GetPhotoListByIndex(PhotoParamViewModel viewModel)
        {
            try
            {
                return Ok(await _unitOfWork.PhotoRepository.FindAllAsync(viewModel.RelatedId, viewModel.RelatedType, viewModel.PageIndex, viewModel.PageSize, viewModel.Lang));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("GetPhotoListByIndex..." + msg);
            }

        }
        [HttpPost]
        public async Task<IHttpActionResult> DeletePhoto(int id)
        {
            try
            {
                return Ok(await _unitOfWork.PhotoRepository.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("GetPhoto..." + msg);
            }

        }
        [HttpPost]
        public async Task<IHttpActionResult> DeletePhotoByRelatedId(PhotoParamViewModel viewModel)
        {
            try
            {
                return Ok(await _unitOfWork.PhotoRepository.DeleteAsyncByRelatedId(viewModel.RelatedId, viewModel.RelatedType));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("DeletePhotoByRelatedId..." + msg);
            }

        }

    }
}