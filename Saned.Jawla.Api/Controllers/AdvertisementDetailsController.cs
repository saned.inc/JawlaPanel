using System;
using System.Threading.Tasks;
using System.Web.Http;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Core.Security.Persistence.Infrastructure;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Data.Core.Enum;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Advertisement")]
    public class AdvertisementDetailsController : BasicController
    {

        private readonly IUnitOfWorkAsync _unitOfWork;

        public AdvertisementDetailsController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }

        [HttpPost]
        [Route("CreateAdvertisement")]
        [Attributes.Authorize(Roles = "Administrator,AdAdd,AdAddedWithoutAdminApprove")]
        public async Task<IHttpActionResult> CreateAdvertisement(AdvertisementViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                string randomImage = null;

                if (viewModel.AttachmentViewModel != null)
                {
                    if (!string.IsNullOrEmpty(viewModel.AttachmentViewModel.Filename) && !string.IsNullOrEmpty(viewModel.AttachmentViewModel.Base64))
                    {
                        randomImage = SaveImage(viewModel.AttachmentViewModel.Filename, viewModel.AttachmentViewModel.Base64);
                    }
                }


                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                _unitOfWork.Advertisement.CreateAsync(new Advertisement()
                {
                    IsDeleted = false,
                    PhotoUrl = randomImage,
                    IsShow = viewModel.IsShow,
                    Name = viewModel.Name,
                    SecondNumber = viewModel.SecondNumber,
                    Url = viewModel.Url,
                    AmbassadorId = u.Id,
                    ModifiedDate = DateTime.Now


                });
                return Ok(await _unitOfWork.Complete());
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [HttpPost]
        [Route("Update")]
        [Attributes.Authorize(Roles = "Administrator ,AdEditByCreator,AdEditAny")]
        public async Task<IHttpActionResult> UpdateAdvertisement(AdvertisementViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);

                if (viewModel.DeleteImage)
                {
                    await _unitOfWork.Advertisement.UpdateAsync(new Advertisement()
                    {
                        PhotoUrl = null,
                        IsShow = viewModel.IsShow,
                        Name = viewModel.Name,
                        SecondNumber = viewModel.SecondNumber,
                        Url = viewModel.Url,
                        AmbassadorId = u.Id,
                        ModifiedDate = DateTime.Now,
                        Id = viewModel.Id
                    });

                    await _unitOfWork.Complete();
                }

                string randomImage = null;

                if (viewModel.AttachmentViewModel != null && !string.IsNullOrEmpty(viewModel.AttachmentViewModel.Filename) && !string.IsNullOrEmpty(viewModel.AttachmentViewModel.Base64))
                {
                    randomImage = SaveImage(viewModel.AttachmentViewModel.Filename, viewModel.AttachmentViewModel.Base64);
                }

                await _unitOfWork.Advertisement.UpdateAsync(new Advertisement()
                {
                    PhotoUrl = randomImage,
                    IsShow = viewModel.IsShow,
                    Name = viewModel.Name,
                    SecondNumber = viewModel.SecondNumber,
                    Url = viewModel.Url,
                    AmbassadorId = u.Id,
                    ModifiedDate = DateTime.Now,
                    Id = viewModel.Id
                });

                return Ok(await _unitOfWork.Complete());
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [Route("GetAdvertisements")]
        [HttpPost]
        public async Task<IHttpActionResult> GetAdvertisements(AdvertisementSearcViewModel viewModel)

        {
            try
            {
                var lst = await _unitOfWork.Advertisement.FindAllAsync(viewModel.PageNumber, viewModel.PageSize);
                return Ok(lst);
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [Route("GetAdvertisementList")]
        [HttpPost]
        public async Task<IHttpActionResult> GetAdvertisementList()

        {
            try
            {
                var lst = await _unitOfWork.Advertisement.GetAsync();
                return Ok(lst);
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }





        #region Admin
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> SelectById(int id)
        {
            try
            {
                return Ok(await _unitOfWork.Advertisement.SelectById(id));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }
        [Attributes.Authorize(Roles = "Administrator,AdViewAny,AdByCreator")]
        [Route("AdminGetAdvertisements")]
        [HttpPost]
        public async Task<IHttpActionResult> AdminFindAllAsync(AdvertisementSearcViewModel viewModel)
        {
            try
            {
                var lst = await _unitOfWork.Advertisement.AdminFindAllAsync(viewModel.PageNumber, viewModel.PageSize, viewModel.Keyword);
                return Ok(lst);
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [Route("Delete/{id}")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,AdDeleteByCreator,AdDeleteAny")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            _unitOfWork.Advertisement.Delete(id);
            return Ok(await _unitOfWork.Complete());
        }
        #endregion 

    }
}