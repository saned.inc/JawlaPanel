using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Municipality")]
    public class MunicipalityController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public MunicipalityController()
        {

            _unitOfWork = new UnitOfWorkAsync();
        }

        [Attributes.Authorize(Roles = "Administrator")]
        [Route("CreateMunicipality")]
        public async Task<IHttpActionResult> CreateMunicipality(CountryViewModel viewModel)
        {
            try
            {

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                //if (string.IsNullOrEmpty(userName))
                //    return BadRequest("401");
                //else
                {
                    _unitOfWork.Municipality.CreateAsync(new Municipality()
                    {
                        ArabicName = viewModel.ArabicName,
                        EnglishName = viewModel.ArabicName,
                        Latitude = viewModel.Latitude,
                        Longitude = viewModel.Longitude,
                        CityId = viewModel.CityId,

                    });
                    var result = await _unitOfWork.Complete();

                    return Ok(result);
                }


            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("CreateMunicipality " + msg);
            }

        }

        [HttpGet]
        [Route("GetMunicipalities")]
        public async Task<IHttpActionResult> GetMunicipalities()
        {
            try
            {
                return Ok(await _unitOfWork.Municipality.GetAsync());
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetMunicipalities " + msg);
            }
        }

        [HttpPost]
        [Route("GetMunicipalitiesWithPaging")]

        public async Task<IHttpActionResult> GetMunicipalitiesWithPaging(PagingViewModel model)
        {
            try
            {
                var lst = await
               _unitOfWork.Municipality.GetAsyncWithPaging(model.PageNumber, model.PageSize);


                var totalCount = await
                    _unitOfWork.Municipality.GetAsync();

                var result = new
                {
                    TotalCount = totalCount.Count,
                    Data = lst
                };

                return Ok(result);

            }
            catch (Exception m)
            {

                return BadRequest("this is GETMunicipalityWithPaging ---" + m.GetaAllMessages());
            }

        }


        [HttpGet]
        [Route("GetMunicipalityByCityId/{cityId}")]

        public async Task<IHttpActionResult> GetMunicipalityByCityId(int cityId)
        {
            try
            {
                return Ok(await _unitOfWork.Municipality.GetMunicipalityByCityById(cityId));
            }
            catch (Exception msg)
            {

                return BadRequest("Error in GetMunicipalityByCityId ->>>>>>" + msg.GetaAllMessages());
            }

        }



        [HttpGet]
        [Route("FindMunicipalities/{id?}")]
        public async Task<IHttpActionResult> FindMunicipalities(int id)
        {
            try
            {
                return Ok(await _unitOfWork.Municipality.FindAsync(id));
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("FindMunicipalities " + msg);
            }
        }

        [HttpGet]
        [Route("GetById/{id}")]

        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var result = await _unitOfWork.Municipality.GetById(id);

                return Ok(new
                {
                    result.Id,
                    result.ArabicName,
                    result.CityId,
                    result.EnglishName,
                    result.IsDeleted,
                    result.Latitude,
                    result.Longitude,
                    CountryId = result.City != null ? result.City.CountryId : 0
                });
            }
            catch (Exception e)
            {

                return BadRequest(" Get Municipality By id ----" + e.GetaAllMessages());
            }


        }


        [HttpGet]
        [Route("DeleteById/{id}")]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> DeleteById(int id)
        {
            try
            {
                _unitOfWork.Municipality.DeleteById(id);
                await _unitOfWork.Complete();
                return Ok(await _unitOfWork.Municipality.GetAsync());
            }
            catch (Exception e)
            {

                return BadRequest("Delete Municpality By id " + e.GetaAllMessages());
            }



        }

        [HttpPost]
        [Route("UpdateMunicipality")]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> UpdateCountry(Municipality m)
        {

            var q = await _unitOfWork.Municipality.GetById(m.Id);
            if (q == null)
            {
                return BadRequest("Error id not exist ");
            }
            else
            {
                _unitOfWork.Municipality.Update(new Municipality
                {
                    Id = m.Id,
                    ArabicName = m.ArabicName,
                    EnglishName = m.EnglishName,
                    Longitude = m.Longitude,
                    Latitude = m.Latitude,
                    CityId = m.CityId
                });

            }
            return Ok(await _unitOfWork.Complete());
        }

    }
}