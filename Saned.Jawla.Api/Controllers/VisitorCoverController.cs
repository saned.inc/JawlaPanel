using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Enum;
using Saned.Modules.Photos;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/VisitorCover")]
    public class VisitorCoverController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public VisitorCoverController()
        {

            _unitOfWork = new UnitOfWorkAsync();
        }
        [Authorize]
        [Route("CreateVisitorCover")]
        public async Task<IHttpActionResult> CreateVisitorCover(VisitorCoverViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                #region CheckUser
                var userName = User.Identity.GetUserName();
                if (string.IsNullOrEmpty(userName))
                    return UserNotLogin();
                var findUser = await GetApplicationUser(userName);
                if (findUser == null)
                    return UserNotFound();
                if (findUser.IsDeleted == true)
                    return UserNotActive();
                #endregion

                viewModel.UserId = findUser.Id;
                var findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
                if (findCover == null)
                {
                    _unitOfWork.VisitorCover.CreateAsync(new VisitorCover()
                    {
                        UseId = viewModel.UserId,
                        CategoryDetailsId = viewModel.CategoryDetailsId,

                    });
                    var c = await _unitOfWork.Complete();
                    return Ok(c);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        //[Route("GetCategoryDetailsCover")]
        [Route("GetCoverList")]
        [HttpPost]
        public async Task<IHttpActionResult> GetCoverList(VisitorCoverListViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                #region CheckUser
                var userName = User.Identity.GetUserName();
                if (string.IsNullOrEmpty(userName))
                    return UserNotLogin();
                var findUser = await GetApplicationUser(userName);
                if (findUser == null)
                    return UserNotFound();
                if (findUser.IsDeleted == true)
                    return UserNotActive();
                #endregion

                viewModel.UserId = findUser.Id;

                var list =
                    await
                        _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.PageNumber, viewModel.PageSize);



                return Ok(list);
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                    msg = ex.InnerException.Message;
                return BadRequest(msg);
            }

        }

        [Route("GetCoverDetails")]
        [HttpPost]
        public async Task<IHttpActionResult> GetCoverDetails(VisitorCoverDetailsViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                #region CheckUser

                var userName = User.Identity.GetUserName();
                if (string.IsNullOrEmpty(userName))
                    return UserNotLogin();
                var findUser = await GetApplicationUser(userName);
                if (findUser == null)
                    return UserNotFound();
                if (findUser.IsDeleted == true)
                    return UserNotActive();
                #endregion

                viewModel.UserId = findUser.Id;

                var find = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.Id);
                var item =
                    await
                        _unitOfWork.VisitorCover.GetAsync(find.Id, viewModel.UserId);



                return Ok(item);
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                    msg = ex.InnerException.Message;
                return BadRequest(msg);
            }

        }
        [Authorize]
        [Route("CreateCoverPhotos")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateCoverPhotos(VisitorCoverViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                #region CheckUser

                var userName = User.Identity.GetUserName();
                if (string.IsNullOrEmpty(userName))
                    return UserNotLogin();
                var findUser = await GetApplicationUser(userName);
                if (findUser == null)
                    return UserNotFound();
                if (findUser.IsDeleted == true)
                    return UserNotActive();
                #endregion

                viewModel.UserId = findUser.Id;

                if (viewModel.Images == null)
                    return PhotoIsRequired();

                var findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
                if (findCover == null)
                {
                    _unitOfWork.VisitorCover.CreateAsync(new VisitorCover()
                    {
                        UseId = viewModel.UserId,
                        CategoryDetailsId = viewModel.CategoryDetailsId,

                    });
                    await _unitOfWork.Complete();

                }
                findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
                if (findCover == null)
                    return BadRequest();
                if (viewModel.Images != null)
                {
                    foreach (var image in viewModel.Images)
                    {
                        if (!string.IsNullOrEmpty(image))
                        {
                            var randomImage = SaveImage("Cover.JPG", image);
                            await CreatePhoto(new Photo()
                            {
                                IsDefault = false,
                                Lang = "ar",
                                ModifiedDate = DateTime.Now,
                                PhotoUrl = randomImage,
                                RelatedId = findCover.Id.ToString(),
                                RelatedTypeId = RelateTypeEnum.PictureVisitorCover.GetHashCode(),
                                Title = "userName",

                            });
                        }
                    }
                }
                return Ok();

            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        [Route("DeleteAllPhotoCover")]
        [HttpPost]
        public async Task<IHttpActionResult> DeleteAllPhotoCover(VisitorCoverViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            #region CheckUser
            var userName = User.Identity.GetUserName();
            if (string.IsNullOrEmpty(userName))
                return UserNotLogin();
            var findUser = await GetApplicationUser(userName);
            if (findUser == null)
                return UserNotFound();
            if (findUser.IsDeleted == true)
                return UserNotActive();
            #endregion

            viewModel.UserId = findUser.Id;

            var findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
            if (findCover == null)
                return VisitorCoverNotFound();

            return Ok(await _unitOfWork.PhotoRepository.DeleteAsyncByRelatedId(findCover.Id.ToString(), RelateTypeEnum.PictureVisitorCover.GetHashCode()));
        }

        [Authorize]
        [Route("DeletePhotoCoverByIds")]
        [HttpPost]
        public async Task<IHttpActionResult> DeletePhotoCoverByIds(CoverPhotoViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            #region CheckUser
            var userName = User.Identity.GetUserName();
            if (string.IsNullOrEmpty(userName))
                return UserNotLogin();
            var findUser = await GetApplicationUser(userName);
            if (findUser == null)
                return UserNotFound();
            if (findUser.IsDeleted == true)
                return UserNotActive();
            #endregion

            viewModel.UserId = findUser.Id;

            var findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
            if (findCover == null)
                return VisitorCoverNotFound();
            var arry = viewModel.Ids.Split(',');
            for (int i = 0; i < arry.Length; i++)
            {
                int photoid = int.Parse(arry[i]);
                await _unitOfWork.PhotoRepository.DeleteAsync(photoid);
            }

            return Ok();
        }

        //[Route("ViewAllCoverPhotoCover")]
        [Route("ViewAllPhotoCover")]
        [HttpPost]
        public async Task<IHttpActionResult> ViewAllCoverPhotoCover(VisitorCoverViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #region CheckUser
            var userName = User.Identity.GetUserName();
            if (string.IsNullOrEmpty(userName))
                return UserNotLogin();
            var findUser = await GetApplicationUser(userName);
            if (findUser == null)
                return UserNotFound();
            if (findUser.IsDeleted == true)
                return UserNotActive();
            #endregion

            viewModel.UserId = findUser.Id;

            var findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
            if (findCover == null)
                return VisitorCoverNotFound();
            return Ok(await _unitOfWork.PhotoRepository.FindAllAsync(findCover.Id.ToString(), RelateTypeEnum.PictureVisitorCover.GetHashCode(), "ar"));

        }

        [Route("DeletePhoto")]
        [HttpPost]
        public async Task<IHttpActionResult> DeletePhoto(VisitorCoverPhotoViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            #region CheckUser
            var userName = User.Identity.GetUserName();
            if (string.IsNullOrEmpty(userName))
                return UserNotLogin();
            var findUser = await GetApplicationUser(userName);
            if (findUser == null)
                return UserNotFound();
            if (findUser.IsDeleted == true)
                return UserNotActive();
            #endregion
            viewModel.UserId = findUser.Id;
            var findCover = await _unitOfWork.VisitorCover.GetAsync(viewModel.UserId, viewModel.CategoryDetailsId);
            if (findCover == null)
                return VisitorCoverNotFound();

            return Ok(await _unitOfWork.PhotoRepository.DeleteAsync(viewModel.PhotoId));
        }

        [Route("ViewPhotos")]
        [HttpPost]
        public async Task<IHttpActionResult> ViewCoverPhotoByCategoryDetailsId(VisitorCoverViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            return Ok(await _unitOfWork.VisitorCover.FindAllAsync(viewModel.CategoryDetailsId));

        }


        [Route("VistorCoverId/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetVistorCoverId(long id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            return Ok(await _unitOfWork.VisitorCover.GetVistorCoverId(id));

        }

        private IHttpActionResult PhotoIsRequired()
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "The Images field is required.");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        private IHttpActionResult VisitorCoverNotFound()
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "Visitor Cover Not Found");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }

        [HttpGet]
        [Route("GetVisitorCoversByCategoryDetailsId/{categoryDetailId}")]
        public IHttpActionResult GetVisitorCoversByCategoryDetailsId(int categoryDetailId)
        {
            var result = _unitOfWork.UserRepository.GetAll(categoryDetailId);
            return Ok(result);
        }
    }
}