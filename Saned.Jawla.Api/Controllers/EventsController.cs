﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Jawla.Api.Properties;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;
using Saned.Jawla.Data.Core.Enum;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Events")]
    public class EventsController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public EventsController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }
        #region Admin_Events
        [Route("")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,EventViewAny")]
        public async Task<IHttpActionResult> Events(CategoryDetailsSearchViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                int? status = null;
                string ambassadorId = null;
                //    bool viewByCreatorRole = IsInRole(u.Id, PremisionEnum.EventViewByCreator.ToString());
                bool viewAnyRole = IsInRole(u.Id, PremisionEnum.EventViewAny.ToString());
                bool administratorRole = IsInRole(u.Id, RolesEnum.Administrator.ToString());
                if (!administratorRole && !viewAnyRole)
                    ambassadorId = u.Id;


                var list = await _unitOfWork.CategoryDetails.FindAllAsync(model.PageNumber, model.PageSize, model.CountryId, model.CityId, model.MunicipalityId, 7, model.CategoryDetailsId, model.Longitude, model.Latitude, status, 5, model.EventStatyDate, ambassadorId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                {
                    msg = ex.InnerException.Message;

                }
                return BadRequest(msg);
            }

        }



        [HttpGet]
        [Route("{id:long}")]
        public async Task<IHttpActionResult> GetEventById(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = _unitOfWork.EventDetails.GetAsync().FirstOrDefault(u => u.EventId == id);
                if (bo == null)
                {
                    return NotFound();
                }
                EventViewModel result = new EventViewModel
                {
                    StartTime = bo.StartTime,
                    EndTime = bo.EndTime,
                    Latitude = bo.CategoryDetails.Latitude,
                    Longitude = bo.CategoryDetails.Longitude,
                    CountryId = bo.CategoryDetails.CountryId,
                    Name = bo.CategoryDetails.Name,
                    CityId = bo.CategoryDetails.CityId,
                    Description = bo.CategoryDetails.Description,
                    EndDate = bo.EndDate,
                    Id = bo.EventId,
                    EventStartDate = bo.CategoryDetails.EventStartDate.Value,
                    IsBin = bo.CategoryDetails.IsBin,
                    MunicipalityId = bo.CategoryDetails.MunicipalityId,
                    Owner = bo.Owner,
                    CategoryId = bo.CategoryDetails.CategoryId
                };

                List<CategoryDetailsContact> contacts = await _unitOfWork.Contact.FindAllAsync(id, 1);

                result.Contacts = contacts;
                //PhoneNumber
                //    WhatsApp
                //    Facebook
                //    Twitter
                //    Google


                //result.Images
                string uploadPath = Settings.Default.UploadFullPath;

                List<Uploader> imagesList = new List<Uploader>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");
                foreach (var image in imgs)
                {
                    if (!image.IsDefault)
                    {
                        Uploader photo = new Uploader()
                        {
                            Url = uploadPath + image.PhotoUrl,
                            IsDefault = image.IsDefault,
                            Id = image.Id
                        };
                        imagesList.Add(photo);
                    }
                    else
                    {
                        result.AttachmentUrl = uploadPath + image.PhotoUrl;
                        result.CoverId = image.Id;
                    }
                }
                result.ImagesList = imagesList;

                return Ok(result);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetEventById " + msg);
            }
        }



        [HttpPost]
        [Route("{id:long}/images")]
        public async Task<IHttpActionResult> GetImagesByEventId(long id, Pager page)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                List<CategoryDetailsImage> images = new List<CategoryDetailsImage>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");

                foreach (var image in imgs.Select(t => new CategoryDetailsImage
                {
                    ImageUrl = t.PhotoUrl,
                    IsDefault = t.IsDefault
                }))
                {
                    images.Add(image);
                }

                return Ok(images);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetImagesByEventId " + msg);
            }
        }




        [HttpPost]
        [Route("Delete/{id:long}")]
        [Attributes.Authorize(Roles = "Administrator,EventDeleteAny")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var events = _unitOfWork.EventDetails.GetEventById(id);
                if (events.CategoryDetails == null)
                {
                    return NotFound();
                }
                _unitOfWork.CategoryDetails.DeleteAsync(events.CategoryDetails);
                //_unitOfWork.EventDetails.DeleteAsync(events);
                await _unitOfWork.Complete();
                return Ok();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Delete " + msg);
            }
        }


        [Route("CreateEvent")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,EventAdd")]
        public async Task<IHttpActionResult> CreateEvent(EventViewModel viewModel)
        {
            try
            {



                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);



                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;
                int status = CategoryDetailsStatusEnum.Approve.GetHashCode();//IsInRole(u.Id, "EventAddedWithoutAdminApprove") || IsInRole(u.Id, "Administrator") ? CategoryDetailsStatusEnum.Approve.GetHashCode() : CategoryDetailsStatusEnum.New.GetHashCode();


                //  if (viewModel.Id == 0)
                {
                    var categoryDetails = new CategoryDetails()
                    {
                        CategoryId = viewModel.CategoryId,
                        Name = viewModel.Name,
                        CountryId = viewModel.CountryId,
                        CityId = viewModel.CityId,
                        MunicipalityId = viewModel.MunicipalityId,
                        Latitude = viewModel.Latitude,
                        Longitude = viewModel.Longitude,
                        IsBin = viewModel.IsBin,
                        Status = status.ToString(),
                        AmbassadorId = viewModel.AmbassadorId,
                        EventStartDate = viewModel.EventStartDate,
                        Description = viewModel.Description
                    };
                    var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);
                    //await _unitOfWork.Complete();


                    categoryDetails.Events = new EventDetails()
                    {

                        Owner = viewModel.Owner,
                        EndDate = viewModel.EndDate,
                        StartTime = viewModel.StartTime,
                        EndTime = viewModel.EndTime,
                    };
                    var res = await _unitOfWork.Complete();

                    viewModel.Contacts.ForEach(s => s.CategoryDetailsId = result.Id);
                    //add contacts

                    _unitOfWork.Contact.AddRange(viewModel.Contacts);
                    await _unitOfWork.Complete();

                    if (viewModel.ImagesList != null)
                    {
                        foreach (var image in viewModel.ImagesList)
                        {
                            if (!string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                            {
                                var randomImage = SaveImage(image.Filename, image.Base64);
                                await CreatePhoto(new Photo()
                                {
                                    IsDefault = false,
                                    Lang = "ar",
                                    ModifiedDate = DateTime.Now,
                                    PhotoUrl = randomImage,
                                    RelatedId = result.Id.ToString(),
                                    RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                                    Title = viewModel.Name

                                });
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                    {
                        string defaultImge = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = true,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = defaultImge,
                            RelatedId = result.Id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = viewModel.Name

                        });

                    }

                    response = Ok(res);

                }
                //else
                //{
                //    var eventDetails = await (_unitOfWork.EventDetails.GetAsync(viewModel.Id));
                //    eventDetails.Update(viewModel.Owner, viewModel.EndDate);
                //    var result = await _unitOfWork.Complete();
                //    response = Ok(result);
                //}

                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }



        [Route("UpdateEvent")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,EventEditAny")]
        public async Task<IHttpActionResult> UpdateEvent(EventViewModel viewModel)
        {
            try
            {



                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);



                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;

                var selectedEvent = _unitOfWork.EventDetails.GetEventById(viewModel.Id);
                if (selectedEvent == null)
                    return NotFound();





                bool editAnyRole = IsInRole(u.Id, PremisionEnum.EventEditAny.ToString());
                //bool editByCreatorRole = IsInRole(u.Id, PremisionEnum.EventEditByCreator.ToString());
                bool administratorRole = IsInRole(u.Id, "Administrator");


                if (!administratorRole && !editAnyRole &&
                    u.Id == selectedEvent.CategoryDetails.AmbassadorId)
                    return Unauthorized();

                #region SaveEventDetails

                selectedEvent.CategoryDetails.CategoryId = viewModel.CategoryId;
                selectedEvent.CategoryDetails.Name = viewModel.Name;
                selectedEvent.CategoryDetails.CountryId = viewModel.CountryId;
                selectedEvent.CategoryDetails.CityId = viewModel.CityId;
                selectedEvent.CategoryDetails.MunicipalityId = viewModel.MunicipalityId;
                selectedEvent.CategoryDetails.Latitude = viewModel.Latitude;
                selectedEvent.CategoryDetails.Longitude = viewModel.Longitude;
                selectedEvent.CategoryDetails.IsBin = viewModel.IsBin;
                //selectedEvent.CategoryDetails.Status = viewModel.Status;
                selectedEvent.CategoryDetails.AmbassadorId = viewModel.AmbassadorId;
                selectedEvent.CategoryDetails.EventStartDate = viewModel.EventStartDate;
                selectedEvent.CategoryDetails.Description = viewModel.Description;
                selectedEvent.EndDate = viewModel.EndDate;
                selectedEvent.StartTime = viewModel.StartTime;
                selectedEvent.EndTime = viewModel.EndTime;
                selectedEvent.Owner = viewModel.Owner;
                var res = await _unitOfWork.Complete();
                #endregion

                #region SaveImages
                await UpdateEventImages(viewModel.ImagesList, viewModel.Id, viewModel.Name, viewModel.CoverId, viewModel.ImageFilename, viewModel.ImageBase64);


                #endregion


                #region SaveContacts

                _unitOfWork.Contact.RemoveRange(viewModel.CategoryId);
                viewModel.Contacts.ForEach(s => s.CategoryDetailsId = viewModel.Id);
                _unitOfWork.Contact.AddRange(viewModel.Contacts);
                await _unitOfWork.Complete();

                #endregion


                response = Ok(res);
                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }

        private async Task UpdateEventImages(List<Uploader> imagesList, long id, string name, int coverId, string imageFilename, string imageBase64)
        {
            if (coverId == 0)
            {
                _unitOfWork.PhotoRepository.DeleteMainImage(id);
            }


            if (!string.IsNullOrEmpty(imageFilename) && !string.IsNullOrEmpty(imageBase64))
            {
                string defaultImge = SaveImage(imageFilename, imageBase64);

                Photo selected = _unitOfWork.PhotoRepository.GetPhoto(coverId);
                if (selected != null)
                {
                    selected.IsDefault = true;
                    selected.Lang = "ar";
                    selected.ModifiedDate = DateTime.Now;
                    selected.PhotoUrl = defaultImge;
                    selected.RelatedId = id.ToString();
                    selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                    selected.Title = name;
                    await _unitOfWork.Complete();
                }
                else
                {
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = name

                    });
                }
            }
            if (imagesList != null)
            {
                List<int> editedImages = imagesList.Select(uploader => uploader.Id).ToList();
                if (coverId != 0)
                {
                    editedImages.Add(coverId);
                }

                _unitOfWork.PhotoRepository.RemoveImages(editedImages, id);
                await _unitOfWork.Complete();


                foreach (var image in imagesList)
                {
                    if (image.Id == 0 && !string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = false,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = randomImage,
                            RelatedId = id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = name
                        });
                    }
                    else if (image.Id != 0 && !string.IsNullOrEmpty(image.Filename) &&
                             !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        Photo selected = _unitOfWork.PhotoRepository.GetPhoto(image.Id);
                        if (selected != null)
                        {
                            selected.IsDefault = false;
                            selected.Lang = "ar";
                            selected.ModifiedDate = DateTime.Now;
                            selected.PhotoUrl = randomImage;
                            selected.RelatedId = id.ToString();
                            selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                            selected.Title = name;
                            await _unitOfWork.Complete();
                        }

                    }
                }
            }
        }





        [HttpPost]
        [Route("Approve/{id:long}")]
        public async Task<IHttpActionResult> Approve(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = await _unitOfWork.CategoryDetails.GetAsync(id);
                if (bo == null)
                {
                    return NotFound();
                }
                bo.Approve();
                await _unitOfWork.Complete();
                return Ok();

            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Approve" + msg);
            }
        }
        #endregion





    }

    public class CategoryImage

    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsDefault { get; set; }
    }
}