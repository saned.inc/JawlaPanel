using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;

namespace Saned.Jawla.Api.Controllers
{
    public class BasicController : ApiController
    {

        protected readonly AuthRepository _repo = null;
        protected readonly IUnitOfWorkAsync _unitOfWork;

        public BasicController()
        {

            _repo = new AuthRepository();
            _unitOfWork = new UnitOfWorkAsync();
        }
        public async Task<ApplicationUser> GetApplicationUser(string userName)
        {
            return await _repo.FindUserByUserName(userName);
        }
        public async Task<ApplicationUser> GetApplicationUserById(string userId)
        {
            return await _repo.FindUserById(userId);
        }
        public IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        public async Task<IdentityResult> CreateUser(string name, string phoneNumber, string email, string userName, string password, int age, string solicalLink, string address, string photoUrl)
        {
            return await _repo.CreateUser(name, phoneNumber, email, userName, password, age, solicalLink, address, photoUrl);
        }
        public async Task<IdentityResult> UpdateUser(string userId, string name, string phoneNumber, string email, int age, string solicalLink, string address, string photoUrl)
        {
            return await _repo.UpdateUser(userId, name, phoneNumber, email, age, solicalLink, address, photoUrl );
        }
        public bool IsInRole(string userId, string role)
        {
            return _repo.IsInRole(userId, role);
        }
        public async Task<bool> IsRoleExit(string role)
        {
            int i = await _repo.IsRoleExit(role);
            return i != 0;
        }


        public async Task AddRoleToUser(string userId, string role)
        {
            var check = IsInRole(userId, role);
            if (check == false)
                await _repo.AddRoleToUser(userId, role);
        }
        public async Task<IdentityResult> UserValidate(string email, string phoneNumber, string userId = "")
        {
            return await _repo.ValidateAsync(email, phoneNumber, userId);
        }
        public async Task<IdentityResult> RemoveFromRole(string userId, string role)
        {
            return await _repo.RemoveFromRole(userId, role);
        }
        public async Task<IdentityResult> RemoveFromAllRoles(string userId)
        {
            var list = await _repo.GetUserRoles(userId);
            string[] strings = new string[list.Count];
            list.CopyTo(strings, 0);
            return await _repo.RemoveFromRoles(userId, strings);
        }
        public async Task<IdentityResult> RemoveFromAllRolesButUser(string userId)
        {
            var list = await _repo.GetUserRoles(userId);

            foreach (string s in list.Where(s => s == RolesEnum.User.ToString()))
            {
                list.Remove(s);
                break;
            }
            string[] strings = new string[list.Count];
            list.CopyTo(strings, 0);
            return await _repo.RemoveFromRoles(userId, strings);
        }
        public async Task<IList<string>> GetUserRoles(string userId)
        {
            return await _repo.GetUserRoles(userId);
        }
        protected string SaveImage(string imageFileName, string imageBase64)
        {
            // create random guid to represent image name
            var randomImage = Guid.NewGuid().ToString() + Path.GetExtension(imageFileName);

            string slogn = "/Uploads/" + randomImage;

            string filePath = (HostingEnvironment.MapPath($"~{slogn}"));

            SaveImageInFileSystem(imageBase64, filePath);

            return randomImage;
        }
        protected void SaveImageInFileSystem(string base64, string filePath)
        {
            if (base64 == null)
                return;
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(filePath, bytes);
        }

        public async Task<IHttpActionResult> CreatePhoto(Photo viewModel)
        {
            try
            {
                Photo bo = new Photo
                {
                    IsDefault = viewModel.IsDefault,
                    Lang = viewModel.Lang,
                    PhotoUrl = viewModel.PhotoUrl,
                    RelatedId = viewModel.RelatedId,
                    RelatedTypeId = viewModel.RelatedTypeId,
                    Title = viewModel.Title,
                };
                return Ok(await _unitOfWork.PhotoRepository.CreateAsync(bo));
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest("CreatePhoto..." + msg);
            }
        }


        public IdentityResult ValidateMessage(List<string> errors, string errorMsg)
        {

            errors.Add(errorMsg);
            return errors.Any()
              ? IdentityResult.Failed(errors.ToArray())
              : IdentityResult.Success;
        }

        #region ErrorMethod
        public IHttpActionResult UserNotLogin()
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "User Not Login");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        public IHttpActionResult UserNotFound()
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "User Not Found");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        public IHttpActionResult UserNotActive()
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "User Not Active");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        public IHttpActionResult RoleNotFound()
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "Role Not Found");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        public IHttpActionResult ValidateZeroOrEmpty(string paramterName)
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, paramterName + " Is Required.");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        #endregion
    }
}