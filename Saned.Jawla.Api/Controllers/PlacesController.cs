﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Jawla.Api.Properties;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using Saned.Modules.Photos;
using Saned.Jawla.Data.Core.Enum;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Places")]
    public class PlacesController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public PlacesController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }

        [Route("")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,HistoricPlaceViewAny,TourPlaceViewAny")]
        public async Task<IHttpActionResult> Places(CategoryDetailsSearchViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                int? status = null;
                string ambassadorId = null;
                // bool viewByCreatorRole = IsInRole(u.Id, PremisionEnum.HistoricPlaceViewByCreator.ToString());
                bool viewAnyRole = IsInRole(u.Id, PremisionEnum.HistoricPlaceViewAny.ToString());
                //bool tourviewByCreatorRole = IsInRole(u.Id, PremisionEnum.TourPlaceViewByCreator.ToString());
                bool tourviewAnyRole = IsInRole(u.Id, PremisionEnum.TourPlaceViewAny.ToString());
                bool administratorRole = IsInRole(u.Id, RolesEnum.Administrator.ToString());
                if ((!administratorRole && !viewAnyRole) || (!administratorRole && !tourviewAnyRole))
                {
                    ambassadorId = u.Id;

                }
                var list = await _unitOfWork.CategoryDetails.FindAllAsync(model.PageNumber, model.PageSize, model.CountryId, model.CityId, model.MunicipalityId, model.CategoryId, model.CategoryDetailsId, model.Longitude, model.Latitude, status, model.ParentCategoryId, model.EventStatyDate, ambassadorId);
                return Ok(list);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                if (ex.InnerException != null)
                {
                    msg = ex.InnerException.Message;

                }
                return BadRequest(msg);
            }

        }



        [HttpGet]
        [Route("{id:long}")]
        //[Attributes.Authorize(Roles = "Administrator,HistoricPlaceViewAny,TourPlaceViewAny")]
        public async Task<IHttpActionResult> GetPlacesById(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = await _unitOfWork.CategoryDetails.GetById(id);

                //  var bo = _unitOfWork.PlaceDetails.GetAsync().FirstOrDefault(u=>u.HotelId==id);
                if (bo == null)
                {
                    return NotFound();
                }
                PlaceViewModel result = new PlaceViewModel
                {

                    // Latitude = bo.Latitude,
                    // Longitude = bo.Longitude,
                    // CountryId = bo.CountryId,
                    // Name = bo.Name,
                    // CityId = bo.CityId,
                    // Description = bo.Description,
                    //// Id = bo.HotelId,                   
                    // IsBin = bo.IsBin,
                    // MunicipalityId = bo.MunicipalityId,
                    // WorkHours = bo.Hotels.WorkHours
                    Id = bo.Id,
                    Name = bo.Name,
                    CountryId = bo.CountryId,
                    CityId = bo.CityId,
                    MunicipalityId = bo.MunicipalityId,
                    Latitude = bo.Latitude,
                    Longitude = bo.Longitude,
                    IsBin = bo.IsBin,
                    Status = bo.Status,
                    AmbassadorId = bo.AmbassadorId,
                    Description = bo.Description,
                    WorkHours = bo.Hotels.WorkHours,
                    CategoryId = bo.CategoryId


                };

                #region QuestionsVsAnswers

                var questions = await _unitOfWork.Faq.GetAllAsync(id);
                if (questions != null)
                {
                    result.Questions = Mapper.Map<List<CategoryDetailsFaq>, List<QuestionViewModel>>(questions);
                }

                #endregion

                //result.Images
                string uploadPath = Settings.Default.UploadFullPath;

                List<Uploader> imagesList = new List<Uploader>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");
                foreach (var image in imgs)
                {
                    if (!image.IsDefault)
                    {
                        Uploader photo = new Uploader()
                        {
                            Url = uploadPath + image.PhotoUrl,
                            IsDefault = image.IsDefault,
                            Id = image.Id
                        };
                        imagesList.Add(photo);
                    }
                    else
                    {
                        result.AttachmentUrl = uploadPath + image.PhotoUrl;
                        result.CoverId = image.Id;
                    }
                }
                result.ImagesList = imagesList;

                List<CategoryDetailsContact> contacts = await _unitOfWork.Contact.FindAllAsync(id, 1);

                //result.Contacts = contacts;
                foreach (var con in contacts)
                {
                    if (con.SoicalType == SoicalTypeEnum.PhoneNumber.ToString())
                        result.ProjectPhoneNumber = con.ContactInfo;
                    if (con.SoicalType == SoicalTypeEnum.Twitter.ToString())
                        result.ProjectTwitter = con.ContactInfo;
                    if (con.SoicalType == SoicalTypeEnum.Facebook.ToString())
                        result.ProjectFacebook = con.ContactInfo;
                    if (con.SoicalType == SoicalTypeEnum.WhatsApp.ToString())
                        result.ProjectWhatsapp = con.ContactInfo;
                    if (con.SoicalType == SoicalTypeEnum.Google.ToString())
                        result.ProjectGoogle = con.ContactInfo;

                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetPlacesById " + msg);
            }
        }



        [HttpPost]
        [Route("{id:long}/images")]
        public async Task<IHttpActionResult> GetImagesByPlacesId(long id, Pager page)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                List<CategoryDetailsImage> images = new List<CategoryDetailsImage>();
                var imgs = await _unitOfWork.PhotoRepository.FindAllAsync(id.ToString(), RelateTypeEnum.CategoryDetails.GetHashCode(), "ar");


                foreach (var image in imgs.Select(t => new CategoryDetailsImage
                {
                    ImageUrl = t.PhotoUrl,
                    IsDefault = t.IsDefault
                }))
                {
                    images.Add(image);
                }

                return Ok(images);
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("GetImagesByPlaceId " + msg);
            }
        }





        [HttpPost]
        [Route("Delete/{id:long}")]
        [Attributes.Authorize(Roles = "Administrator,HistoricPlaceDeleteAny,TourPlaceDeleteAny")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var place = _unitOfWork.PlaceDetails.GetPlaceById(id);
                if (place == null)
                {
                    return NotFound();
                }

                //_unitOfWork.PlaceDetails.DeleteAsync(place);
                _unitOfWork.CategoryDetails.DeleteAsync(place.CategoryDetails);
                await _unitOfWork.Complete();
                return Ok();
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Delete " + msg);
            }
        }


        [Route("CreatePlaces")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,HistoricPlaceAdd,TourPlaceAdd")]

        public async Task<IHttpActionResult> CreatePlace(PlaceViewModel viewModel)
        {

            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var userName = User.Identity.GetUserName();
                var u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;

                int status = CategoryDetailsStatusEnum.Approve.GetHashCode();//(IsInRole(u.Id, "HistoricPlaceAddedWithoutAdminApprove") || IsInRole(u.Id, "TourPlaceAddedWithoutAdminApprove") || IsInRole(u.Id, "Administrator")) ? CategoryDetailsStatusEnum.Approve.GetHashCode() : CategoryDetailsStatusEnum.New.GetHashCode();

                var categoryDetails = new CategoryDetails()
                {
                    CategoryId = viewModel.CategoryId,
                    Name = viewModel.Name,
                    CountryId = viewModel.CountryId,
                    CityId = viewModel.CityId,
                    MunicipalityId = viewModel.MunicipalityId,
                    Latitude = viewModel.Latitude,
                    Longitude = viewModel.Longitude,
                    IsBin = viewModel.IsBin,
                    Status = status.ToString(),
                    AmbassadorId = viewModel.AmbassadorId,
                    Description = viewModel.Description
                };
                if (viewModel.Questions.Count > 0)
                {
                    categoryDetails.CategoryDetailsFaqs = viewModel.Questions
                                                      .Select(a => new CategoryDetailsFaq
                                                      {
                                                          Answer = a.Answer,
                                                          Question = a.Question,
                                                      }).ToList();
                }
                var result = _unitOfWork.CategoryDetails.CreateAsync(categoryDetails);

                categoryDetails.Hotels = new HotelsDetails()
                {
                    WorkHours = viewModel.WorkHours

                };

                #region Contact


                categoryDetails.CategoryDetailsContacts = new List<CategoryDetailsContact>();
                if (viewModel.ProjectPhoneNumber != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.ProjectWhatsapp != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()

                    });
                if (viewModel.ProjectFacebook != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.ProjectTwitter != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.ProjectGoogle != null)
                    categoryDetails.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = result.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion

                IHttpActionResult response = Ok(await _unitOfWork.Complete());

                #region Image List

                if (viewModel.ImagesList != null)
                {
                    foreach (var image in viewModel.ImagesList)
                    {
                        if (!string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                        {
                            var randomImage = SaveImage(image.Filename, image.Base64);
                            await CreatePhoto(new Photo()
                            {
                                IsDefault = false,
                                Lang = "ar",
                                ModifiedDate = DateTime.Now,
                                PhotoUrl = randomImage,
                                RelatedId = result.Id.ToString(),
                                RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                                Title = viewModel.Name

                            });
                        }
                    }
                }

                #endregion

                #region DefaultImage

                if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                {
                    string defaultImge = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = result.Id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = viewModel.Name

                    });

                }

                #endregion





                return response;
            }
            catch (Exception ex)
            {
                var msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }
        [Route("UpdatePlaces")]
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator,HistoricPlaceEditAny,TourPlaceEditAny")]
        public async Task<IHttpActionResult> UpdatePlaces(PlaceViewModel viewModel)
        {
            try
            {



                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);



                string userName = User.Identity.GetUserName();
                ApplicationUser u = await GetApplicationUser(userName);
                viewModel.AmbassadorId = u.Id;

                // var selectedPlace = _unitOfWork.PlaceDetails.GetPlaceById(viewModel.Id);
                var selectedPlace = await _unitOfWork.CategoryDetails.GetById(viewModel.Id);

                if (selectedPlace == null)
                    return NotFound();

                #region SaveplaceDetails

                selectedPlace.CategoryId = selectedPlace.CategoryId;
                selectedPlace.Name = viewModel.Name;
                selectedPlace.CountryId = viewModel.CountryId;
                selectedPlace.CityId = viewModel.CityId;
                selectedPlace.MunicipalityId = viewModel.MunicipalityId;
                selectedPlace.Latitude = viewModel.Latitude;
                selectedPlace.Longitude = viewModel.Longitude;
                selectedPlace.IsBin = viewModel.IsBin;
                // selectedPlace.Status = CategoryDetailsStatusEnum.Approve.GetHashCode().ToString();
                selectedPlace.AmbassadorId = viewModel.AmbassadorId;
                selectedPlace.Description = viewModel.Description;
                selectedPlace.Hotels.WorkHours = viewModel.WorkHours;


                if (viewModel.Questions.Count > 0)
                {
                    _unitOfWork.Faq.RemoveRange(selectedPlace.CategoryDetailsFaqs);
                    selectedPlace.CategoryDetailsFaqs = viewModel.Questions
                                                      .Select(a => new CategoryDetailsFaq
                                                      {
                                                          Answer = a.Answer,
                                                          Question = a.Question,
                                                      }).ToList();
                }

                //selectedPlace.Hotels = new HotelsDetails()
                //{
                //    WorkHours = viewModel.WorkHours

                //};

                var res = await _unitOfWork.Complete();
                #endregion

                #region SaveImages
                await UpdatePlaceImages(viewModel.ImagesList, viewModel.Id, viewModel.Name, viewModel.CoverId, viewModel.ImageFilename, viewModel.ImageBase64);


                #endregion


                #region Contact
                _unitOfWork.Contact.RemoveRange(viewModel.Id);
                if (viewModel.ProjectPhoneNumber != null)
                    selectedPlace.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectPhoneNumber,
                        Icon = "ionicons ion-android-call",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.PhoneNumber.ToString()
                    });
                if (viewModel.ProjectWhatsapp != null)
                    selectedPlace.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectWhatsapp,
                        Icon = "ionicons ion-social-whatsapp",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.WhatsApp.ToString()
                    });
                if (viewModel.ProjectFacebook != null)
                    selectedPlace.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectFacebook,
                        Icon = "ionicons ion-social-facebook",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Facebook.ToString()
                    });
                if (viewModel.ProjectTwitter != null)
                    selectedPlace.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {
                        ContactInfo = viewModel.ProjectTwitter,
                        Icon = "ionicons ion-social-twitter",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Twitter.ToString()
                    });
                if (viewModel.ProjectGoogle != null)
                    selectedPlace.CategoryDetailsContacts.Add(new CategoryDetailsContact()
                    {

                        ContactInfo = viewModel.ProjectGoogle,
                        Icon = "ionicons ion-social-googleplus",
                        CategoryDetailsId = viewModel.Id,
                        ContactTypeId = ContactTypeEnum.ProjectContact.GetHashCode(),
                        SoicalType = SoicalTypeEnum.Google.ToString()
                    });

                #endregion


                await _unitOfWork.Complete();


                response = Ok(res);



                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }


        [HttpPost]
        [Route("Approve/{id:long}")]
        public async Task<IHttpActionResult> Approve(long id)
        {
            try
            {
                if (id == 0)
                    return NotFound();
                var bo = await _unitOfWork.CategoryDetails.GetAsync(id);
                if (bo == null)
                {
                    return NotFound();
                }
                bo.Approve();
                await _unitOfWork.Complete();
                return Ok();

            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("Approve" + msg);
            }
        }






        private async Task UpdatePlaceImages(List<Uploader> imagesList, long id, string name, int coverId, string imageFilename, string imageBase64)
        {

            if (coverId == 0)
            {
                _unitOfWork.PhotoRepository.DeleteMainImage(id);
            }

            if (!string.IsNullOrEmpty(imageFilename) && !string.IsNullOrEmpty(imageBase64))
            {
                string defaultImge = SaveImage(imageFilename, imageBase64);
                Photo selected = _unitOfWork.PhotoRepository.GetPhoto(coverId);
                if (selected != null)
                {
                    selected.IsDefault = true;
                    selected.Lang = "ar";
                    selected.ModifiedDate = DateTime.Now;
                    selected.PhotoUrl = defaultImge;
                    selected.RelatedId = id.ToString();
                    selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                    selected.Title = name;
                    await _unitOfWork.Complete();
                }
                else
                {
                    await CreatePhoto(new Photo()
                    {
                        IsDefault = true,
                        Lang = "ar",
                        ModifiedDate = DateTime.Now,
                        PhotoUrl = defaultImge,
                        RelatedId = id.ToString(),
                        RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                        Title = name

                    });
                }

            }
            if (imagesList != null)
            {
                List<int> editedImages = imagesList.Select(uploader => uploader.Id).ToList();
                if (coverId != 0)
                {
                    editedImages.Add(coverId);
                }

                _unitOfWork.PhotoRepository.RemoveImages(editedImages, id);
                await _unitOfWork.Complete();


                foreach (var image in imagesList)
                {
                    if (image.Id == 0 && !string.IsNullOrEmpty(image.Filename) && !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        await CreatePhoto(new Photo()
                        {
                            IsDefault = false,
                            Lang = "ar",
                            ModifiedDate = DateTime.Now,
                            PhotoUrl = randomImage,
                            RelatedId = id.ToString(),
                            RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode(),
                            Title = name
                        });
                    }
                    else if (image.Id != 0 && !string.IsNullOrEmpty(image.Filename) &&
                             !string.IsNullOrEmpty(image.Base64))
                    {
                        var randomImage = SaveImage(image.Filename, image.Base64);
                        Photo selected = _unitOfWork.PhotoRepository.GetPhoto(image.Id);
                        if (selected != null)
                        {
                            selected.IsDefault = false;
                            selected.Lang = "ar";
                            selected.ModifiedDate = DateTime.Now;
                            selected.PhotoUrl = randomImage;
                            selected.RelatedId = id.ToString();
                            selected.RelatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
                            selected.Title = name;
                            await _unitOfWork.Complete();
                        }

                    }
                }
            }
        }


    }


}