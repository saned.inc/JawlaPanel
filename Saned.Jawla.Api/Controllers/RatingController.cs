using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Common.Rating.ComplexType;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Persistence;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Rating")]
    //[Authorize]
    public class RatingController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public RatingController()
        {
            _unitOfWork = new UnitOfWorkAsync();
        }
        public RatingController(IUnitOfWorkAsync unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [Authorize]
        [Route("GetRatings")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllRatings()
        {
            try
            {
                return Ok(await _unitOfWork.Rating.GetAll());
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetRatings --- " + msg);
            }

        }

        [Authorize]
        [Route("SaveRating")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveRating([FromBody] RatingInfo rating)
        {
            try
            {
                rating.RatingDate = DateTime.Now;
                // rating.RateValues;
                //rating.RelatedId;
                // rating.UserId 9ce1b863-7134-4653-9bb3-d7eb468f8f02;
                // rating.RelatedType;

                return Ok(await _unitOfWork.Rating.SaveRating(rating));
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("SaveRating --- " + msg);
            }
        }


        [Route("UserRatings")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserRatings()
        {
            try
            {
                var userName = User.Identity.GetUserName();
                var u = await GetApplicationUser(userName);
                return Ok(await _unitOfWork.Rating.GetAll());
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetRatings --- " + msg);
            }

        }
    }
}