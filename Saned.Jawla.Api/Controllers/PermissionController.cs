using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Jawla.Api.ViewModels;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Permission")]
    public class PermissionController : BasicController
    {
        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> AddUserInRole(PermissionViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //--TODO Check Empty Parammter 
            List<string> errors = new List<string>();

            if (string.IsNullOrEmpty(viewModel.Role.Trim()))
                return ValidateZeroOrEmpty(nameof(viewModel.Role));

            if (string.IsNullOrEmpty(viewModel.UserId.Trim()))
                return ValidateZeroOrEmpty(nameof(viewModel.UserId));

            //--TODO Check User Exit/Active And In Safer Role

            var user = await GetApplicationUserById(viewModel.UserId.Trim());
            if (user == null)
                return UserNotFound();


            //--TODO Check Role Exit
            var checkRole = await IsRoleExit(viewModel.Role.Trim());
            if (checkRole == false)
                return RoleNotFound();

            //--TODO Add User In Role
            await AddRoleToUser(viewModel.UserId, viewModel.Role);
            await _unitOfWork.Complete();
            return Ok();
        }

        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> RemoveUserFromRole(PermissionViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //--TODO Check Empty Parammter 
            List<string> errors = new List<string>();

            if (string.IsNullOrEmpty(viewModel.Role.Trim()))
                return ValidateZeroOrEmpty(nameof(viewModel.Role));

            if (string.IsNullOrEmpty(viewModel.UserId.Trim()))
                return ValidateZeroOrEmpty(nameof(viewModel.UserId));

            //--TODO Check User Exit/Active And In Safer Role

            var user = await GetApplicationUserById(viewModel.UserId.Trim());
            if (user == null)
                return UserNotFound();


            //--TODO Check Role Exit
            var checkRole = await IsRoleExit(viewModel.Role.Trim());
            if (checkRole == false)
                return RoleNotFound();

            //--TODO Remove User In Role
            IdentityResult result = await RemoveFromRole(viewModel.UserId, viewModel.Role);
            await _unitOfWork.Complete();
            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok();


        }

        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> GetUserRoles(UserRoleViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //--TODO Check Empty Parammter 
            List<string> errors = new List<string>();

            ;

            //--TODO Check User Exit/Active And In Safer Role

            var user = await GetApplicationUserById(viewModel.UserId.Trim());
            if (user == null)
                return UserNotFound();


            //--TODO Get User Role
            var list = await GetUserRoles(viewModel.UserId.Trim());

            return Ok(list);


        }

        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> RemoveUserRoles(UserRoleViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //--TODO Check Empty Parammter 
            List<string> errors = new List<string>();

            //--TODO Check User Exit/Active And In Safer Role

            var user = await GetApplicationUserById(viewModel.UserId.Trim());
            if (user == null)
                return UserNotFound();

            //--TODO Remove User Role
            IdentityResult result = await RemoveFromAllRoles(viewModel.UserId.Trim());
            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok();
        }

        [HttpPost]
        [Attributes.Authorize(Roles = "Administrator")]
        public async Task<IHttpActionResult> RemoveFromAllRolesButUser(UserRoleViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //--TODO Check Empty Parammter 
            List<string> errors = new List<string>();

            //--TODO Check User Exit/Active And In Safer Role

            var user = await GetApplicationUserById(viewModel.UserId.Trim());
            if (user == null)
                return UserNotFound();

            //--TODO Remove User Role
            IdentityResult result = await RemoveFromAllRolesButUser(viewModel.UserId.Trim());
            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null)
                return errorResult;

            return Ok();
        }

        [HttpPost]
       [Attributes.Authorize(Roles = "Administrator")]
        [Route("PermissionView")]
        public async Task<IHttpActionResult> PermissionView(UserRoleViewModel model)
        {
            var permissionViews = new List<PermissionView>();
            var rols = (await _repo.GetRoles()).ToList();
            var users = _unitOfWork.AmbassadorRequest.GetAsync().Where(x=>x.UserId!= null && x.UserId==model.UserId);
          //  var userss = (await _unitOfWork.AmbassadorRequest.GetAllAsync());
        
            foreach (var user in users)
            {

                var userPermission =(await _repo.GetUserRoles(user.UserId));

                var permission = new PermissionView()
                {
                    UserId = user.UserId,
                    UserName = user.FullName,

                    Roles = rols?.Select(x => new RoleViewModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                       ArabicName =RoleViewModel.RolesList().Find(r=>r.Name== x.Name)!=null? RoleViewModel.RolesList().Find(r => r.Name == x.Name).ArabicName:String.Empty,
                        IsAdded = userPermission.Any(u=>u.Equals(x.Name))
                    }).Where(x=>x.Name != RolesEnum.Safeer.ToString() && x.Name != RolesEnum.User.ToString() && x.Name != RolesEnum.Administrator.ToString())
                    .ToList() ?? new List<RoleViewModel>(),


                };
                permissionViews.Add(permission);
            }
            return Ok(permissionViews);
        }


        private string ConvertArabic(string value)
        {
            byte[] enBuff = Encoding.GetEncoding("windows-1256").GetBytes(value);
            value = Encoding.GetEncoding("windows-1252").GetString(enBuff);
            return value;
        }
    }

    public class PermissionView
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public List<RoleViewModel> Roles { get; set; }
    }

   
}