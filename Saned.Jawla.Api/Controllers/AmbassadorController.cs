﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Saned.Core.Security.Core.Enum;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;
using System.Linq;
using System.Web.Security;
using Saned.Core.Security.Core.Models;
using Saned.Core.Security.Persistence.Tools;
using Saned.Core.Security.Persistence.Repositories;
using Saned.Core.Security.Persistence.Infrastructure;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Ambassador")]
    public class AmbassadorController : BasicController
    {
        //private readonly IApplicationDbContext _context;

        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly Saned.Core.Security.Core.IUnitOfWorkAsync _securityUnitOfWork;
        private readonly AuthRepository repo;
        public AmbassadorController()
        {

            _unitOfWork = new UnitOfWorkAsync();
            _securityUnitOfWork = new Saned.Core.Security.Persistence.UnitOfWorkAsync();
            repo = new AuthRepository();
        }

        [AllowAnonymous]
        [Route("CreateAmbassador")]
        public async Task<IHttpActionResult> CreateAmbassador(AmbassadorViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                string userId = null;
                var randomImage = string.Empty;
                var userName = User.Identity.GetUserName();
                IdentityResult errorAmbassadorRequest = null;
                if (string.IsNullOrEmpty(userName))
                {
                    // --TODO Add Fuction To Check If Device Request
                    // --TODO Add Fuction To Check If Email Or Phone Number Exit in Request
                    // --TODO Add Fuction To Check If Email Or Phone Number Exit in User
                    IdentityResult baseResult = await UserValidate(viewModel.Email, viewModel.PhoneNumber);
                    List<string> errors = new List<string>(baseResult.Errors);
                    errorAmbassadorRequest = await _unitOfWork.AmbassadorRequest.ValidateAsync(errors, viewModel.Email, viewModel.PhoneNumber);


                }
                else
                {
                    // --TODO Add Fuction To Check If Current User have Request
                    //var errors = new List<string>();
                    //var user = await GetApplicationUser(userName);
                    //if (user != null)
                    //    userId = user.Id;
                    //else
                    //    return BadRequest("Can't Get User");

                    #region CheckUser
                    if (string.IsNullOrEmpty(userName))
                        return UserNotLogin();
                    var user = await GetApplicationUser(userName);
                    if (user == null)
                        return UserNotFound();
                    if (user.IsDeleted == true)
                        return UserNotActive();
                    userId = user.Id;
                    #endregion


                    IdentityResult baseResult = await UserValidate(viewModel.Email, viewModel.PhoneNumber, userId);
                    List<string> errors = new List<string>(baseResult.Errors);
                    errorAmbassadorRequest = await _unitOfWork.AmbassadorRequest.ValidateAsync(errors, viewModel.Email, viewModel.PhoneNumber, userId);
                    randomImage = user.PhotoUrl;
                }

                if (errorAmbassadorRequest.Succeeded)
                {
                    if (!string.IsNullOrEmpty(viewModel.ImageFilename) && !string.IsNullOrEmpty(viewModel.ImageBase64))
                    {
                        randomImage = SaveImage(viewModel.ImageFilename, viewModel.ImageBase64);
                    }
                    _unitOfWork.AmbassadorRequest.CreateAsync(new AmbassadorRequest()
                    {
                        ModifiedDate = DateTime.Now,
                        Status = AmbassadorRequestStatusEnum.New.GetHashCode(),
                        UserId = userId,
                        PhoneNumber = viewModel.PhoneNumber,
                        Address = viewModel.Address,
                        Age = viewModel.Age,
                        Email = viewModel.Email,
                        FullName = viewModel.Name,
                        SoicalLinks = viewModel.SoicalLinks,
                        PhotoUrl = randomImage
                    });

                    var result = await _unitOfWork.Complete();

                    return Ok(result);

                }

                IHttpActionResult errorResult = GetErrorResult(errorAmbassadorRequest);
                return errorResult;




            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest("CreateAmbassador " + msg);
            }

        }   

        //Mobile List
        [HttpPost]
        [Route("GetAmbassadors")]
        public async Task<IHttpActionResult> GetAmbassadors(AmbassadorsSearchViewModel viewModel)
        {
            try
            {
                return Ok(await _unitOfWork.AmbassadorRequest.FindAllAsync(viewModel.PageNumber, viewModel.PageSize));
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetAmbassadors --- " + msg);
            }
        }
        [Route("GetAllAmbassadors")]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(_unitOfWork.AmbassadorRequest.GetAsync());
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetAllAmbassadors --- " + msg);
            }
        }

        //Mobile Details
        [HttpPost]
        [Route("GetAmbassador")]
        public async Task<IHttpActionResult> GetAmbassador(AmbassadorsSearchViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                List<string> errors = new List<string>();

                if (viewModel.AmbassadorId == 0)
                    return ValidateZeroOrEmpty(nameof(viewModel.AmbassadorId));

                //AmbassadorId Is RequestId
                //--TODO  Check Table Ambassador Request.
                var request = await _unitOfWork.AmbassadorRequest.GetAsync(viewModel.AmbassadorId);
                if (request == null)
                    return RequestNotFound(viewModel.AmbassadorId);

                var user = await GetApplicationUserById(request.UserId);
                if (user == null)
                    return AmbassadorUserNotFound(request.UserId);

                var categoryDetails =
                    await
                        _unitOfWork.CategoryDetails.FindAsync(request.UserId, viewModel.PageNumber,
                            viewModel.PageSize);

                Ambassador ambassador = new Ambassador()
                {
                    PhotoUrl = user.PhotoUrl,
                    Name = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    SoicalLinks = user.SoicalLinks,
                    Age = user.Age,
                    Address = user.Address,
                    Items = categoryDetails,
                };
                return Ok(ambassador);
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetAmbassador --- " + msg);
            }
        }

        //Read All Request
        [HttpPost]
        [Route("GetAmbassadorRequest")]
        public async Task<IHttpActionResult> GetAmbassadorRequest()
        {
            try
            {
                return Ok(await _unitOfWork.AmbassadorRequest.GetAllAsync());
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetAmbassadorRequest --- " + msg);
            }
        }

        [HttpGet]
        [Route("CheckAmbassadorRequest")]
        public async Task<IHttpActionResult> CheckAmbassadorRequest(string userId)
        {
            try
            {
                var req = _unitOfWork.AmbassadorRequest.GetIQuerable()
                    .FirstOrDefault(f => f.UserId == userId);
                return Ok(req);
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetAmbassadorRequest --- " + msg);
            }
        }


        public string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }



        [HttpPost]
        [Route("ApproveAmbassadors")]
        public async Task<IHttpActionResult> ApproveAmbassadors(AmbassadorRequestAproveViewModel viewModel)
        {
            try
            {

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                //--TODO Check Request Id Sent Not Zero
                if (viewModel.RequestId == 0)
                    return ValidateZeroOrEmpty(nameof(viewModel.RequestId));

                //--TODO Get Ambassadors Request To Approve
                var request = await _unitOfWork.AmbassadorRequest.GetAsync(viewModel.RequestId);
                if (request == null)
                    return RequestNotFound(viewModel.RequestId);

                if (!string.IsNullOrEmpty(viewModel.DuplicatedUserId))
                {
                    request.UserId = viewModel.DuplicatedUserId;
                }

                //--TODO Check IF Request Related To User IF NOT Create User 
                var userId = request.UserId;
                //--TODO SendEmail TO User With Login Infromation
                EmailSetting emailSettings = new EmailSetting();

                if (string.IsNullOrEmpty(userId))
                {
                    emailSettings = _securityUnitOfWork.EmailSetting.GetEmailSetting("3");
                    var userName = "ambassador_" + RandomString(3);
                    string password = Guid.NewGuid().ToString("N").ToLower()
                 .Replace("1", "").Replace("o", "").Replace("0", "")
                 .Substring(0, 6);

                    var users = await repo.GetDuplicatedUsers(request.Email, request.PhoneNumber);

                    if (users != null && users.Count > 0)
                    {
                        return Ok(new { DuplicatedUsers = users });
                    }

                    //--TODO Create User ( Before Create User Check Phone And Email Is Unique)
                    var creatUserResult =
                        await
                            CreateUser(
                                request.FullName,
                                request.PhoneNumber,
                                request.Email,
                                userName,
                                password,
                                request.Age,
                                request.SoicalLinks,
                                request.Address,
                                request.PhotoUrl
                                );

                    if (creatUserResult.Succeeded)
                    {
                        #region CheckUser
                        var user = await GetApplicationUser(userName);
                        if (user == null)
                            return UserNotFound();
                        if (user.IsDeleted == true)
                            return UserNotActive();
                        #endregion

                        //--TODO Check User In RolesEnum  IF NoT Add User RolesEnum
                        await AddRoleToUser(user.Id, RolesEnum.User.ToString());
                        await AddRoleToUser(user.Id, RolesEnum.Safeer.ToString());

                        //--TODO Update Ambassadors Request Set UserId
                        request.UserId = user.Id;

                        //--TODO Update Ambassadors Request To Status Approve
                        request.Approve();

                        var result = Utilty.SendMail(emailSettings.Host,
                                       emailSettings.Port,
                                       emailSettings.FromEmail, emailSettings.Password,
                                       user.Email,
                                       emailSettings.SubjectAr,
                                       emailSettings.MessageBodyAr
                                       .Replace("@FullName", user.Name)
                                       .Replace("@ambassadordashboard", "http://jawlaadmin.saned-projects.com/login.html")
                                       .Replace("@username", userName)
                                       .Replace("@password", password), "");

                        //Utilty.sendFromGmail(user.Email, emailSettings.SubjectAr, emailSettings.MessageBodyAr
                        //               .Replace("@fullname", user.Name)
                        //               .Replace("@ambassadordashboard", "http://jawlaadmin.saned-projects.com/login.html")
                        //               .Replace("@username", userName)
                        //               .Replace("@password", password));

                    }
                    else
                    {
                        IHttpActionResult errorResult = GetErrorResult(creatUserResult);
                        return errorResult;
                    }


                }
                else
                {



                    var user = await GetApplicationUserById(userId);

                    if (user == null)
                        return UserNotFound();
                    else if (user.IsDeleted == true)
                        return UserNotActive();
                    else
                    {

                        await UpdateUser(request.UserId,
                                 request.FullName,
                                 request.PhoneNumber,
                                 request.Email,
                                 request.Age,
                                 request.SoicalLinks,
                                 request.Address,
                                 request.PhotoUrl);





                        //--TODO Check User In RolesEnum  IF NoT Add User RolesEnum
                        await AddRoleToUser(user.Id, RolesEnum.User.ToString());
                        await AddRoleToUser(user.Id, RolesEnum.Safeer.ToString());
                        //--TODO Update Ambassadors Request To Status Approve
                        request.Approve();

                        if (string.IsNullOrEmpty(user.ConfirmedToken))
                        {
                            ApplicationUserManagerImpl UserManager = new ApplicationUserManagerImpl();
                            //var password = Membership.GeneratePassword(6, 1);
                            string password = Guid.NewGuid().ToString("N").ToLower()
                      .Replace("1", "").Replace("o", "").Replace("0", "")
                      .Substring(0, 6);
                            string code = UserManager.GeneratePasswordResetToken(request.UserId);
                            await UserManager.ResetPasswordAsync(request.UserId, code, password);

                            emailSettings = _securityUnitOfWork.EmailSetting.GetEmailSetting("3");

                            var result = Utilty.SendMail(emailSettings.Host,
                            emailSettings.Port,
                            emailSettings.FromEmail, emailSettings.Password,
                            user.Email,
                            emailSettings.SubjectAr,
                            emailSettings.MessageBodyAr
                            .Replace("@FullName", user.Name)
                            .Replace("@ambassadordashboard", "http://jawlaapi.saned-projects.com/login.html")
                            .Replace("@username", user.UserName)
                            .Replace("@password", password), "");
                        }
                        else
                        {
                            emailSettings = _securityUnitOfWork.EmailSetting.GetEmailSetting("4");

                            var result = Utilty.SendMail(emailSettings.Host,
                                   emailSettings.Port,
                                   emailSettings.FromEmail, emailSettings.Password,
                                   user.Email,
                                   emailSettings.SubjectAr,
                                   emailSettings.MessageBodyAr
                                   .Replace("@FullName", user.Name)
                                   .Replace("@ambassadordashboard", "http://jawlaapi.saned-projects.com/login.html"), "");

                        }

                        //return Ok(await _unitOfWork.Complete());
                    }
                }




                return Ok(await _unitOfWork.Complete());


            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("ApproveAmbassadors --- " + msg);
            }
        }

        #region Admin
        //Admin List
        [HttpPost]
        [Route("GetAmbassadorRequestList")]
        public async Task<IHttpActionResult> GetAmbassadorRequestList(AmbassadorRequestSearchViewModel viewModel)
        {
            try
            {
                var lst =
                    await
                        _unitOfWork.AmbassadorRequest.FindAllAsyncByStatus(viewModel.PageNumber, viewModel.PageSize,
                            viewModel.Status);


                var totalCount = await
                        _unitOfWork.AmbassadorRequest.FindAllAsyncByStatus(viewModel.Status);

                var result = new
                {
                    TotalCount = totalCount.Count,
                    Data = lst
                };
                return Ok(result);

            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("GetAmbassadorRequestList --- " + msg);
            }
        }


        [HttpPost]
        [Route("AdminSearchAmbassadorRequest")]
        public IHttpActionResult AdminSearchAmbassadorRequest(AmbassadorRequestSearchViewModel model)
        {


            try
            {
                return Ok(new
                {
                    Data =
                  _unitOfWork.AmbassadorRequest.GetIQuerable().Where(x => string.IsNullOrEmpty(model.Name) || x.FullName.ToLower().Contains(model.Name.ToLower()))
                 .OrderBy(a => a.Id)
                 .Skip(model.PageNumber * model.PageSize)
                 .Take(model.PageSize)
                 .ToList(),
                    TotalCount = _unitOfWork.AmbassadorRequest.GetIQuerable()
                                  .Where(x => string.IsNullOrEmpty(model.Name)
                                  || x.FullName.ToLower().Contains(model.Name.ToLower()))
                                  .Count()

                });
            }
            catch (Exception e)
            {

                string msg = e.GetaAllMessages();
                return BadRequest("AdminSearchAmbassadorRequest --- " + msg);
            }
        }


        [HttpGet]
        [Route("AdminGetById/{id}")]
        public IHttpActionResult AdminGetById(int id)
        {
            try
            {
                return Ok(_unitOfWork.AmbassadorRequest.GetIQuerable().FirstOrDefault(x => x.Id == id));
            }
            catch (Exception e)
            {
                string msg = e.GetaAllMessages();
                return BadRequest("GetAmbassadorRequestList --- " + msg);
            }
        }

        [HttpPost]
        [Route("RejectAmbassador")]
        public async Task<IHttpActionResult> RejectAmbassador(AmbassadorRequestAproveViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //--TODO Check Request Id Sent Not Zero
            if (viewModel.RequestId == 0)
                return ValidateZeroOrEmpty(nameof(viewModel.RequestId));

            //--TODO Get Ambassadors Request To Approve
            var request = await _unitOfWork.AmbassadorRequest.GetAsync(viewModel.RequestId);
            if (request == null)
                return RequestNotFound(viewModel.RequestId);


            if (!string.IsNullOrEmpty(request.UserId))
            {
                var user = await GetApplicationUserById(request.UserId);

                if (user != null)
                {
                    await RemoveFromRole(request.UserId, RolesEnum.Safeer.ToString());

                }


                await _unitOfWork.AmbassadorRequest.DeactivateCategoryDetailsByUserId(request.UserId);
            }

            _unitOfWork.AmbassadorRequest.DeleteAsync(request);
            var result = await _unitOfWork.Complete();

            return Ok(result);
        }
        #endregion


        #region Error Message
        private IHttpActionResult RequestNotFound(long requestId)
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "Ambassador Request Number " + requestId + " Not Found.");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }
        private IHttpActionResult AmbassadorUserNotFound(string userId)
        {
            List<string> errors = new List<string>();
            var error = ValidateMessage(errors, "Ambassador Request With UserId " + userId + " Not Found.");
            IHttpActionResult errorResult = GetErrorResult(error);
            return errorResult;
        }

        #endregion


    }
}