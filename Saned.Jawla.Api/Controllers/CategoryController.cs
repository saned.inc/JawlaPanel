﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Api.Results;
using Saned.Jawla.Api.ViewModels;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Persistence;

namespace Saned.Jawla.Api.Controllers
{
    [RoutePrefix("api/Category")]
    public class CategoryController : BasicController
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        public CategoryController()
        {

            _unitOfWork = new UnitOfWorkAsync();
        }

        [Route("CreateCategory")]
        public async Task<IHttpActionResult> CreateCategory(CategoryViewModel viewModel)
        {
            try
            {
                IHttpActionResult response;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var userName = User.Identity.GetUserName();

                if (viewModel.Id == 0)
                {
                    var bo = Mapper.Map<CategoryViewModel, Category>(viewModel);
                    var randomImage = SaveImage(viewModel.AttachmentViewModel.Filename, viewModel.AttachmentViewModel.Base64);
                    bo.PhotoUrl = randomImage;
                    _unitOfWork.Category.CreateAsync(bo);

                    var result = await _unitOfWork.Complete();
                    response = Ok(result);
                }
                else
                {
                    var category = await (_unitOfWork.Category.GetAsync(viewModel.Id));
                    //category.Update(viewModel.Name,viewModel.PhotoUrl);

                    if (viewModel.AttachmentViewModel != null
                        && !string.IsNullOrEmpty(viewModel.AttachmentViewModel.Filename)
                        && !string.IsNullOrEmpty(viewModel.AttachmentViewModel.Base64))
                    {
                        var randomImage = SaveImage(viewModel.AttachmentViewModel.Filename, viewModel.AttachmentViewModel.Base64);
                        category.PhotoUrl = randomImage;
                    }

                    category.Name = viewModel.Name;
                    category.ParentId = viewModel.ParentId;

                    var result = await _unitOfWork.Complete();

                    response = Ok(category.Id);
                }

                return response;
            }
            catch (Exception ex)
            {
                string msg = ex.GetaAllMessages();
                return BadRequest(msg);
            }

        }


        [HttpPost]
        [Route("GetCategories")]
        public async Task<IHttpActionResult> GetCategories()
        {
            try
            {
                return Ok(await _unitOfWork.Category.FindAllMainCategoryAsync());
            }
            catch (Exception e)
            {
                string msg = e.GetaAllMessages();
                return BadRequest(msg);
            }
        }

        [HttpPost]
        [Route("GetChildCategories")]
        public async Task<IHttpActionResult> GetChildCategories(PagingViewModel viewModel)
        {
            try
            {
                viewModel.PageNumber = viewModel.PageNumber - 1;
                var result = await _unitOfWork.Category.FindAllChildAsync(viewModel.PageNumber, viewModel.PageSize);
                return Ok(result);
            }
            catch (Exception e)
            {
                string msg = e.GetaAllMessages();
                return BadRequest(msg);
            }
        }


        [HttpGet]
        [Route("GetCategoryByParentId/{parentCategoryId}")]
        public async Task<IHttpActionResult> GetCategoryByParentId(int parentCategoryId)
        {
            try
            {
                
                var result = await _unitOfWork.Category.GetCategoryByParentId(parentCategoryId);
                return Ok(result);
            }
            catch (Exception e)
            {
                string msg = e.GetaAllMessages();
                return BadRequest(msg);
            }
        }
        [HttpPost]
        [Route("GetParentCategories")]
        public async Task<IHttpActionResult> GetParentCategories()
        {
            try
            {
                return Ok(await _unitOfWork.Category.FindAllParentAsync());
            }
            catch (Exception e)
            {
                string msg = e.GetaAllMessages();
                return BadRequest(msg);
            }
        }


        [HttpGet]
        [Route("GetAllCategories")]

        public async Task<IHttpActionResult> GetAllCategories()
        {
            try
            {
                return Ok(await _unitOfWork.Category.GetAllCategories());
            }
            catch (Exception e)
            {

                return BadRequest("GetAllCategories---" + e.GetaAllMessages());
            }

        }


        [HttpPost]
        [Route("DeleteById/{id}")]
        public IHttpActionResult DeleteById(int id)
        {
            try
            {
                _unitOfWork.Category.DeleteById(id);
                return Ok(_unitOfWork.Complete());
            }
            catch (Exception e)
            {

                return BadRequest("Delete CategoryBy Id ----" + e.GetaAllMessages());
            }


        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<IHttpActionResult> GetById(int id)
        {

            try
            {
                return Ok(await _unitOfWork.Category.GetAsync(id));

            }
            catch (Exception e)
            {

                return BadRequest("Get CategoryBy Id ---" + e.GetaAllMessages());
            }

        }


        [HttpPost]
        [Route("GetAllCategoriesForAdmin")]
        public async Task<IHttpActionResult> AdminGetAllCategories(PagingViewModel viewModel)
        {
            IHttpActionResult response;
            try
            {
                var categories = await _unitOfWork.Category.FindAllAsync(viewModel.PageNumber, viewModel.PageSize);

                int totalCount = _unitOfWork.Category.GetAsync().Count();
                PaginationSet<Category> pagedSet = new PaginationSet<Category>()
                {
                    Items = categories,
                    Page = viewModel.PageNumber,
                    TotalCount = totalCount,
                    TotalPages = (int)Math.Ceiling((decimal)totalCount / viewModel.PageNumber)
                };

                response = Ok(pagedSet);
            }

            catch (Exception ex)
            {

                response = InternalServerError(ex);
            }

            return response;
        }
    }
}