﻿(function () {

    var translation = function ($resource) {

        var _getLanguageFile = function (path) {
            return path + 'ar.json';

           // return path + $cookies.get("lang") + '.json';
        }

        this.getAppDefaults = function () {
            var path = "/App/Translations/translation_";
            var languageFilePath = _getLanguageFile(path);
            return $resource(languageFilePath);
        };


    };



    var module = angular.module("App");
    module.service("translation", ["$resource", "$cookies", translation]);

}());