﻿'use strict';
angular.module("SecurityModule").factory('authService', ['$q', '$injector', 'localStorageService', 'ngAuthSettings', '$window', '$location',
    function ($q, $injector, localStorageService, ngAuthSettings, $window, $location) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;
        var $http;
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: "",
            useRefreshTokens: false
        };


        var _permissions = {
            "EventAdd": false,
            "EventViewByCreator": false,
            "EventEditByCreator": false,
            "EventDeleteByCreator": false,
            "EventAddedWithoutAdminApprove": false,
            "EventViewAny": false,
            "EventEditAny": false,
            "EventDeleteAny": false,

            "ProjectAdd": false,
            "ProjectByCreator": false,
            "ProjectEditByCreator": false,
            "ProjectDeleteByCreator": false,
            "ProjectAddedWithoutAdminApprove": false,
            "ProjectViewAny": false,
            "ProjectlEditAny": false,
            "ProjectDeleteAny": false,

            "RestaurantAdd": false,
            "RestaurantViewByCreator": false,
            "RestaurantEditByCreator": false,
            "RestaurantDeleteByCreator": false,
            "RestaurantAddedWithoutAdminApprove": false,
            "RestaurantViewAny": false,
            "RestaurantEditAny": false,
            "RestaurantDeleteAny": false,

            "HotelAdd": false,
            "HotelViewByCreator": false,
            "HotelEditByCreator": false,
            "HotelDeleteByCreator": false,
            "HotelAddedWithoutAdminApprove": false,
            "HotelViewAny": false,
            "HotelEditAny": false,
            "HotelDeleteAny": false,

            "TourPlaceAdd": false,
            "TourPlaceViewByCreator": false,
            "TourPlaceEditByCreator": false,
            "TourPlaceDeleteByCreator": false,
            "TourPlaceAddedWithoutAdminApprove": false,
            "TourPlaceViewAny": false,
            "TourPlaceEditAny": false,
            "TourPlaceDeleteAny": false,

            "HistoricPlaceAdd": false,
            "HistoricPlaceViewByCreator": false,
            "HistoricPlaceEditByCreator": false,
            "HistoricPlaceDeleteByCreator": false,
            "HistoricPlaceAddedWithoutAdminApprove": false,
            "HistoricPlaceViewAny": false,
            "HistoricPlaceEditAny": false,
            "HistoricPlaceDeleteAny": false,

            "AdAdd": false,
            "AdByCreator": false,
            "AdEditByCreator": false,
            "AdDeleteByCreator": false,
            "AdAddedWithoutAdminApprove": false,
            "AdViewAny": false,
            "AdEditAny": false,
            "AdDeleteAny": false,
            "Administrator": false


        }

        var _saveRegistration = function (registration) {
            _clearData();
            $http = $http || $injector.get('$http');
            return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
                return response;
            });

        };

        var _clearData = function () {
            localStorageService.remove('authorizationData');
            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.useRefreshTokens = false;

        };
        var _userRoles = function () {
            var deferred = $q.defer();
            $http = $http || $injector.get('$http');
            $http.post(serviceBase + '/Account/UserRols', { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then(function (response) {
                if (response.status == 200) {

                    localStorageService.set('userRoles', response);

                    deferred.resolve(response);
                } else {

                    localStorageService.set('userRoles', null);
                }


            }).catch(function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        };



        var _login = function (loginData) {
            var data = "role=administrator&grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
          
            if (loginData.useRefreshTokens)
                data = data + "&client_id=ngAuthApp";

            var deferred = $q.defer();
            $http = $http || $injector.get('$http');

            $http.post(serviceBase + 'token', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                if (response.status == 200) {

                    if (loginData.useRefreshTokens) {
                        localStorageService.set('authorizationData', { token: response.data.access_token, userName: loginData.userName, refreshToken: response.data.refresh_token, useRefreshTokens: true });
                    }
                    else {
                        localStorageService.set('authorizationData', { token: response.data.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
                    }

                    for (var index in _permissions) {
                        if (response.data.roles.lastIndexOf("Administrator") > -1)
                            _permissions[index] = true;
                        else if (response.data.roles.lastIndexOf(index) > -1)
                            _permissions[index] = true;

                    }

                    localStorageService.set('permissions', _permissions);



                    _authentication.isAuth = true;
                    _authentication.userName = loginData.userName;
                    _authentication.useRefreshTokens = loginData.useRefreshTokens;
                    $window.location.href = '/';
                    deferred.resolve(response);
                } else {
                    _logOut();
                    // deferred.reject(err);
                }


            }).catch(function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;

        };

        var _logOut = function () {
            localStorageService.remove('authorizationData');
            localStorageService.remove('permissions');

            for (var index in _permissions) {
                _permissions[index] = false;
            }


            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.useRefreshTokens = false;

            var path = window.location.pathname;
            var page = path.split("/").pop();

            if (path !== '/login.html') {
                $window.location.href = '/login.html';
            }

        };

        var _fillAuthData = function () {
            var authData = localStorageService.get('authorizationData');
            if (authData) {

                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                _authentication.useRefreshTokens = authData.useRefreshTokens;

            } else
                _logOut();
        };

        var _fillpermissionsData = function () {
            var permissionData = localStorageService.get('permissions');
            if (permissionData) {

                for (var index in permissionData) {

                    _permissions[index] = permissionData[index];
                }


            } else
                _logOut();
        };

        var _refreshToken = function () {
            var deferred = $q.defer();
            var authData = localStorageService.get('authorizationData');
            if (authData && authData.useRefreshTokens) {
                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId;
                localStorageService.remove('authorizationData');
                $http = $http || $injector.get('$http');
                $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then(function (response) {
                    if (response.status == 200) {
                        localStorageService.set('authorizationData', { token: response.data.access_token, userName: response.data.userName, refreshToken: response.data.refresh_token, useRefreshTokens: true });
                        deferred.resolve(response);
                    } else {
                        _logOut();
                        // deferred.reject(err);
                    }

                })
            } else {
                deferred.reject();
            }

            return deferred.promise;
        };


        authServiceFactory.saveRegistration = _saveRegistration;
        authServiceFactory.login = _login;
        authServiceFactory.logOut = _logOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;
        authServiceFactory.refreshToken = _refreshToken;
        authServiceFactory.clearData = _clearData;
        authServiceFactory.userRoles = _userRoles;
        authServiceFactory.fillpermission = _fillpermissionsData;
        authServiceFactory.permissions = _permissions;


        return authServiceFactory;
    }]);