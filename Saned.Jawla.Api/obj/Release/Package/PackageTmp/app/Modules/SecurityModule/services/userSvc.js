﻿(function () {

    var userSvc = function ($http, $q, localStorageService,ngAuthSettings) {
        var apiUri = ngAuthSettings.apiServiceBaseUri;

        var getAll = function () {


            var deferred = $q.defer();

            $http.get(apiUri +"/api/Account").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });


            return deferred.promise;
        };

        var get = function (id) {


            var deferred = $q.defer();


            $http.get(apiUri +"/api/Users/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });


            return deferred.promise;
        };


        var add = function (model) {


            var deferred = $q.defer();


            $http.post(apiUri +"/api/Users/", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });


            return deferred.promise;


        };

        var update = function (model) {


            var deferred = $q.defer();

            $http.post(apiUri +"/api/Users", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            return deferred.promise;

        };

        var remove = function (id) {


            var deferred = $q.defer();


            $http.post(apiUri +"/api/Users/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            return deferred.promise;

        };


        var getUserData = function () {

            var deferred = $q.defer();

            $http.get(apiUri +"/api/Users/GetData").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            return deferred.promise;
        };


        var changePassword = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            model.userName = localStorageService.get('authorizationData').userName;
            $http.post(apiUri +"/api/Account/ChangePassword", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };


        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
            getUserData: getUserData,
            changePassword: changePassword
        }


    };

    var module = angular.module("SecurityModule");
    module.factory("userSvc", ["$http", "$q", "localStorageService", "ngAuthSettings", userSvc]);

}());