﻿(function () {

    var contactInformationSvc = function ($http, $q,ngAuthSettings) {
        var api  = ngAuthSettings.apiServiceBaseUri;
        //var returnAll = function () {
        //    // Get the deferred object
        //    var deferred = $q.defer();
        //    // Initiates the AJAX call
        //    $http.get(api + "/api/Contact/GetApplicationInforamtion").then(function (successResponse) {
        //        deferred.resolve(successResponse);
        //    }, function (failureResponse) {
        //        deferred.reject(failureResponse);
        //    });

        //    // Returns the promise - Contains result once request completes
        //    return deferred.promise;

        //}

        var convert = function (input) {

            return Number(input);
        }

        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
           
            $http.get(api + "/api/Contact/GetApplicationInforamtion").then(function (successResponse) {
               
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (model, currentPage, pageSize) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("api/Country/search/" + pageSize + "/" + currentPage, model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
           
            $http.get(api + "/api/Contact/GetById/"+id).then(function (successResponse) {
                deferred.resolve(successResponse);
             
            }, function (failureResponse) {
            
                deferred.reject(failureResponse);
              
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(api + "/api/Contact/CreateContact", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api+"/api/Contact/CreateContact", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "/api/Contact/DeleteById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            convert:convert
        }


    };

    var module = angular.module("AppModule");
    module.factory("contactInformationSvc", ["$http", "$q", "$rootScope", "ngAuthSettings", contactInformationSvc]);

}());