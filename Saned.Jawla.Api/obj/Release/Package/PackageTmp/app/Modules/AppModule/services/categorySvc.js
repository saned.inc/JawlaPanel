﻿(function () {

    var categorySvc = function ($http, $q,ngAuthSettings) {
        var api = ngAuthSettings.apiServiceBaseUri;
        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Category/GetCategories").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }

        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            var model = { PageNumber: currentPage, PageSize: pageSize };
            $http.post(api + "/api/Category/GetChildCategories", model).then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            debugger;
            // Returns the promise - Contains result once request completes
            return deferred.promise;
            debugger;
        };

        var getAllParent = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "/api/Category/GetParentCategories").then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            debugger;
            // Returns the promise - Contains result once request completes
            return deferred.promise;
            debugger;
        };
        var search = function (model, currentPage, pageSize) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "api/Category/search/" + pageSize + "/" + currentPage, model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(api + "/api/Category/GetById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(api + "/api/Category/CreateCategory", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "/api/Category/CreateCategory", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "/api/Category/DeleteById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };


        var getCategoryByParentId = function (parentId)
        {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Category/GetCategoryByParentId/" + parentId).then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        return {
            getAll: getAll,
            getAllParent:getAllParent,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            getCategoryByParentId: getCategoryByParentId
        }


    };

    var module = angular.module("AppModule");
    module.factory("categorySvc", ["$http", "$q", "ngAuthSettings","$rootScope", categorySvc]);

}());