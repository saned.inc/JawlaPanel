﻿(function () {
    var rolesController = function ($rootScope, $scope, $route, $bootbox, $q, notify, $window, rolesSvc, appSettings) {


        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 5;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.errorMessage = "هذه الدولة مدرج تحتها عدد من المدن";
        $scope.data = [];
        $scope.rolesId = 0;
        $rootScope.roles = [];
        $scope.users = [];
        /*---------------Search------------------------*/

        $scope.changeRole = function (e,id,userId) {
            debugger;
            var model = { UserId: userId, Role: id };
            if (e == true) {
                $scope.addRole(model);
            } else if (e == false) {
                $scope.removeRole(model);
            }
        }
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }
        $scope.loadAmbassador = function () {
            debugger;
            rolesSvc.getUsers().then(function (data) {
                $scope.users = data.data;
                $scope.isDataLoading = false;
            });
        };
        $scope.onUserChange = function (id) {
            debugger;
            var model = { UserId: id};
            rolesSvc.getAll(model).then(function (response) {
                debugger;
                $scope.data = response.data;
                $scope.isDataLoading = false;

            });
        };
        $scope.cancelSearch = function () {
            if ($scope.model)
                $scope.getData($scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.rolesSearch = function () {
            $scope.isDataLoading = true;
            $scope.currentPage = 1;
            rolesSvc.search($scope.rolesId, $scope.currentPage, $scope.pageSize).then(function (response) {
                $scope.data = [response.data];
                $scope.totalData = [response.data].totalCount;
                $scope.isDataLoading = false;

            });

        }

        /*---------------Paging------------------------*/

        $scope.addRole = function (model) {
            rolesSvc.addRoleAll(model).then(function (response) {
                $scope.roles = response.data;
                $scope.isDataLoading = false;
              
            });
        }


        $scope.removeRole = function (model) {
            rolesSvc.removeRoleAll(model).then(function (response) {
                $scope.roles = response.data;
                $scope.isDataLoading = false;
            });
        }


        //$scope.getRoles = function () {
        //    debugger;
        //    rolesSvc.getRoles().then(function (response) {
        //        debugger;
        //        $rootScope.roles = response.data;
        //     //   $scope.getRoles();
        //        $scope.isDataLoading = false;
        //    });
        //}


        $scope.getData = function (currentPage, pageSize) {
          
            rolesSvc.returnAll().then(function (response) {
                debugger;
                $scope.data = response.data;            
                $scope.isDataLoading = false;

            });
        }

        
        $scope.loadAmbassador();
      //  $scope.getData($scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {
            $scope.getData(currentPage, pageSize);
        };

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    rolesSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;

                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.errorMessage);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };


        /* ------------- Sort function ------------ */
        $scope.sortKey = 'nameAr';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("rolesController", ["$rootScope", "$scope", "$route", "$bootbox", "$q", "notify", "$window", "rolesSvc", "translation", "appSettings", rolesController]);

}());