﻿(function () {
    var hotelsController = function ($rootScope, $scope, $route, $bootbox, notify, $window, hotelsSvc, countrySvc, citySvc, municipalitySvc, appSettings, ngAuthSettings) {

        $scope.showResult = $rootScope.pageSizes;
        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.model = {};
        $scope.isDataLoading = true;
        $scope.data = [];
        /*---------------Search------------------------*/
        $scope.disabledMunicipalityDrp = true;
        $scope.disabledCityDrp = true;
        countrySvc.returnAll().then(function (data) {
            $scope.countries = data.data;
        });

        $scope.onCityChange = function (id) {
            municipalitySvc.returnAllByCityId(id).then(function (data) {
                $scope.disabledMunicipalityDrp = false;
                $scope.Municipalities = [];
                $scope.Municipalities = data.data;
            });
        }
        $scope.onCountryChange = function (id) {
            citySvc.returnAllByCountryId(id).then(function (data) {
                $scope.disabledCityDrp = false;
                $scope.cities = [];
                $scope.cities = data.data;
            });
        }

        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {
            if ($scope.model)
                $scope.getData($scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.ContactUsSearch = function (pageWithsearch) {
            $scope.isDataLoading = true;
            $scope.currentPage = pageWithsearch !== true ? 1 : $scope.currentPage;
            $scope.model.countryId = angular.isUndefined($scope.model.countryId) ? null : $scope.model.countryId;
            $scope.model.cityId = (angular.isUndefined($scope.model.cityId) || $scope.model.cityId == null) ? null : $scope.model.cityId;
            $scope.model.municipalityId = (angular.isUndefined($scope.model.municipalityId) || $scope.model.municipalityId == null) ? null : $scope.model.municipalityId;

            hotelsSvc.search($scope.model, $scope.currentPage, $scope.pageSize).then(function (response) {
                $scope.data = response.data;
                $scope.totalData = response.data.length == 0 ? 0 : response.data[0].overAllCount;
                $scope.isDataLoading = false;

            });

        }
        /*---------------Paging------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            hotelsSvc.postAll(pageSize, currentPage).then(function (response) {
                $scope.data = response.data;
                $scope.totalData = response.data.length == 0 ? 0 : response.data[0].overAllCount;
                $scope.isDataLoading = false;
            });
        }

        $scope.getData($scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {
            if (angular.isUndefined($scope.model))
                $scope.getData(currentPage, pageSize);
            else
                $scope.ContactUsSearch(true);
        };



        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {

            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    hotelsSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;

                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);

                        for (var i = 0; i < $scope.data.length; i++) {
                            if ($scope.data[i].id == id) {
                                index = i;
                                break;
                            }
                        }

                        $scope.data.splice(index, 1);

                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };

        $scope.approve = function (id) {
            hotelsSvc.approve(id).then(function (response) {
                var status = response.status;
                var data = response.data;
                notify.success($rootScope.transaction.NotifySuccess);
            }, function (response) {

                var status = response.status;
                var data = response.data;

                if (status === 400) {
                    notify.error($scope.toasterWarning);
                }
                else if (status === 500)
                    notify.error(JSON.stringify(data));
            });

        };

        $scope.unApprove = function (id) {
            hotelsSvc.unApprove(id).then(function (response) {
                var status = response.status;
                var data = response.data;
                notify.success($rootScope.transaction.NotifySuccess);
            }, function (response) {

                var status = response.status;
                var data = response.data;

                if (status === 400) {
                    notify.error($scope.toasterWarning);
                }
                else if (status === 500)
                    notify.error(JSON.stringify(data));
            });

        };

        /* ------------- Sort function ------------ */
        $scope.sortKey = 'name';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("hotelsController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "hotelsSvc", "countrySvc", "citySvc", "municipalitySvc", "translation", "appSettings", "ngAuthSettings", hotelsController]);

}());