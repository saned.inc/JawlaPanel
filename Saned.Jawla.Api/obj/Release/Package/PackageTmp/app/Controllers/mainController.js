﻿
(function () {
    angular.module("App").controller('mainController', ['$scope', 'localStorageService', '$window',
     function ($scope, localStorageService, $window) {
         'use strict';

         $scope.logout = function () {
             localStorageService.clearAll();
             $window.location.href = 'login.html';
         };
         $scope.userName = localStorageService.get('authorizationData').userName;

     }]);
})();