﻿/// <reference path="FileUploaderTemplate.html" />
// Note --> To Bind in edit mode 
// Ex: List="{{dbList}}"  -- dbList= ['url','url']
var app = angular.module('fileUploader', [])

.directive('fileUploader', function () {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: 'Scripts/angular-file-uploader/FileUploaderTemplate.html',

        scope: {
            ngModel: '=',
            List: '='
        },
        link: function ($scope, element, attrs, ngModel, $rootscope) {
            // To Show Image In Thumbnail div
            $scope.imgLst = attrs.list ? JSON.parse(attrs.list) : [];
            //$scope.ngModel = [];
            $scope.Attachments = $scope.ngModel ? $scope.ngModel : [];

            $scope.fetchImage = function () {
                for (var i = 0; i < $scope.ngModel.length; i++) {
                    if ($scope.ngModel[i].filetype.indexOf("image") > -1 && $scope.Attachments.length < 8) {
                        $scope.imgLst.push({
                            url: 'data:image/png;base64,' + $scope.ngModel[i].base64,
                            name: $scope.ngModel[i].filename
                        });
                        $scope.ngModel[i]['Index'] = i;
                        $scope.Attachments.push($scope.ngModel[i]);
                    }
                    
                }
                $scope.ngModel = $scope.Attachments;

            };

            $scope.removeSingle = function (index) {
                $scope.imgLst.splice(index, 1);
                $scope.ngModel.splice(index, 1);
            }

            $scope.removeAll = function () {
                $scope.imgLst = [];
                $scope.ngModel = undefined;
                $scope.Attachments = [];
            }

            $scope.$watch('ngModel', function (value) {
                if (value == undefined) {
                    //Do something
                    $scope.removeAll();
                }
            });


            $scope.showImage = function (index, src) {
                var body = document.getElementsByTagName("BODY")[0];
                body.style.overflow = 'hidden';

                $scope.currentIndex = index;
                // Get the modal
                var modal = document.getElementById('ImageViewer');

                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var img = document.getElementById(index);
                $scope.modalImg = document.getElementById("imgModal");

                $scope.captionText = document.getElementById("caption");

                modal.style.display = "block";
                if (src.name != undefined) {
                    $scope.modalImg.src = src.url;
                    $scope.captionText.innerHTML = src.name;
                }

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks on <span> (x), close the modal
                span.onclick = function () {
                    modal.style.display = "none";
                }

            };

            $scope.closeModal = function () {
                document.getElementById('ImageViewer').style.display = 'none';

                var body = document.getElementsByTagName("BODY")[0];
                body.style.overflow = 'scroll';
            };


            $scope.previousImage = function () {
                if ($scope.currentIndex == 0)
                    $scope.currentIndex = $scope.imgLst.length - 1;
                else
                    $scope.currentIndex--;

                var img = document.getElementById($scope.currentIndex);
                if (img.alt != "") {
                    $scope.modalImg.src = img.src;
                    $scope.captionText.innerHTML = img.alt;
                }
            };

            $scope.nextImage = function () {
                if ($scope.currentIndex == $scope.imgLst.length - 1)
                    $scope.currentIndex = 0;
                else
                    $scope.currentIndex++;

                var img = document.getElementById($scope.currentIndex);
                if (img.alt != "") {
                    $scope.modalImg.src = img.src;
                    $scope.captionText.innerHTML = img.alt;
                }

            };

        }

    };
});