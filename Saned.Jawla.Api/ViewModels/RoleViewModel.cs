﻿using System.Collections.Generic;
using Saned.Core.Security.Core.Enum;

namespace Saned.Jawla.Api.ViewModels
{
    public class RoleViewModel
    {
      
        public string Id { get; set; }
        public string Name { get; set; }
        public string ArabicName { get; set; }
        public bool IsAdded { get; set; }

        public static List<RoleViewModel> RolesList()
        {
            List<RoleViewModel> Rols = new List<RoleViewModel>();
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة فاعلية", Name = PremisionEnum.EventAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض الفاعليات الخاصة بالسفير", Name = PremisionEnum.EventViewByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الفاعلية من قبل صاحبها", Name = PremisionEnum.EventEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الفاعلية من قبل صاحبها", Name = PremisionEnum.EventDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة فاعلية بدون موافقة", Name = PremisionEnum.EventAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض الفاعليات", Name = PremisionEnum.EventViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الفاعليات", Name = PremisionEnum.EventEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الفاعليات", Name = PremisionEnum.EventDeleteAny.ToString() });



            Rols.Add(new RoleViewModel() { ArabicName = "اضافة مشروع", Name = PremisionEnum.ProjectAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض المشروعات الخاصة بالسفير", Name = PremisionEnum.ProjectByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل المشروع من قبل صاحبها", Name = PremisionEnum.ProjectEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف المشروعات من قبل صاحبها", Name = PremisionEnum.ProjectDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة مشروع بدون موافقة", Name = PremisionEnum.ProjectAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض المشروعات", Name = PremisionEnum.ProjectViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل المشروعات", Name = PremisionEnum.ProjectlEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف المشروعات", Name = PremisionEnum.ProjectDeleteAny.ToString() });

            Rols.Add(new RoleViewModel() { ArabicName = "اضافة مطعم", Name = PremisionEnum.RestaurantAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض المطاعم الخاصة بالسفير", Name = PremisionEnum.RestaurantViewByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل المطعم من قبل صاحبه", Name = PremisionEnum.RestaurantEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف المطاعم من قبل صاحبها", Name = PremisionEnum.RestaurantDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة مطعم بدون موافقة", Name = PremisionEnum.RestaurantAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض المطاعم", Name = PremisionEnum.RestaurantViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل المطاعم", Name = PremisionEnum.RestaurantEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف المطاعم", Name = PremisionEnum.RestaurantDeleteAny.ToString() });


            Rols.Add(new RoleViewModel() { ArabicName = "اضافة فندق", Name = PremisionEnum.HotelAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض فنادق الخاصة بالسفير", Name = PremisionEnum.HotelViewByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل فندق من قبل صاحبه", Name = PremisionEnum.HotelEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف فنادق من قبل صاحبها", Name = PremisionEnum.HotelDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة فندق بدون موافقة", Name = PremisionEnum.HotelAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض فنادق", Name = PremisionEnum.HotelViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل فنادق", Name = PremisionEnum.HotelEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف فنادق", Name = PremisionEnum.HotelDeleteAny.ToString() });


            Rols.Add(new RoleViewModel() { ArabicName = "اضافة مكان سياحى", Name = PremisionEnum.TourPlaceAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض  الامكان سياحية الخاصة بالسفير", Name = PremisionEnum.TourPlaceViewByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الامكان سياحية من قبل صاحبها", Name = PremisionEnum.TourPlaceEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الامكان سياحية من قبل صاحبها", Name = PremisionEnum.TourPlaceDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة مطعم بدون موافقة", Name = PremisionEnum.TourPlaceAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض الامكان سياحية", Name = PremisionEnum.TourPlaceViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الامكان سياحية", Name = PremisionEnum.TourPlaceEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الامكان سياحية", Name = PremisionEnum.TourPlaceDeleteAny.ToString() });


            Rols.Add(new RoleViewModel() { ArabicName = "اضافة امكان تاريخية", Name = PremisionEnum.HistoricPlaceAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض الامكان التاريخية الخاصة بالسفير", Name = PremisionEnum.HistoricPlaceViewByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الامكان التاريخية  من قبل صاحبها", Name = PremisionEnum.HistoricPlaceEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الامكان التاريخية  من قبل صاحبها", Name = PremisionEnum.HistoricPlaceDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة امكان تاريخية بدون موافقة", Name = PremisionEnum.HistoricPlaceAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض الامكان التاريخية ", Name = PremisionEnum.HistoricPlaceViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الامكان التاريخية ", Name = PremisionEnum.HistoricPlaceEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الامكان التاريخية ", Name = PremisionEnum.HistoricPlaceDeleteAny.ToString() });


            Rols.Add(new RoleViewModel() { ArabicName = "اضافة اعلان", Name = PremisionEnum.AdAdd.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض اعلان الخاصة بالسفير", Name = PremisionEnum.AdByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل اعلان من قبل صاحبها", Name = PremisionEnum.AdEditByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف اعلان من قبل صاحبها", Name = PremisionEnum.AdDeleteByCreator.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "اضافة اعلان بدون موافقة", Name = PremisionEnum.AdAddedWithoutAdminApprove.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "استعراض الاعلان", Name = PremisionEnum.AdViewAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "تعديل الاعلان", Name = PremisionEnum.AdEditAny.ToString() });
            Rols.Add(new RoleViewModel() { ArabicName = "حذف الاعلان", Name = PremisionEnum.AdDeleteAny.ToString() });
            return Rols;

        }

    }
}