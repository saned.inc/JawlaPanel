﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saned.Jawla.Api.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime ModifiedDate { get; set; }=DateTime.Now;
        public bool? IsDeleted { get; set; } = false;
        public int OverAllCount { get; set; }
        public int ParentId { get; set; }
        public AttachmentViewModel AttachmentViewModel { get; set; }
    }
}