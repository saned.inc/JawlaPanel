﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Api.ViewModels
{
    //public class PlaceViewModel : CategoryDetailsViewModel
    //{
    //    public PlaceViewModel()
    //    {
    //        Branches = new List<CategoryDetailsBrancheDto>();
    //        Faqs = new List<CategoryDetailsFaqDto>();
    //    }
    //    public string AttachmentUrl;
    //    public int AllHit { get; set; }
    //    public int Rating { get; set; }
        
    //    public string Country { get; set; }
    //    public string City { get; set; }
    //    public string Municipality { get; set; }
    //    public string Ambassador { get; set; }
        
    //    public string Category { get; set; }
    //    public long PlaceId { get; set; }
    //    public string WorkHours { get; set; }
    //    public bool IsFavourite { get; set; }
    //    public List<CategoryDetailsBrancheDto> Branches { get; set; }
    //    public List<CategoryDetailsFaqDto> Faqs { get; set; }
    //    public bool IsOwner { get; set; }
    //    public int UserRating { get; set; }

    //    public string ProjectPhoneNumber { get; set; }
    //    public string ProjectFacebook { get; set; }
    //    public string ProjectWhatsapp { get; set; }
    //    public string ProjectTwitter { get; set; }
    //    public string ProjectGoogle { get; set; }
    //    public bool IsAmbassador { get; set; }
    //    public long AmbassadorRequestId { get; set; }
    //    public int CoverId { get; set; }
    //    public List<CategoryDetailsContact> Contacts { get; set; }
    //}


    public class PlaceViewModel : CategoryDetailsViewModel
    {
        //[EnsureOneElement(ErrorMessage = "سؤال واحد على الاقل")]
        public List<QuestionViewModel> Questions { get; set; }

        public string AttachmentUrl { get; set; }
        public int CoverId { get; set; }

        

        public string WorkHours { get; set; }

      
        public string ProjectPhoneNumber { get; set; }
    
        public string ProjectFacebook { get; set; }
        
        public string ProjectWhatsapp { get; set; }
     
        public string ProjectTwitter { get; set; }
    
        public string ProjectGoogle { get; set; }
        public List<CategoryDetailsContact> Contacts { get; set; }

    }
}