﻿using System;
using System.Linq;
using System.Web;

namespace Saned.Jawla.Api.ViewModels
{
    public class FavouriteViewModel
    {
        public int Id { get; set; }
        public long CategoryDetailsId { get; set; }
        public string UserId { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime ModifiedDate { get; set; }=DateTime.Now;

    }
}