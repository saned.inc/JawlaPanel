namespace Saned.Jawla.Api.ViewModels
{
    public class CountryViewModel
    {
        public int Id { get; set; }

        public string ArabicName { get; set; }
        public string EnglishName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
    }
}