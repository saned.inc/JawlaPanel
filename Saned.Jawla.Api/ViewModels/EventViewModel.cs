﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Api.ViewModels
{
    public class EventViewModel : CategoryDetailsViewModel
    {
        public string AttachmentUrl;
        public string Owner { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public DateTime EventStartDate { get; set; }

        public int CoverId { get; set; }
        public List<CategoryDetailsContact> Contacts { get; set; }
    }
}
   
        
