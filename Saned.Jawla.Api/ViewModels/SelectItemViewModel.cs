namespace Saned.Jawla.Api.ViewModels
{
    public class SelectItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}