namespace Saned.Jawla.Api.ViewModels
{
    public class UserImageViewModel
    {
        public string ImageFilename { get; set; }
        public string ImageBase64 { get; set; }
    }
}