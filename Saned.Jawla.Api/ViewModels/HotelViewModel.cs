﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Api.ViewModels
{
    public class HotelViewModel : CategoryDetailsViewModel
    {
        //[Required]

        public string WorkHours { get; set; }

        //[Required]
        public string ProjectPhoneNumber { get; set; }
        //[Required]
        public string ProjectFacebook { get; set; }
        //[Required]
        public string ProjectWhatsapp { get; set; }
        //[Required]
        public string ProjectTwitter { get; set; }
        //[Required]
        public string ProjectGoogle { get; set; }
    }

    public class RestaurantViewModel : HotelViewModel
    {
        //[EnsureOneElement(ErrorMessage = "سؤال واحد على الاقل")]
        public List<QuestionViewModel> Questions { get; set; }
        //[EnsureOneElement(ErrorMessage = "سؤال واحد على الاقل")]

        public List<BranchViewModel> Branches { get; set; }
        public string AttachmentUrl { get; set; }
        public int CoverId { get; set; }
    }

    public class BranchViewModel

    {
        public virtual CountryModel Country { get; set; }
        public virtual CityModel City { get; set; }
        public virtual MunicipalityModel Municipality { get; set; }


        



        public string Name { get; set; }
        public long Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime ModifiedDate { get; set; }=DateTime.Now;

    }

    public class QuestionViewModel
    {
        public int Id { get; set; }
        public DateTime ModifiedDate { get; set; } = DateTime.Now;

        public string Answer { get; set; }
        public string Question { get; set; }
    }


    public class EnsureOneElementAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as IList;
            if (list != null)
            {
                return list.Count > 0;
            }
            return false;
        }
    }
}