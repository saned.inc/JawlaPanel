﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Saned.Jawla.Api.ViewModels
{
    public class CategoryDetailsViewModel
    {
        public long Id { get; set; }
        public int CategoryId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        public int CityId { get; set; }

        public int? MunicipalityId { get; set; }
        [Required]
        public string Latitude { get; set; }
        [Required]
        public string Longitude { get; set; }
       
        public DateTime ModifiedDate { get; set; } = DateTime.Now;
        [Required]
        public bool? IsDeleted { get; set; } = false;
        public bool? IsBin { get; set; } = false;

        [Required]
        public string Status { get; set; } = "1";
        //[Required]
        public string AmbassadorId { get; set; }
   
     
        public string Description { get; set; }

        public string ImageFilename { get; set; }
        public string ImageBase64 { get; set; }
        public Dictionary<string,string> Images { get; set; }
        public List<Uploader> ImagesList { get; set; }
    }

    public class Uploader

    {
        public string Filename { get; set; }
        public string Base64 { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsDefault { get; set; }



    }



    
}