using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class ProjectViewModel : CategoryDetailsViewModel
    {

        [Required]
        public string Owner { get; set; }
        [Required]
        public string OwnerPhoneNumber { get; set; }
        [Required]
        public string OwnerFacebook { get; set; }
        [Required]
        public string OwnerWhatsapp { get; set; }
        [Required]
        public string OwnerTwitter { get; set; }
        [Required]
        public string OwnerGoogle { get; set; }
        [Required]
        public string ProjectPhoneNumber { get; set; }
        [Required]
        public string ProjectFacebook { get; set; }
        [Required]
        public string ProjectWhatsapp { get; set; }
        [Required]
        public string ProjectTwitter { get; set; }
        [Required]
        public string ProjectGoogle { get; set; }
    }
}