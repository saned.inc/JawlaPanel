using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class PermissionViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string Role { get; set; }
    }
    public class UserRoleViewModel
    {
        [Required]
        public string UserId { get; set; }
     
    }
}