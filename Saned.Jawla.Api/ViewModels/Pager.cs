namespace Saned.Jawla.Api.ViewModels
{
    public class Pager
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
      
    }
}