using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class AmbassadorViewModel
    {
        public AmbassadorViewModel()
        {

        }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

     

        [Required]
        [Display(Name = "Age")]
        public int Age { get; set; }

        [Required]
        [Display(Name = "SoicalLinks")]
        public string SoicalLinks { get; set; }


        public string ImageFilename { get; set; }
        public string ImageBase64 { get; set; }
        [Required]
        public string DeviceId { get; set; }
        [Required]
        public string Address { get; set; }
    }
}