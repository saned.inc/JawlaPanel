using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class AmbassadorRequestSearchViewModel
    {

        [Required]
        public int PageNumber { get; set; }
        [Required]
        public int PageSize { get; set; }
        public int? Status { get; set; }
        public string Name { get; set; }

    }
}