using System.Collections.Generic;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Api.ViewModels
{
    public class ProjectDetailsViewModel
    {
        public ProjectDetailsViewModel()
        {
            Images = new List<CategoryDetailsImage>();


        }
        public string Name { get; set; }
        public int AllHit { get; set; }
        public int Rating { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Municipality { get; set; }
        public string Description { get; set; }
        public string Ambassador { get; set; }
        public bool? IsBin { get; set; }
        public string Category { get; set; }
        public long ProjectId { get; set; }
        public string Owner { get; set; }
        public bool IsFavourite { get; set; }
        public List<CategoryDetailsImage> Images { get; set; }


        public bool IsOwner { get; set; }
        public int UserRating { get; set; }


        public bool IsAmbassador { get; set; }
        public long AmbassadorRequestId { get; set; }



        public string ProjectPhoneNumber { get; set; }
        public string ProjectFacebook { get; set; }
        public string ProjectWhatsapp { get; set; }
        public string ProjectTwitter { get; set; }
        public string ProjectGoogle { get; set; }

        public string OwnerPhoneNumber { get; set; }
        public string OwnerFacebook { get; set; }
        public string OwnerWhatsapp { get; set; }
        public string OwnerTwitter { get; set; }
        public string OwnerGoogle { get; set; }
        public string Idea { get; set; }
    }
}