﻿using System.Collections.Generic;
using System.Security.AccessControl;
using Saned.Modules.Photos;

namespace Saned.Jawla.Api.ViewModels
{
    public class EventDetailsViewModel
    {
        public EventDetailsViewModel()
        {
            Images = new List<CategoryDetailsImage>();
        }
        public string Name { get; set; }
        public int AllHit { get; set; }
        public int Rating { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Municipality { get; set; }
        public string Owner { get; set; }

        public string EventStartDate { get; set; }
        public string EventEndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
        public string Ambassador { get; set; }
        public bool? IsBin { get; set; }
        public string Category { get; set; }
        public long EventId { get; set; }

        public bool IsFavourite { get; set; }
        public bool IsOwner { get; set; }
        public bool IsAmbassador { get; set; }
        public long AmbassadorRequestId { get; set; }
        public List<CategoryDetailsImage> Images { get; set; }
        public List<Uploader> ImagesList { get; set; }



        public int UserRating { get; set; }

        public string ContactPhoneNumber { get; set; }
        public string ContactWhatsUp { get; set; }
        public string ContactFaceBook { get; set; }
        public string ContactTwitter { get; set; }
        public string ContactGoogle { get; set; }
        
    }
}