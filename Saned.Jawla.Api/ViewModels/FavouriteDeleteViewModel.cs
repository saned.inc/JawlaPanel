namespace Saned.Jawla.Api.ViewModels
{
    public class FavouriteDeleteViewModel
    {
        public string UserId { get; set; }
        public long CategoryDetailsId { get; set; }
    }
}