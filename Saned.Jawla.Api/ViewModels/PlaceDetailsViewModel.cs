﻿using System.Collections.Generic;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Api.ViewModels
{
    public class PlaceDetailsViewModel
    {
        public PlaceDetailsViewModel()
        {
            Images = new List<CategoryDetailsImage>();
            Branches = new List<CategoryDetailsBrancheDto>();
            Faqs = new List<CategoryDetailsFaqDto>();
        }
        public string Name { get; set; }
        public int AllHit { get; set; }
        public int Rating { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Municipality { get; set; }
        public string Description { get; set; }
        public string Ambassador { get; set; }
        public bool? IsBin { get; set; }
        public string Category { get; set; }
        public long PlaceId { get; set; }
        public string WorkHours { get; set; }
        public bool IsFavourite { get; set; }
        public List<CategoryDetailsImage> Images { get; set; }
        public List<CategoryDetailsBrancheDto> Branches { get; set; }
        public List<CategoryDetailsFaqDto> Faqs { get; set; }
        public bool IsOwner { get; set; }
        public int UserRating { get; set; }

        public string ProjectPhoneNumber { get; set; }
        public string ProjectFacebook { get; set; }
        public string ProjectWhatsapp { get; set; }
        public string ProjectTwitter { get; set; }
        public string ProjectGoogle { get; set; }
        public bool IsAmbassador { get; set; }
        public long AmbassadorRequestId { get; set; }
    }
}