﻿using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class AdvertisementViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
     
        public string Url { get; set; }
        [Required]
        public bool IsShow { get; set; }
        [Required]
        public int SecondNumber { get; set; }
        //[Required]
        //public string ImageFilename { get; set; }
        //[Required]
        //public string ImageBase64 { get; set; }

        public AttachmentViewModel AttachmentViewModel { get; set; }

        public bool DeleteImage { get; set; }
    }

    public class AttachmentViewModel
    {
        
        public string Base64 { get; set; }
        public string Filename { get; set; }
        public long FileSize { get; set; }
        public string Filetype { get; set; }
    }

    public class AdvertisementSearcViewModel
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 4;
        public string Keyword { get; set; }
    }
}