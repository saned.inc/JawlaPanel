using AutoMapper;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Api.ViewModels
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<CategoryViewModel, Category>();
            CreateMap<FavouriteViewModel, Favourite>();
            CreateMap<BranchViewModel, CategoryDetailsBranche>();
            CreateMap<CategoryDetailsBranche, BranchViewModel>();
            CreateMap<CategoryDetailsFaq, QuestionViewModel> ();
            CreateMap<City, CityModel> ();
            CreateMap<Country, CountryModel> ();
            CreateMap<Municipality, MunicipalityModel> ();
        }

    }
}