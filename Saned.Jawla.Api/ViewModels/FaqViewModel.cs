﻿using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class FaqViewModel
    {
        [Required]
        public long CategoryDetailsId { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public string Answer { get; set; }
    }
}