using System;

namespace Saned.Jawla.Api.ViewModels
{
    public class GetDetailsViewModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long Id { get; set; }
        public string UserId = null;
        public string DeviceId { get; set; } = "0";
        
    }
}