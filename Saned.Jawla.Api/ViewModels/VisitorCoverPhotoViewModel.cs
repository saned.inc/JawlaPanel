﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class VisitorCoverPhotoViewModel
    {
        public VisitorCoverPhotoViewModel()
        {
            Images=new List<string>();
        }
        [Required]
        public int PhotoId { get; set; }
        public string UserId { get; set; }
        [Required]
        public long CategoryDetailsId { get; set; }
        public List<string> Images { get; set; }
    }
}