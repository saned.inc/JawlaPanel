﻿using Saned.Jawla.Data.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saned.Jawla.Api.ViewModels
{
    public class FeatureProjectViewModel : CategoryDetailsViewModel
    {
        public string AttachmentUrl;
        public string Owner { get; set; }

  

        public int CoverId { get; set; }
        public List<CategoryDetailsContact> Contacts { get; set; }

        public List<CategoryDetailsContact> OwnerContacts { get; set; }
        public string Idea { get; set; }
    }
}