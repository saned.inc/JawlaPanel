using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class ApplicationContactInformationViewModel
    {

       
   

        public int Id { get; set; }
       
        public string PhoneNumber { get; set; }
       
        public string Email { get; set; }
       
        public string FacebookUrl { get; set; }
       
        public string TwitterUrl { get; set; }
       
        public string InstagrameUrl { get; set; }
       
        public string YoutubeUrl { get; set; }
       
        public string GooglePlusUrl { get; set; }
       
        public string LinkedInUrl { get; set; }

        public string AboutUs { get; set; }

    }
}