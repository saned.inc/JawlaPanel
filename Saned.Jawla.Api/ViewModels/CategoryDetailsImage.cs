namespace Saned.Jawla.Api.ViewModels
{
    public class CategoryDetailsImage
    {
        public string ImageUrl { get; set; }
        public bool IsDefault { get; set; }
    }
}