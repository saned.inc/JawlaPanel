using System.Collections;
using System.Collections.Generic;
using Saned.Jawla.Data.Core.Dtos;

namespace Saned.Jawla.Api.ViewModels
{
    public class Ambassador
    {
        public Ambassador()
        {
            Items=new List<SelectItemDto>();
        }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string PhotoUrl { get; set; }
     

        public IList Items { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string SoicalLinks { get; set; }
    }
}