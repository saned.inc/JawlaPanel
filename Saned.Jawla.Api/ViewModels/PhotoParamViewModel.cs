namespace Saned.Jawla.Api.ViewModels
{
    public class PhotoParamViewModel
    {
        public int Id { get; set; }
        public string RelatedId { get; set; }
        public int RelatedType { get; set; }
        public string Lang { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
}