using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class BrancheViewModel
    {
        [Required]
        public long CategoryDetailsId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public int? CountryId { get; set; }
        [Required]
        public int? CityId { get; set; }
        [Required]
        public int? MunicipalityId { get; set; }
        [Required]
        public string Latitude { get; set; }
        [Required]
        public string Longitude { get; set; }
    }

    public class CategoryNameViewModel
    {
        public int? CategoryId { get; set; }
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public int? MunicipalityId { get; set; }
        public int? SubCategoryId { get; set; }
    }
}