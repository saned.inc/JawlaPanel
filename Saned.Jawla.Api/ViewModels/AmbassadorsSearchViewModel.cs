namespace Saned.Jawla.Api.ViewModels
{
    public class AmbassadorsSearchViewModel
    {
        
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public long AmbassadorId { get; set; }
    }
}