﻿using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class UserProfileViewModel

    {
        [Required(ErrorMessage = "اسم المستخدم مطلوب")]
        public string Name { get; set; }
        [Required(ErrorMessage = "عمر المستخدم مطلوب")]
        public int Age { get; set; }
        [Required(ErrorMessage = "عنوان المستخدم مطلوب")]
        public string Address { get; set; }
        [Required(ErrorMessage = "رقم الهاتف مطلوب")]
        public string PhoneNumber { get; set; }

    }

    public class UserPhotoViewModel

    {
        [Required(ErrorMessage = "صورة المستخدم مطلوب")]

        public string Picture { get; set; }

    }



}