﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Saned.Jawla.Api.ViewModels
{
    public class CategoryDetailsSearchViewModel
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 8;
        public int? CountryId { get; set; }

        public int? CityId { get; set; }
        public int? MunicipalityId { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public long? CategoryDetailsId  { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public DateTime? EventStatyDate { get; set; } = null;

        public int? ParentCategoryId { get; set; }

    }

    public class VisitorCoverListViewModel
    {
        [Required]
        public int PageNumber { get; set; } = 1;
        [Required]
        public int PageSize { get; set; } = 8;
        public string UserId { get; set; }
    }

    public class VisitorCoverDetailsViewModel
    {
        [Required]
        public long Id { get; set; }
        public string UserId { get; set; }

    }
}