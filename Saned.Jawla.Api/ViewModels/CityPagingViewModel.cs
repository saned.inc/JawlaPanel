﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saned.Jawla.Api.ViewModels
{
    public class CityPagingViewModel
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int countryId { get; set; }
    }
}