﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Saned.Jawla.Api.ViewModels
{
    public class VisitorCoverViewModel
    {
        public string UserId { get; set; }
        [Required]
        public long CategoryDetailsId { get; set; }

        public List<string> Images { get; set; }
    }
    public class CoverPhotoViewModel
    {
        public string UserId { get; set; }
        [Required]
        public long CategoryDetailsId { get; set; }
        [Required]
        public string Ids { get; set; }
    }
}