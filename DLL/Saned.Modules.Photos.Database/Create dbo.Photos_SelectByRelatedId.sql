﻿CREATE PROCEDURE Photos_SelectByRelatedId
	@RelatedId Nvarchar(Max),
	@RelatedTypeId INT,
	@Lang NVARCHAR(2)=NULL
AS
	SELECT [Id],[RelatedId],[RelatedTypeId],[PhotoUrl],[Lang],[IsDefault],[ModifiedDate],[Title]
	FROM [dbo].[Photos]
	WHERE RelatedId=@RelatedId AND [RelatedTypeId]=@RelatedTypeId AND Lang=ISNULL(@Lang,Lang)
