﻿CREATE PROCEDURE [dbo].[Photo_Update]
    @Id [int],
    @RelatedId [nvarchar](max),
    @RelatedTypeId [int],
    @PhotoUrl [nvarchar](2048),
    @Lang [nvarchar](2),
    @IsDefault [bit],
    @ModifiedDate [datetime],
    @Title [nvarchar](150)
AS
BEGIN
    UPDATE [dbo].[Photos]
    SET [RelatedId] = @RelatedId, [RelatedTypeId] = @RelatedTypeId, [PhotoUrl] = @PhotoUrl, [Lang] = @Lang, [IsDefault] = @IsDefault, [ModifiedDate] = @ModifiedDate, [Title] = @Title
    WHERE ([Id] = @Id)
END
