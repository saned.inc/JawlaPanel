﻿Create PROCEDURE [dbo].[Photo_Insert]
    @RelatedId [nvarchar](max),
    @RelatedTypeId [int],
    @PhotoUrl [nvarchar](2048),
    @Lang [nvarchar](2),
    @IsDefault [bit],
    @ModifiedDate [datetime],
    @Title [nvarchar](150)
AS
BEGIN
    INSERT [dbo].[Photos]([RelatedId], [RelatedTypeId], [PhotoUrl], [Lang], [IsDefault], [ModifiedDate], [Title])
    VALUES (@RelatedId, @RelatedTypeId, @PhotoUrl, @Lang, @IsDefault, @ModifiedDate, @Title)
    
    DECLARE @Id int
    SELECT @Id = [Id]
    FROM [dbo].[Photos]
    WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
    
    SELECT t0.[Id]
    FROM [dbo].[Photos] AS t0
    WHERE @@ROWCOUNT > 0 AND t0.[Id] = @Id
END
