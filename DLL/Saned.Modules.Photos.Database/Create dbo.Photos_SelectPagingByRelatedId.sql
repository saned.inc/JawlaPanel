﻿CREATE PROCEDURE Photos_SelectPagingByRelatedId
	@RelatedId Nvarchar(Max),
	@RelatedTypeId INT,
	@Lang NVARCHAR(2)=NULL,
	@PageNumber INT,
	@PageSize INT

AS
	SELECT [Id],[RelatedId],[RelatedTypeId],[PhotoUrl],[Lang],[IsDefault],[ModifiedDate],[Title], OverAllCount = COUNT(1) OVER()
	FROM [dbo].[Photos]
	WHERE RelatedId=@RelatedId AND [RelatedTypeId]=@RelatedTypeId AND Lang=ISNULL(@Lang,Lang)
	ORDER BY  [ModifiedDate] Desc
    OFFSET @PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)
