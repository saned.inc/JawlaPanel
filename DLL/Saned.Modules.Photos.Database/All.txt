--CREATE PhotoTable
CREATE TABLE [dbo].[Photos] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [RelatedId]     NVARCHAR (MAX)  NOT NULL,
    [RelatedTypeId] INT             NOT NULL,
    [PhotoUrl]      NVARCHAR (2048) NOT NULL,
    [Lang]          NVARCHAR (2)    NULL,
    [IsDefault]     BIT             NOT NULL,
    [ModifiedDate]  DATETIME        NULL,
    [Title]         NVARCHAR (150)  NULL
);

GO
Create PROCEDURE [dbo].[Photo_Insert]
    @RelatedId [nvarchar](max),
    @RelatedTypeId [int],
    @PhotoUrl [nvarchar](2048),
    @Lang [nvarchar](2),
    @IsDefault [bit],
    @ModifiedDate [datetime],
    @Title [nvarchar](150)
AS
BEGIN
    INSERT [dbo].[Photos]([RelatedId], [RelatedTypeId], [PhotoUrl], [Lang], [IsDefault], [ModifiedDate], [Title])
    VALUES (@RelatedId, @RelatedTypeId, @PhotoUrl, @Lang, @IsDefault, @ModifiedDate, @Title)
    
    DECLARE @Id int
    SELECT @Id = [Id]
    FROM [dbo].[Photos]
    WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
    
    SELECT t0.[Id]
    FROM [dbo].[Photos] AS t0
    WHERE @@ROWCOUNT > 0 AND t0.[Id] = @Id
END
GO
CREATE PROCEDURE Photo_SelectById
	@Id INT 
AS
	 SELECT
	 [Id],[RelatedId],[RelatedTypeId],[PhotoUrl],[Lang],[IsDefault],[ModifiedDate],[Title]
	 FROM [dbo].[Photos]
	 WHERE Id=@Id

GO
CREATE PROCEDURE [dbo].[Photo_Update]
    @Id [int],
    @RelatedId [nvarchar](max),
    @RelatedTypeId [int],
    @PhotoUrl [nvarchar](2048),
    @Lang [nvarchar](2),
    @IsDefault [bit],
    @ModifiedDate [datetime],
    @Title [nvarchar](150)
AS
BEGIN
    UPDATE [dbo].[Photos]
    SET [RelatedId] = @RelatedId, [RelatedTypeId] = @RelatedTypeId, [PhotoUrl] = @PhotoUrl, [Lang] = @Lang, [IsDefault] = @IsDefault, [ModifiedDate] = @ModifiedDate, [Title] = @Title
    WHERE ([Id] = @Id)
END
GO
CREATE PROCEDURE Photos_SelectByRelatedId
	@RelatedId Nvarchar(Max),
	@RelatedTypeId INT,
	@Lang NVARCHAR(2)=NULL
AS
	SELECT [Id],[RelatedId],[RelatedTypeId],[PhotoUrl],[Lang],[IsDefault],[ModifiedDate],[Title]
	FROM [dbo].[Photos]
	WHERE RelatedId=@RelatedId AND [RelatedTypeId]=@RelatedTypeId AND Lang=ISNULL(@Lang,Lang)

GO
CREATE PROCEDURE Photos_SelectPagingByRelatedId
	@RelatedId Nvarchar(Max),
	@RelatedTypeId INT,
	@Lang NVARCHAR(2)=NULL,
	@PageNumber INT,
	@PageSize INT

AS
	SELECT [Id],[RelatedId],[RelatedTypeId],[PhotoUrl],[Lang],[IsDefault],[ModifiedDate],[Title], OverAllCount = COUNT(1) OVER()
	FROM [dbo].[Photos]
	WHERE RelatedId=@RelatedId AND [RelatedTypeId]=@RelatedTypeId AND Lang=ISNULL(@Lang,Lang)
	ORDER BY  [ModifiedDate] Desc
    OFFSET @PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)
