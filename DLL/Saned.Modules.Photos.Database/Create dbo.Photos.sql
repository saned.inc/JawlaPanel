﻿--CREATE PhotoTable
CREATE TABLE [dbo].[Photos] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [RelatedId]     NVARCHAR (MAX)  NOT NULL,
    [RelatedTypeId] INT             NOT NULL,
    [PhotoUrl]      NVARCHAR (2048) NOT NULL,
    [Lang]          NVARCHAR (2)    NULL,
    [IsDefault]     BIT             NOT NULL,
    [ModifiedDate]  DATETIME        NULL,
    [Title]         NVARCHAR (150)  NULL
);


