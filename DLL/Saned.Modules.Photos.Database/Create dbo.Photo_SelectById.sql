﻿CREATE PROCEDURE Photo_SelectById
	@Id INT 
AS
	 SELECT
	 [Id],[RelatedId],[RelatedTypeId],[PhotoUrl],[Lang],[IsDefault],[ModifiedDate],[Title]
	 FROM [dbo].[Photos]
	 WHERE Id=@Id
