 function dynamicSort(property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
 }

 function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] == obj) {
            return true;
        }
    }
    return false;
}

function containsById(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i].id == obj) {
            return true;
        }
    }
    return false;
}
 //function sortFn() {
 //    var sortByProps = Array.prototype.slice.call(arguments),
 //        cmpFn = function(left, right, sortOrder) {
 //            var sortMultiplier = sortOrder === "asc" ? 1 : -1;

 //            if (left > right) {
 //                return +1 * sortMultiplier;
 //            }
 //            if (left < right) {
 //                return -1 * sortMultiplier;
 //            }
 //            return 0;
 //        };
 //    return function(sortLeft, sortRight) {
 //        // get value from object by complex key
 //        var getValueByStr = function(obj, path) {
 //            var i, len;

 //            //prepare keys
 //            path = path.replace('[', '.');
 //            path = path.replace(']', '');
 //            path = path.split('.');

 //            len = path.length;

 //            for (i = 0; i < len; i++) {
 //                if (!obj || typeof obj !== 'object') {
 //                    return obj;
 //                }
 //                obj = obj[path[i]];
 //            }

 //            return obj;    };   return sortByProps.map(function(property) {
 //                return cmpFn(getValueByStr(sortLeft, property.prop), getValueByStr(sortRight, property.prop), property.sortOrder);
 //            }).reduceRight(function(left, right) {
 //                return right || left;
 //            });
 //    };
 //}
