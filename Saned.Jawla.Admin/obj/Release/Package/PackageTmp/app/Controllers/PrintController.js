﻿(function () {

    var PrintController = function ($scope, $localStorage, $sce, print) {

        if ($localStorage.reportData) {

            var reportData = JSON.parse($localStorage.reportData);
            //alert($localStorage.reportData);
            //alert(reportData);
            var uri = reportData.uri;
            var params = reportData.params;

            //$scope.test = $localStorage.LocalMessage;

            print.getPdf(uri, params).success(function (response) {

                var file = new Blob([response], { type: 'application/pdf' });
                var fileUrl = URL.createObjectURL(file);

                $scope.pdfData = $sce.trustAsResourceUrl(fileUrl);

            }).error(function (response) {
                var status = response.status;
                var data = response.data;
            });

        }

    };

    var module = angular.module("App");
    module.controller("PrintController", ["$scope", "$localStorage", "$sce", "print", PrintController]);

}());