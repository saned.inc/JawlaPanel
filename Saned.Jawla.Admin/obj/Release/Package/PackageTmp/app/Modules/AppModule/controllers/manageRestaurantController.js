﻿
(function () {
    var manageRestaurantController = function ($rootScope,
        $scope,
        $q,
        $routeParams,
        notify,
        restaurantsSvc,
        countrySvc,
        citySvc,
        municipalitySvc, $filter, datashare, $bootbox, $location, categorySvc) {


        $scope.$watch('showbranchform', function (newValue, oldValue) {
            if (newValue !== oldValue) {
                if (newValue) {
                    $('#restaurant-map').css('visibility', 'visible');
                    $('#pac-input').text('');
                } else {
                    $('#restaurant-map').css('visibility', 'hidden');
                }
            }
        });

        categorySvc.getCategoryByParentId(1).then(function (response) {
            $scope.childCategories = response.data;
        });


        $scope.uploadedImageWidth = 0;
        $scope.uploadedImageHeight = 0;
        $scope.imageChange = function (e) {

            var file, img, adsFU;
            adsFU = $('#adsFU');
            var _URL = window.URL || window.webkitURL;
            if ((file = adsFU[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    debugger;
                    //alert(this.width + " " + this.height);
                    $scope.uploadedImageWidth = this.width;
                    $scope.uploadedImageHeight = this.height;
                    $scope.$apply();
                    $scope.$apply();
                };
                img.onerror = function () {
                    //alert("not a valid file: " + file.type);
                    $scope.uploadedImageWidth = 0;
                    $scope.uploadedImageHeight = 0
                    $scope.$apply()
                };
                img.src = _URL.createObjectURL(file);


            }

        }

        /*---------------Init------------------------*/
        $scope.title = "اضافة / تعديل مطعم";
        $scope.FormDetails = {};
        $scope.model = {};
        $scope.FormContacts = {};
        $scope.Contact = {};
        $scope.branch = {};
        $scope.question = {};
        $scope.branch.latitude = 24.713552;
        $scope.branch.longitude = 46.675296;
        $scope.model.branches = [];
        $scope.model.questions = [];
        $scope.FormImages = {};
        $scope.images = {};
        $scope.Map = {};
        $scope.FormMap = {};
        $scope.Question = {};
        $scope.FormQuestion = {};
        $scope.branchesIsEmpty = true;
        $scope.questinsIsEmpty = true;

        $scope.removeImageUrl = function () {
            $scope.model.attachmentUrl = '';
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
            $scope.model.coverId = 0;
            $scope.$apply();
        }

        //#region questions

        $scope.Addquestion = function (question) {

            question["id"] = 0;
            if (angular.isUndefined($scope.model.questions) || $scope.model.questions === null)
                $scope.model.questions = [];
            $scope.model.questions.push(question);
            $scope.question = {};
            $scope.showquestionform = false;
            $scope.questinsIsEmpty = false;
            $scope.FormQuestion.Question.$setPristine();
        }

        $scope.deletequestion = function (index) {
            $scope.model.questions.splice(index, 1);
            $scope.$apply();

            if ($scope.model.branches.length <= 0)
                $scope.questinsIsEmpty = true;
        }

        $scope.Editquestion = function (index) {
            $scope.showquestionform = true;
            $scope.question = $scope.model.questions[index];
            $scope.addquestionMode = false;
        }

        $scope.openConfirmQuestion = function (index) {

            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    $scope.deletequestion(index);
                }
            });
        };
        $scope.clearquestionform = function () {
            $scope.showquestionform = false;
            $scope.FormQuestion.Question.$setPristine();

        }

        $scope.showquestionform = false;
        $scope.SwitchquestionMode = function (mode) {
            $scope.addquestionMode = mode;
            $scope.showquestionform = true;
            if (mode)
                $scope.Question = {};

        }
        //#endregion 


        //#region branches
        $scope.AddBranch = function (branch) {
            debugger;
            branch.latitude = $('#maplat').val();
            branch.longitude = $('#maplng').val();

            $scope.disabledMunicipalityDrp = true;
            $scope.disabledCityDrp = true;
            branch["id"] = 0;
            $scope.model.branches.push(branch);
            $scope.branch = {};
            $scope.branch.latitude = 24.713552;
            $scope.branch.longitude = 46.675296;
            $scope.showbranchform = false;
            $scope.branchesIsEmpty = false;
            //$scope.dirOptions.DeleteMarkers();

            clearOverlays();

            var myLatLng = new google.maps.LatLng(24.713552, 46.675296);

            map.setCenter(myLatLng);

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!',
                zoom: 4,
            });

            markersArray.push(marker);

            $scope.FormMap.Map.$setPristine();
        }

        $scope.deletebranch = function (index) {
            $scope.model.branches.splice(index, 1);
            if ($scope.model.branches.length <= 0)
                $scope.branchesIsEmpty = true;

        }

        $scope.Editbranch = function (index) {
            debugger;
            $scope.branch = $scope.model.branches[index];
            $scope.branch.index = index;
            $scope.branch.country = $filter('filter')($scope.countries, { id: $scope.branch.country.id })[0];
            $scope.onCountryChange($scope.branch.country.id);
            $scope.showbranchform = true;
            $scope.disabledCityDrp = true;
            $scope.addBranchMode = false;
            $scope.disabledMunicipalityDrp = true;


            var lat = $('#maplat');
            lat.val($scope.branch.latitude);
            lat.trigger('input');
            lat.trigger('change');

            var lng = $('#maplng');
            lng.val($scope.branch.longitude);
            lng.trigger('input');
            lng.trigger('change');

            clearOverlays();

            var myLatLng = new google.maps.LatLng($scope.branch.latitude, $scope.branch.longitude);

            map.setCenter(myLatLng);

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!',
                zoom: 4,
            });


            markersArray.push(marker);


        }


        $scope.dirOptions = {};
        $scope.placeMarker = function () {
            $scope.dirOptions.PlaceMarker($scope.branch.latitude, $scope.branch.longitude);
        };


        $scope.openConfirm = function (index) {

            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    $scope.deletebranch(index);
                }
            });
        };
        $scope.clearbranchform = function () {

            debugger;
            $scope.showbranchform = false;

            if ($scope.model.branches[$scope.branch.index]) {
                $scope.branch.latitude = $('#maplat').val();
                $scope.branch.longitude = $('#maplng').val();
                $scope.model.branches[$scope.branch.index].latitude = $scope.branch.latitude;
                $scope.model.branches[$scope.branch.index].longitude = $scope.branch.longitude;
            }




            //$scope.branch.latitude = 24.713552;
            //$scope.branch.longitude = 46.675296;

            clearOverlays();

            var myLatLng = new google.maps.LatLng(24.713552, 46.675296);

            map.setCenter(myLatLng);

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!',
                zoom: 4,
            });

            markersArray.push(marker);





        }

        $scope.showbranchform = false;
        $scope.SwitchMode = function (mode) {
            $scope.addBranchMode = mode;
            //$scope.dirOptions.DeleteMarkers();
            //$('#maplat').val('24.713552');
            //$('#maplng').val('46.675296');
            $scope.showbranchform = true;
            if (mode) {
                $scope.branch = {};
                $scope.branch.latitude = 24.713552;
                $scope.branch.longitude = 46.675296;
            }

        }


        $scope.onChangeMainbranch = function () {
            if ($scope.branch.IsMainbranch)
                $scope.branch.name = $scope.model.name;

        }
        //#endregion





        // #region loading_DropList

        $scope.onCityChange = function (id) {
            if (angular.isUndefined(id))
                return;
            municipalitySvc.returnAllByCityId(id).then(function (data) {

                $scope.disabledMunicipalityDrp = false;
                $scope.Municipalities = [];
                $scope.Municipalities = data.data;
                if (!angular.isUndefined($scope.branch.municipality)) {
                    $scope.branch.municipality = $filter('filter')($scope.Municipalities, { id: $scope.branch.municipality.id })[0];

                }
            });
        }
        $scope.onCountryChange = function (id) {
            if (angular.isUndefined(id))
                return;
            citySvc.returnAllByCountryIdwithArabic(id).then(function (data) {

                $scope.disabledCityDrp = false;
                $scope.cities = [];
                $scope.cities = data.data;
                if (!angular.isUndefined($scope.branch.city)) {
                    $scope.onCityChange($scope.branch.city.id);
                    $scope.branch.city = $filter('filter')($scope.cities, { id: $scope.branch.city.id })[0];
                }

            });
        }

        $scope.disabledCityDrp = true;
        $scope.disabledMunicipalityDrp = true;

        $scope.loading_DropList = function () {
            countrySvc.returnAllwithArabicName().then(function (data) {
                $scope.countries = data.data;
                $scope.LoadData();


            });

        }






        // #endregion

        // #region DatePickers
        $scope.today = function () {
            $scope.dt = new Date();
        };


        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.openStart = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();
            $scope.model["openedStart"] = !$scope.model["openedStart"];
        };
        $scope.openEnd = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();
            $scope.model["openedEnd"] = !$scope.model["openedEnd"];
        };

        //$scope.disabled = function (date, mode) {
        //    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        //};

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        // $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


        $scope.OnChangeEndDate = function () {
            var endDate = new Date($scope.model.endDate);
            var startDate = new Date($scope.model.eventStartDate);
            $scope.FormDetails.model.endDate.$setValidity("endBeforeStart", endDate >= startDate);
        }

        $scope.OnChangeStartDate = function () {

            var endDate = new Date($scope.model.endDate);
            var startDate = new Date($scope.model.eventStartDate);
            if (endDate == 'Invalid Date')
                $scope.FormDetails.model.eventStartDate.$setValidity("startbeforeEnd", true);
            else
                $scope.FormDetails.model.eventStartDate.$setValidity("startbeforeEnd", startDate <= endDate);
        }



        // #endregion


        // #region SaveVsUpdate





        $scope.isAdd = true;
        $scope.LoadData = function () {
            restaurantsSvc.get($routeParams.id)
                      .then(function (response) {

                          $scope.model = response.data;
                          $scope.model.startTime = new Date($scope.model.startTime);
                          $scope.model.endTime = new Date($scope.model.endTime);
                          $scope.branchesIsEmpty = false;
                          $scope.questinsIsEmpty = false;
                          $scope.model.workHours = $scope.model.workHours;
                      }, function (response) {
                          var status = response.status;
                          var data = response.data;

                          if (status === 500)
                              alert(JSON.stringify(data));
                      });
        }

        $scope.routeParam = $routeParams.id;
        if ($scope.routeParam) {
            $scope.isAdd = false;
            //#region All Promise
            $q.all([countrySvc.returnAllwithArabicName(), restaurantsSvc.get($routeParams.id)]).then(function (values) {
                $scope.countries = values[0].data;
                $scope.model = values[1].data;
                $scope.model.startTime = new Date($scope.model.startTime);
                $scope.model.endTime = new Date($scope.model.endTime);
                $scope.branchesIsEmpty = false;
                $scope.questinsIsEmpty = false;
                $scope.model.workHours = $scope.model.workHours;
            }, function (response) {
                notify.success("حدث خطأ أثناء الاتصال بالخادم . اعد تحميل الصفحة.");
            });
        }
        else {
            countrySvc.returnAllwithArabicName().then(function (data) {
                $scope.countries = data.data;
                // $scope.LoadData();
                //$scope.branch.latitude = 24.713552;
                //$scope.branch.longitude = 46.675296;
            });
        }
        //#endregion 









        $scope.save = function () {

            $scope.model["imageFilename"] = angular.isUndefined($scope.model.attachmentViewModel)
                ? ""
                : $scope.model.attachmentViewModel.filename;
            $scope.model["imageBase64"] = angular.isUndefined($scope.model.attachmentViewModel)
                ? ""
                : $scope.model.attachmentViewModel.base64;

            var selected = $filter('filter')($scope.model.branches, { IsMainbranch: true })[0];
            if (angular.isUndefined(selected)) {
                selected = $scope.model.branches[0];
            }
            $scope.model.cityId = selected.city.id;
            $scope.model.countryId = selected.country.id;
            if (selected.municipality) {
                $scope.model.municipalityId = selected.municipality.id;
            } else {
                $scope.model.municipalityId = null;
            }

            $scope.model["longitude"] = selected.longitude;
            $scope.model["latitude"] = selected.latitude;


            if ($scope.isAdd) {
                restaurantsSvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        $scope.clearFormsVsGotofirststep();
                        $scope.gotoStep(1);
                        notify.success($rootScope.transaction.NotifySuccess);
                        $location.url('/restaurants');
                    }

                },
                    function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            alert(JSON.stringify(data));
                        }


                        if (status === 409) {
                            alert($scope.toasterWarning);
                        } else if (status === 500)
                            alert(JSON.stringify(data));
                    });
            } else
                restaurantsSvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.gotoStep(1);
                        $location.url('/restaurants');

                    }

                },
                    function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 409) {
                            notify.error("not updated there is an error");
                        } else if (status === 500)
                            alert(JSON.stringify(data));
                    });

        }







        $scope.clearFormsVsGotofirststep = function () {

            $scope.model = {};
            $scope.model.branches = [];
            $scope.model.questions = [];
            $scope.branchesIsEmpty = true;
            $scope.questinsIsEmpty = true;
            $scope.images = {};
            $scope.Map = {};
            $scope.gotoStep(1);
        };

        $scope.function = function () {
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
        }



        // #endregion


        // #region Tabs
        $scope.currentStep = 1;
        $scope.steps = [
            {
                step: 1,
                name: "البيانات الاساسية",
                template: "/app/Modules/AppModule/views/Restaurant/Edit.html"

            },
             {
                 step: 2,
                 name: "الفروع",
                 template: "/app/Modules/AppModule/views/Restaurant/Map.html"
             },
            {
                step: 3,
                name: "الصور",
                template: "/app/Modules/AppModule/views/Restaurant/images.html"
            },
             {
                 step: 4,
                 name: "حسابات التواصل",
                 template: "/app/Modules/AppModule/views/Restaurant/Contacts.html"
             },
            {
                step: 5,
                name: "سؤال وجواب",
                template: "/app/Modules/AppModule/views/Restaurant/questionVsAnswers.html"
            }

        ];
        $scope.user = {};
        $scope.getFormTemplateValidation = function () {


            //return false;
            //|| (model.attachmentViewModel == undefined && model.photoUrl == undefined) 
            if ($scope.currentStep === 1) {
                if (!angular.isUndefined($scope.FormDetails.model)) {
                    if (!($scope.FormDetails.model.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormDetails.model.$invalid;
                    } else return true;
                } else {
                    return true;
                }
            }

            else if ($scope.currentStep === 2) {
                if (!angular.isUndefined($scope.model.branches)) {
                    if ((!$scope.branchesIsEmpty && $scope.currentStep <= $scope.steps.length)) {
                        return false;
                    }
                    else return true;
                }
                else
                    return true;
            }

            else if ($scope.currentStep === 3) {
                //if (!angular.isUndefined($scope.FormImages.images)) {
                //    if (!($scope.FormImages.images.$invalid && $scope.currentStep >= $scope.steps.length)) {
                //        return $scope.FormImages.images.$invalid;
                //    }
                //    else return true;
                //}


                if (!angular.isUndefined($scope.model.imagesList)) {
                    //    if (!($scope.model.imagesList.length > 0 && $scope.currentStep >= $scope.steps.length)) {
                    //        return $scope.model.imagesList.length <= 0;
                    //    }
                    //}
                    //else
                    //    return true;

                    return false;
                }
            }

            else if ($scope.currentStep === 4) {
                if (!angular.isUndefined($scope.FormContacts.Contact)) {
                    if (!($scope.FormContacts.Contact.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormContacts.Contact.$invalid;
                    }
                    else return true;
                }
                else
                    return true;
            }
            else
                return true;
        }
        $scope.gotoStep = function (newStep) {
            $scope.currentStep = newStep;
        }
        $scope.getStepTemplate = function () {
            for (var i = 0; i < $scope.steps.length; i++) {
                if ($scope.currentStep == $scope.steps[i].step) {
                    return $scope.steps[i].template;
                }
            }
        }
        // #endregion


        $scope.delteimage = function () {
            //if ($scope.model.attachmentViewModel) {
            //    $scope.model.attachmentViewModel.attachmentUrl = undefined;
            //    $scope.model.attachmentId = 0;
            //    $scope.model.attachmentViewModel = undefined;
            //}
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
        }

    };

    var module = angular.module("AppModule");
    module.controller("manageRestaurantController", ["$rootScope", "$scope", "$q", "$routeParams", "notify", "restaurantsSvc", "countrySvc", "citySvc", "municipalitySvc", "$filter", "datashare", "$bootbox", "$location", "categorySvc", manageRestaurantController]);

}());





