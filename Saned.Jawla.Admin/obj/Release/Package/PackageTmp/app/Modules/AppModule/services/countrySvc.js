﻿(function () {

    var countrySvc = function ($http, $q, $rootScope, ngAuthSettings) {
       
        var api = ngAuthSettings.apiServiceBaseUri;

        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Country/GetCountries").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse); 
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }


        var returnAllwithArabicName= function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Country/GetCountrieswithArabicName").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }


        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            var model = { pageIndex: currentPage, pageSize: pageSize };
            $http.post(api + "/api/Country/GetAllCountriesWithPaging", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (countryId, currentPage, pageSize) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            var model = { 
            pageIndex: currentPage,
            pageSize: pageSize,
            keyword: countryId
            };
            $http.post(api + "/api/Country/SearchCountriesWithPaging", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(api + "/api/Country/GetById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(api+"/api/Country/CreateCountry", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api+"/api/Country/UpdateCountry", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(api + "/api/Country/DeleteById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            returnAllwithArabicName:returnAllwithArabicName,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            returnAll: returnAll
        }


    };

    var module = angular.module("AppModule");
    module.factory("countrySvc", ["$http", "$q", "$rootScope", "ngAuthSettings", countrySvc]);

}());