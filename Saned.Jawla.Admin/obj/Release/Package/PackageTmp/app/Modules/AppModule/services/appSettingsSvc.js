﻿


(function () {

    var appSettingsSvc = function ($http, $q) {


        var get = function () {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get("/api/AppSettings").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var setSubscriptionValue = function (value) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post("/api/AppSettings/SetSubscriptionValue/" + value).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        return {
            get: get,
            setSubscriptionValue: setSubscriptionValue
        }


    };

    var module = angular.module("AppModule");
    module.factory("appSettingsSvc", ["$http", "$q", "$rootScope", appSettingsSvc]);

}());