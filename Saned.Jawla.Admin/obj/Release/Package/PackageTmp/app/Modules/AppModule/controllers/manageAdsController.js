﻿(function () {
    var manageAdsController = function ($rootScope, $scope, $routeParams, $window, notify, adsSvc, ngAuthSettings) {
        debugger;
        $scope.uploadedImageWidth = 0;
        $scope.uploadedImageHeight = 0;

        $scope.removeImageUrl = function () {
            debugger;
            $scope.model.photoUrl = '';
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
            $scope.model.deleteImage = true;
            $scope.$apply();
        }

        $scope.imageChange = function (e) {

            var file, img, adsFU;
            adsFU = $('#adsFU');
            var _URL = window.URL || window.webkitURL;
            if ((file = adsFU[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    debugger;
                    //alert(this.width + " " + this.height);
                    $scope.uploadedImageWidth = this.width;
                    $scope.uploadedImageHeight = this.height;
                    $scope.$apply();
                    $scope.$apply();
                };
                img.onerror = function () {
                    //alert("not a valid file: " + file.type);
                    $scope.uploadedImageWidth = 0;
                    $scope.uploadedImageHeight = 0
                    $scope.$apply()
                };
                img.src = _URL.createObjectURL(file);


            }

        }
        /*---------------Init------------------------*/
        $scope.deletehidebutton = false;
        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true;
        $scope.isDataLoaded = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        $scope.model = {
            id: $scope.routeParam,
            attachmentViewModel: undefined
        }

        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0
            }

        } else {
            debugger;
            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            adsSvc.get($routeParams.id).then(function (response) {
                var status = response.status;
                var data = response.data;
                debugger;
                $scope.model = data;
                if ($scope.model.photoUrl) {
                    $scope.model.photoUrl = ngAuthSettings.imagesFolderUri + $scope.model.photoUrl;
                }
              
                if ($scope.model.attachmentUrl)
                    $scope.deletehidebutton = true;

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /*---------------Save------------------------*/
        $scope.save = function (form) {
            debugger;
            if ($scope.isAdd) {
                $scope.model.id = 0;
                adsSvc.add($scope.model).then(function (response) {
                    var status = response.status;

                    if (status === 200) {
                        document.getElementsByClassName('fileinput-preview')[0].innerHTML = "<img />";
                        $scope.model = {};
                        form.$setUntouched();
                        form.$setPristine();
                        notify.success($rootScope.transaction.NotifySuccess);
                        $window.location = '#/ads';
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        alert($scope.toasterWarning);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                adsSvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success($rootScope.transaction.NotifySuccess);
                        $window.location = '#/ads';
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        notify.error("not updated there is an error");
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
        }

        /*Image*/
        $scope.delteimage = function () {
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
            $scope.uploadedImageWidth = 0; $scope.uploadedImageHeight = 0;

        }
    };

    var module = angular.module("AppModule");
    module.controller("manageAdsController", ["$rootScope", "$scope", "$routeParams", '$window', "notify", "adsSvc", "ngAuthSettings", manageAdsController]);

}());