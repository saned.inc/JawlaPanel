﻿(function () {
    var countryController = function ($rootScope, $scope, $route, $bootbox, notify, $window, countrySvc,translation, appSettings) {

        debugger;
        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.errorMessage="هذه الدولة مدرج تحتها عدد من المدن";
        $scope.data = [];
        $scope.countryId = 0;
        /*---------------Search------------------------*/
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {
            if ($scope.model)
                $scope.getData($scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.countrySearch = function () {
            $scope.isDataLoading = true;
            $scope.currentPage = 1;
            countrySvc.search($scope.model.name, $scope.currentPage, $scope.pageSize).then(function (response) {
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;

            });

        }

        /*---------------Paging------------------------*/
      $scope.getData = function (currentPage, pageSize) {
         
          // $scope.currentPage, $scope.pageSize
          $scope.model = {
              pageIndex: currentPage,
              pageSize: pageSize
              //,Status:1
          };
          countrySvc.getAll(pageSize, currentPage).then(function (response) {
             
              $scope.data = response.data.data;
              $scope.totalData = response.data.totalCount;
              $scope.isDataLoading = false;

          });

        }

      /*  $scope.getAll = function (pageSize, currentPage) {
            countrySvc.getAll(pageSize, currentPage).then(function (response) {
                $scope.allContries = response.data;
            });
        }*/
      //  $scope.getAll($scope.currentPage, $scope.pageSize);

        $scope.getData($scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {
            debugger;
            $scope.getData(currentPage, pageSize);
        };

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    countrySvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;

                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.errorMessage);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };


        /* ------------- Sort function ------------ */
        $scope.sortKey = 'nameAr';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("countryController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "countrySvc", "translation", "appSettings", countryController]);

}());