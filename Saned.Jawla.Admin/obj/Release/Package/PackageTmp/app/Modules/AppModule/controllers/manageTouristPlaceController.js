﻿
(function () {
    var manageTouristPlaceController = function ($rootScope,
        $scope,
        $routeParams,
        notify,
        touristPlaceSvc,
        countrySvc,
        citySvc,
        municipalitySvc, $filter, datashare, $location, $bootbox, categorySvc) {

        categorySvc.getCategoryByParentId(2).then(function (response) {
            $scope.childCategories = response.data;
        });


        $scope.removeImageUrl = function () {
            debugger;
            $scope.model.attachmentUrl = '';
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }
            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;
            $scope.model.coverId = 0;
            $scope.$apply();
        }

        $scope.uploadedImageWidth = 0;
        $scope.uploadedImageHeight = 0;
        $scope.imageChange = function (e) {

            var file, img, adsFU;
            adsFU = $('#adsFU');
            var _URL = window.URL || window.webkitURL;
            if ((file = adsFU[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    debugger;
                    //alert(this.width + " " + this.height);
                    $scope.uploadedImageWidth = this.width;
                    $scope.uploadedImageHeight = this.height;
                    $scope.$apply();
                    $scope.$apply();
                };
                img.onerror = function () {
                    //alert("not a valid file: " + file.type);
                    $scope.uploadedImageWidth = 0;
                    $scope.uploadedImageHeight = 0
                    $scope.$apply()
                };
                img.src = _URL.createObjectURL(file);


            }

        }


        /*---------------Init------------------------*/
        $scope.FormDetails = {};
        $scope.FormContacts = {};
        $scope.Contact = {};
        $scope.model = {};
        $scope.model.questions = [];
        $scope.FormImages = {};
        $scope.images = {};
        $scope.Map = {};
        $scope.FormMap = {};
        $scope.Question = {};
        $scope.FormQuestion = {};
        $scope.questinsIsEmpty = true;
        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }

        //#region questions

        $scope.Addquestion = function (question) {

            question["id"] = 0;
            if (angular.isUndefined($scope.model.questions) || $scope.model.questions === null)
                $scope.model.questions = [];

            $scope.model.questions.push(question);
            $scope.question = {};
            $scope.showquestionform = false;
            $scope.questinsIsEmpty = false;
            $scope.FormQuestion.Question.$setPristine();
        }

        $scope.deletequestion = function (index) {
            $scope.model.questions.splice(index, 1);
            $scope.$apply();
            if ($scope.model.branches.length <= 0)
                $scope.questinsIsEmpty = true;
        }

        $scope.Editquestion = function (index) {
            $scope.showquestionform = true;
            $scope.question = $scope.model.questions[index];
            $scope.addquestionMode = false;
        }

        $scope.openConfirmQuestion = function (index) {

            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    $scope.deletequestion(index);
                }
            });
        };
        $scope.clearquestionform = function () {
            $scope.showquestionform = false;
            $scope.FormQuestion.Question.$setPristine();

        }

        $scope.showquestionform = false;
        $scope.SwitchquestionMode = function (mode) {
            $scope.addquestionMode = mode;
            $scope.showquestionform = true;
            if (mode)
                $scope.Question = {};

        }
        //#endregion 

        $scope.LoadData = function () {
            touristPlaceSvc.get($routeParams.id)
                      .then(function (response) {
                          var data = response.data;
                          $scope.model = data;
                          //$scope.model.startTime = new Date($scope.model.startTime);
                          //$scope.model.endTime = new Date($scope.model.endTime);
                          $scope.model.countryId = $filter('filter')($scope.countries, { id: data.countryId })[0];
                          $scope.onCountryChange(data.countryId);
                      }, function (response) {
                          var status = response.status;
                          var data = response.data;

                          if (status === 500)
                              alert(JSON.stringify(data));
                      });
        }


        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0,
                latitude: 24.713552,
                longitude: 46.675296,
                CategoryId: 2,
                questions: []

            }
            countrySvc.returnAll().then(function (data) {

                $scope.countries = data.data;
                $scope.disabledCityDrp = true;
                $scope.disabledMunicipalityDrp = true;


            });



        }
        else {

            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            touristPlaceSvc.get($routeParams.id).then(function (response) {

                var data = response.data;
                $scope.model = data;
                $scope.loading_DropList();
            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }

        // #region loading_DropList
        $scope.clearFormsVsGotofirststep = function () {

            $scope.model = {};
            $scope.images = {};
            $scope.Map = {};
            $scope.gotoStep(1);
        };
        $scope.onCityChange = function (city) {
            if (angular.isUndefined(city))
                return;
            if (city) {
                municipalitySvc.returnAllByCityId(city.id).then(function (data) {
                    $scope.disabledMunicipalityDrp = false;
                    $scope.Municipalities = [];
                    $scope.Municipalities = data.data;
                    $scope.model.municipalityId = $filter('filter')($scope.Municipalities, { id: $scope.model.municipalityId })[0];
                });
            }
        }
        $scope.onCountryChange = function (country) {
            if (angular.isUndefined(country))
                return;
            citySvc.returnAllByCountryId(country.id).then(function (data) {
                $scope.disabledCityDrp = false;
                $scope.cities = [];
                $scope.cities = data.data;
                $scope.model.cityId = $filter('filter')($scope.cities, { id: $scope.model.cityId })[0];
                $scope.onCityChange($scope.model.cityId);
            });
        }

        $scope.disabledCityDrp = true;
        $scope.disabledMunicipalityDrp = true;

        $scope.loading_DropList = function () {
            countrySvc.returnAll().then(function (data) {
                $scope.countries = data.data;

                $scope.LoadData();


            });

        }






        // #endregion

        // #region DatePickers
        $scope.today = function () {
            $scope.dt = new Date();
        };


        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.openStart = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();
            $scope.model["openedStart"] = !$scope.model["openedStart"];
        };
        $scope.openEnd = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();
            $scope.model["openedEnd"] = !$scope.model["openedEnd"];
        };

        //$scope.disabled = function (date, mode) {
        //    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        //};

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        // $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


        $scope.OnChangeEndDate = function () {
            var endDate = new Date($scope.model.endDate);
            var startDate = new Date($scope.model.eventStartDate);
            $scope.FormDetails.model.endDate.$setValidity("endBeforeStart", endDate >= startDate);
        }


        // #endregion





        ///*---------------Save------------------------*/  


        $scope.save = function () {
            debugger;
            $scope.model.latitude = $('#hdnLatitude').val();
            $scope.model.longitude = $('#hdnLongitude').val();

            $scope.model["imageFilename"] = angular.isUndefined($scope.model.attachmentViewModel) ? "" : $scope.model.attachmentViewModel.filename;
            $scope.model["imageBase64"] = angular.isUndefined($scope.model.attachmentViewModel) ? "" : $scope.model.attachmentViewModel.base64;



            $scope.model.cityId = $scope.model.cityId.id;
            $scope.model.countryId = $scope.model.countryId.id;
            if ($scope.model.municipalityId) {
                $scope.model.municipalityId = $scope.model.municipalityId.id;
            } else {
                $scope.model.municipalityId = null;
            }

            if ($scope.isAdd) {

                touristPlaceSvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        // $scope.clearFormsVsGotofirststep();
                        $scope.gotoStep(1);
                        notify.success($rootScope.transaction.NotifySuccess);
                        $location.url('/touristPlaces');
                    }

                },
                    function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            alert(JSON.stringify(data));
                        }


                        if (status === 409) {
                            alert($scope.toasterWarning);
                        } else if (status === 500)
                            alert(JSON.stringify(data));
                    });
            } else {

                touristPlaceSvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success($rootScope.transaction.NotifySuccess);
                        $location.url('/touristPlaces');
                    }

                },
                    function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 409) {
                            notify.error("not updated there is an error");
                        } else if (status === 500)
                            alert(JSON.stringify(data));
                    });
            }

        }






        $scope.delteimage = function () {
            if ($scope.model.attachmentViewModel) {
                $scope.model.attachmentViewModel.attachmentUrl = undefined;
                $scope.model.attachmentId = 0;
                $scope.model.attachmentViewModel = undefined;
            }

            $scope.uploadedImageWidth = 0;
            $scope.uploadedImageHeight = 0;

        }



        //Model
        $scope.currentStep = 1;
        $scope.steps = [
            {
                step: 1,
                name: "البيانات الاساسية",
                template: "/app/Modules/AppModule/views/touristPlace/Edit.html"

            },
            {
                step: 2,
                name: "الصور",
                template: "/app/Modules/AppModule/views/touristPlace/images.html"
            },
            {
                step: 3,
                name: "الموقع",
                template: "/app/Modules/AppModule/views/touristPlace/Map.html"
            }, {
                step: 4,
                name: "حسابات التواصل",
                template: "/app/Modules/AppModule/views/touristPlace/Contacts.html"
            },
             {
                 step: 5,
                 name: "سؤال وجواب",
                 template: "/app/Modules/AppModule/views/touristPlace/questionVsAnswers.html"
             }
        ];
        $scope.user = {};
        //step 1 validate Edit

        $scope.getFormTemplateValidation = function () {

            //|| (model.attachmentViewModel == undefined && model.photoUrl == undefined) 
            if ($scope.currentStep === 1) {
                if (!angular.isUndefined($scope.FormDetails.model)) {
                    if (!($scope.FormDetails.model.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormDetails.model.$invalid;
                    } else return true;
                } else {
                    return true;
                }
            } else if ($scope.currentStep === 2) {
                if (!angular.isUndefined($scope.FormImages.images)) {
                    if (!($scope.FormImages.images.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormImages.images.$invalid;
                    }
                    else return true;
                }
                else
                    return true;
            }
            else if ($scope.currentStep === 3) {

                if (!angular.isUndefined($scope.FormMap.Map)) {
                    if (!($scope.FormMap.Map.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormMap.Map.$invalid;
                    }
                    else return true;;
                }

                else {
                    return true;
                }
            }
            else if ($scope.currentStep === 4) {

                if (!angular.isUndefined($scope.FormContacts.Contact)) {
                    if (!($scope.FormContacts.Contact.$invalid && $scope.currentStep >= $scope.steps.length)) {
                        return $scope.FormContacts.Contact.$invalid;
                    } else return true;
                } else {
                    return true;
                }
            }




            else
                return true;
        }
        //Functions
        $scope.gotoStep = function (newStep) {


            $scope.currentStep = newStep;


        }



        $scope.getStepTemplate = function () {
            for (var i = 0; i < $scope.steps.length; i++) {
                if ($scope.currentStep == $scope.steps[i].step) {
                    return $scope.steps[i].template;
                }
            }
        }


    };

    var module = angular.module("AppModule");
    module.controller("manageTouristPlaceController", ["$rootScope", "$scope", "$routeParams", "notify", "touristPlaceSvc", "countrySvc", "citySvc", "municipalitySvc", "$filter", "datashare", "$location", "$bootbox","categorySvc", manageTouristPlaceController]);

}());





