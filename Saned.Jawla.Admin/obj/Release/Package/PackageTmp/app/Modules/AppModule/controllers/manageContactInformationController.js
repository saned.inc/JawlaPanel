﻿
(function () {
    var manageContactInformationController = function ($rootScope, $scope, $routeParams, notify, contactInformationSvc) {

        /*---------------Init------------------------*/
        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        $scope.model = {
            id: $scope.routeParam
        }

        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0
            }

        } else {

            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            debugger;
            contactInformationSvc.get($routeParams.id).then(function (response) {
                debugger;
                var data = response.data;
                debugger;
                $scope.model = data;
                debugger;

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /*---------------Save------------------------*/
        $scope.save = function (form) {

            if ($scope.isAdd) {
                contactInformationSvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {

                        $scope.model = {

                        };
                        form.$setUntouched();
                        form.$setPristine();

                        notify.success($rootScope.transaction.NotifySuccess);
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        alert($scope.toasterWarning);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                contactInformationSvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success("لقد تمت عملية التعديل بنجاح ");
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        notify.error("خطا لم تتم عملية التعديل");
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });

        }

        $scope.StringToNumber = function (value) {
            debugger;
            if (!angular.isUndefined(value)) {
                var intValue = parseInt(value);

                return intValue;
            }
            return 0;

        }

    };

    var module = angular.module("AppModule");
    module.controller("manageContactInformationController", ["$rootScope", "$scope", "$routeParams", "notify", "contactInformationSvc", manageContactInformationController]);

}());