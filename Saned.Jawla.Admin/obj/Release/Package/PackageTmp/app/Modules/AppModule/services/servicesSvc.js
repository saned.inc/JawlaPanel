﻿(function () {

    var servicesSvc = function ($http, $q) {
        // my function to test when API is up and running again 
        var returnAll = function () {
            $http.get("api/ContactUs/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
        }




        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Service/GetAll/" + pageSize + "/" + currentPage).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getAllByUserId = function (pageSize, currentPage, serviceId) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Service/GetAllByUserId/" + pageSize + "/" + currentPage + "/" + serviceId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var searchForAll = function (model, currentPage, pageSize, serviceId) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("api/Service/SearchForAll/" + pageSize + "/" + currentPage, model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (model, currentPage, pageSize, serviceId) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("api/Service/search/" + pageSize + "/" + currentPage + "/" + serviceId, model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get("/api/Service/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("/api/Service/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var getNextPage = function (pageSize, currentPageForComments, serviceId) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Service/getNextPage/" + pageSize + "/" + currentPageForComments + "/" + serviceId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getNextRatePage = function (pageSize, currentPageForRates, serviceId) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Service/GetNextRatePages/" + pageSize + "/" + currentPageForRates + "/" + serviceId).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        return {
            getAll: getAll,
            getAllByUserId: getAllByUserId,
            get: get,
            remove: remove,
            searchForAll: searchForAll,
            search: search,
            getNextPage: getNextPage,
            getNextRatePage: getNextRatePage
        }


    };

    var module = angular.module("AppModule");
    module.factory("servicesSvc", ["$http", "$q", "$rootScope", servicesSvc]);

}());