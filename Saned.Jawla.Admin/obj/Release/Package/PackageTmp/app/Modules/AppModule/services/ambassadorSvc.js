﻿(function () {

    var ambassadorSvc = function ($http, $q, ngAuthSettings) {
        var api = ngAuthSettings.apiServiceBaseUri;
        var getAll = function (name, pageSize, currentPage) {

            var deferred = $q.defer();
            var model =
                {
                    PageNumber: currentPage - 1,
                    PageSize: pageSize,
                    Name: name
                };
            $http.post(api + "/api/Ambassador/AdminSearchAmbassadorRequest", model).then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {

                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };
        var search = function (name, currentPage, pageSize) {

            var deferred = $q.defer();
            var model =
                {
                    PageNumber: currentPage - 1,
                    PageSize: pageSize,
                    Name: name
                };
            $http.post(api + "api/Ambassador/AdminSearchAmbassadorRequest", model).then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {

                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };
        var get = function (id) {
            var deferred = $q.defer();

            $http.get(api + "api/Ambassador/AdminGetById/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
                console.log(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            return deferred.promise;
        };

        var approve = function (requestId, duplicatedUserId) {

            var deferred = $q.defer();
            var model =
                {
                    requestId: requestId,
                    duplicatedUserId: duplicatedUserId
                };

            $http.post(api + "api/Ambassador/ApproveAmbassadors", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });


            return deferred.promise;
        }

        var reject = function (requestId) {

            var deferred = $q.defer();
            var model =
                {
                    requestId: requestId
                };

            $http.post(api + "api/Ambassador/RejectAmbassador", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });


            return deferred.promise;
        }

        return {
            getAll: getAll,
            get: get,
            search: search,
            approve: approve,
            reject: reject
        }
    };

    var module = angular.module("AppModule");
    module.factory("ambassadorSvc", ["$http", "$q", "ngAuthSettings", "$rootScope", ambassadorSvc]);

}());