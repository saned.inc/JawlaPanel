﻿(function () {

    var notify = function (pinesNotifications) {

        var success = function (text, title, hide) {

            if (title !== "undefined") {

                pinesNotifications.notify({
                    title: title,
                    text: text,
                    type: 'success',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            } else {
                pinesNotifications.notisfy({
                    text: text,
                    type: 'success',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            }

        };

        var warning = function (text, title, hide) {

            if (title !== "undefined") {

                pinesNotifications.notify({
                    title: title,
                    text: text,
                    type: 'warning',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            } else {
                pinesNotifications.notify({
                    title: title,
                    type: 'warning',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            }

        };

        var info = function (text, title, hide) {

            if (title !== "undefined") {

                pinesNotifications.notify({
                    title: title,
                    text: text,
                    type: 'info',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            } else {
                pinesNotifications.notify({
                    title: title,
                    type: 'info',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            }

        };

        var error = function (text, title, hide) {

            if (title !== "undefined") {

                pinesNotifications.notify({
                    title: title,
                    text: text,
                    type: 'error',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            } else {
                pinesNotifications.notify({
                    title: title,
                    type: 'error',
                    hide: typeof hide !== 'undefined' ? hide : true
                });
            }

        };

        return {
            success: success,
            info: info,
            warning: warning,
            error: error
        }

    };



    var module = angular.module("App");
    module.service("notify", ["pinesNotifications", notify]);

}());