﻿(function () {

    var print = function ($http) {

        function getPdf(uri, params) {
            return $http({
                method: 'GET',
                url: uri,
                params: params,
                responseType: 'arraybuffer'
            });
        }

        return {
            getPdf: getPdf
        }

    };

    var module = angular.module("App");
    module.service("print", ["$http", print]);

}());