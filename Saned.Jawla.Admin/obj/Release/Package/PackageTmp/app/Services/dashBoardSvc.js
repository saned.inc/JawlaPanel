﻿(function () {

    var dashBoardSvc = function ($http, $q) {

        var getAll = function () {
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/DashBoard").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
             return deferred.promise;
        };


        return {
            getAll: getAll
        }


    };

    var module = angular.module("App");
    module.factory("dashBoardSvc", ["$http", "$q", dashBoardSvc]);

}());