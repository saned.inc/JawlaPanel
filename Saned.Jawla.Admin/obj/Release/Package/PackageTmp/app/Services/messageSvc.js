﻿(function () {

    var messageSvc = function ($rootScope, $http, $q) {

        var getTopTen = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("/api/Message").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getAllNotSeenCount = function () {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("/api/Message/NotSeenCount").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var read = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Message/Read/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getAll = function (currentPage, pageSize) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Message/GetAll/" + pageSize + "/" + currentPage).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var remove = function (id) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("/api/Message/" + id).success(deferred.resolve).error(deferred.reject);
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getTopTen: getTopTen,
            read: read,
            getAll: getAll,
            remove: remove,
            getAllNotSeenCount: getAllNotSeenCount

        }


    };

    var module = angular.module("App");
    module.factory("messageSvc", ["$rootScope", "$http", "$q", messageSvc]);

}());