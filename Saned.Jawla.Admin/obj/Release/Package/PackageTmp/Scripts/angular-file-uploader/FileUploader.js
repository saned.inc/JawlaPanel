﻿/// <reference path="FileUploaderTemplate.html" />
// Note --> To Bind in edit mode 
// Ex: List="{{dbList}}"  -- dbList= ['url','url']
var app = angular.module('fileUploader', [])

.directive('fileUploader', function () {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: 'Scripts/angular-file-uploader/FileUploaderTemplate.html',

        scope: {
            ngModel: '='

        },
        link: function ($scope, element, attrs, ngModel, $rootscope) {
           
            $scope.SelectedImages = [];

            if (!$scope.ngModel)
                $scope.ngModel = [];

            $scope.fetchImage = function () {
                
                for (var i = 0; i < $scope.SelectedImages.length; i++) {
                   
                    if ($scope.SelectedImages[i].filetype.indexOf("image/") > -1 ) {
                        $scope.ngModel.push({
                            base64:$scope.SelectedImages[i].base64,
                            fileName: $scope.SelectedImages[i].filename
                        });
                        $scope.ngModel[i]['Index'] = i;
                       
                       // $scope.Attachments.push($scope.ngModel[i]);
                    }

                }
                //$scope.ngModel = $scope.Attachments;

            };

            $scope.removeSingle = function (index) {
                
                if ($scope.ngModel[index].id)
                    $scope.ngModel[index].remove = true;
                
               $scope.ngModel.splice(index, 1);
            }

            $scope.removeAll = function () {
                $scope.ready = false;
                $scope.ready = true;
                for(var i in $scope.ngModel) {
                    if ($scope.ngModel[i].id)
                        $scope.ngModel[i].remove = true;
                    else
                        delete $scope.ngModel[i];
                }
                $scope.ready = true;
            }

            $scope.$watch('ngModel', function (value) {
                if (value == undefined) {
                    //Do something
                    // $scope.removeAll();
                    // $scope.setList();
                }
            });


            $scope.showImage = function (index, src) {
                var body = document.getElementsByTagName("BODY")[0];
                body.style.overflow = 'hidden';

                $scope.currentIndex = index;
                // Get the modal
                var modal = document.getElementById('ImageViewer');

                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var img = document.getElementById(index);
                $scope.modalImg = document.getElementById("imgModal");

                $scope.captionText = document.getElementById("caption");

                modal.style.display = "block";
                if (src.fileName != undefined) {
                    if (src.base64)
                        $scope.modalImg.src = "data:image/png;base64," + src.base64;

                } else {
                    $scope.modalImg.src = src.url;

                }


                $scope.captionText.innerHTML = src.fileName;
               

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks on <span> (x), close the modal
                span.onclick = function () {
                    modal.style.display = "none";
                }

            };

            $scope.closeModal = function () {
                document.getElementById('ImageViewer').style.display = 'none';

                var body = document.getElementsByTagName("BODY")[0];
                body.style.overflow = 'scroll';
            };


            $scope.previousImage = function () {
                if ($scope.currentIndex == 0)
                    $scope.currentIndex = $scope.ngModel.length - 1;
                else
                    $scope.currentIndex--;

                var img = document.getElementById($scope.currentIndex);
                if (img.alt != "") {
                    $scope.modalImg.src = img.src;
                    $scope.captionText.innerHTML = img.alt;
                }
            };

            $scope.nextImage = function () {
                if ($scope.currentIndex == $scope.ngModel.length - 1)
                    $scope.currentIndex = 0;
                else
                    $scope.currentIndex++;

                var img = document.getElementById($scope.currentIndex);
                if (img.alt != "") {
                    $scope.modalImg.src = img.src;
                    $scope.captionText.innerHTML = img.alt;
                }

            };

        }

    };
});