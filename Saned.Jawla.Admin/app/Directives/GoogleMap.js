﻿var app = angular.module('googleMap', [])
    .directive('googleMap', function () {
        return {
            restrict: 'A',           
            scope: {
                ngModel: '='   ,
                options: '='
            }
           , link: function (scope, elem, attrs) {
               if (!angular.isUndefined(scope.options))
                { angular.extend(scope.options, {
                   DeleteMarkers: deleteMarkers,
                   PlaceMarker :placeMarker
               });
               }

               initMapWithCoordinates(attrs.id, attrs.lat, attrs.lng, attrs.location, attrs.show, attrs.maplat, attrs.maplng);


               

               function deleteMarkers() {
                   clearMarkers();
                   markers = [];
               }

               function clearMarkers() {
                   setMapOnAll();
               }

               function setMapOnAll() {
                   for (var i = 0; i < markers.length; i++) {
                       markers[i].setMap(null);
                   }
                   markers = [];
               }

               var markers = [];
               var map;

               function initMapWithCoordinates(divId, lat, lng, title, showMarkerOnDrawing, maplat, maplng) {
                   debugger;
                   var mapDiv = document.getElementById(divId);

                   map = new google.maps.Map(mapDiv, {
                       center: new google.maps.LatLng(lat, lng),
                       zoom: 7,
                       mapTypeId: 'terrain'
                   });




                   if (showMarkerOnDrawing === "true") {
                       var myLatlng = new google.maps.LatLng(lat, lng);
                       var marker = new google.maps.Marker({
                           position: myLatlng,
                           map: map,
                           title: title,
                           draggable: true

                       });
                       markers = [];
                       map.panTo(myLatlng);
                       markers.push(marker);

                       google.maps.event.addListener(
                        marker,
                        'drag',
                        function () {
                            debugger;
                            var lat = $('#' + maplat);
                            lat.val(marker.position.lat());
                            lat.trigger('input'); // Use for Chrome/Firefox/Edge
                            lat.trigger('change');


                            var lng = $('#' + maplng);
                            lng.val(marker.position.lng());
                            lng.trigger('input'); // Use for Chrome/Firefox/Edge
                            lng.trigger('change');



                        }
                    );




                   }
                   else {


                       map.addListener('click', function (e) {
                           placeMarkerAndPanTo(e.latLng, map, maplat, maplng);
                       });

                   }

               }


               function placeMarkerAndPanTo(latLng, map, maplat, maplng) {
                   deleteMarkers();  // edited 15.0000
                   var marker = new google.maps.Marker({
                       position: latLng,
                       map: map,
                       draggable: true
                   });
                   map.panTo(latLng);
                  markers.push(marker);

                   var lat = $('#' + maplat);
                   lat.val(latLng.lat());
                   lat.trigger('input'); // Use for Chrome/Firefox/Edge
                   lat.trigger('change');


                   var lng = $('#' + maplng);
                   lng.val(latLng.lng());
                   lng.trigger('input'); // Use for Chrome/Firefox/Edge
                   lng.trigger('change');
               }


               function placeMarker(latitude,longtide) {
                   deleteMarkers();  // edited 15.0000

                   var myLatlng = new google.maps.LatLng(latitude, longtide);

                   var marker = new google.maps.Marker({
                       position: myLatlng,
                       map: map,
                       draggable: true
                   });


                   var maplat = attrs.maplat;
                   var maplng = attrs.maplng;

                   google.maps.event.addListener(
                        marker,
                        'drag',
                        function () {
                            var lat = $('#' + maplat);
                            lat.val(marker.position.lat());
                            lat.trigger('input'); // Use for Chrome/Firefox/Edge
                            lat.trigger('change');


                            var lng = $('#' + maplng);
                            lng.val(marker.position.lng());
                            lng.trigger('input'); // Use for Chrome/Firefox/Edge
                            lng.trigger('change');



                        }
                    );



                   map.panTo(myLatlng);
                   markers.push(marker);

                 
                   var lat = $('#' + maplat);
                   lat.val(myLatlng.lat());
                   lat.trigger('input'); // Use for Chrome/Firefox/Edge
                   lat.trigger('change');


                   var lng = $('#' + maplng);
                   lng.val(myLatlng.lng());
                   lng.trigger('input'); // Use for Chrome/Firefox/Edge
                   lng.trigger('change');
               }

           }


        };

       

    });






