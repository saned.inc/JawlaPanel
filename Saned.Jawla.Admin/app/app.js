﻿/// <reference path="Modules/AppModule/views/manageEvent.html" />
/// <reference path="Views/test.html" />
/// <reference path="Views/test.html" />
(function () {
    // Define all your modules with no dependencies
    angular.module("MyModule", []);
    angular.module("themesApp", []);
    angular.module("AppModule", []);
    var securityModule = angular.module("SecurityModule", ['LocalStorageModule', 'angular-spinkit', "ngRoute"]);
    securityModule.constant('ngAuthSettings', {
        //  apiServiceBaseUri: 'http://tabukapi.saned-projects.com/', 
        // apiServiceBaseUri: 'http://localhost:28307/',
        //apiServiceBaseUri: 'http://localhost:28307/',
        //imagesFolderUri: 'http://localhost:28307/uploads/',
        apiServiceBaseUri: 'http://jawlaapi.saned-projects.com/',
        imagesFolderUri: 'http://jawlaapi.saned-projects.com/uploads/',

        clientId: 'ngAuthAdmin'
    }).config([
            '$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
                $httpProvider.interceptors.push('authInterceptorService');

                //var onlyLoggedIn = function () {
                //    if (!$rootScope.permissions.EventViewAny) {
                //        $location.url('/login');
                //        }              
                //};



            $routeProvider
                .when("/ChangePassword",
                {
                    templateUrl: "/App/Modules/SecurityModule/views/ChangePassword.html",
                    controller: "changePasswordController"
                }).when("/ContactUs", {
                    templateUrl: "/App/Modules/AppModule/views/contactUs.html",
                    controller: "contactUsController"
                })
                .when("/Event", {
                    templateUrl: "/App/Modules/AppModule/views/events.html",
                    controller: "eventsController"//,
                    //resolve: {
                    //    loggedIn: onlyLoggedIn
                    //}

                })
                 .when("/HistoricalPlaces", {
                     templateUrl: "/App/Modules/AppModule/views/historicalPlaces.html",
                     controller: "historicalPlacesController"
                 })
                 .when("/touristPlaces", {
                     templateUrl: "/App/Modules/AppModule/views/touristPlace.html",
                     controller: "touristPlaceController"
                 })
                 .when("/manageEvent", {
                     templateUrl: "/App/Modules/AppModule/views/manageEvent.html",
                     controller: "manageEventController"
                 })

                .when("/Users", {
                    templateUrl: "/App/Modules/AppModule/views/users.html",
                    controller: "usersController"
                }).when("/Ambassador", {
                    templateUrl: "/App/Modules/AppModule/views/ambassador.html",
                    controller: "ambassadorController"
                }).when("/ContactInformation", {
                    templateUrl: "/App/Modules/AppModule/views/contactInformation.html",
                    controller: "contactInformationController"
                }).when("/Category", {
                    templateUrl: "/App/Modules/AppModule/views/category.html",
                    controller: "categoryController"
                }).when("/Country", {
                    templateUrl: "/App/Modules/AppModule/views/country.html",
                    controller: "countryController"
                }).when("/City", {
                    templateUrl: "/App/Modules/AppModule/views/Cities.html",
                    controller: "CitiesController"
                }).when("/Municipality", {
                    templateUrl: "/App/Modules/AppModule/views/municipality.html",
                    controller: "municipalityController"
                }).when("/Photos", {
                    templateUrl: "/App/Modules/AppModule/views/photos.html",
                    controller: "photosController"
                }).when("/Rates", {
                    templateUrl: "/App/Modules/AppModule/views/rates.html",
                    controller: "ratesController"
                }).when("/Roles", {
                    templateUrl: "/App/Modules/AppModule/views/roles.html",
                    controller: "rolesController"
                })
                .when("/restaurants", {
                templateUrl: "/App/Modules/AppModule/views/restaurants.html",
                controller: "restaurantsController"


                }).when("/hotels",
                    {
                        templateUrl: "/App/Modules/AppModule/views/hotels.html",
                        controller: "hotelsController"


                    }).when("/manageHotel",
                    {
                        templateUrl: "/App/Modules/AppModule/views/manageHotel.html",
                        controller: "manageHotelController"


                    })


                ;
        }
    ]).run([
            '$rootScope', 'authService', function ($rootScope, authService) {
                $rootScope.isLoaderShow = false;
                authService.fillAuthData();

            }
    ]);

    var app = angular.module("App", ['theme','naif.base64' ,'theme.demos', 'theme.core.services', 'smart-table', 'ngSanitize', 'ngStorage', 'ngMessages', 'LocalStorageModule',
        "ngRoute", "ngAnimate", "ngResource", "ngCookies", "ngTable", "datePicker", "ui.bootstrap", "AxelSoft", "moment-picker", "angularUtils.directives.dirPagination",
        "MyModule", "AppModule", "SecurityModule", "themesApp", 'validation', 'validation.rule', "naif.base64", "fileUploader", "googleMap", "angularMoment", "InfoDirectiveModule",
        "ngYoutubeEmbed", "ngRateIt"])
        //,"ngPersian"
       .constant('appSettings', {
           showResult: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
           //,
         //cfrtsApiUrl: 'http://localhost:28307',
         //apiServiceBaseUri: 'http://api.arousqatar.com/',
         //clientId: 'ngAuthApp'
   

       })

        .config([
        '$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
            $httpProvider.interceptors.push('authInterceptorService');
            $routeProvider
                       .when('/', {
                           templateUrl: 'App/Views/dashboard.html'
                       })
                       .when('/:templateFile', {
                           templateUrl: function (param) {
                               return 'App/Modules/AppModule/views/' + param.templateFile + '.html';
                           }
                       })
                    .when('/:templateFile/:temp', {
                        templateUrl: function (param) {
                            return 'App/' + param.templateFile + '/' + param.temp + '.html';
                        }
                    })
                    .when('#', {
                        templateUrl: 'App/Views/dashboard.html'
                    })

                   .otherwise({
                       redirectTo: '/'
                   });
        }
        ]).run(['$rootScope', 'authService', '$location','translation', function ($rootScope, authService,$location, translation) {
            $rootScope.isLoaderShow = false;
            authService.fillAuthData();
            authService.fillpermission();
            $rootScope.permissions = authService.permissions;
               
            // notificationSvc.callSignalR();
            /*---------------Transalation------------------------*/
            translation.getAppDefaults().get(function (data) {
                $rootScope.transaction = data;
            });
          //$rootScope.apiServiceBaseUri = 'http://localhost:28307/';
            $rootScope.apiServiceBaseUri = 'http://jawlaapi.saned-projects.com/';
            $rootScope.pageSizes = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
           // $rootScope.imagesFolderUri = 'http://jawlaapi.saned-projects.com/uploads/';
            $rootScope.imagesFolderUri = 'http://localhost:28307/uploads/';


            //Global Back Function
            $rootScope.backButton = function () {
                window.history.back();
            };


            $rootScope.$on('$routeChangeError',
                function(event, current, previous, rejection) {
                    if (rejection === 'Unauthorized') {

                        $location.path('/');
                    }
                });

        }]);



    //app.run(['$rootScope', '$location', function ($rootScope, $location) {
    //    // If the route change failed due to our "Unauthorized" error, redirect them
    //    $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
    //        if (rejection === 'Unauthorized') {
    //            $location.path('/');
    //        }
    //    })
    //}])

}());