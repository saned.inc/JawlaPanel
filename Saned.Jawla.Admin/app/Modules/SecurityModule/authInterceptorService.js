﻿
'use strict';
angular.module("SecurityModule").factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService', '$rootScope',
    function ($q, $injector, $location, localStorageService, $rootScope) {

        var authInterceptorServiceFactory = {};
        var $http;
        var _request = function (config) {
           // if (config.url.indexOf("FindCities") === -1 && config.url.indexOf("GetMunicipalityByCityId") === -1)
                //   $rootScope.showLoader = $rootScope.isLoaderShow = true;
            config.headers = config.headers || {};
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }
            return config;
        };

        var _response = function (config) {

            $rootScope.showLoader = $rootScope.isLoaderShow = false;
            return config;
        }

        var _responseError = function (rejection) {
            $rootScope.showLoader =$rootScope.isLoaderShow =  false;
            var deferred = $q.defer();
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                authService.refreshToken().then(function (response) {
                    _retryHttpRequest(rejection.config, deferred);
                }, function () {
                    authService.logOut();
                });
            } else {
                deferred.reject(rejection);
            }
            return deferred.promise;
        }

        var _retryHttpRequest = function (config, deferred) {
            $http = $http || $injector.get('$http');
            $http(config).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
        }

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;
        authInterceptorServiceFactory.response = _response;


        return authInterceptorServiceFactory;
    }]);

