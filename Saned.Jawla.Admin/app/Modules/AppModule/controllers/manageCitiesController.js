﻿
(function () {
    var manageCitiesController= function ($rootScope, $scope, $routeParams, notify, citySvc) {
       
        /*---------------Init------------------------*/
        
   
       
        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true ;


        $scope.getAll = function (pageSize, currentPage) {
            citySvc.getAllCon(pageSize, currentPage).then(function (response) {
                $scope.allContries = response.data;
            });
        }
        $scope.getAll(1, 10);
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        $scope.model = {
            id: $scope.routeParam
        }

        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0
            }

        } else {

            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            citySvc.get($routeParams.id).then(function (response) {
                var status = response.status;
                var data = response.data;

                $scope.model = data;

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /*---------------Save------------------------*/
        $scope.save = function (form) {

            if ($scope.isAdd) {
                citySvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {

                        $scope.model = {

                        };
                        form.$setUntouched();
                        form.$setPristine();

                        notify.success($rootScope.transaction.NotifySuccess);
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        alert($scope.toasterWarning);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                citySvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success("لقد تمت عملية التعديل بنجاح ");
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        notify.error("خطا لم تتم عملية التعديل");
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });

        }

    };

    var module = angular.module("AppModule");
    module.controller("manageCitiesController", ["$rootScope", "$scope", "$routeParams", "notify", "citySvc", manageCitiesController]);

}());