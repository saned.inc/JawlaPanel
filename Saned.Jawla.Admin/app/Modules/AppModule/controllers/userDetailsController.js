﻿(function () {
    var userDetailsController = function ($rootScope, $scope, $route, $routeParams, $bootbox, notify, $window, usersSvc, appSettings) {


        /*---------------Init------------------------*/
        $scope.userId = $routeParams.id;
        $scope.isDataLoading = true;
        $scope.userData = {};

        /*---------------Get User Information------------------------*/
        $scope.loadData = function () {
            $scope.isDataLoading = true;
            debugger;
            usersSvc.get($scope.userId).then(function (response) {
                debugger;
                $scope.userData = response.data;
                $scope.isDataLoading = false;
                console.log(response.data);
            });
        }
        $scope.loadData();

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (status) {
            $bootbox.confirm(status === 'reject' ? $rootScope.transaction.ConfirmReject : $rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.remove($scope.userId).then(function (response) {
                        var status = response.status;
                        var data = response.data;
                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        $window.location.href = '#/users?type=ProductiveFamily';
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };


        /*----------- Confirm Payment ----------------*/
        $scope.confirmPayment = function () {
            usersSvc.confirmPayment($scope.userId).then(function (response) {
                if (response.status === 200) {
                    $scope.userData.isPaymentConfirmed = true;
                    notify.success($rootScope.transaction.NotifySuccess);
                }
            });
        };

        /*---------- Activate And DeActivate ---------------*/
        $scope.activateDeActivate = function () {
            usersSvc.activeUser($scope.userId).then(function (response) {
                if (response.status === 200) {
                    $scope.userData.isActive = !$scope.userData.isActive;
                    notify.success($rootScope.transaction.NotifySuccess);
                }
            });
        };

        /*--------- Confirm User --------------*/
        $scope.confirm = function (accountStatus) { 
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.confirm($scope.userId, accountStatus).then(function (response) {
                        if (response.status === 200) {
                            $scope.userData.accountStatus = accountStatus;
                            notify.success($rootScope.transaction.NotifySuccess);
                        }
                    });
                }
            });  
        };





    };

    var module = angular.module("AppModule");
    module.controller("userDetailsController", ["$rootScope", "$scope", "$route", "$routeParams", "$bootbox", "notify", "$window", "usersSvc", "appSettings", userDetailsController]);

}());