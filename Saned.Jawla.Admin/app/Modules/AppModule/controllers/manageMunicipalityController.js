﻿
(function () {
    var manageMunicipalityController = function ($rootScope, $scope, $routeParams, notify, municipalitySvc) {

        $scope.uploadedImageWidth = 0;
        $scope.uploadedImageHeight = 0;
        $scope.imageChange = function (e) {

            var file, img, adsFU;
            adsFU = $('#adsFU');
            var _URL = window.URL || window.webkitURL;
            if ((file = adsFU[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    debugger;
                    //alert(this.width + " " + this.height);
                    $scope.uploadedImageWidth = this.width;
                    $scope.uploadedImageHeight = this.height;
                    $scope.$apply();
                    $scope.$apply();
                };
                img.onerror = function () {
                    //alert("not a valid file: " + file.type);
                    $scope.uploadedImageWidth = 0;
                    $scope.uploadedImageHeight = 0
                    $scope.$apply()
                };
                img.src = _URL.createObjectURL(file);


            }

        }

        /*---------------Init------------------------*/
        $scope.countryId = 0;
        $scope.getAll = function (pageSize, currentPage) {
            municipalitySvc.getAllCities(pageSize, currentPage).then(function (response) {
                debugger;
                $scope.allCities = response.data;
            });
        }
        $scope.getAllCon = function (pageSize, currentPage) {
            municipalitySvc.getAllCon(pageSize, currentPage).then(function (response) {
                $scope.allCountries = response.data;
            });
        }

        $scope.getAll(1, 1000);
        $scope.getAllCon(1, 1000)

        $scope.routeParam = $routeParams.id;
        $scope.isAdd = true;
        if ($scope.routeParam) {
            $scope.isAdd = false;
        }
        $scope.model = {
            id: $scope.routeParam
        }

        if ($scope.isAdd) {
            $scope.title = $rootScope.transaction.Add;
            $scope.model = {
                id: 0
            }

        } else {

            $scope.title = $rootScope.transaction.Edit;
            /*---------------LoadData------------------------*/
            municipalitySvc.get($routeParams.id).then(function (response) {
                debugger;
                var data = response.data;
                $scope.model = data;

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /*---------------Save------------------------*/
        $scope.save = function (form) {

            if ($scope.isAdd) {
                municipalitySvc.add($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {

                        $scope.model = {

                        };
                        form.$setUntouched();
                        form.$setPristine();

                        notify.success($rootScope.transaction.NotifySuccess);
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        alert($scope.toasterWarning);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                municipalitySvc.update($scope.model).then(function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 200) {
                        notify.success("لقد تمت عملية التعديل بنجاح ");
                    }

                }, function (response) {

                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        notify.error("خطا لم تتم عملية التعديل");
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });

        }

        $scope.StringToNumber = function (value) {

            if (!angular.isUndefined(value)) {
                var intValue = parseInt(value);

                return intValue;
            }
            return 0;

        }

    };

    var module = angular.module("AppModule");
    module.controller("manageMunicipalityController", ["$rootScope", "$scope", "$routeParams", "notify", "municipalitySvc", manageMunicipalityController]);

}());