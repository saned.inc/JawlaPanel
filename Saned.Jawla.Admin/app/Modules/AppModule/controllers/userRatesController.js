﻿(function () {
    var userRatesController = function ($rootScope, $scope, $route, $bootbox, notify, $window, userRatesSvc, appSettings) {


        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        /*---------------Search------------------------*/
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {
            if ($scope.model)
                $scope.getData($scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.userRateSearch = function () {
            $scope.isDataLoading = true;
            $scope.currentPage = 1;
            userRatesSvc.search($scope.model, $scope.currentPage, $scope.pageSize).then(function (response) {
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;

            });

        }
        /*---------------Paging------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            userRatesSvc.getAll(pageSize, currentPage).then(function (response) {
                $scope.data = response.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }

        $scope.getData($scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {
            $scope.getData(currentPage, pageSize);
        };



        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {

            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    userRatesSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;

                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };


        /* ------------- Sort function ------------ */
        $scope.sortKey = 'name';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("userRatesController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "userRatesSvc", "translation", "appSettings", userRatesController]);

}());