﻿(function () {
    var municipalityController = function ($rootScope, $scope, $route, $bootbox, notify, $window, municipalitySvc, translation, appSettings) {



        
        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        $scope.cityId = 0;
        $scope.countryId = 1;

        /*---------------Search------------------------*/
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {
            debugger;
            if ($scope.model)
                $scope.getData($scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.municipalitySearch = function () {
            $scope.isDataLoading = true;
            $scope.currentPage = 1;
            $scope.pageSize = 1000;
            municipalitySvc.search($scope.cityId, $scope.currentPage, $scope.pageSize).then(function (response) {
             
                $scope.data = response.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;

            });

        }

        /*---------------Paging------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            municipalitySvc.getAll(pageSize, currentPage).then(function (response) {
               
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }
        ///search depending on this
        $scope.myFilter = function (item) { 
            return item.countryId === $scope.countryId;
};



        $scope.getAllCon = function (pageSize, currentPage) {
            municipalitySvc.getAllCon(pageSize, currentPage).then(function (response) {
                $scope.allCountries = response.data;
            });
        }

        $scope.getAll = function (pageSize, currentPage) {
            municipalitySvc.getAllCities(pageSize, currentPage).then(function (response) {
                $scope.allCities = response.data;
            });
        }


      /*  $scope.getAllagain= function (pageSize, currentPage) {
            municipalitySvc.getAllCon(pageSize, currentPage).then(function (response) {
                $scope.allContries = response.data;
            });
        }*/

        //  $scope.getAllagain($scope.currentPage, $scope.pageSize);
        $scope.getAllCon($scope.currentPage, $scope.pageSize);
        $scope.getData($scope.currentPage, $scope.pageSize);
        $scope.getAll($scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {
            $scope.getData(currentPage, pageSize);
        };

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm("جميع الاماكن (الاماكن التاريخية ,الاماكن السياحية , المطاعم ,الفنادق ) , المشاريع المميزة و الفعاليات المرتبطة بذلك الحي سوق يتم حذفها , هل انت متأكد ؟", function (result) {
                if (result === true) {
                 //   alert('Confirm Id: ' + id);
                    municipalitySvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data; 
                       
                       
                        notify.success($rootScope.transaction.NotifySuccess);
                    
                        
                      $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 405) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                       
                           
                    });
                }
            });
        };


        /* ------------- Sort function ------------ */
        $scope.sortKey = 'nameAr';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("municipalityController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "municipalitySvc", "translation", "appSettings", municipalityController]);

}());