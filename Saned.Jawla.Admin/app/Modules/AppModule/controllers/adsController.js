﻿(function () {
    var adsController = function ($rootScope, $scope, $route, $bootbox, notify, $window, adsSvc, appSettings) {
        debugger;
        $scope.video1 = 'https://www.youtube.com/watch?v=E813VYySueM';
         

        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        $scope.model = { name: '' };

        /*---------------Search------------------------*/
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {

            $scope.model.name = '';
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.adSearch = function () {
            $scope.isDataLoading = true;
            $scope.currentPage = 1;
            debugger;
            adsSvc.search($scope.model.name, $scope.currentPage, $scope.pageSize).then(function (response) {
                debugger;
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;

            });

        }

        /*---------------Paging------------------------*/
        $scope.getData = function (keword, currentPage, pageSize) {
            adsSvc.getAll(keword, pageSize, currentPage).then(function (response) {
                debugger;
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }

        $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {

            $scope.getData($scope.model.name, currentPage, pageSize);
        };

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert(id);
                    //alert('Confirm Id: ' + id);
                    adsSvc.remove(id).then(function (response) {

                        debugger;
                        $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };

        $scope.selectItemToShow = function (id, index) {
            adsSvc.makeItemSelected(id).then(function (response) {

                var status = response.status;
                var data = response.data;
                if (status === 200) {
                    notify.success($rootScope.transaction.NotifySuccess);
                }

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 400) {
                    $scope.data.map(function (item) {
                        if (item.advertisementId === id)
                            item.isShow = false;
                        return item;
                    });
                    notify.error(data.message);
                }
                else if (status === 409) {
                    alert(JSON.stringify(data));
                }
                else if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /* ------------- Sort function ------------ */
        $scope.sortKey = 'nameAr';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };
    };

    var module = angular.module("AppModule");
    module.controller("adsController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "adsSvc", "appSettings", adsController]);

}());