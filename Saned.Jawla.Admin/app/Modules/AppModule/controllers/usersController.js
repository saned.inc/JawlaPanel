﻿(function () {
    var usersController = function ($rootScope, $scope, $route, $bootbox, notify, $window, usersSvc, appSettings, ngAuthSettings) {
        debugger;
        $scope.myForm = {};
       
        $scope.showUserRatings = false;
        $scope.showUserFavourites = false;
        $scope.showUserImages = false;
        $scope.showUserComments = false;
        $scope.showResetPassword = false;
        $scope.changePasswordModel =
            {
                newPassword: '',
                confirmNewPassword: ''
            }
        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {};
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        $scope.model = { name: '' };

        /*---------------Search------------------------*/
        $scope.isSearch = false;
        $scope.showseach = function (isSearch) {
            $scope.isSearch = !isSearch;
        }

        $scope.cancelSearch = function () {
            $scope.model.name = '';
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);
            $scope.model = {};
            $scope.isSearch = false;
        }

        $scope.usersSearch = function () {
            debugger;
            $scope.isDataLoading = true;
            $scope.currentPage = 1;

            usersSvc.search($scope.model.name, $scope.currentPage, $scope.pageSize).then(function (response) {
                $scope.data = response.data.items;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
                debugger;
                $('[data-toggle="tooltip"]').tooltip();
            });

        }

        /*---------------Paging------------------------*/
        $scope.getData = function (keword, currentPage, pageSize) {
            debugger;
            usersSvc.getAll(keword, pageSize, currentPage).then(function (response) {
                debugger;
                $scope.data = response.data.items;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
                debugger;
                $('[data-toggle="tooltip"]').tooltip();
            });
        }

        $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);

        $scope.pageChanged = function (currentPage, pageSize) {

            $scope.getData($scope.model.name, currentPage, pageSize);
        };

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.remove(id).then(function (response) {
                        $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);
                        notify.success($rootScope.transaction.NotifySuccess);
                        $scope.data.splice(index, 1);
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };

        $scope.selectItemToShow = function (id, index) {
            usersSvc.makeItemSelected(id).then(function (response) {

                var status = response.status;
                var data = response.data;
                if (status === 200) {
                    notify.success($rootScope.transaction.NotifySuccess);
                }

            }, function (response) {
                var status = response.status;
                var data = response.data;

                if (status === 400) {
                    $scope.data.map(function (item) {
                        if (item.advertisementId === id)
                            item.isShow = false;
                        return item;
                    });
                    notify.error(data.message);
                }
                else if (status === 409) {
                    alert(JSON.stringify(data));
                }
                else if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        /* ------------- Sort function ------------ */
        $scope.sortKey = 'userName';
        $scope.sortReverse = false;
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.sortReverse = !$scope.sortReverse;
        };


        $scope.showResetPasswordForm = function (user) {

            $scope.myForm.$setPristine();
            $scope.myForm.$setUntouched();
            $scope.selectedUser = user;

            $scope.showResetPassword = true;
            $scope.showUserRatings = false;
            $scope.showUserFavourites = false;
            $scope.showUserImages = false;
            $scope.showUserComments = false;
        }

        $scope.getUserCommnets = function (user) {
            usersSvc.getUserCommnets(user.id).then(function (response) {
                console.log(response);
                debugger;
                $scope.userComments = response.data;
            },
                function (response) { });
            $scope.showUserComments = true;
            $scope.showUserRatings = false;
            $scope.showUserFavourites = false;
            $scope.showUserImages = false;
            $scope.showResetPassword = false;
            $scope.selectedUser = user;

        };

        $scope.getUserPhotos = function (user) {
            debugger;
            usersSvc.getUserPhotos(user.id).then(function (response) {
                console.log(response);
                $scope.userPhotos = response.data;
                debugger;
                for (var i = 0; i < $scope.userPhotos.length; i++) {
                    $scope.userPhotos[i].photoUrl = ngAuthSettings.imagesFolderUri + $scope.userPhotos[i].photoUrl;
                }
            },
                function (response) { });
            $scope.showUserImages = true;
            $scope.showUserRatings = false;
            $scope.showUserFavourites = false;
            $scope.showUserComments = false;
            $scope.showResetPassword = false;
            $scope.selectedUser = user;

        };



        $scope.getUserFavouries = function (user) {
            usersSvc.getUserFavourites(user.id).then(function (response) {
                console.log(response);
                $scope.userFavourites = response.data;

            }, function (response) { });

            $scope.showUserFavourites = true;
            $scope.showUserRatings = false;
            $scope.showUserImages = false;
            $scope.showUserComments = false;
            $scope.showResetPassword = false;
            $scope.selectedUser = user;

        };


        $scope.getUserRating = function (user) {
            usersSvc.getUserRating(user.id).then(function (response) {
                console.log(response);
                $scope.userRating = response.data;

            }, function (response) { });

            $scope.showUserRatings = true;
            $scope.showUserFavourites = false;
            $scope.showUserImages = false;
            $scope.showUserComments = false;
            $scope.showResetPassword = false;
            $scope.selectedUser = user;

        };


        $scope.back = function () {
            $scope.showUserRatings = false;
            $scope.showUserFavourites = false;
            $scope.showUserImages = false;
            $scope.showUserComments = false;
            $scope.showResetPassword = false;
            if ($scope.changePasswordModel) {
                $scope.changePasswordModel.newPassword = '';
                $scope.changePasswordModel.confirmNewPassword = '';
            }

        };

        $scope.changePassword = function (form) {
            debugger;
            $scope.changePasswordModel.userId = $scope.selectedUser.id;
            usersSvc.changePassword($scope.changePasswordModel).then(function (response) {
                notify.success("تم تغيير كلمة المرور بنجاح");
                $scope.back();
            }, function (response) { });
        };



        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.deleteUserComment(id).then(function (response) {
                        debugger;
                        for (var i = 0; i < $scope.userComments.length; i++) {
                            if ($scope.userComments[i].id == id) {

                                var index = $scope.userComments.indexOf($scope.userComments[i]);
                                $scope.userComments.splice(index, 1);
                            }
                        }
                    }, function () { });
                }
            });
        };



        $scope.openDeleteUserPhotosConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.deleteUserPhoto(id).then(function (response) {
                        debugger;
                        for (var i = 0; i < $scope.userPhotos.length; i++) {
                            if ($scope.userPhotos[i].id == id) {

                                var index = $scope.userPhotos.indexOf($scope.userPhotos[i]);
                                $scope.userPhotos.splice(index, 1);
                            }
                        }
                    }, function () { });
                }
            });
        };



        $scope.openDeleteUserRatingConfirm = function (id, index) {
            debugger;
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    debugger;
                    usersSvc.deleteUserRating(id).then(function (response) {

                        for (var i = 0; i < $scope.userRating.length; i++) {
                            if ($scope.userRating[i].id == id) {

                                var index = $scope.userRating.indexOf($scope.userRating[i]);
                                $scope.userRating.splice(index, 1);
                            }
                        }
                    }, function () { });
                }
            });
        };
        $scope.openActivateUserConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.activateUser(id).then(function (response) {
                        for (var i = 0; i < $scope.data.length; i++) {
                            if ($scope.data[i].id == id) {
                                $scope.data[i].isDeleted = false;
                            }
                        }
                    }, function () { });
                }
            });
        };
        $scope.openDeactivateUserConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    usersSvc.deActivateUser(id).then(function (response) {
                        for (var i = 0; i < $scope.data.length; i++) {
                            if ($scope.data[i].id == id) {
                                $scope.data[i].isDeleted = true;
                            }
                        }
                    }, function () { });
                }
            });
        };

        $scope.openActivateUserDelete = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                debugger;
                if (result === true) {
                    usersSvc.deleteUser(id).then(function (response) {
                        debugger;
                        $scope.getData($scope.model.name, $scope.currentPage, $scope.pageSize);
                    }, function () { });
                }
            });
        };
        
    };






    var module = angular.module("AppModule");
    module.controller("usersController", ["$rootScope", "$scope", "$route", "$bootbox", "notify", "$window", "usersSvc", "appSettings", "ngAuthSettings", usersController]);

}());

(function () {
    'use strict';
    var module = angular.module("AppModule");
    var directiveId = 'ngMatch';
    module.directive(directiveId, ['$parse', function ($parse) {

        var directive = {
            link: link,
            restrict: 'A',
            require: '?ngModel'
        };
        return directive;

        function link(scope, elem, attrs, ctrl) {
            // if ngModel is not defined, we don't need to do anything
            if (!ctrl) return;
            if (!attrs[directiveId]) return;

            var firstPassword = $parse(attrs[directiveId]);

            var validator = function (value) {
                var temp = firstPassword(scope),
                v = value === temp;
                ctrl.$setValidity('match', v);
                return value;
            }

            ctrl.$parsers.unshift(validator);
            ctrl.$formatters.push(validator);
            attrs.$observe(directiveId, function () {
                validator(ctrl.$viewValue);
            });

        }
    }]);
})();