﻿(function () {
    var hotelsSvc = function ($http, $q, ngAuthSettings) {
        var apiUri = ngAuthSettings.apiServiceBaseUri;
        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/CategoryDetails/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }

        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            var model = { PageNumber: currentPage, PageSize: pageSize };

            // Initiates the AJAX call
            $http.post(apiUri + "/api/Hotels", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var getAllPost = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();

            var model = { PageNumber: currentPage, PageSize: pageSize };
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Hotels", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (model, currentPage, pageSize) {
            model["PageNumber"] = currentPage;
            model["PageSize"] = pageSize;

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Hotels", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            return $http.get(apiUri + "/api/Hotels/" + id);
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(apiUri + "/api/Hotels/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(apiUri + "/api/Hotels/Create", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var approve = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(apiUri + "/api/Hotels/Approve/"+ model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var unApprove = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(apiUri + "/api/Hotels/UnApprove/" + model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Hotels/Update", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.post(apiUri + "/api/Hotels/Delete/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            postAll: getAllPost,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            approve:approve,
            unApprove: unApprove
        }


    };

    var module = angular.module("AppModule");
    module.factory("hotelsSvc", ["$http", "$q", "ngAuthSettings", "$rootScope", hotelsSvc]);

}());