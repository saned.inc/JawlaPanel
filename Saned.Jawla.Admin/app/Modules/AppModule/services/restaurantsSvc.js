﻿(function () {
    var restaurantsSvc = function ($http, $q, ngAuthSettings) {
        var apiUri = ngAuthSettings.apiServiceBaseUri;
        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/CategoryDetails/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }

        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            var model = { PageNumber: currentPage, PageSize: pageSize };

            // Initiates the AJAX call
            $http.post(apiUri + "/api/Restaurants", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };
        var getAllPost = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();

            var model = { PageNumber: currentPage, PageSize: pageSize };
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Restaurants", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (model, currentPage, pageSize) {
            model["PageNumber"] = currentPage;
            model["PageSize"] = pageSize;

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Restaurants", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(apiUri + "/api/Restaurants/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(apiUri + "/api/Restaurants/Create", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/Restaurants/Update", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.post(apiUri + "/api/Restaurants/Delete/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            postAll: getAllPost,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search
        }


    };

    var module = angular.module("AppModule");
    module.factory("restaurantsSvc", ["$http", "$q", "ngAuthSettings", "$rootScope", restaurantsSvc]);

}());