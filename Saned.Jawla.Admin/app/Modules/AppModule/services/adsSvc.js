﻿(function () {

    var adsSvc = function ($http, $q, ngAuthSettings) {
        var api = ngAuthSettings.apiServiceBaseUri;
        var getAll = function (keyword, pageSize, currentPage) {
            debugger;
            var deferred = $q.defer();
            var model =
                {
                    PageNumber: currentPage - 1,
                    PageSize: pageSize,
                    Keyword: keyword
                };
            $http.post(api + "/api/Advertisement/AdminGetAdvertisements", model).then(function (successResponse) {
                debugger;
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                debugger;
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };
        var search = function (keyword, currentPage, pageSize) {

            var deferred = $q.defer();
            var model =
                {
                    PageNumber: currentPage - 1,
                    PageSize: pageSize,
                    Keyword: keyword
                };
            $http.post(api + "api/Advertisement/AdminGetAdvertisements", model).then(function (successResponse) {
                debugger;
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                debugger;
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };


        var get = function (id) {
            var deferred = $q.defer();

            $http.get(api + "api/Advertisement/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            return deferred.promise;
        };

        var add = function (model) {
            debugger;
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(api + "api/Advertisement/CreateAdvertisement", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            debugger;
            var deferred = $q.defer();

            $http.post(api + "api/Advertisement/Update", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(api + "/api/Advertisement/Delete/" + id).then(function (successResponse) {

                deferred.resolve(successResponse);
            }, function (failureResponse) {

                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        //var makeItemSelected = function (id) {

        //    // Get the deferred object
        //    var deferred = $q.defer();
        //    // Initiates the AJAX call
        //    $http.get("/api/Ads/MakeItemSelected/" + id).then(function (successResponse) {
        //        deferred.resolve(successResponse);
        //    }, function (failureResponse) {
        //        deferred.reject(failureResponse);
        //    });
        //    // Returns the promise - Contains result once request completes
        //    return deferred.promise;

        //};

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search,
            //makeItemSelected: makeItemSelected
        }


    };

    var module = angular.module("AppModule");
    module.factory("adsSvc", ["$http", "$q", "ngAuthSettings", "$rootScope", adsSvc]);

}());


