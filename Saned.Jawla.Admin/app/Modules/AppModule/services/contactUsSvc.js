﻿(function () {
   // var apiUri='http://localhost:28307/';
    var contactUsSvc = function ($http, $q, ngAuthSettings) {
        var apiUri = ngAuthSettings.apiServiceBaseUri;
        var returnAll = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri+"/api/ContactUs/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;

        }

        var getAll = function (pageSize, currentPage) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "/api/ContactUs/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var search = function (keyword) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get(apiUri + "api/ContactUs/search/" + keyword).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var get = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call

            $http.get(apiUri + "/api/ContactUs/FindContactUs/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var add = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            //post

            $http.post(apiUri + "/api/ContactUs/AddMessage", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;


        };

        var update = function (model) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post(apiUri + "/api/ContactUs/AddMessage", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        var remove = function (id) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            debugger;
            $http.post(apiUri + "/api/ContactUs/RemoveContactUs/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
            search: search
        }


    };

    var module = angular.module("AppModule");
    module.factory("contactUsSvc", ["$http", "$q", "$rootScope", "ngAuthSettings", contactUsSvc]);

}());