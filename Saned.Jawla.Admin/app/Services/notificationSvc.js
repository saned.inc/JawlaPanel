﻿(function () {

    var notificationSvc = function ($rootScope, $http, $q) {

        var getTopTen = function () {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("/api/Notification").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getAllNotSeenCount = function () {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("/api/Notification/NotSeenCount").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var read = function (id) {
            debugger;
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("api/Notification/Read/" + id).then(function (successResponse) {
                debugger;
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                debugger;
                deferred.reject(failureResponse);
            });
            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var getAll = function (currentPage, pageSize) {

            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.get("api/Notification/GetAll/" + pageSize + "/" + currentPage).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

            // Returns the promise - Contains result once request completes
            return deferred.promise;
        };

        var remove = function (id) {
            // Get the deferred object
            var deferred = $q.defer();
            // Initiates the AJAX call
            $http.post("/api/Notification/" + id).success(deferred.resolve).error(deferred.reject);
            // Returns the promise - Contains result once request completes
            return deferred.promise;

        };

        return {
            getTopTen: getTopTen,
            read: read,
            getAll: getAll,
            remove: remove,
            getAllNotSeenCount: getAllNotSeenCount

        }


    };

    var module = angular.module("App");
    module.factory("notificationSvc", ["$rootScope", "$http", "$q", notificationSvc]);

}());