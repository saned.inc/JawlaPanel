﻿(function () {

    var NavController = function ($rootScope, $scope, $location, $timeout, translation) {

        translation.getAppDefaults().get(function (data) {

            $scope.menu = [
          {
              label: data.Dashboard,
              iconClasses: 'glyphicon glyphicon-home',
              url: '#/'
          },
          {
              label: 'الإعدادات الرئيسية',
              iconClasses: 'glyphicon glyphicon-th-list',
              hide: !$rootScope.permissions.Administrator,
              children: [
                   {
                       label: data.Country,
                       iconClasses: 'fa fa-flag',
                       url: '#/Country'
                       
                   },
                   {
                       label: data.Cities,
                       url: '#/cities',
                       iconClasses: 'fa fa-building'
                    
                   },
                    {
                        label: data.Municipality,
                        iconClasses: 'fa fa-home',
                        url: '#/Municipality'

                    },
                  {
                      label: data.ContactInformation,
                      iconClasses: 'fa fa-phone',
                      url: '#/ContactInformation'
                  },
                  {
                      label: 'التصنيفات الفرعية',
                      iconClasses: 'fa fa-th',
                      url: '#/Category'
                  }

              ]
          },
          {
              label: data.Alluser,
              iconClasses: 'fa fa-users',
              hide: !$rootScope.permissions.Administrator,
              children: [
                  {
                      label: data.Roles,
                      iconClasses: 'glyphicon glyphicon-lock',
                      url: '#/Roles'
                      //hide: !$rootScope.hasPermission('chance/view')
                  },

                  {
                      label: data.User,
                      iconClasses: 'glyphicon glyphicon-user',
                      url: '#/Users'
                      //hide: !$rootScope.hasPermission('chance/view')
                  },
                  {
                      label: "طلبات السفير",
                      iconClasses: 'fa fa-user-o',
                      url: '#/Ambassador'
                      //hide: !$rootScope.hasPermission('chance/view')
                  }
              ]

          },
                {
                    label: data.Places,
                    iconClasses: 'fa fa-map-marker',
                    children: [
                        {
                            label: "المشاريع المميزة",
                            iconClasses: 'glyphicon glyphicon-time',
                            url: '#/FeaturedProjects',
                            hide: !($rootScope.permissions.Administrator || $rootScope.permissions.ProjectViewAny )
                        },

                        {
                            label: data.HistoricalPlaces,
                            iconClasses: 'fa fa-history',
                            url: '#/HistoricalPlaces',
                            hide: !($rootScope.permissions.Administrator  || $rootScope.permissions.HistoricPlaceViewAny)


                        },
                        {
                            label: data.touristPlaces,
                            iconClasses: 'fa fa-tripadvisor',
                            url: '#/touristPlaces',
                            hide: !($rootScope.permissions.Administrator  || $rootScope.permissions.TourPlaceViewAny)


                        },
                         {
                             label: data.restaurants,
                             iconClasses: 'fa fa-cutlery',
                             url: '#/restaurants',
                             hide: !($rootScope.permissions.Administrator || $rootScope.permissions.RestaurantViewAny )
        },

                         {
                             label: data.Hotels,
                             iconClasses: 'fa fa-building',
                             url: '#/hotels',
                             hide: !($rootScope.permissions.Administrator  || $rootScope.permissions.HotelViewAny)
        }
                    ],
                    hide: !(
                        $rootScope.permissions.Administrator || $rootScope.permissions.ProjectViewAny 
                         || $rootScope.permissions.HistoricPlaceViewAny
                         || $rootScope.permissions.TourPlaceViewAny
                        || $rootScope.permissions.RestaurantViewAny 
                         || $rootScope.permissions.HotelViewAny
                        )

                    },




                 {
                     label: data.Events,
                     iconClasses: 'glyphicon glyphicon-time',
                     url: '#/Event',
                     hide: !($rootScope.permissions.Administrator || $rootScope.permissions.EventViewAny )
        },

                  {
                      label: data.Ads,
                      url: '#/ads',
                      iconClasses: 'glyphicon glyphicon-bullhorn',
                      hide: !($rootScope.permissions.Administrator || $rootScope.permissions.AdViewAny )
        },


          {
              label: data.ContactUs,
              iconClasses: 'glyphicon glyphicon-envelope',
              url: '#/ContactUs',
              hide: !($rootScope.permissions.Administrator )
          }
          //,





           ////{
           ////    label: data.Photos,
           ////    iconClasses: 'glyphicon glyphicon-picture',
           ////    url: '#/Photos'
           ////    //hide: !$rootScope.hasPermission('chance/view')
           ////},
           ////{
           ////    label: data.Rates,
           ////    iconClasses: 'glyphicon glyphicon-star',
           ////    url: '#/Rates'
           ////    //hide: !$rootScope.hasPermission('chance/view')
           ////},

            ];

            var setParent = function (children, parent) {
                angular.forEach(children, function (child) {
                    child.parent = parent;
                    if (child.children !== undefined) {
                        setParent(child.children, child);
                    }
                });
            };

            setParent($scope.menu, null);


        });


        $scope.findItemByUrl = function (children, url) {
            for (var i = 0, length = children.length; i < length; i++) {
                if (children[i].url && children[i].url.replace('#', '') === url) {
                    return children[i];
                }
                if (children[i].children !== undefined) {
                    var item = $scope.findItemByUrl(children[i].children, url);
                    if (item) {
                        return item;
                    }
                }
            }
        };


        $scope.openItems = []; $scope.selectedItems = []; $scope.selectedFromNavMenu = false;

        $scope.select = function (item) {
            // close open nodes
            if (item.open) {
                item.open = false;
                return;
            }
            for (var i = $scope.openItems.length - 1; i >= 0; i--) {
                $scope.openItems[i].open = false;
            }
            $scope.openItems = [];
            var parentRef = item;
            while (parentRef !== null) {
                parentRef.open = true;
                $scope.openItems.push(parentRef);
                parentRef = parentRef.parent;
            }

            // handle leaf nodes
            if (!item.children || (item.children && item.children.length < 1)) {
                $scope.selectedFromNavMenu = true;
                for (var j = $scope.selectedItems.length - 1; j >= 0; j--) {
                    $scope.selectedItems[j].selected = false;
                }
                $scope.selectedItems = [];
                parentRef = item;
                while (parentRef !== null) {
                    parentRef.selected = true;
                    $scope.selectedItems.push(parentRef);
                    parentRef = parentRef.parent;
                }
            }
        };

        $scope.highlightedItems = [];
        var highlight = function (item) {
            var parentRef = item;
            while (parentRef !== null) {
                if (parentRef.selected) {
                    parentRef = null;
                    continue;
                }
                parentRef.selected = true;
                $scope.highlightedItems.push(parentRef);
                parentRef = parentRef.parent;
            }
        };

        var highlightItems = function (children, query) {
            angular.forEach(children, function (child) {
                if (child.label.toLowerCase().indexOf(query) > -1) {
                    highlight(child);
                }
                if (child.children !== undefined) {
                    highlightItems(child.children, query);
                }
            });
        };

        // $scope.searchQuery = '';
        $scope.$watch('searchQuery', function (newVal, oldVal) {
            var currentPath = '#' + $location.path();
            if (newVal === '') {
                for (var i = $scope.highlightedItems.length - 1; i >= 0; i--) {
                    if ($scope.selectedItems.indexOf($scope.highlightedItems[i]) < 0) {
                        if ($scope.highlightedItems[i] && $scope.highlightedItems[i] !== currentPath) {
                            $scope.highlightedItems[i].selected = false;
                        }
                    }
                }
                $scope.highlightedItems = [];
            } else
                if (newVal !== oldVal) {
                    for (var j = $scope.highlightedItems.length - 1; j >= 0; j--) {
                        if ($scope.selectedItems.indexOf($scope.highlightedItems[j]) < 0) {
                            $scope.highlightedItems[j].selected = false;
                        }
                    }
                    $scope.highlightedItems = [];
                    highlightItems($scope.menu, newVal.toLowerCase());
                }
        });

        $scope.$on('$routeChangeSuccess', function () {
            if ($scope.menu) {

                if ($scope.selectedFromNavMenu === false) {
                    var item = $scope.findItemByUrl($scope.menu, $location.path());
                    if (item) {
                        $timeout(function () {
                            $scope.select(item);
                        });
                    }
                }
                $scope.selectedFromNavMenu = false;
                $scope.searchQuery = '';
            }
        });



    };

    var module = angular.module("App");
    module.controller("NavController", ["$rootScope", "$scope", "$location", "$timeout", "translation", NavController]);

}());
