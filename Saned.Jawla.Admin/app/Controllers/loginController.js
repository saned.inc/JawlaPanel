﻿'use strict';
angular.module("SecurityModule").controller('loginController', ['$scope', '$window', 'authService', '$http', function ($scope, $window, authService, $http) {
    $scope.loginData = {
        userName: "",
        password: "",
        useRefreshTokens: false
    };
    $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $scope.message = "";
    $scope.login = function () {
        debugger;
        $scope.isLoadData = true;
        authService.login($scope.loginData).then(function (response) {
            $scope.isLoadData = false;
            debugger;
            if (response.status === 200)
                $window.location.href = '/';
            if (response.status === 400)
                $scope.message = "كلمه المرور او اسم المستخدم خطأ .";
        });
    };

}]);