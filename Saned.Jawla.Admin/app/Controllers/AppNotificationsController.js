﻿
(function () {

    var appNotificationsController = function ($rootScope, $scope, notificationSvc, $window, $routeParams, notify, $bootbox, appSettings) {

        /*---------------Init------------------------*/
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.totalData = {}; 
        $scope.showResult = appSettings.showResult;
        $scope.isDataLoading = true;
        $scope.data = [];
        $rootScope.notifications = [];
        /*---------------AllCountNotSeen------------------------*/
        notificationSvc.getAllNotSeenCount().then(function (response) {
            $rootScope.unseenNotificationCount = response.data;
        });

        /*---------------TopTen------------------------*/
        notificationSvc.getTopTen().then(function (response) {
            $rootScope.notifications = response.data;
        });


        /*---------------Set Object Seen------------------------*/
        $rootScope.setSeen = function (id) {
            $scope.isDataLoading = true;
            notificationSvc.read(id).then(function (response) {
                debugger;
                $scope.data.push(response.data);
                $scope.isDataLoading = false;
                $window.location = '#/Views/notification?id=' + response.data.dbNotificationId;
            });
        }

        /*---------------Paging------------------------*/
        $scope.getData = function (currentPage, pageSize) {
            $scope.isDataLoading = true;
            notificationSvc.getAll(pageSize, currentPage).then(function (response) {
                $scope.data = response.data.data;
                $scope.totalData = response.data.totalCount;
                $scope.isDataLoading = false;
            });
        }

        $scope.pageChanged = function (currentPage, pageSize) {
            $scope.getData(currentPage, pageSize);
        };

        /*---------------Load Data------------------------*/
        if ($routeParams.id)
            $rootScope.setSeen($routeParams.id);
        else
            $scope.getData($scope.currentPage, $scope.pageSize);

        /*---------------Delete------------------------*/
        $scope.openConfirm = function (id, index) {
            $bootbox.confirm($rootScope.transaction.ConfirmText, function (result) {
                if (result === true) {
                    //alert('Confirm Id: ' + id);
                    notificationSvc.remove(id).then(function (response) {
                        var status = response.status;
                        var data = response.data;
                        //$route.reload();
                        notify.success($rootScope.transaction.NotifySuccess);
                        if ($routeParams.id)
                            $window.location = '#/';
                        else {
                            $scope.data.splice(index, 1);
                            $rootScope.notifications.filter(function (item) {
                                if (item.dbNotificationId === id) {
                                    $rootScope.notifications.splice($rootScope.notifications.indexOf(item), 1);
                                    if (item.isSeen===false)
                                        $rootScope.unseenNotificationCount -= 1;
                                }
                            });
                            if ($scope.data.length === 0)
                                $scope.getData($scope.currentPage, $scope.pageSize);
                        }
                    }, function (response) {

                        var status = response.status;
                        var data = response.data;

                        if (status === 400) {
                            notify.error($scope.toasterWarning);
                        }
                        else if (status === 500)
                            notify.error(JSON.stringify(data));
                    });
                }
            });
        };
    };

    var module = angular.module("App");
    module.controller("appNotificationsController", ["$rootScope", "$scope", "notificationSvc", "$window", "$routeParams", "notify", "$bootbox", "appSettings", appNotificationsController]);

}());