﻿using System.Web.Optimization;

namespace Saned.Jawla.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Clear();
            bundles.ResetAll();
            BundleTable.EnableOptimizations = true;

            // bundling Scripts
            bundles.Add(new ScriptBundle("~/bundles/app")
            .IncludeDirectory("~/App/", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/app/constants")
             .IncludeDirectory("~/App/Constants", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/controllers")
              .IncludeDirectory("~/App/Controllers", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/directives")
              .IncludeDirectory("~/App/Directives", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/modules/appModule")
              .IncludeDirectory("~/App/Modules/AppModule", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/modules/AppModule/services")
            .IncludeDirectory("~/App/Modules/AppModule/services", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/modules/AppModule/controllers")
            .IncludeDirectory("~/App/Modules/AppModule/controllers", "*.js", false));


            bundles.Add(new ScriptBundle("~/bundles/modules/security")
              .IncludeDirectory("~/App/Modules/SecurityModule", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/modules/security/services")
            .IncludeDirectory("~/App/Modules/SecurityModule/services", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/modules/security/controllers")
            .IncludeDirectory("~/App/Modules/SecurityModule/controllers", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/services")
                    .IncludeDirectory("~/App/Services", "*.js", false));
            bundles.Add(new ScriptBundle("~/bundles/spinkit").Include(
                      "~/Scripts/angular-spinkit/angular-spinkit.js"));

            // bundling Style
            bundles.Add(new StyleBundle("~/bundles/login/css").Include(
                      "~/Template/assets/less_ar/bootstrap_ar.css",
                      "~/Template/assets/css/login/style-ar.css",
                      "~/Scripts/angular-spinkit/angular-spinkit.css",
                      "~/Template/assets/css/login/webarch.rtl.css"));


            //remove bundle
            foreach (var bundle in BundleTable.Bundles)
            {
                bundle.Transforms.Clear();
            }



            //var root = HttpContext.Current.Server.MapPath("~/app");

            //var files = Directory.EnumerateFiles(root, "*.*", SearchOption.AllDirectories)
            //  .Where(s => s.EndsWith(".js"));

            //var builder = new ScriptBundle("~/bundles/app"); 
            //foreach (var file in files)
            //{
            //    var app = file.Split(new string[] { "\\app\\" }, StringSplitOptions.None);
            //    var url = $"~/{app[1].Replace('\\', '/')}"; 
            //    builder.Include(url);
            //} 
            //bundles.Add(builder);

        }

    }
}