﻿using System.Data.Entity;
using Saned.Core.Security.Core.Models;

namespace Saned.Core.Security
{
    public interface ISecurityDbContext
    {
        DbSet<EmailSetting> EmailSettings { get; set; }
        DbSet<Client> Clients { get; set; }
        DbSet<RefreshToken> RefreshTokens { get; set; }
        void Dispose();
    }
}