﻿using Saned.Core.Security.Core.Models;

namespace Saned.Core.Security.Core.Repositories
{
    public interface IEmailSettingRepository
    {
        EmailSetting GetEmailSetting(string type);
    }
}