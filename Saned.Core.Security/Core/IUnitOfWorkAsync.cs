using System;
using Saned.Core.Security.Core.Repositories;

namespace Saned.Core.Security.Core
{
    public interface IUnitOfWorkAsync : IDisposable
    {
        IEmailSettingRepository EmailSetting { get; }
    }
}