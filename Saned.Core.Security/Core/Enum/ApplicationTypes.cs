namespace Saned.Core.Security.Core.Enum
{
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };
}