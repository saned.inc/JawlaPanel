namespace Saned.Core.Security.Core.Enum
{
    public enum RolesEnum
    {
        User,
        Administrator,
        Safeer,



    }

    public enum PremisionEnum
    {
       
        EventAdd,
        EventViewByCreator,
        EventEditByCreator,
        EventDeleteByCreator,
        EventAddedWithoutAdminApprove,
        EventViewAny,
        EventEditAny,
        EventDeleteAny,

        ProjectAdd,
        ProjectByCreator,
        ProjectEditByCreator,
        ProjectDeleteByCreator,
        ProjectAddedWithoutAdminApprove,
        ProjectViewAny,
        ProjectlEditAny,
        ProjectDeleteAny,

        RestaurantAdd,
        RestaurantViewByCreator,
        RestaurantEditByCreator,
        RestaurantDeleteByCreator,
        RestaurantAddedWithoutAdminApprove,
        RestaurantViewAny,
        RestaurantEditAny,
        RestaurantDeleteAny,

        HotelAdd,
        HotelViewByCreator,
        HotelEditByCreator,
        HotelDeleteByCreator,
        HotelAddedWithoutAdminApprove,
        HotelViewAny,
        HotelEditAny,
        HotelDeleteAny,

        TourPlaceAdd,
        TourPlaceViewByCreator,
        TourPlaceEditByCreator,
        TourPlaceDeleteByCreator,
        TourPlaceAddedWithoutAdminApprove,
        TourPlaceViewAny,
        TourPlaceEditAny,
        TourPlaceDeleteAny,

        HistoricPlaceAdd,
        HistoricPlaceViewByCreator,
        HistoricPlaceEditByCreator,
        HistoricPlaceDeleteByCreator,
        HistoricPlaceAddedWithoutAdminApprove,
        HistoricPlaceViewAny,
        HistoricPlaceEditAny,
        HistoricPlaceDeleteAny,

        AdAdd,
        AdByCreator,
        AdEditByCreator,
        AdDeleteByCreator,
        AdAddedWithoutAdminApprove,
        AdViewAny,
        AdEditAny,
        AdDeleteAny,


    }
}