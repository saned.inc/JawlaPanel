using System;
using Saned.Core.Security.Core;
using Saned.Core.Security.Core.Repositories;
using Saned.Core.Security.Persistence.Repositories;

namespace Saned.Core.Security.Persistence
{
    public class UnitOfWorkAsync : IUnitOfWorkAsync
    {
        ISecurityDbContext _context;
        public IEmailSettingRepository EmailSetting { get; }

        public UnitOfWorkAsync()
        {
            _context = new SecurityDbContext();
            EmailSetting = new EmailSettingRepository(_context);
        }
        public UnitOfWorkAsync(ISecurityDbContext context)
        {
            _context = context;
            EmailSetting = new EmailSettingRepository(context);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (this._context == null)
            {
                return;
            }

            this._context.Dispose();
            this._context = null;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}