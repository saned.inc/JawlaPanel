using System.Linq;
using Saned.Core.Security.Core.Models;
using Saned.Core.Security.Core.Repositories;

namespace Saned.Core.Security.Persistence.Repositories
{
    public class EmailSettingRepository : IEmailSettingRepository
    {
        private readonly ISecurityDbContext _context;

        public EmailSettingRepository(ISecurityDbContext context)
        {
            _context = context;
        }

        public EmailSetting GetEmailSetting(string type)
        {
            return _context.EmailSettings.SingleOrDefault(u => u.EmailSettingType == type.Trim());
        }
    }


}