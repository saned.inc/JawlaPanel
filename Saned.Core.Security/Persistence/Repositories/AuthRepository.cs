﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Saned.Core.Security.Core;
using Saned.Core.Security.Core.Models;
using Saned.Core.Security.Migrations;
using Saned.Core.Security.Persistence.Infrastructure;
using Saned.Core.Security.Core.Dtos;

namespace Saned.Core.Security.Persistence.Repositories
{
    public enum EmailType
    {
        EmailConfirmation = 1,
        ForgetPassword = 2,
    }

    public class AuthRepository : IDisposable
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        private SecurityDbContext _context;

        private readonly ApplicationUserManagerImpl _userManager;

        public AuthRepository()
        {
            _context = new SecurityDbContext();
            _userManager = new ApplicationUserManagerImpl();
            _unitOfWork = new UnitOfWorkAsync(_context);
        }

        #region user



        public async Task<IdentityResult> RegisterUser(
            string name,
            string phoneNumber,
            string email,
            string userName,
            string password,
            string role = "User"
            )
        {
            var user = GetApplicationUser(
                name,
                phoneNumber,
                email,
                userName);

            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded) return result;
            await AddRoleToUser(user.Id, role);
            await SendEmailConfirmation(user);
            return result;
        }

        public async Task<IdentityResult> CreateUser(
         string name,
         string phoneNumber,
         string email,
         string userName, string password,
         int age,
         string soicalLinks,
         string address,
         string photoUrl
         )
        {
            var user = GetApplicationUser(
                name,
                phoneNumber,
                email,
                userName);

            user.Age = age;
            user.SoicalLinks = soicalLinks;
            user.Address = address;
            user.PhotoUrl = photoUrl;
            user.EmailConfirmed = true;

            var result = await _userManager.CreateAsync(user, password);
            return result;
        }

        public async Task<IdentityResult> UpdateUser(string userid,
       string name,
       string phoneNumber,
       string email,
       int age,
       string soicalLinks,
       string address,
       string photoUrl
       )
        {

            ApplicationUser user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == userid);

            user.Name = name;
            user.PhoneNumber = phoneNumber;
            user.Email = email;
            user.Age = age;
            user.SoicalLinks = soicalLinks;
            user.Address = address;
            user.EmailConfirmed = true;
            if (!string.IsNullOrEmpty(photoUrl))
            {
                user.PhotoUrl = photoUrl;
            }
            return await _userManager.UpdateAsync(user);

        }
        public async Task ForgetPassword(string email)
        {

            var user = await _userManager.FindByEmailAsync(email);
            await SendPasswordResetToken(user);

        }
        public async Task<IdentityResult> ResetPassword(string userId = "", string code = "", string newPassword = "")
        {

            IdentityResult result = await _userManager.ResetPasswordAsync(userId, code, newPassword);
            return result;
        }
        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public async Task<List<ApplicationUserDto>> FindAllUser(int pageNumber, int pageSize, string keyword)
        {
            var user = await _userManager.Users.Where(x =>
               string.IsNullOrEmpty(keyword)
               || x.Name.ToLower().Contains(keyword.ToLower())
               || x.UserName.ToLower().Contains(keyword.ToLower())
               || x.Email.ToLower().Contains(keyword.ToLower())
               || x.PhoneNumber.ToLower().Contains(keyword.ToLower()))
               .OrderBy(x => x.Id).Skip(pageSize * (pageNumber - 1)).Take(pageSize)
               .Select(x => new ApplicationUserDto()
               {
                   Address = x.Address,
                   Age = x.Age,
                   Email = x.Email,
                   Id = x.Id,
                   Name = x.Name,
                   PhoneNumber = x.PhoneNumber,
                   RoleName = x.Roles.Any(r => r.RoleId == "841bef54-4694-469b-91f8-8c52e35ecec2") ? "سفير" : "مستخدم",
                   UserName = x.UserName,
                   IsDeleted = x.IsDeleted
               }).ToListAsync();

            return user;
        }

        public async Task<List<ApplicationUser>> FindAllUser(string keyword)
        {
            var user = await _userManager.Users.Where(x =>
               string.IsNullOrEmpty(keyword)
               || x.Name.ToLower().Contains(keyword.ToLower())
               || x.UserName.ToLower().Contains(keyword.ToLower())
               || x.Email.ToLower().Contains(keyword.ToLower())
               || x.PhoneNumber.ToLower().Contains(keyword.ToLower())).OrderBy(x => x.Id).ToListAsync();
            return user;
        }


        /// <summary>
        /// update User Info like Name && Phone Number
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="name"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>0 if User doesn't exist or 1 if user data updated successfully </returns>
        public int UpdateUser(string userId, string name, string phoneNumber, int age, string address)
        {

            ApplicationUser user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
            {
                return 0;
            }
            user.Name = name;
            user.PhoneNumber = phoneNumber;
            user.Age = age;
            user.Address = address;
            _context.SaveChanges();
            return 1;

        }

        /// <summary>
        /// update user photo
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="imgUrl"></param>
        /// <returns>0 if User doesn't exist or 1 if user data updated successfully</returns>
        public int UpdateUserImage(string userId, string imgUrl)
        {

            ApplicationUser user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
            {
                return 0;
            }
            user.PhotoUrl = imgUrl;
            _context.SaveChanges();
            return 1;


        }



        public bool CheckifPhoneAvailable(string phoneNumber)
        {

            return _context.Users.Any(u => u.PhoneNumber == phoneNumber);
        }

        public bool CheckifPhoneAvailable(string phoneNumber, string userId)
        {

            return _context.Users.Any(u => u.PhoneNumber == phoneNumber && u.Id != userId);
        }




        public async Task<bool> IsEmailConfirme(string userId)
        {
            return await _userManager.IsEmailConfirmedAsync(userId);
        }
        public async Task<bool> IsUserArchieve(string userId)
        {
            var user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == userId);
            return user.IsDeleted != null && user.IsDeleted.Value;
        }

        public async Task<ApplicationUser> FindUser(string email)
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(email);
            return user;
        }

        public ApplicationUser FindUserbyEmail(string email)
        {
            ApplicationUser user = _userManager.FindByEmail(email);
            return user;
        }

        public async Task<IdentityResult> ConfirmEmail(string userId, string code)
        {
            IdentityResult result = await this._userManager.ConfirmEmailAsync(userId, code);
            return result;
        }

        public IList<string> GetRoles(string userId)
        {
            IList<string> lst = _userManager.GetRoles(userId);
            return lst;
        }

        private EmailSetting GetEmailMessage(string toName, string code, string messageTemplate)
        {
            EmailSetting emailSettings = _unitOfWork.EmailSetting.GetEmailSetting(messageTemplate);
            emailSettings.MessageBodyAr = emailSettings.MessageBodyAr.Replace("@FullName", toName);
            emailSettings.MessageBodyAr = emailSettings.MessageBodyAr.Replace("@code", "Code=" + code);

            return emailSettings;
        }
        private ApplicationUser GetApplicationUser(string name, string phone, string email, string userName)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userName,
                PhoneNumber = phone,
                Email = email,
                Name = name,
            };
            return user;
        }

        public async Task<IdentityResult> AddRoleToUser(string userId, string role)
        {
            var result = await _userManager.AddToRoleAsync(userId, role);
            return result;
        }

        private async Task SendEmailConfirmation(ApplicationUser user)
        {

            var code = await GenerateToken(user.Id, EmailType.EmailConfirmation);
            await SendEmail(user, code, EmailType.EmailConfirmation.GetHashCode().ToString());
        }
        public async Task ReSendEmailConfirmation(ApplicationUser user)
        {

            var code = await GenerateToken(user.Id, EmailType.EmailConfirmation);
            await SendEmail(user, code, EmailType.EmailConfirmation.GetHashCode().ToString());
        }
        private async Task SendPasswordResetToken(ApplicationUser user)
        {
            var code = await GenerateToken(user.Id, EmailType.ForgetPassword);
            await SendEmail(user, code, EmailType.ForgetPassword.GetHashCode().ToString());
        }
        private async Task<string> GenerateToken(string userId, EmailType emailType)
        {
            switch (emailType)
            {
                case EmailType.EmailConfirmation:
                    return await _userManager.GenerateEmailConfirmationTokenAsync(userId);
                case EmailType.ForgetPassword:
                    return await _userManager.GeneratePasswordResetTokenAsync(userId);
                default:
                    return "";
            }

        }
        public async Task SendEmail(ApplicationUser user, string code, string messageTemplate)
        {
            EmailSetting emailMessage = GetEmailMessage(user.UserName, code, messageTemplate);
            await _userManager.SendEmailAsync(user.Id, messageTemplate, emailMessage.MessageBodyAr);
        }
        public async Task<IdentityResult> ChangePassword(string userId, string oldPassword, string newPassword)
        {
            IdentityResult result = await _userManager.ChangePasswordAsync(userId, oldPassword, newPassword);
            return result;
        }


        #endregion
        
        #region token

        public Client FindClient(string clientId)
        {
            var client = _context.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken =
                _context.RefreshTokens
                    .Where(r => r.Subject == token.Subject && r.ClientId == token.ClientId);

            if (existingToken.Count()>0)
            {
                var result = await RemoveRefreshTokens(existingToken);
            }

            _context.RefreshTokens.Add(token);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _context.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                _context.RefreshTokens.Remove(refreshToken);
                return await _context.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _context.RefreshTokens.Remove(refreshToken);
            return await _context.SaveChangesAsync() > 0;
        }
        public async Task<bool> RemoveRefreshTokens(IQueryable<RefreshToken> refreshTokens)
        {
            _context.RefreshTokens.RemoveRange(refreshTokens);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _context.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return _context.RefreshTokens.ToList();
        }

        #endregion

        #region Soical
        public async Task<ApplicationUser> FindAsync(UserLoginInfo loginInfo)
        {
            ApplicationUser user = await _userManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user)
        {
            var result = await _userManager.CreateAsync(user);
            if (result.Succeeded)
            {
                await AddRoleToUser(user.Id, "User");
            }

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }
        #endregion

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (this._context == null)
            {
                return;
            }

            this._context.Dispose();
            this._userManager.Dispose();
            this._context = null;
            this._unitOfWork.Dispose();
        }

        public async Task<ApplicationUser> FindUserByUserName(string userName)
        {
            ApplicationUser user = await _userManager.Users
                .SingleOrDefaultAsync(x => x.UserName == userName);
            return user;
        }
        public ApplicationUser CheckUserNameExist(string userName)
        {
            ApplicationUser user = _userManager.Users.SingleOrDefault(x => x.UserName == userName);
            return user;
        }
       
        public async Task<ApplicationUser> FindUserByUserId(string id)
        {
            ApplicationUser user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == id);
            return user;
        }

        public async Task<IdentityResult> EditImage(string picture, string id)
        {
            ApplicationUser user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == id);
            user.PhotoUrl = picture;
            return await _userManager.UpdateAsync(user);
        }

        public bool IsInRole(string id, string role)
        {
            return _userManager.IsInRole(id, role);
        }

        public async Task<ApplicationUser> FindUserById(string userId)
        {
            ApplicationUser user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == userId);
            return user;
        }

        public async Task<IdentityResult> ValidateAsync(string email, string phoneNumber, string userId = "")
        {
            List<string> errors = new List<string>();

            if (!string.IsNullOrEmpty(email))
            {
                var findAsyncByEmail = await FindAsyncByEmail(email, userId);
                if (findAsyncByEmail != null)
                {
                    string errorMsg = "Email '" + email + "' is already related to User.";
                    errors.Add(errorMsg);
                }

            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                var findAsyncByEmail = await FindAsyncByPhone(phoneNumber, userId);
                if (findAsyncByEmail != null)
                {
                    string errorMsg = "PhoneNumber '" + phoneNumber + "' is already related to User.";
                    errors.Add(errorMsg);
                }
            }




            return errors.Any()
                ? IdentityResult.Failed(errors.ToArray())
                : IdentityResult.Success;
        }

        private async Task<ApplicationUser> FindAsyncByPhone(string phoneNumber, string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                ApplicationUser user = await _userManager.Users.
                    SingleOrDefaultAsync(x => x.PhoneNumber == phoneNumber);
                return user;
            }
            else
            {
                ApplicationUser user = await _userManager.Users.
                    SingleOrDefaultAsync(x => x.PhoneNumber == phoneNumber && x.Id != userId);
                return user;
            }

        }

        private async Task<ApplicationUser> FindAsyncByEmail(string email, string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                ApplicationUser user = await _userManager.Users.SingleOrDefaultAsync(x => x.Email == email);
                return user;
            }
            else
            {
                ApplicationUser user = await _userManager.Users.
                    SingleOrDefaultAsync(x => x.Email == email && x.Id != userId);
                return user;
            }
        }


        public async void DeleteById(string userId)
        {
            ApplicationUser user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == userId);
            if (user != null)
                _userManager.Delete(user);

        }

        public async Task<int> IsRoleExit(string role)
        {
            var item = await _context.Roles.FirstOrDefaultAsync(z => z.Name == role);
            return item != null ? 1 : 0;
        }

        public async Task<IdentityResult> RemoveFromRole(string userId, string role)
        {
            return await _userManager.RemoveFromRoleAsync(userId, role);
        }
        public async Task<IdentityResult> RemoveFromRoles(string userId, string[] role)
        {
            return await _userManager.RemoveFromRolesAsync(userId, role);
        }
        public async Task<IList<string>> GetUserRoles(string userId)
        {
            return await _userManager.GetRolesAsync(userId);
        }
        public async Task<List<IdentityRole>> GetRoles()
        {
            return await _context.Roles.Select(x => x).ToListAsync();
        }


        public async Task<IList<ApplicationUser>> GetDuplicatedUsers(string email, string phoneNumber)
        {
            return await _userManager.Users.Where(x => x.Email == email || x.UserName == phoneNumber).ToListAsync();
        }


        public ApplicationUser FindUserByName(string userName)
        {
            ApplicationUser user = _userManager.Users.SingleOrDefault(x => x.UserName == userName);
            return user;
        }

    }
}