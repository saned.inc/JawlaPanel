﻿using System;
using Saned.Core.Security.Core;
using Saned.Core.Security.Core.Models;

namespace Saned.Core.Security.Persistence.Tools
{
    public class EmailManager : IDisposable
    {
        private readonly IUnitOfWorkAsync _unitOfWork;

        public EmailManager()
        {
            _unitOfWork = new UnitOfWorkAsync();

        }

    
        public string SendActivationEmail(string messageTamplate, string toEmail, string messageBodyAr)
        {
            EmailSetting emailSettings = _unitOfWork.EmailSetting.GetEmailSetting(messageTamplate);

            var result = Utilty.SendMail(emailSettings.Host,
                emailSettings.Port, 
                emailSettings.FromEmail,
                emailSettings.Password,
                toEmail, 
                emailSettings.SubjectAr,
                messageBodyAr, "");

            return result;

        }


        public string SendActivationEmail(
            string messageTamplate,
            int port,
            string toEmail,
            string messageBodyAr,
            string host,
            string fromEmail,
            string password,
            string subjectAr
            )
        {

            var result = Utilty.SendMail(host, port, fromEmail, password, toEmail, subjectAr, messageBodyAr, "");
            return result;

        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _unitOfWork.Dispose();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
