﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Saned.Core.Security.Persistence.Infrastructure
{
    public partial class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            IsDeleted = false;
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string ConfirmedToken { get; set; }
        public string ResetPasswordlToken { get; set; }
        public bool? IsDeleted { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public int Age { get; set; }
        public string SoicalLinks { get; set; }
        public string Address { get; set; }
        
    }
}
