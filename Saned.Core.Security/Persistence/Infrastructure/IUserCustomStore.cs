using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Saned.Core.Security.Persistence.Infrastructure
{
    public interface IUserCustomStore<TUser> : IUserStore<TUser> , IDisposable where TUser : ApplicationUser, IUser<string>
    {
        Task<TUser> FindByPhoneNumberAsync(string phoneNumber);
        Task<TUser> FindByEmailAddressAsync(string phoneNumber);
    }
}