﻿using Saned.Core.Security.Persistence.Providers;

namespace Saned.Core.Security.Persistence.Infrastructure
{
    public class ApplicationUserManagerImpl : ApplicationUserManager<ApplicationUser>
    {
        public ApplicationUserManagerImpl() : base(new ApplicationUserStoreImpl())
        {
            this.UserTokenProvider = new ApplicationTokenProvider<ApplicationUser>();

        }

        
    }
}