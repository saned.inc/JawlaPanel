﻿using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Saned.Core.Security.Core.Models;
using Saned.Core.Security.Migrations;
using Saned.Core.Security.Persistence.EntityConfigurations;
using Saned.Core.Security.Persistence.Infrastructure;

namespace Saned.Core.Security
{
    public class SecurityDbContext : IdentityDbContext<ApplicationUser>, ISecurityDbContext
    {
        public DbSet<EmailSetting> EmailSettings { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        // static string _connection;



        public static string _connection
        {
            get
            {

                return "connectionString";
            }
            set
            {
                if (string.IsNullOrEmpty(_connection))
                    _connection = "connectionString";
            }

        }

        public SecurityDbContext() : base("connectionString")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public SecurityDbContext(string connectionString) : base(connectionString)
        {
            _connection = connectionString;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SecurityDbContext, Configuration>());
            var configuration = new Configuration();
            var migrator = new System.Data.Entity.Migrations.DbMigrator(configuration);
            if (migrator.GetPendingMigrations().Any())
            {
                migrator.Update();
            }
        }
        //public static SecurityDbContext Create()
        //{
        //    return new SecurityDbContext();
        //}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new EmailSettingConfigurations());
            modelBuilder.Configurations.Add(new ClientConfigurations());
            modelBuilder.Configurations.Add(new RefreshTokenConfigurations());
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            base.OnModelCreating(modelBuilder);
        }



    }

}
