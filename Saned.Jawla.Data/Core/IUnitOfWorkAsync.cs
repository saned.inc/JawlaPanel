﻿using System.Threading.Tasks;
using Saned.Common.Rating.Repository;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.Photos;
using Saned.Modules.Views;

namespace Saned.Jawla.Data.Core
{
    public interface IUnitOfWorkAsync
    {
        IAmbassadorRequestRepository AmbassadorRequest { get; }
        IApplicationContactInformationRepository ApplicationContact { get; }

        ICategoryDetailsRepository CategoryDetails { get; }
        ICategoryDetailsFaqRepository Faq { get; }
        ICategoryDetailsBrancheRepository Branche { get; }
        ICategoryRepository Category { get; }

        IApplicationContactUsRepository ApplicationContactUsRepository { get; }

        IPhotoRepository PhotoRepository { get; set; }
        IEventDetailsRepository EventDetails { get; }
        IProjectDetailsRepository ProjectDetails { get; }
        IPlaceDetailsRepository PlaceDetails { get; }
        IRatingElementRepositoryAsync Rating { get; }
        ICategoryDetailsContactRepository Contact { get; }
        ICountryRepository Country { get; }
        ICityRepository City { get; }
        IMunicipalityRepository Municipality { get; }
        IFavouriteRepository Favourite { get; }
        IAdvertisementRepository Advertisement { get; }
        IVisitorCoverRepository VisitorCover { get; }
        IHitCountDetailsRepository HitCountDetailsRepository { get; set; }

        IUserRepository UserRepository { get; set; }
        Task<int> Complete();
        
       


    }
}