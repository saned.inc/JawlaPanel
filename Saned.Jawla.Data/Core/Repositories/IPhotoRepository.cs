using System.Collections.Generic;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Modules.Photos;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IPhotoRepository
    {
        Task<object> CreateAsync(Photo entity);
        Task<int> DeleteAsync(int id);
        Task<int> DeleteAsyncByRelatedId(string relatedId, int relatedType);
        Task<int> UpdateAsync(Photo entity);
        Task<Photo> GetAsync(int id);
        Photo GetPhoto(int id);
        void RemoveImages(List<int> ids, long relatedId);

        Task<IList<Photo>> FindAllAsync(string relatedId, int relatedType, string lang = null);
        Task<IList<PhotoDto>> FindAllAsync(string relatedId, int relatedType, int pageIndex, int pageSize, string lang);

        void DeleteMainImage(long relatedId);




    }
}