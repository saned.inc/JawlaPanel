﻿using System.Collections;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using System.Collections.Generic;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICityRepository
    {
        void CreateAsync(City entity);
        void DeleteAsync(City entity);
        void DeleteById(int id);
        void Update(City Entity);

        Task<IList> DeleteByIdReturnAll(int id);
        Task<City> GetById(int id);
        Task<IList<City>> GetCityByCountryId(int CountryId);
        Task<IList<City>> GetCityByCountryIdWithPaging(int CountrId, int pageNumber, int pageSize);


        Task<IList> FindAsync(int countryId);

        Task<IList> FindAsyncWitharabicName(int countryId);

        Task<IList> GetAllCitiesComplete(int pageIndex, int pageSize);
        Task<IList> GetAllCitiesCompleteWithPaging(int pageIndex, int pageSize);

        Task<IList> FindAllAsync(int pageIndex, int pageSize, int countryId);
        Task<IList> GetAsync();


    }
}