﻿using Saned.Jawla.Data.Core.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IApplicationContactUsRepository
    {
        void CreateAsync(ApplicationContactUs entity);
        void DeleteAsync(ApplicationContactUs entity);

        Task<List<ApplicationContactUs>> GetAsync();
        Task<ApplicationContactUs> FindAsync(int id);
    }
}
