﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;
using Saned.Modules.Photos;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IVisitorCoverRepository
    {
        void CreateAsync(VisitorCover entity);
        Task<VisitorCoverCategoryDetailsDto> GetAsync(long id, string userId = null);
        Task<IList> GetAsync(string userId, int pageNumber, int pageSize);
        Task<VisitorCover> GetAsync(string userId, long categoryDetailsId);
        Task<IList<Photo>> FindAllAsync(long categoryDetailsId);

        Task<long> GetVistorCoverId(long categoryDetailsId);
    }
}