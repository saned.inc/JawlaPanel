﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICategoryDetailsContactRepository
    {
        Task<List<CategoryDetailsContact>> FindAllAsync(long categoryDetailsId, int type);
        void AddRange(List<CategoryDetailsContact> viewModelContacts);


        void RemoveRange(long categoryId);
    }


}