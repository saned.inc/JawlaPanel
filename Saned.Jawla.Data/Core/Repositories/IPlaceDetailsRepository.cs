﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IPlaceDetailsRepository
    {
        void CreateAsync(HotelsDetails entity);
        void DeleteAsync(HotelsDetails entity);
        Task<IList<HotelsDetails>> FindAllAsync(int pageIndex, int pageSize);
        Task<HotelDetailsDto> GetAsync(long id, string userId = null);
        IQueryable<HotelsDetails> GetAsync();
        HotelsDetails GetPlaceById(long id);
    }
}