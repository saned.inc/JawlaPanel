using System.Collections;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Dtos;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IAdvertisementRepository
    {
        void CreateAsync(Advertisement entity);
        Task<IList> FindAllAsync(int pageIndex, int pageSize);
        Task<IList> GetAsync();
        void Delete(int id);
        Task<Advertisement> SelectById(int id);
        Task<AdvertisementPagingDTO> AdminFindAllAsync(int pageIndex, int pageSize, string keyword);

        Task UpdateAsync(Advertisement entity);
    }
}