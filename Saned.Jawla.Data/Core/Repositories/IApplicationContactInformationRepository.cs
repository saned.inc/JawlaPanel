﻿using Saned.Jawla.Data.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IApplicationContactInformationRepository
    {
        void CreateAsync(ApplicationContactInformation entity);
        void DeleteAsync(ApplicationContactInformation entity);

        void DeleteById(int id);
        Task<ApplicationContactInformation> GetById(int id);

        Task<ApplicationContactInformation> GetAsync();


    }
}
