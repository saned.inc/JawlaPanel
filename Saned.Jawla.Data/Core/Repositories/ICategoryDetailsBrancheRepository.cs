using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICategoryDetailsBrancheRepository
    {
        void CreateAsync(CategoryDetailsBranche entity);
        Task<IList> FindAllAsync(int pageIndex, int pageSize, long categoryDetailsId);
        Task<IList> FindAllAsync(long categoryDetailsId);

        void AddRange(List<CategoryDetailsBranche> branches);

        Task<List<CategoryDetailsBranche>> FindAllAsyncWithInclude(long categoryDetailsId);
        void RemoveRangeByCategoryDetailsId(long categoryDetailsId);
    }
}