using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICategoryDetailsRepository
    {
        CategoryDetails CreateAsync(CategoryDetails entity);
        void DeleteAsync(CategoryDetails entity);
        Task<IList<CategoryDetails>> FindAllAsync(int pageIndex, int pageSize);
        Task<IList> FindAsync(int? categoryId, int? subCategoryId, int? countryId = null, int? cityId = null, int? municipalityId = null);
        Task<CategoryDetails> FindAsync(long id);
        Task<CategoryDetails> GetAsync(long categoryDetailsId);
        Task<IList> FindAsync(string ambassadorId, int pageIndex, int pageSize);
        Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize, int? countryId, int? cityId, int? municipalityId, int? categoryId, long? categoryDetailsId, string longitude, string latitude,int? parentCategoryId, DateTime? eventStatyDate = null);

        Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize, int? countryId,
            int? cityId, int? municipalityId, int? categoryId, long? categoryDetailsId, string longitude,
            string latitude, int? status, int? parentCategoryId, DateTime? eventStatyDate = null, string ambassadorId = null);
        //Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize, int? countryId, int? cityId, int? municipalityId, int categoryDetailsId, out int totalCount);
        Task<CategoryDetails> GetById(long id);
        //Task<CategoryDetails> GetRestaurantById(long viewModelId);
        void RemoveImagesbyCategoryId(long id);
        
    }
}