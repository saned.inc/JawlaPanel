﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Dtos;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICategoryRepository
    {
        void CreateAsync(Category entity);
        void DeleteAsync(Category entity);
        void DeleteById(int id);

       
        Task<IList<Category>> FindAllAsync(int pageIndex, int pageSize);

        Task<IList<ChildCategoryDto>> FindAllChildAsync(int pageIndex, int pageSize);

        Task<IList<Category>> FindAllParentAsync();

        IQueryable<Category> GetAsync();
        Task<Category> GetAsync(int categorytId);
        Task<IList<Category>> GetAllCategories();
        Task<IList<Category>> GetCategoryByParentId(int parentCategoryId);

        Task<IList<MainCategoryDto>> FindAllMainCategoryAsync();
    }
}
