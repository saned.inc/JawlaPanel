﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IFavouriteRepository
    {
        void CreateAsync(Favourite entity);
        void DeleteAsync(Favourite entity);

        Task<Favourite> FindAsync(int id);
        Task<Favourite> FindAsync(string userId, long detailsId);
        Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize,string userId, int? countryId, int? cityId, int? municipalityId, int? categoryId, long? categoryDetailsId, string longitude, string latitude, DateTime? eventStatyDate = null);

    }
}