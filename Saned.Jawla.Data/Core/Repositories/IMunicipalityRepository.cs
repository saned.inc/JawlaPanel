﻿using System.Collections;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IMunicipalityRepository
    {
        void CreateAsync(Municipality entity);
        void DeleteAsync(Municipality entity);
        Task<IList> DeleteByIdReturnAll(int id);
        void DeleteById(int id);
        void Update(Municipality entity);
        Task<IList> GetMunicipalityByCityById(int cityId);
        Task<Municipality> GetById(int id);
        Task<IList> FindAsync(int cityId);
        Task<IList> GetAsync();

        Task<IList> GetAsyncWithPaging(int pageNumber , int pageSize);
    }
}