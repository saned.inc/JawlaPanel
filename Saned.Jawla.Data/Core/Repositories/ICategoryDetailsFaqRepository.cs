﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICategoryDetailsFaqRepository
    {
        void CreateAsync(CategoryDetailsFaq entity);

        Task<IEnumerable<CategoryDetailsFaqDto>>  FindAllAsync(int pageIndex, int pageSize, long categoryDetailsId);


        Task<List<CategoryDetailsFaq>> GetAllAsync(long categoryDetailsId);


        void RemoveRange(List<CategoryDetailsFaq> questions);


    }
}