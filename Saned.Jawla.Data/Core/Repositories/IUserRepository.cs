﻿using Saned.Jawla.Data.Core.Dtos;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IUserRepository
    {
        Task<IList> GetUserComment(string userId);
        Task<IList> GetUserPhoto(string userId);
        Task<IList> GetUserFavourites(string userId);

        Task<IList> GetUserRating(string userId);
       
        Task DeleteUserRating(int id);
        Task SetUserIsDeleted(bool isDeleted, string userId);

        Task DeleteUser(string userId);
        List<VisitorCoverDTO> GetAll(int categoryDetailId);
    }
}