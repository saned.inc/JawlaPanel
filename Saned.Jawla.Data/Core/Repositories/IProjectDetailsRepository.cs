using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IProjectDetailsRepository
    {
        void CreateAsync(ProjectDetails entity);
        void DeleteAsync(ProjectDetails entity);
        Task<IList<ProjectDetails>> FindAllAsync(int pageIndex, int pageSize);
        IQueryable<ProjectDetails> GetAsync();
        Task<ProjectDetails> GetAsync(long id);
        Task<ProjectDetailsDto> GetAsync(long id, string userId = null);
        ProjectDetails GetFeatureProjectById(long id);
    }
}