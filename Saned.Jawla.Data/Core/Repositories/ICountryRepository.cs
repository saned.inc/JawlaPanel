﻿using System.Collections;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using System.Collections.Generic;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface ICountryRepository
    {
        void CreateAsync(Country entity);
        void DeleteAsync(Country entity);

        void Update(Country entity);
        Task<IList> FindAllAsync(int pageIndex, int pageSize);

        Task<IList<Country>> GetAllComplete(int pageIndex, int pageSize);
        Task<List<Country>> GetAllCompleteWithPaging(int pageIndex, int pageSize);

        Task<List<Country>> SearchCountryWithPaging(int pageIndex, int pageSize, string keyword);

        Task<int> SearchAllComplete(int pageIndex, int pageSize, string keyword);


        void DeleteById(int id);

        Task<IList> DeleteByIdReturnAll(int id);
        Task<Country> GetById(int id);

        Task<IList> GetAsync();
        Task<IList> GetAllAsync();
        Task<IList> GetAsyncWithArabic();
    }
}