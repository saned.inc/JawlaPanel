﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IEventDetailsRepository
    {
        void CreateAsync(EventDetails entity);
        void DeleteAsync(EventDetails entity);
        Task<IList<EventDetails>> FindAllAsync(int pageIndex, int pageSize);
        IQueryable<EventDetails> GetAsync();
        //Task<EventDetails> GetAsync(long id);
        Task<EventDetailsDto> GetAsync(long id, string userId = null);


        EventDetails GetEventById(long id);
    }
}
