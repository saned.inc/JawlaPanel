﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Core.Repositories
{
    public interface IAmbassadorRequestRepository
    {
        void CreateAsync(AmbassadorRequest entity);
        void DeleteAsync(AmbassadorRequest entity);

        Task<IList<AmbassadorRequest>> FindAllAsyncByStatus(int pageIndex, int pageSize, int? status = null);
        Task<IList<AmbassadorRequest>> FindAllAsyncByStatus(int? status = null);

        IQueryable<AmbassadorRequest> GetAsync();

        Task<AmbassadorRequest> GetAsync(long requestId);
        Task<IList<AmbassadorRequestDto>> FindAllAsync(int pageNumber, int pageSize);

        Task<AmbassadorRequest> FindAsyncByUserId(string userId);
        Task<IList<AmbassadorRequest>> FindAsyncByPhone(string phoneNumber);
        Task<IList<AmbassadorRequest>> FindAsyncByEmail(string email);
        Task<IdentityResult> ValidateAsync(List<string> errors, string email, string phoneNumber, string userId = null);
        Task<IList> GetAllAsync();

        IQueryable<AmbassadorRequest> GetIQuerable();

        Task DeactivateCategoryDetailsByUserId(string userId);
    }
}