﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Dtos
{
    public class VisitorCoverDTO
    {
        public string UseId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public Int64 Id { get; set; }
        public Int64? AmbassadorRequestId { get; set; }
        public bool IsAmbassadoor { get; set; }
    }
}
