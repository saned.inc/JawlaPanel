﻿
namespace Saned.Jawla.Data.Core.Dtos
{
    public class SelectItemDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ArabicName { get; set; }
    }
    public class AmbassadorSelectItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }
}
