﻿namespace Saned.Jawla.Data.Core.Dtos
{
    public class UserFavouritesDTO
    {

        public string CategoryDetailsName { get; set; }
        public string CategoryName { get; set; }
    }
}
