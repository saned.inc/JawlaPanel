﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Dtos
{
    public class UserCoverDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public int CoverId { get; set; }
        public string CoverModifiedDate { get; set; }
        public int  UserRating { get; set; }
        public int CommentCount  { get; set; }
        public int PhotoCount  { get; set; }
    }
}
