﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Dtos
{
    public class MainCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public int OverAllCount { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool HasChildren { get; set; }
    }

    public class ChildCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public int OverAllCount { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ParentName { get; set; }
    }
}
