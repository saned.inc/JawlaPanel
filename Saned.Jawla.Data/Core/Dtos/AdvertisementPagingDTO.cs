﻿using Saned.Jawla.Data.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Dtos
{
    public class AdvertisementPagingDTO
    {
        public int TotalCount { get; set; }
        public List<Advertisement> Data { get; set; }
    }
}
