﻿namespace Saned.Jawla.Data.Core.Dtos
{
    public class UserPhotosDTO
    {
        public int Id { get; set; }
        public string PhotoUrl { get; set; }
        public string Title { get; set; }
        public string CategoryDetailsName { get; set; }
        public string CategoryName { get; set; }
    }
}
