using System;

namespace Saned.Modules.Photos
{
    public class PhotoDto
    {
        public int Id { get; set; }
        public string RelatedId { get; set; }
        public int RelatedTypeId { get; set; }
        public string PhotoUrl { get; set; }
        public string Lang { get; set; }
        public bool IsDefault { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Title { get; set; }
        public int OverAllCount { get; set; }
    }
}