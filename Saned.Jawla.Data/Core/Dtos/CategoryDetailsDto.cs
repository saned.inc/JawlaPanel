﻿namespace Saned.Jawla.Data.Core.Dtos
{
    public class CategoryDetailsDto
    {
        public long? Id { get; set; }
        //public int CategoryId { get; set; }
        public string Name { get; set; }
        //public int CountryId { get; set; }
        //public int CityId { get; set; }
        //public int MunicipalityId { get; set; }
        //public string Latitude { get; set; }
        //public string Longitude { get; set; }
        //public DateTime ModifiedDate { get; set; }
        //public bool? IsDeleted { get; set; }
        public bool? IsBin { get; set; }
        // public int? Status { get; set; }
        //public string AmbassadorId { get; set; }
        //public DateTime EventStartDate { get; set; }
        //public string Description { get; set; }
        //public string Owner { get; set; }
        //public long ProjectId { get; set; }
        //public long HotelId { get; set; }
        //public string WorkHours { get; set; }
        //public long EventId { get; set; }
        //public DateTime EndDate { get; set; }
        public int? ParentCategoryId { get; set; }
        public string Category { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Municipality { get; set; }
        public int Rating { get; set; }
        public int AllHit { get; set; }
        public string EventStartDate { get; set; }
        public string ImageUrl { get; set; } = "";
        public int OverAllCount { get; set; }
        public int CategoryId { get; set; }
        public bool IsFavourite { get; set; }

    }
}