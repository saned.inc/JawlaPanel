﻿using System;

namespace Saned.Jawla.Data.Core.Dtos
{
    class UserCommentsDTO
    {
        public int Id { get; set; }
        public string CommentText { get; set; }
        public string CategoryDetailsName { get; set; }
        public string CategoryName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
