﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Dtos
{
    public class AmbassadorRequestDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public int OverAllCount { get; set; }
    }

   


}
