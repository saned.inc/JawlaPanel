﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Dtos
{
    public class UserRatingDTO
    {
        public Int64 Id { get; set; }
        public int TotalDegree { get; set; }
        public DateTime RatingDate { get; set; }
        public string CategoryDetailsName { get; set; }
        public string CategoryName { get; set; }
    }
}
