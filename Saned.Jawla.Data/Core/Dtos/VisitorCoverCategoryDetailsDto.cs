﻿namespace Saned.Jawla.Data.Core.Dtos
{
    public class VisitorCoverCategoryDetailsDto
    {
        public  long CategoryDetailsId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public int Rating { get; set; }
        public int AllHit { get; set; }
        public int UserRating { get; set; }
        public string AmbassadorId { get; set; }
        public int CategoryId { get; set; }
        public string Ambassador { get; set; }
    }
}