namespace Saned.Jawla.Data.Core.Dtos
{
    public class CategoryDetailsFaqDto
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}