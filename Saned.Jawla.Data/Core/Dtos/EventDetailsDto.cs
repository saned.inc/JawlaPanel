﻿namespace Saned.Jawla.Data.Core.Dtos
{
    public class EventDetailsDto
    {
        public long? Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }

        public bool? IsBin { get; set; }

        public string Category { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Municipality { get; set; }
        public int Rating { get; set; }
        public int AllHit { get; set; }
        public string EventStartDate { get; set; }
        public string EventEndDate { get; set; }
        //public string ImageUrl { get; set; }
        public int OverAllCount { get; set; }
        public string Owner { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public long EventId { get; set; }
        public string Description { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string AmbassadorId { get; set; }
        public int UserRating { get; set; }
        public string Ambassador { get; set; }
    }
}