﻿namespace Saned.Jawla.Data.Core.Enum
{
    public enum CategoryTypeEnum
    {
        Restaurant = 1,
        TouristPlace = 2,
        HistoricalPlace = 5,
        Hotels = 6,
        Event = 7,
        Project =8,
    }
}