﻿namespace Saned.Jawla.Data.Core.Enum
{
    public enum AmbassadorRequestStatusEnum
    {
        New = 1,
        Approve = 2,
        Reject = 3
    }
}