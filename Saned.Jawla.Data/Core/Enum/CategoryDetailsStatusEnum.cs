namespace Saned.Jawla.Data.Core.Enum
{
    public enum CategoryDetailsStatusEnum
    {
        New = 1,
        Approve = 2,
        Reject = 3
    }
}