﻿

namespace Saned.Jawla.Data.Core.Models
{
    public class City 
    {
        public City()
        {
            IsDeleted = false;
        }

        public int Id { get; set; }
        public string ArabicName { get; set; }
        public string EnglishName { get; set; }
        public bool? IsDeleted { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        void Archieve()
        {
            IsDeleted = true;
          
        }

        public void Update(string arabicName, string englishName, string latitude, string longitude,int countryId)
        {
            ArabicName = arabicName;
            EnglishName = englishName;
            Latitude = latitude;
            Longitude = longitude;
            CountryId = countryId;
        }

        public void Modify(string name, string englishName)
        {
            ArabicName = name;
            EnglishName = englishName;
        }
    }

}
