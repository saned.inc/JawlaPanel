﻿using System;

namespace Saned.Jawla.Data.Core.Models
{
    public class Advertisement
    {
        public Advertisement()
        {
            ModifiedDate = new DateTime();
            IsDeleted = false;
            IsShow = false;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string Url { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public bool IsShow { get; set; }
        public int SecondNumber { get; set; }
        public string AmbassadorId { get; set; }

    }
}