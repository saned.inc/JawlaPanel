﻿using System;

namespace Saned.Jawla.Data.Core.Models
{
    public class CategoryDetailsBranche
    {
        public CategoryDetailsBranche()
        {
            ModifiedDate = DateTime.Now;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public int? MunicipalityId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public long CategoryDetailsId { get; set; }
        public virtual CategoryDetails CategoryDetails { get; set; }

        public virtual Country Country { get; set; }
        public virtual City City { get; set; }
        public virtual Municipality Municipality { get; set; }
    }
}
