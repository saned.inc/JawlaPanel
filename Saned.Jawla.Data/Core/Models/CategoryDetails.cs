﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Enum;

namespace Saned.Jawla.Data.Core.Models
{
    public class CategoryDetails
    {
        public long Id { get; set; }

        public CategoryDetails()
        {
            ModifiedDate = DateTime.Now;
            IsDeleted = false;
            IsBin = false;
        }

        public int CategoryId { get; set; }

        public string Name { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int? MunicipalityId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public DateTime ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsBin { get; set; }
        public string Status { get; set; }
        public string AmbassadorId { get; set ; }
        public DateTime? EventStartDate { get; set; } = null;
        public string Description { get; set; }

        public virtual Country Country { get; set; }
        public virtual City City { get; set; }
        public virtual Municipality Municipality { get; set; }
        public virtual Category Category { get; set; }
        public EventDetails Events { get; set; }
        public HotelsDetails Hotels { get; set; }
        public ProjectDetails Projects { get; set; }

        public List<CategoryDetailsContact> CategoryDetailsContacts { get; set; }
        public List<CategoryDetailsFaq> CategoryDetailsFaqs { get; set; }

        public void Approve()
        {
            Status = CategoryDetailsStatusEnum.Approve.GetHashCode().ToString();
        }

    }

}
