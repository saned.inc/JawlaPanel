﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Saned.Jawla.Data.Core.Models
{
    public class Category
    {
        public Category()
        {
            ModifiedDate = DateTime.Now;
            IsDeleted = false;
            ChildCategories = new HashSet<Category>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }

        public int? ParentId { get; set; }

        [ForeignKey("ParentId")]
        public Category ParentCategory { get; set; }

        public virtual ICollection<Category> ChildCategories { get; set; }
        

        public void Update(string name, string photoUrl)
        {
            Name = name;
            PhotoUrl = photoUrl;
        }
    }
}
