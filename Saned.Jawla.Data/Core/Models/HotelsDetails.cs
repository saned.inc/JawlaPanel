﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Models
{
    public class HotelsDetails
    {
        public long HotelId { get; set; }
        public string WorkHours { get; set; }
        public CategoryDetails CategoryDetails { get; set; }

        public void Update(string workHours)
        {
            WorkHours = workHours;
        
        }


    }
}
