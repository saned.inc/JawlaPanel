﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Models
{
    public class EventDetails
    {
        public long EventId { get; set; }
        public string Owner { get; set; }
        public DateTime EndDate { get; set; }
        public CategoryDetails CategoryDetails { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public void Update(string owner, DateTime endDate)
        {
            Owner = owner;
            EndDate = endDate;
        }

    }


}
