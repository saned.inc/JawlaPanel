﻿namespace Saned.Jawla.Data.Core.Models
{
    public class CategoryContactType
    {
        public int Id { get; set; }
        public string TypeValue { get; set; }
    }
}