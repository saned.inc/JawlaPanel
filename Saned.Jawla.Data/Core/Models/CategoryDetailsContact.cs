﻿
namespace Saned.Jawla.Data.Core.Models
{
    public class CategoryDetailsContact
    {
        public long Id { get; set; }
        public string ContactInfo { get; set; }
        public string Icon { get; set; }
        public int ContactTypeId { get; set; }
        public long CategoryDetailsId { get; set; }
        public virtual CategoryDetails CategoryDetails { get; set; }
        public virtual CategoryContactType ContactType { get; set; }
        public string SoicalType { get; set; }
    }
}
