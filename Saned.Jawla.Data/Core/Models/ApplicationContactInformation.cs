﻿
namespace Saned.Jawla.Data.Core.Models
{
    public class ApplicationContactInformation
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string InstagrameUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string GooglePlusUrl { get; set; }
        public string LinkedInUrl { get; set; }
        public string AboutUs { get; set; }

        public void Update(
            string phoneNumber ,
         string email ,
         string facebookUrl ,
         string twitterUrl ,
         string instagrameUrl ,
         string youtubeUrl,
         string googlePlusUrl,
         string linkedInUrl,
         string aboutUs
            )
        {
            PhoneNumber = phoneNumber;
            Email = email;
            FacebookUrl = facebookUrl;
            TwitterUrl = twitterUrl;
            InstagrameUrl = instagrameUrl;
            YoutubeUrl = youtubeUrl;
            GooglePlusUrl= googlePlusUrl;
            LinkedInUrl = linkedInUrl;
            AboutUs = aboutUs;

        }
    }
}
