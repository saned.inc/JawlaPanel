﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saned.Jawla.Data.Core.Models
{
    public class Favourite
    {
        public Favourite()
        {
            ModifiedDate = DateTime.Now;
            IsDeleted = false;
        }
        public int Id { get; set; }  
        public long CategoryDetailsId { get; set; }     
        public string UserId { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime ModifiedDate { get; set; }
        public CategoryDetails CategoryDetails { get; set; }

        void Archieve()
        {
            IsDeleted = true;

        }

     
    }
}
