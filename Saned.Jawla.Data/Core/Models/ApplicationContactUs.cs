﻿using System;
namespace Saned.Jawla.Data.Core.Models
{
   public class ApplicationContactUs
    {
       public ApplicationContactUs()
       {
            ModifiedDate = DateTime.Now;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
