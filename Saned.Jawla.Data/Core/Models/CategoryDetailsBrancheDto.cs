﻿namespace Saned.Jawla.Data.Core.Models
{
    public class CategoryDetailsBrancheDto
    {
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Municipality { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}