﻿namespace Saned.Jawla.Data.Core.Models
{
    public class Country
    {
        public Country()
        {
            IsDeleted = false;
        }

        public int Id { get; set; }
        public string ArabicName { get; set; }
        public string EnglishName { get; set; }
        public bool? IsDeleted { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        void Archieve()
        {
            IsDeleted = true;

        }

        public void Update(string arabicName, string englishName, string latitude, string longitude)
        {
            ArabicName = arabicName;
            EnglishName = englishName;
            Latitude = latitude;
            Longitude = longitude;

        }

        public void Modify(string name, string englishName)
        {
            ArabicName = name;
            EnglishName = englishName;
        }
    }
}
