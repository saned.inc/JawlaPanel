﻿using System;

namespace Saned.Jawla.Data.Core.Models
{
    public class VisitorCover
    {
        public VisitorCover()
        {
            ModifiedDate = new DateTime();
        }
        public long Id { get; set; }
        public string UseId { get; set; }
        public long CategoryDetailsId { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}