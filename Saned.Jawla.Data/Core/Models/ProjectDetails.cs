﻿namespace Saned.Jawla.Data.Core.Models
{
    public class ProjectDetails
    {
        public long ProjectId { get; set; }
        public string Owner { get; set; }
        public string Idea { get; set; }
        public CategoryDetails CategoryDetails { get; set; }

        public void Update(string owner)
        {
            Owner = owner;

        }

    }

}
