﻿
using System;

namespace Saned.Jawla.Data.Core.Models
{
    public class CategoryDetailsFaq
    {
        public CategoryDetailsFaq()
        {
            ModifiedDate=DateTime.Now;
        }
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public long CategoryDetailsId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public virtual CategoryDetails CategoryDetails { get; set; }

    }
}
