﻿using System;
using Saned.Jawla.Data.Core.Enum;


namespace Saned.Jawla.Data.Core.Models
{
    public class AmbassadorRequest
    {
        public long Id { get; set; }

        public string FullName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string SoicalLinks { get; set; }
        public string PhotoUrl { get; set; }
        
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public string UserId { get; set; }
        public string Notes { get; set; }

        public void Approve()
        {
            Status = AmbassadorRequestStatusEnum.Approve.GetHashCode();
        }
        public void Reject()
        {
            Status = AmbassadorRequestStatusEnum.Reject.GetHashCode();
        }
    }
}
