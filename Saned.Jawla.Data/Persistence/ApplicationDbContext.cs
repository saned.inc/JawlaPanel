﻿using System;
using System.Data.Entity;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Migrations;
using Saned.Jawla.Data.Persistence.EntityConfigurations;
using Saned.Core.Security.Persistence.Infrastructure;

namespace Saned.Jawla.Data.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public DbSet<Municipality> Municipalities { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<AmbassadorRequest> AmbassadorRequests { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryDetails> CategoryDetails { get; set; }
        public DbSet<EventDetails> EventDetails { get; set; }
        public DbSet<HotelsDetails> HotelsDetails { get; set; }
        public DbSet<ProjectDetails> ProjectDetails { get; set; }
        public DbSet<CategoryContactType> CategoryContactTypes { get; set; }
        public DbSet<CategoryDetailsBranche> CategoryDetailsBranche { get; set; }
        public DbSet<CategoryDetailsContact> CategoryDetailsContact { get; set; }
        public DbSet<CategoryDetailsFaq> CategoryDetailsFaq { get; set; }
        public DbSet<ApplicationContactInformation> ApplicationContactInformation { get; set; }
        public DbSet<ApplicationContactUs> ApplicationContactUs { get; set; }
        public DbSet<Favourite> Favourite { get; set; }
        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<VisitorCover> VisitorCovers { get; set; }
        public DbSet<Photo> Photos { get; set; }



        public ApplicationDbContext() : base("connectionString")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;

        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MunicipalityConfigurations());
            modelBuilder.Configurations.Add(new CityConfigurations());
            modelBuilder.Configurations.Add(new CountryConfigurations());
            modelBuilder.Configurations.Add(new AmbassadorRequestConfigurations());
            modelBuilder.Configurations.Add(new CategoriesConfiguration());
            modelBuilder.Configurations.Add(new CategoryDetailsConfigurations());
            modelBuilder.Configurations.Add(new EventDetailsConfigurations());
            modelBuilder.Configurations.Add(new HotelsDetailsConfigurations());
            modelBuilder.Configurations.Add(new ProjectDetailsConfigurations());
            modelBuilder.Configurations.Add(new CategoryContactTypeConfigurations());
            modelBuilder.Configurations.Add(new CategoryDetailsBrancheConfigurations());
            modelBuilder.Configurations.Add(new CategoryDetailsContactConfigurations());
            modelBuilder.Configurations.Add(new CategoryDetailsFaqConfigurations());
            modelBuilder.Configurations.Add(new ApplicationContactInformationConfiguration());
            modelBuilder.Configurations.Add(new ApplicationContactUsConfiguration());
            modelBuilder.Configurations.Add(new FavouriteConfiguration());
            modelBuilder.Configurations.Add(new VisitorCoverConfigurations());

            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
            base.OnModelCreating(modelBuilder);
        }

    }



}
