﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    class ProjectDetailsConfigurations : EntityTypeConfiguration<ProjectDetails>
    {
        public ProjectDetailsConfigurations()
        {
            this.HasKey(p => p.ProjectId);

        }
    }
}
