using System.Data.Entity.ModelConfiguration;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class AmbassadorRequestConfigurations : EntityTypeConfiguration<AmbassadorRequest>
    {
        public AmbassadorRequestConfigurations()
        {
            //this.HasKey(p => p.RequestId);
            Property(x => x.FullName).HasMaxLength(250);
            Property(x => x.PhoneNumber).HasMaxLength(250);
            Property(x => x.Address).HasMaxLength(500);
            Property(x => x.PhotoUrl).HasMaxLength(500);
            Property(x => x.SoicalLinks).HasMaxLength(500);

        }
    }

}