﻿using Saned.Jawla.Data.Core.Models;
using System.Data.Entity.ModelConfiguration;


namespace Saned.Jawla.Data.Persistence.EntityConfigurations 
{
    class ApplicationContactUsConfiguration : EntityTypeConfiguration<ApplicationContactUs>
    {
        public ApplicationContactUsConfiguration()
        {
            Property(x => x.Email).HasMaxLength(250);
            Property(x => x.PhoneNumber).HasMaxLength(100);
            Property(x => x.Name).HasMaxLength(150);
            
        }
    }
}
