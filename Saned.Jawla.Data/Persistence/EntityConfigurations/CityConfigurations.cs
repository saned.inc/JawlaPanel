﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class CityConfigurations : EntityTypeConfiguration<City>
    {
        public CityConfigurations()
        {
            Property(a => a.ArabicName).IsRequired().HasMaxLength(150);
            Property(a => a.EnglishName).IsRequired().HasMaxLength(150);
            HasRequired<Country>(s => s.Country).WithMany().WillCascadeOnDelete(false);

        }
    }
}
