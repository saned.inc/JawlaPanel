﻿using System.Data.Entity.ModelConfiguration;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class CategoryDetailsBrancheConfigurations : EntityTypeConfiguration<CategoryDetailsBranche>
    {
        public CategoryDetailsBrancheConfigurations()
        {
           
              HasOptional<City>(s => s.City).WithMany().WillCascadeOnDelete(false);
              HasOptional<Municipality>(s => s.Municipality).WithMany().WillCascadeOnDelete(false);
              HasOptional<Country>(s => s.Country).WithMany().WillCascadeOnDelete(false);

        }
    }


 
}
