﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class CategoryDetailsConfigurations : EntityTypeConfiguration<CategoryDetails>
    {
        public CategoryDetailsConfigurations()
        {
            HasOptional(s => s.Events)
                .WithRequired(ad => ad.CategoryDetails);

            HasOptional(s => s.Hotels)
                .WithRequired(ad => ad.CategoryDetails);

            HasOptional(s => s.Projects)
               .WithRequired(ad => ad.CategoryDetails);


            Property(a => a.Name).IsRequired().HasMaxLength(150);
            Property(x => x.MunicipalityId).IsOptional();

            HasOptional(x => x.Hotels).WithRequired(c => c.CategoryDetails).WillCascadeOnDelete(true);
            HasOptional(x => x.Events).WithRequired(c => c.CategoryDetails).WillCascadeOnDelete(true);
            HasOptional(x => x.Projects).WithRequired(c => c.CategoryDetails).WillCascadeOnDelete(true);
           
        }
    }
}
