using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class CategoryContactTypeConfigurations : EntityTypeConfiguration<CategoryContactType>
    {
        public CategoryContactTypeConfigurations()
        {

            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

        }
    }
}