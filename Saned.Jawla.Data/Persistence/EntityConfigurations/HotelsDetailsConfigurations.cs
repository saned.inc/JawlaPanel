﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class HotelsDetailsConfigurations : EntityTypeConfiguration<HotelsDetails>
    {
        public HotelsDetailsConfigurations()
        {
         this.HasKey(p => p.HotelId);

        }
    }
}
