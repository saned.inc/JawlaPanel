﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    class EventDetailsConfigurations : EntityTypeConfiguration<EventDetails>
    {
        public EventDetailsConfigurations()
        {
            HasKey(p => p.EventId);

            Property(a => a.Owner).HasMaxLength(150);
            Property(a => a.EndTime).HasMaxLength(50);
            Property(a => a.StartTime).HasMaxLength(50);
        }
    }
}
