﻿using System.Data.Entity.ModelConfiguration;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class VisitorCoverConfigurations : EntityTypeConfiguration<VisitorCover>
    {
        public VisitorCoverConfigurations()
        {
            Property(a => a.UseId).IsRequired().HasMaxLength(128);
        }

    }
}