﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    public class CategoriesConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoriesConfiguration()
        {
            //Property(c => c.Id)
            //.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Name).HasMaxLength(150);
            Property(x => x.PhotoUrl).HasMaxLength(250);
            Property(x => x.ParentId).IsOptional();

            HasOptional(x => x.ParentCategory).WithMany(x => x.ChildCategories);

            MapToStoredProcedures();

        }
    }
}
