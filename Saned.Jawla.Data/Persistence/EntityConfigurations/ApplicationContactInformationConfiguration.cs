﻿using Saned.Jawla.Data.Core.Models;
using System.Data.Entity.ModelConfiguration;


namespace Saned.Jawla.Data.Persistence.EntityConfigurations
{
    class ApplicationContactInformationConfiguration : EntityTypeConfiguration<ApplicationContactInformation>
    {
        public ApplicationContactInformationConfiguration()
        {
            Property(x => x.Email).HasMaxLength(250);
            Property(x => x.FacebookUrl).HasMaxLength(250);
            Property(x => x.LinkedInUrl).HasMaxLength(250);
            Property(x => x.PhoneNumber).HasMaxLength(100);
            Property(x => x.TwitterUrl).HasMaxLength(250);
            Property(x => x.YoutubeUrl).HasMaxLength(250);
            Property(x => x.InstagrameUrl).HasMaxLength(250);
            Property(x => x.GooglePlusUrl).HasMaxLength(250);
        }
    }


   
}
