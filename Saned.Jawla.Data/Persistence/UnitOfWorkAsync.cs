﻿using System;
using System.Threading.Tasks;
using Saned.Common.Rating.Repository;
using Saned.Jawla.Data.Core;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Jawla.Data.Persistence.Repositories;
using Saned.Modules.Photos;
using Saned.Modules.Views;

namespace Saned.Jawla.Data.Persistence
{
    public class UnitOfWorkAsync : IUnitOfWorkAsync, IDisposable
    {
        private readonly ApplicationDbContext _context;
        public IAmbassadorRequestRepository AmbassadorRequest { get; }
        public IApplicationContactInformationRepository ApplicationContact { get; }
        public IApplicationContactUsRepository ApplicationContactUsRepository { get; }
        public ICategoryDetailsRepository CategoryDetails { get; }
        public ICategoryDetailsFaqRepository Faq { get; }
        public ICategoryDetailsBrancheRepository Branche { get; }
        public ICategoryRepository Category { get; }
        public IPhotoRepository PhotoRepository { get; set; }
        public IEventDetailsRepository EventDetails { get; set; }
        public IProjectDetailsRepository ProjectDetails { get; }
        public IPlaceDetailsRepository PlaceDetails { get; }
        public IRatingElementRepositoryAsync Rating { get; }
        public ICategoryDetailsContactRepository Contact { get; }
        public ICountryRepository Country { get; }
        public ICityRepository City { get; }
        public IMunicipalityRepository Municipality { get; }
        public IFavouriteRepository Favourite { get; }
        public IAdvertisementRepository Advertisement { get; }
        public IVisitorCoverRepository VisitorCover { get; }
        public IHitCountDetailsRepository HitCountDetailsRepository { get; set; }

        public IUserRepository UserRepository { get; set; }
        public UnitOfWorkAsync()
        {
            _context = new ApplicationDbContext();
            string connectionString = _context.Database.Connection.ConnectionString;
            AmbassadorRequest = new AmbassadorRequestRepository(_context, connectionString);
            ApplicationContact = new ApplicationContactInformationRepository(_context);
            CategoryDetails = new CategoryDetailsRepository(_context, connectionString);
            Category = new CategoryRepository(_context);
            ApplicationContactUsRepository = new ApplicationContactUsRepository(_context);
            PhotoRepository = new PhotoRepository(_context,connectionString);
            HitCountDetailsRepository = new HitCountDetailsRepository(connectionString);
            EventDetails = new EventDetailsRepository(_context);
            ProjectDetails = new ProjectDetailsRepository(_context);
            PlaceDetails = new PlaceDetailsRepository(_context);
            Rating = new RatingElementRepositoryAsync(connectionString);
            Country = new CountryRepository(_context);
            City = new CityRepository(_context);
            Municipality = new MunicipalityRepository(_context);
            Favourite = new FavouriteRepository(_context, connectionString);
            Faq = new CategoryDetailsFaqRepository(_context);
            Branche = new CategoryDetailsBrancheRepository(_context);
            Advertisement = new AdvertisementRepository(_context);
            Contact = new CategoryDetailsContactRepository(_context);
            VisitorCover = new VisitorCoverRepository(_context, connectionString);
            UserRepository = new UserRepository(_context);
        }
        public UnitOfWorkAsync(ApplicationDbContext context)
        {
            _context = context;
            string connectionString = _context.Database.Connection.ConnectionString;
            AmbassadorRequest = new AmbassadorRequestRepository(_context, connectionString);
            ApplicationContact = new ApplicationContactInformationRepository(context);
            CategoryDetails = new CategoryDetailsRepository(_context, connectionString);
            Category = new CategoryRepository(_context);
            ApplicationContactUsRepository = new ApplicationContactUsRepository(_context);
            PhotoRepository = new PhotoRepository(_context,connectionString);
            EventDetails = new EventDetailsRepository(_context);
            ProjectDetails = new ProjectDetailsRepository(_context);
            PlaceDetails = new PlaceDetailsRepository(_context);
            Rating = new RatingElementRepositoryAsync(connectionString);
            Country = new CountryRepository(_context);
            City = new CityRepository(_context);
            Municipality = new MunicipalityRepository(_context);
            Favourite = new FavouriteRepository(_context, connectionString);
            Faq = new CategoryDetailsFaqRepository(_context);
            Branche = new CategoryDetailsBrancheRepository(_context);
            Advertisement = new AdvertisementRepository(_context);
            Contact = new CategoryDetailsContactRepository(_context);
            VisitorCover = new VisitorCoverRepository(_context, connectionString);
            UserRepository = new UserRepository(_context);
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {


            _context.Dispose();
        }
    }
}