﻿using System.Data.Entity;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Persistence
{
    public interface IApplicationDbContext
    {
        DbSet<City> Cities { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Municipality> Municipalities { get; set; }
        DbSet<AmbassadorRequest> AmbassadorRequests { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<CategoryDetails> CategoryDetails { get; set; }
        DbSet<EventDetails> EventDetails { get; set; }
        DbSet<HotelsDetails> HotelsDetails { get; set; }
        DbSet<ProjectDetails> ProjectDetails { get; set; }
        DbSet<CategoryContactType> CategoryContactTypes { get; set; }
        DbSet<CategoryDetailsBranche> CategoryDetailsBranche { get; set; }
        DbSet<CategoryDetailsContact> CategoryDetailsContact { get; set; }
        DbSet<CategoryDetailsFaq> CategoryDetailsFaq { get; set; }
        DbSet<ApplicationContactInformation> ApplicationContactInformation { get; set; }
        DbSet<ApplicationContactUs> ApplicationContactUs { get; set; }
        DbSet<Favourite> Favourite { get; set; }
        DbSet<Advertisement> Advertisements { get; set; }
        DbSet<VisitorCover> VisitorCovers { get; set; }
        DbSet<Photo> Photos { get; set; }
    }
}
