using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;
using System.Collections.Generic;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class CountryRepository : ICountryRepository
    {

        private readonly IApplicationDbContext _context;

        public CountryRepository(ApplicationDbContext context)
        {
            _context = context;

        }

        public void CreateAsync(Country entity)
        {
            _context.Countries.Add(entity);
        }
        public async Task<IList> GetAsync()
        {
            var country = _context.Countries.Select(s =>
                       new SelectItemDto()
                       {
                           Id = s.Id.ToString(),
                           Name = s.ArabicName
                       });
            return await country.ToListAsync();
        }
        public async Task<IList> GetAllAsync()
        {
            var country = _context.Countries.Select(s => s);
            return await country.ToListAsync();
        }
        public void DeleteAsync(Country entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            if (_context.Cities.Where(x => x.CountryId == entity.Id).Count() > 0 || _context.CategoryDetails.Where(x => x.CountryId == entity.Id).Count() > 0)
            {
                throw new ArgumentException(nameof(entity));
            }
            else
            {
                _context.Countries.Remove(entity);

            }
        }

        public async Task<IList> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
              Cities.Select(c => c)
             .Skip(pageIndex)
             .Take(pageSize)
             .ToListAsync();

        }

        public async Task<IList<Country>> GetAllComplete(int pageIndex, int pageSize)
        {
            return await _context.Countries.ToArrayAsync();
        }

        public async Task<List<Country>> GetAllCompleteWithPaging(int pageIndex, int pageSize)
        {
            return await _context.Countries.OrderBy(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }


        public void DeleteById(int id)
        {


            var query = (from city in _context.Cities where city.CountryId == id select city).ToList();
            if (query.Count == 0)
            {
                var q = (from country in _context.Countries where country.Id == id select country).FirstOrDefault();
                _context.Countries.Remove(q);
            }
            else
            {
                throw new ArgumentNullException(nameof(id));

            }

        }


        public async Task<IList> DeleteByIdReturnAll(int id)
        {
            var q = (from contry in _context.Countries where contry.Id == id select contry).FirstOrDefault();
            _context.Countries.Remove(q);
            return await _context.Countries.ToArrayAsync();

        }
        public async Task<Country> GetById(int id)
        {
            // if (id <= 0) throw new ArgumentNullException(nameof(id));
            var q = await (from contry in _context.Countries where contry.Id == id select contry).FirstOrDefaultAsync();
            return q;
        }

        public void Update(Country entity)
        {
            var q = (from country in _context.Countries where country.Id == entity.Id select country).FirstOrDefault();
            q.Update(entity.ArabicName, entity.EnglishName, entity.Latitude, entity.Longitude);
        }

        public async Task<IList> GetAsyncWithArabic()
        {
            var country = _context.Countries.Select(s =>
                       new SelectItemDto()
                       {
                           Id = s.Id.ToString(),
                           ArabicName = s.ArabicName
                       });
            return await country.ToListAsync();
        }

        public async Task<List<Country>> SearchCountryWithPaging(int pageIndex, int pageSize, string keyword)
        {
            return await _context.Countries.Where(x => string.IsNullOrEmpty(keyword) ||
               x.ArabicName.ToLower().Contains(keyword.ToLower())).OrderBy(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<int> SearchAllComplete(int pageIndex, int pageSize, string keyword)
        {
            return await _context.Countries.Where(x => string.IsNullOrEmpty(keyword) ||
               x.ArabicName.ToLower().Contains(keyword.ToLower())).OrderBy(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize).CountAsync();

        }
    }
}