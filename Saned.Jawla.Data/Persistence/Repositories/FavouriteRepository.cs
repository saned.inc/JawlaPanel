using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;

namespace Saned.Jawla.Data.Persistence.Repositories
{
   
    public class FavouriteRepository : IFavouriteRepository
    {
        private readonly IApplicationDbContext _context;
        AdoHelper _adoHelper;
        public FavouriteRepository(IApplicationDbContext context, string connectionString)
        {
            _context = context;
            AdoHelper.ConnectionString = connectionString;
        }

        public void CreateAsync(Favourite entity)
        {
            _context.Favourite.Add(entity);
        }

        public void DeleteAsync(Favourite entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.Favourite.Remove(entity);
        }

        public async Task<Favourite> FindAsync(int id)
        {
            return await _context.Favourite.Where(u => u.Id == id).FirstOrDefaultAsync();
        }
        public async Task<Favourite> FindAsync(string userId, long detailsId)
        {
            return await _context.Favourite.Where(u => u.UserId == userId && u.CategoryDetailsId==detailsId).FirstOrDefaultAsync();
        }

        public async Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize,string userId , int? countryId, int? cityId, int? municipalityId, int? categoryId, long? categoryDetailsId, string longitude, string latitude, DateTime? eventStatyDate = null)
        {
            var args = new object[]
            {
                 "@PageNumber", pageNumber,
                 "@PageSize", pageSize,
                 "@UserId", userId,
                "@CountryId", countryId,
                 "@CityId", cityId,
                 "@MunicipalityId", municipalityId,
                 "@CategoryId", categoryId,
                 "@CategoryDetailsId",  categoryDetailsId,
                 "@Lang", longitude,
                 "@Lat", latitude,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@EventStartDate",eventStatyDate
            };

            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(CategoryDetailsStoredProcedureEnum.Mobile_CategoryDetails_SelectByUserId.ToString(), args))
            {
                var items = new List<CategoryDetailsDto>();
                while (await reader.ReadAsync())
                {
                    var item = new CategoryDetailsDto();
                    Map(reader, item);
                    items.Add(item);
                }
                _adoHelper.Dispose();
                return items;
            }

        }
        private static void Map(IDataRecord record, CategoryDetailsDto dto)
        {
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Country = record[3] is DBNull ? "" : (string)record[3];
            dto.City = record[4] is DBNull ? "" : (string)record[4];
            dto.Municipality = record[5] is DBNull ? "" : (string)record[5];
            dto.Rating = record[6] is DBNull ? 0 : (int)record[6];
            dto.IsBin = !(record[7] is DBNull) && (bool)record[7];
            dto.EventStartDate = record[8] is DBNull ? "" : (string)record[8];

            dto.AllHit = record[9] is DBNull ? 0 : (int)record[9];
            dto.OverAllCount = record[10] is DBNull ? 0 : (int)record[10];
            dto.CategoryId = record[11] is DBNull ? 0 : (int)record[11];
            dto.ImageUrl = record[12] is DBNull ? "" : (string)record[12];
        }
    }

   
}