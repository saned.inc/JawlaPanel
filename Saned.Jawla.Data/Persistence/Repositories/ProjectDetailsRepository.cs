using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;

namespace Saned.Jawla.Data.Persistence.Repositories
{

    public class ProjectDetailsRepository : IProjectDetailsRepository
    {
        internal enum CategoryDetailsProjectStoredProcedure
        {
            Mobile_ProjectDetails_SelectById = 0,
        }

        private readonly IApplicationDbContext _context;
        AdoHelper _adoHelper;
        public ProjectDetailsRepository(ApplicationDbContext context)
        {
            _context = context;

        }

        public void CreateAsync(ProjectDetails entity)
        {
            _context.ProjectDetails.Add(entity);
        }

        public void DeleteAsync(ProjectDetails entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.ProjectDetails.Remove(entity);
        }

        public async Task<IList<ProjectDetails>> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
                ProjectDetails.Select(c => c)
                .Skip(pageIndex)
                .Take(pageSize)
                .ToListAsync();
        }

        public IQueryable<ProjectDetails> GetAsync()
        {
            return _context.ProjectDetails.AsNoTracking().Include(u => u.CategoryDetails);
        }
        public async Task<ProjectDetailsDto> GetAsync(long id, string userId = null)
        {
            var args = new object[]
           {
                 "@Id",id,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@UserId",userId
           };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(CategoryDetailsProjectStoredProcedure.Mobile_ProjectDetails_SelectById.ToString(), args))
            {
                if (!await reader.ReadAsync())
                    throw new EntityNotFoundException(typeof(EventDetailsDto), "Failed to find 'Project' with id '" + id + "'.");

                var entity = new ProjectDetailsDto();
                Map(reader, entity);
                _adoHelper.Dispose();
                return entity;
            }
        }
        public async Task<ProjectDetails> GetAsync(long id)
        {
            return await _context.ProjectDetails
                .Where(u => u.ProjectId == id)
                .FirstOrDefaultAsync();
        }
        private static void Map(IDataRecord record, ProjectDetailsDto dto)
        {
            //--Id,Name,Category,Country City Municipality Rating IsBin AllHit OverAllCount [CategoryId] [HotelId] [Latitude] [Longitude] [Description] [Description] [AmbassadorId]
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Country = record[3] is DBNull ? "" : (string)record[3];
            dto.City = record[4] is DBNull ? "" : (string)record[4];
            dto.Municipality = record[5] is DBNull ? "" : (string)record[5];
            dto.Rating = record[6] is DBNull ? 0 : (int)record[6];
            dto.IsBin = !(record[7] is DBNull) && (bool)record[7];
            dto.AllHit = record[8] is DBNull ? 0 : (int)record[8];
            dto.OverAllCount = record[9] is DBNull ? 0 : (int)record[9];
            dto.CategoryId = record[10] is DBNull ? 0 : (int)record[10];
            dto.ProjectId = record[11] is DBNull ? 0 : (long)record[11];
            dto.Latitude = record[12] is DBNull ? "" : (string)record[12];
            dto.Longitude = record[13] is DBNull ? "" : (string)record[13];
            dto.Description = record[14] is DBNull ? "" : (string)record[14];
            dto.Owner = record[15] is DBNull ? "" : (string)record[15];
            dto.AmbassadorId = record[16] is DBNull ? "" : (string)record[16];
            dto.UserRating = record[17] is DBNull ? 0 : (int)record[17];
            dto.Ambassador = record[18] is DBNull ? "" : (string)record[18];
            dto.Idea = record[19] is DBNull ? "" : (string)record[19];
        }

        public ProjectDetails GetFeatureProjectById(long id)
        {
            return _context.ProjectDetails.Include(u => u.CategoryDetails).FirstOrDefault(u => u.ProjectId == id);
        }

    }
}