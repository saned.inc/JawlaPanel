using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;
using System.Collections.Generic;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class CityRepository : ICityRepository
    {

        private readonly IApplicationDbContext _context;

        public CityRepository(ApplicationDbContext context)
        {
            _context = context;

        }

        public void CreateAsync(City entity)
        {
            _context.Cities.Add(entity);
        }
        public async Task<IList> GetAsync()
        {

            //return await _context.Cities.ToListAsync();

            var city = _context.Cities.Select(s =>
        new SelectItemDto()
        {
            Id = s.Id.ToString(),
            Name = s.ArabicName
        });
            return await city.ToListAsync();
        }

        public async Task<IList> FindAsync(int countryId)
        {
            var city = _context.Cities.Where(u => u.CountryId == countryId).Select(s =>
                 new SelectItemDto()
                 {
                     Id = s.Id.ToString(),
                     Name = s.ArabicName
                 });
            return await city.ToListAsync();
            //return await _context.Cities.Where(u => u.CountryId == countryId).ToListAsync();
        }

        public async Task<IList> FindAsyncWitharabicName(int countryId)
        {
            var city = _context.Cities.Where(u => u.CountryId == countryId).Select(s =>
                 new SelectItemDto()
                 {
                     Id = s.Id.ToString(),
                     ArabicName = s.ArabicName
                 });
            return await city.ToListAsync();
            //return await _context.Cities.Where(u => u.CountryId == countryId).ToListAsync();
        }
        public void DeleteAsync(City entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.Cities.Remove(entity);
        }

        public async Task<IList> FindAllAsync(int pageIndex, int pageSize, int countryId)
        {
            var q = (from x in _context.Countries where x.Id == countryId select x).FirstOrDefault();

            if (q == null)
            {
                q = _context.Countries.FirstOrDefault();
            }
            return await _context.
           Cities.Select(c => c)
          .Where(c => c.CountryId == q.Id)
          .Skip(pageIndex)
          .Take(pageSize)
          .ToListAsync();


        }

        public void DeleteById(int id)
        {
            var query = (from municipality in _context.Municipalities where municipality.CityId == id select municipality).ToList();
            if (query.Count == 0)
            {
                var q = (from city in _context.Cities where city.Id == id select city).FirstOrDefault();
                _context.Cities.Remove(q);
            }
            else
            {
                throw new ArgumentNullException(nameof(id));

            }



        }

        public async Task<IList> DeleteByIdReturnAll(int id)
        {
            var q = (from city in _context.Cities where city.Id == id select city).FirstOrDefault();
            _context.Cities.Remove(q);

            return await _context.Cities.ToArrayAsync();
        }

        public async Task<City> GetById(int id)
        {
            //  if (id <= 0) throw new ArgumentNullException(nameof(id));
            var q = await (from city in _context.Cities where city.Id == id select city).FirstOrDefaultAsync();
            return q;
        }


        //return with all content of the object plus the list of all of the countries in the database 
        public async Task<IList> GetAllCitiesComplete(int pageIndex, int pageSize)
        {
            return await _context.Cities.ToListAsync();
        }

        public async Task<IList> GetAllCitiesCompleteWithPaging(int pageIndex, int pageSize)
        {
            return await _context.Cities.OrderBy(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public void Update(City Entity)
        {
            var q = (from city in _context.Cities where city.Id == Entity.Id select city).FirstOrDefault();
            q.Update(Entity.ArabicName, Entity.EnglishName, Entity.Latitude, Entity.Longitude, Entity.CountryId);
        }

        public async Task<IList<City>> GetCityByCountryId(int CountryId)
        {
            var cities = from city in _context.Cities where CountryId == 0 || city.CountryId == CountryId select city;
            return await cities.ToListAsync();
        }

        public async Task<IList<City>> GetCityByCountryIdWithPaging(int CountrId, int pageNumber, int pageSize)
        {
            var cities = await (from city in _context.Cities where city.CountryId == CountrId select city).
                OrderBy(x => x.Id).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
            return cities;
        }


    }
}