using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class MunicipalityRepository : IMunicipalityRepository
    {
        private readonly IApplicationDbContext _context;

        public MunicipalityRepository(ApplicationDbContext context)
        {
            _context = context;

        }
        public void CreateAsync(Municipality entity)
        {
            _context.Municipalities.Add(entity);
        }
        public async Task<IList> GetAsync()
        {
            return await _context.Municipalities.ToListAsync();
        }

        public async Task<IList> GetAsyncWithPaging(int pageNumber, int pageSize)
        {
            return await _context.Municipalities.OrderBy(x => x.Id).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();

        }

        public void DeleteAsync(Municipality entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.Municipalities.Remove(entity);
        }

        public async Task<IList> FindAsync(int cityId)
        {
            var city = _context.Municipalities.Where(u => u.CityId == cityId).Select(s =>
                new SelectItemDto()
                {
                    Id = s.Id.ToString(),
                    Name = s.ArabicName
                });
            return await city.ToListAsync();

            //return await _context.Municipalities.Where(u => u.CityId == cityId).ToListAsync();
        }

        public void DeleteById(int id)
        {
            if (id <= 0) throw new ArgumentNullException(nameof(id));
            var q = (from mun in _context.Municipalities where mun.Id == id select mun).FirstOrDefault();
            _context.Municipalities.Remove(q);
        }

        public async Task<Municipality> GetById(int id)
        {
            if (id <= 0) throw new ArgumentNullException(nameof(id));
            var q = await (from mun in _context.Municipalities where mun.Id == id select mun).Include(x => x.City).FirstOrDefaultAsync();
            return q;
        }

        public void Update(Municipality entity)
        {
            var q = (from mun in _context.Municipalities where mun.Id == entity.Id select mun).FirstOrDefault();
            q.Update(entity.ArabicName, entity.EnglishName, entity.Latitude, entity.Longitude, entity.CityId);
        }

        public async Task<IList> GetMunicipalityByCityById(int cityId)
        {
            var mun = from municipality in _context.Municipalities where cityId == 0 || municipality.CityId == cityId select municipality;
            return await mun.ToArrayAsync();
        }

        public async Task<IList> DeleteByIdReturnAll(int id)
        {
            if (id <= 0) throw new ArgumentNullException(nameof(id));
            var q = (from mun in _context.Municipalities where mun.Id == id select mun).FirstOrDefault();
            _context.Municipalities.Remove(q);

            return await _context.Municipalities.ToArrayAsync();

        }


    }
}