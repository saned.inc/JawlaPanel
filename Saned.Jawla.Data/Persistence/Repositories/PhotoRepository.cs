﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;
using Saned.Modules.Photos;
using System.Data.SqlClient;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public enum PhotoProcName
    {
        Photo_Insert,
        Photo_Delete,
        Photo_DeleteByRelatedId,
        Photo_Update,
        Photo_SelectById,
        Photos_SelectByRelatedId,
        Photos_SelectPagingByRelatedId
    }

    public class PhotoRepository : IPhotoRepository, IDisposable
    {
        AdoHelper _adoHelper;
        private readonly ApplicationDbContext _context;

        public PhotoRepository(ApplicationDbContext context, string connectionString)
        {
            AdoHelper.ConnectionString = connectionString;
            _context = context;


        }
        public async Task<object> CreateAsync(Photo entity)
        {
            var args = new object[]
            {
                "@RelatedId", entity.RelatedId,
                "@RelatedTypeId", entity.RelatedTypeId,
                "@PhotoUrl", entity.PhotoUrl,
                "@Lang", entity.Lang,
                "@IsDefault", entity.IsDefault,
                "@ModifiedDate", entity.ModifiedDate,
                "@Title", entity.Title
            };
            _adoHelper = new AdoHelper();
            var result = await _adoHelper.ExecScalarProc(PhotoProcName.Photo_Insert.ToString(), args);
            _adoHelper.Dispose();
            return result;
        }
        public async Task<int> DeleteAsync(int id)
        {
            var args = new object[]
            {
                 "@Id",id,
            };
            _adoHelper = new AdoHelper();
            var result = await _adoHelper.ExecNonQueryProc(PhotoProcName.Photo_Delete.ToString(), args);
            _adoHelper.Dispose();
            return result;
        }
        public async Task<int> DeleteAsyncByRelatedId(string relatedId, int relatedType)
        {
            var args = new object[]
            {
                "@RelatedId", relatedId,
                "@RelatedTypeId", relatedType
            };
            _adoHelper = new AdoHelper();
            var result = await _adoHelper.ExecNonQueryProc(PhotoProcName.Photo_DeleteByRelatedId.ToString(), args);
            _adoHelper.Dispose();
            return result;
        }
        public async Task<int> UpdateAsync(Photo entity)
        {
            var args = new object[]
            {
                "@Id", entity.Id,
                "@RelatedId", entity.RelatedId,
                "@RelatedTypeId", entity.RelatedTypeId,
                "@PhotoUrl", entity.PhotoUrl,
                "@Lang", entity.Lang,
                "@IsDefault", entity.IsDefault,
                "@ModifiedDate", entity.ModifiedDate,
                "@Title", entity.Title
            };
            _adoHelper = new AdoHelper();
            var result = await _adoHelper.ExecNonQueryProc(PhotoProcName.Photo_Update.ToString(), args);
            _adoHelper.Dispose();
            return result;
        }
        public async Task<Photo> GetAsync(int id)
        {
            var args = new object[]
           {
                 "@Id",id,
           };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(PhotoProcName.Photo_Update.ToString(), args))
            {
                if (!await reader.ReadAsync())
                    throw new EntityNotFoundException(typeof(Photo), "Failed to find 'Photo' with id '" + id + "'.");

                var entity = new Photo();
                Map(reader, entity);
                _adoHelper.Dispose();
                return entity;
            }
        }
        public async Task<IList<Photo>> FindAllAsync(string relatedId, int relatedType, string lang = null)
        {

            var args = new object[]
            {
                 "@RelatedId",relatedId,
                 "@RelatedTypeId",relatedType,
                 "@Lang",lang

            };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(PhotoProcName.Photos_SelectByRelatedId.ToString(), args))
            {
                var items = new List<Photo>();
                while (await reader.ReadAsync())
                {
                    var item = new Photo();
                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        public Photo GetPhoto(int id)
        {
            var original = _context.Photos.FirstOrDefault(u => u.Id == id);
            return original;

        }

        public async Task<IList<PhotoDto>> FindAllAsync(string relatedId, int relatedType, int pageIndex, int pageSize, string lang)
        {
            var args = new object[]
            {
                "@RelatedId", relatedId,
                "@RelatedTypeId", relatedType,
                "@Lang", lang,
                "@PageNumber", pageIndex,
                "@PageSize", pageSize,

            };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(PhotoProcName.Photos_SelectPagingByRelatedId.ToString(), args))
            {
                var items = new List<PhotoDto>();
                while (await reader.ReadAsync())
                {
                    var item = new PhotoDto();
                    Map(reader, item);
                    items.Add(item);
                }
                _adoHelper.Dispose();
                return items;
            }
        }
        private static void Map(IDataRecord record, Photo dto)
        {
            dto.Id = (int)record[0];
            dto.RelatedId = record[1] is DBNull ? "" : (string)record[1];
            dto.RelatedTypeId = record[2] is DBNull ? 0 : (int)record[2];
            dto.PhotoUrl = record[3] is DBNull ? "" : (string)record[3];
            dto.Lang = record[4] is DBNull ? "" : (string)record[4];
            dto.IsDefault = (bool)record[5];
            dto.ModifiedDate = (DateTime)record[6];
            dto.Title = record[7] is DBNull ? "" : (string)record[7];
        }
        private static void Map(IDataRecord record, PhotoDto dto)
        {
            dto.Id = (int)record[0];
            dto.RelatedId = record[1] is DBNull ? "" : (string)record[1];
            dto.RelatedTypeId = record[2] is DBNull ? 0 : (int)record[2];
            dto.PhotoUrl = record[3] is DBNull ? "" : (string)record[3];
            dto.Lang = record[4] is DBNull ? "" : (string)record[4];
            dto.IsDefault = (bool)record[5];
            dto.ModifiedDate = (DateTime)record[6];
            dto.Title = record[7] is DBNull ? "" : (string)record[7];
            dto.OverAllCount = record[8] is DBNull ? 0 : (int)record[8];

        }




        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _adoHelper.Dispose();
        }

        public void RemoveImages(List<int> ids, long relatedId)
        {
            int relatedTypeId = RelateTypeEnum.CategoryDetails.GetHashCode();
            var deletedimages = _context.Photos.Where(u => !ids.Contains(u.Id) && u.RelatedId == relatedId.ToString() && u.RelatedTypeId == relatedTypeId
            && !u.IsDefault);
            if (deletedimages.Any())
                _context.Photos.RemoveRange(deletedimages);

        }

        public void DeleteMainImage(long relatedId)
        {
            var res =
      (_context.Database.SqlQuery<object>(
          @"DELETE FROM [dbo].[Photos]
            WHERE RelatedId = @RelatedId  
            AND RelatedTypeId = 1
            AND [IsDefault]=1",
              new SqlParameter("RelatedId", SqlDbType.Int) { Value = relatedId }
            ).FirstOrDefault());

        }
    }
}
