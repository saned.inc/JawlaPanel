using System.Collections;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Common.Comments.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext _dbContext;

        public UserRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IList> GetUserCover(string userId, int pageIndex = 8, int pageSize = 1)
        {   

            var res =
               (await _dbContext.Database.SqlQuery<UserCoverDto>(
                       "EXEC GetUserCover  @PageNumber, @PageSize , @UserId",
                       new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageIndex },
                       new SqlParameter("PageSize", SqlDbType.Int) { Value = pageSize },
                       new SqlParameter("UserId", SqlDbType.NVarChar) { Value = userId }
                     ).ToListAsync());

            return res;
            // throw new InvalidExpressionException();
        }
        public async Task<IList> GetUserComment(string userId)
        {
            var res =
               (await _dbContext.Database.SqlQuery<UserCommentsDTO>(
                       @"select comments.Id,comments.CommentText,comments.CreatedDate , CategoryDetails.name as CategoryDetailsName, categories.name as CategoryName from comments 
inner join CategoryDetails on comments.relatedid = CategoryDetails.id
inner join categories on CategoryDetails.categoryid = categories.id where comments.userid = @UserId",
                       new SqlParameter("UserId", SqlDbType.NVarChar) { Value = userId }
                     ).ToListAsync());

            return res;
        }

        public async Task<IList> GetUserPhoto(string userId)
        {
            //---, CategoryDetails.name as CategoryDetailsName,
            //  --inner join CategoryDetails on photos.relatedid = CategoryDetails.id
            //            --inner join categories on CategoryDetails.categoryid = categories.id
            var res =
            (await _dbContext.Database.SqlQuery<UserPhotosDTO>(
                    @"	select photos.Id , photos.PhotoUrl , photos.Title,CategoryDetails.name as CategoryDetailsName,
						categories.Name as categoryName
                        from photos inner join VisitorCovers on 
                        photos.relatedId = VisitorCovers.Id
                      inner join CategoryDetails on VisitorCovers.CategoryDetailsId = CategoryDetails.id
					  inner join categories on CategoryDetails.categoryid = categories.id
                        where VisitorCovers.useId  = @UserId",
                    new SqlParameter("UserId", SqlDbType.NVarChar) { Value = userId }
                  ).ToListAsync());

            return res;
        }

        public async Task<IList> GetUserFavourites(string userId)
        {
            var res =
            (await _dbContext.Database.SqlQuery<UserFavouritesDTO>(
                  @"select 
                    CategoryDetails.name as CategoryDetailsName,
                    categories.name as CategoryName from Favourites
                    inner join  CategoryDetails on  Favourites.CategoryDetailsId =  CategoryDetails.id
                    inner join categories on CategoryDetails.categoryid = categories.id
                    where Favourites.IsDeleted = 0 and Favourites.UserId = @UserId",
                    new SqlParameter("UserId", SqlDbType.NVarChar) { Value = userId }
                  ).ToListAsync());

            return res;
        }

        public async Task<IList> GetUserRating(string userId)
        {
            var res =
           (await _dbContext.Database.SqlQuery<UserRatingDTO>(
                 @"select RatingUsers.Id, TotalDegree , RatingDate ,
                    CategoryDetails.name as CategoryDetailsName,
                    categories.name as CategoryName from RatingUsers
                    inner join  CategoryDetails on  RatingUsers.RelatedId =  CategoryDetails.id
                    inner join categories on CategoryDetails.categoryid = categories.id
                    where RatingUsers.UserId = @UserId",
                   new SqlParameter("UserId", SqlDbType.NVarChar) { Value = userId }
                 ).ToListAsync());

            return res;
        }

    

        public async Task DeleteUserRating(int id)
        {
            var res =
          (await _dbContext.Database.SqlQuery<object>(
                @"delete from ratingusers where id = @id
                  delete from RatingUserDetails where ratinguserid = @id",
                  new SqlParameter("id", SqlDbType.Int) { Value = id }
                ).FirstOrDefaultAsync());

        }

        public async Task SetUserIsDeleted(bool isDeleted, string userId)
        {
            var res =
            (await _dbContext.Database.SqlQuery<object>(
                  @"update aspnetusers set IsDeleted = @IsDeleted where id = @UserId",
                    new SqlParameter("IsDeleted", SqlDbType.Bit) { Value = isDeleted },
                    new SqlParameter("UserId", SqlDbType.NVarChar) { Value = userId }
                  ).FirstOrDefaultAsync());
        }

        public async Task DeleteUser(string userId)
        {
            var res =
            (await _dbContext.Database.SqlQuery<object>(
                  @"DELETE FROM Advertisements WHERE AmbassadorId = @Id
		            DELETE FROM AmbassadorRequests  WHERE UserId = @Id
		            DELETE FROM CategoryDetails WHERE AmbassadorId = @Id
		            DELETE FROM Comments WHERE UserId = @Id
		            DELETE FROM Favourites WHERE UserId = @Id
		            DELETE FROM HitCountDetails WHERE UserId = @Id
		            DELETE FROM RatingUsers WHERE UserId = @Id
		            DELETE FROM VisitorCovers WHERE UseId = @Id
                    delete from RefreshTokens where subject = 
	                (select username  from aspnetusers where id = @Id)
		            DELETE FROM AspNetUsers WHERE Id = @Id",
                    new SqlParameter("Id", SqlDbType.NVarChar) { Value = userId }
                  ).FirstOrDefaultAsync());
        }

        public List<VisitorCoverDTO> GetAll(int categoryDetailId)
        {
            var res =
           (_dbContext.Database.SqlQuery<VisitorCoverDTO>(
                 @"	 select  (select top 1 AmbassadorRequests.Id  from AmbassadorRequests where UserId = VisitorCovers.UseId) as AmbassadorRequestId
	              ,  VisitorCovers.Id ,  VisitorCovers.UseId , AspNetUsers.Name , AspNetUsers.UserName
	                from   VisitorCovers inner join AspNetUsers on AspNetUsers.Id =  VisitorCovers.UseId 
	                where  VisitorCovers.CategoryDetailsId = @Id",
                   new SqlParameter("@Id", SqlDbType.Int) { Value = categoryDetailId }
                 ).ToList());

            return res;
        }
    }
}