using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Jawla.Data.Core.Dtos;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class AdvertisementRepository : IAdvertisementRepository
    {
        private readonly IApplicationDbContext _context;
        public AdvertisementRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public void CreateAsync(Advertisement entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.Advertisements.Add(entity);
        }

        public async Task UpdateAsync(Advertisement entity)
        {
            var oldEntity = await _context.Advertisements.FirstOrDefaultAsync(x => x.Id == entity.Id);
            oldEntity.AmbassadorId = entity.AmbassadorId;
            oldEntity.IsShow = entity.IsShow;
            oldEntity.ModifiedDate = DateTime.Now;
            oldEntity.Name = entity.Name;
            //oldEntity.PhotoUrl = !string.IsNullOrEmpty(entity.PhotoUrl) ? entity.PhotoUrl : oldEntity.PhotoUrl;
            oldEntity.PhotoUrl = entity.PhotoUrl;
            oldEntity.SecondNumber = entity.SecondNumber;
            oldEntity.Url = entity.Url;
        }
        public async Task<IList> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
              Advertisements
              .OrderBy(a => a.ModifiedDate)
              .Where(a => a.IsShow)
              .Skip(pageIndex * pageSize)
              .Take(pageSize)
              .ToListAsync();
        }

        public async Task<AdvertisementPagingDTO> AdminFindAllAsync(int pageIndex, int pageSize, string keyword)
        {
            return new AdvertisementPagingDTO
            {
                Data = await _context.
              Advertisements.Where(x => string.IsNullOrEmpty(keyword) || x.Name.ToLower().Contains(keyword.ToLower()))
              .OrderBy(a => a.Id)
              .Skip(pageIndex * pageSize)
              .Take(pageSize)
              .ToListAsync(),
                TotalCount = await _context
                                  .Advertisements
                                  .Where(x => string.IsNullOrEmpty(keyword)
                                  || x.Name.ToLower().Contains(keyword.ToLower()))
                                  .CountAsync()
            };
        }

        public void Delete(int id)
        {
            var advertisement = _context.Advertisements.FirstOrDefault(x => x.Id == id);
            _context.Advertisements.Remove(advertisement);
        }

        public async Task<Advertisement> SelectById(int id)
        {
            return await _context.Advertisements.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList> GetAsync()
        {
            return await _context.
              Advertisements
              .OrderByDescending(a => a.ModifiedDate)
              .Where(a => a.IsShow)
             .ToListAsync();
        }
    }
}