using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;
using Saned.Modules.Photos;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public enum VisitorCoverStoredProcedureEnum
    {
        Mobile_VisitorCovers_SelectListUserId,
        Mobile_VisitorCovers_SelectById,

    }
    public class VisitorCoverRepository : IVisitorCoverRepository
    {
        private readonly IApplicationDbContext _context;
        AdoHelper _adoHelper;
        public VisitorCoverRepository(IApplicationDbContext context, string connectionString)
        {
            _context = context;
            AdoHelper.ConnectionString = connectionString;
        }

        public void CreateAsync(VisitorCover entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.VisitorCovers.Add(entity);

        }

        public async Task<VisitorCover> GetAsync(string userId, long categoryDetailsId)
        {
            var checkExit = await _context.VisitorCovers.FirstOrDefaultAsync(
                v => v.UseId == userId && v.CategoryDetailsId == categoryDetailsId);
            return checkExit;
        }
        public async Task<IList> GetAsync(string userId, int pageNumber, int pageSize)
        {
            var args = new object[]
           {
                 "@PageNumber", pageNumber,
                 "@PageSize", pageSize,
                 "@UserId", userId,
                 "@Status",CategoryDetailsStatusEnum.Approve.GetHashCode(),
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),

           };

            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(VisitorCoverStoredProcedureEnum.Mobile_VisitorCovers_SelectListUserId.ToString(), args))
            {
                var items = new List<CategoryDetailsDto>();
                while (await reader.ReadAsync())
                {
                    var item = new CategoryDetailsDto();
                    Maplist(reader, item);
                    items.Add(item);
                }
                _adoHelper.Dispose();
                return items;
            }
        }
        public async Task<VisitorCoverCategoryDetailsDto> GetAsync(long id, string userId = null)
        {
            var args = new object[]
           {
                 "@Id",id,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@UserId",userId
           };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(VisitorCoverStoredProcedureEnum.Mobile_VisitorCovers_SelectById.ToString(), args))
            {
                if (!await reader.ReadAsync())
                    throw new EntityNotFoundException(typeof(VisitorCoverCategoryDetailsDto), "Failed to find 'item' with id '" + id + "'.");

                var entity = new VisitorCoverCategoryDetailsDto();
                Map(reader, entity);
                _adoHelper.Dispose();
                return entity;
            }
        }

        public async Task<IList<Photo>> FindAllAsync(long categoryDetailsId)
        {

            var args = new object[]
            {
                 "@CategoryDetailsId",categoryDetailsId,
                 "@RelatedTypeId",RelateTypeEnum.PictureVisitorCover.GetHashCode().ToString()

            };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc("VisitorCovers_SelectByCategoryDetailsId", args))
            {
                var items = new List<Photo>();
                while (await reader.ReadAsync())
                {
                    var item = new Photo();
                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        public async Task<long> GetVistorCoverId(long categoryDetailsId)
        {

            var visitorCoversId =
                (await _context.VisitorCovers.FirstOrDefaultAsync(x => x.CategoryDetailsId == categoryDetailsId)).Id;
            return visitorCoversId;
        }

        private static void Map(IDataRecord record, Photo dto)
        {
            dto.Id = (int)record[0];
            dto.RelatedId = record[1] is DBNull ? "" : (string)record[1];
            dto.RelatedTypeId = record[2] is DBNull ? 0 : (int)record[2];
            dto.PhotoUrl = record[3] is DBNull ? "" : (string)record[3];
            dto.Lang = record[4] is DBNull ? "" : (string)record[4];
            dto.IsDefault = (bool)record[5];
            dto.ModifiedDate = (DateTime)record[6];
            dto.Title = record[7] is DBNull ? "" : (string)record[7];
        }
        private static void Maplist(IDataRecord record, CategoryDetailsDto dto)
        {
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Country = record[3] is DBNull ? "" : (string)record[3];
            dto.City = record[4] is DBNull ? "" : (string)record[4];
            dto.Municipality = record[5] is DBNull ? "" : (string)record[5];
            dto.Rating = record[6] is DBNull ? 0 : (int)record[6];
            dto.IsBin = !(record[7] is DBNull) && (bool)record[7];
            dto.EventStartDate = record[8] is DBNull ? "" : (string)record[8];

            dto.AllHit = record[9] is DBNull ? 0 : (int)record[9];
            dto.OverAllCount = record[10] is DBNull ? 0 : (int)record[10];
            dto.CategoryId = record[11] is DBNull ? 0 : (int)record[11];
            dto.ImageUrl = record[12] is DBNull ? "" : (string)record[12];
        }
        private static void Map(IDataRecord record, VisitorCoverCategoryDetailsDto dto)
        {
            dto.CategoryDetailsId = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Rating = record[3] is DBNull ? 0 : (int)record[3];
            dto.AllHit = record[4] is DBNull ? 0 : (int)record[4];
            dto.CategoryId = record[5] is DBNull ? 0 : (int)record[5];
            dto.AmbassadorId = record[6] is DBNull ? "" : (string)record[6];
            dto.UserRating = record[7] is DBNull ? 0 : (int)record[7];
            dto.Ambassador = record[8] is DBNull ? "" : (string)record[8];
        }

    }
}