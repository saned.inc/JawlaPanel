using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class CategoryDetailsFaqRepository : ICategoryDetailsFaqRepository
    {
        private readonly IApplicationDbContext _context;
        public CategoryDetailsFaqRepository(IApplicationDbContext context)
        {
            _context = context;
        }
        public void CreateAsync(CategoryDetailsFaq entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.CategoryDetailsFaq.Add(entity);
        }
        public async Task<IEnumerable<CategoryDetailsFaqDto>> FindAllAsync(int pageIndex, int pageSize, long categoryDetailsId)
        {
            return await _context.
                CategoryDetailsFaq
                .Where(u => u.CategoryDetailsId == categoryDetailsId).OrderByDescending(u => u.ModifiedDate)
                .Select(x => new CategoryDetailsFaqDto
                {
                    Answer = x.Answer,
                    Question = x.Question
                })
              .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }


        public async Task<List<CategoryDetailsFaq>> GetAllAsync(long categoryDetailsId)
        {
            return await _context.
                CategoryDetailsFaq
                .Where(u => u.CategoryDetailsId == categoryDetailsId).OrderByDescending(u => u.ModifiedDate)
                .ToListAsync();
        }

        public void RemoveRange(List<CategoryDetailsFaq> questions)
        {
              _context.
                CategoryDetailsFaq.RemoveRange(questions);
        }
    }
}