﻿using Saned.Jawla.Data.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Modules.DataLayer.Repository;
using System.Data.Entity;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class ApplicationContactInformationRepository : IApplicationContactInformationRepository
    {
        private readonly IApplicationDbContext _context;
 
        public ApplicationContactInformationRepository(IApplicationDbContext context)
        {
            _context = context;
           
        }

        public void CreateAsync(ApplicationContactInformation entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.ApplicationContactInformation.Add(entity);

        }


        public void DeleteAsync(ApplicationContactInformation entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.ApplicationContactInformation.Remove(entity);
        }

        public void DeleteById(int id)
        {
            if (id <=0) throw new ArgumentException(nameof(id));
            var q = (from contact in _context.ApplicationContactInformation where contact.Id == id select contact).FirstOrDefault();
            _context.ApplicationContactInformation.Remove(q);
        }

        public async Task<ApplicationContactInformation> GetAsync()
        {
            return await _context.ApplicationContactInformation.FirstOrDefaultAsync();
        }

        public async Task<ApplicationContactInformation> GetById(int id)
        {
            var q = await (from contact in _context.ApplicationContactInformation where contact.Id == id select contact).FirstOrDefaultAsync();
            return q;
        }
    }
}
