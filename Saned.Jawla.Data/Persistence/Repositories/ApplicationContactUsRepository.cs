﻿using Saned.Jawla.Data.Core.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Modules.DataLayer.Repository;
using System.Data.Entity;
using Saned.Jawla.Data.Core.Dtos;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class ApplicationContactUsRepository : IApplicationContactUsRepository
    {
        private readonly IApplicationDbContext _context;
  

        public ApplicationContactUsRepository(IApplicationDbContext context) {
            _context = context;
      

        }
        public void CreateAsync(ApplicationContactUs entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.ApplicationContactUs.Add(entity);

            
        }

        public void DeleteAsync(ApplicationContactUs entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.ApplicationContactUs.Remove(entity);
        }

        public async Task<List<ApplicationContactUs>> GetAsync()
        {
            return await _context.ApplicationContactUs.ToListAsync();
        }

        public async Task<ApplicationContactUs> FindAsync(int id)
        {
        
            return await _context.ApplicationContactUs.Where(u => u.Id == id).FirstOrDefaultAsync();
        }
    }
}
