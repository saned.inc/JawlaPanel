﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Jawla.Data.Core.Dtos;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IApplicationDbContext _context;
        public CategoryRepository(IApplicationDbContext context)
        {
            _context = context;
        }
        public void CreateAsync(Category entity)
        {
            _context.Categories.Add(entity);
        }

        public void DeleteAsync(Category entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.Categories.Remove(entity);
        }

        public void DeleteById(int id)
        {
            //if (id <= 0) throw new ArgumentNullException(nameof(id));
            //var q = (from cat in _context.Categories where cat.Id == id select cat).FirstOrDefault();

            var cat = _context.Categories.Find(id);
            _context.Categories.Remove(cat);


        }

        public async Task<IList<Category>> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
                 Categories.Select(c => c).OrderBy(c => c.Id)
                 .Skip(pageIndex)
                 .Take(pageSize)
                 .ToListAsync();
        }

        public async Task<IList<ChildCategoryDto>> FindAllChildAsync(int pageIndex, int pageSize)
        {
            return await _context.
                   Categories.Where(x => x.ParentId.HasValue).Include(x => x.ParentCategory)
                   .Select(c => new ChildCategoryDto()
                   {
                       Id = c.Id,
                       ModifiedDate = c.ModifiedDate,
                       Name = c.Name,
                       ParentName = c.ParentCategory.Name,
                       PhotoUrl = c.PhotoUrl
                   }).OrderBy(c => c.Id)
                   .ToListAsync();
        }

        public async Task<IList<Category>> GetCategoryByParentId(int parentCategoryId)
        {
            return await _context.
                 Categories.Where(x => x.ParentId == parentCategoryId).Select(c => c)
                 .ToListAsync();
        }

        public async Task<IList<Category>> FindAllParentAsync()
        {
            return await _context.
                Categories.Where(x => !x.ParentId.HasValue).Include(x => x.ParentCategory).Select(c => c).OrderBy(c => c.Id)
                .ToListAsync();
        }

        public async Task<IList<MainCategoryDto>> FindAllMainCategoryAsync()
        {
            return await _context.
                Categories.Where(x => !x.ParentId.HasValue).Include(x => x.ChildCategories).Select(c => new MainCategoryDto
                {
                    HasChildren = c.ChildCategories.Count() > 0,
                    Id = c.Id,
                    ModifiedDate = c.ModifiedDate,
                    Name = c.Name,
                    PhotoUrl = c.PhotoUrl
                }).OrderBy(c => c.Id)
                .ToListAsync();
        }

        public async Task<IList<Category>> GetAllCategories()
        {
            return await _context.Categories.ToArrayAsync();
        }

        public IQueryable<Category> GetAsync()
        {
            return _context.Categories.AsNoTracking();
        }
        public async Task<Category> GetAsync(int categorytId)
        {

            return await _context.
                Categories
                .Where(u => u.Id == categorytId)
                .FirstOrDefaultAsync();
        }



        async Task<Category> GetById(int id)
        {
            if (id <= 0) throw new ArgumentNullException(nameof(id));
            var q = await (from cat in _context.Categories where cat.Id == id select cat).FirstOrDefaultAsync();
            return q;

        }
    }
}
