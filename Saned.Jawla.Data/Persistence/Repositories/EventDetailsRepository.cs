﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class EventDetailsRepository : IEventDetailsRepository
    {
        private readonly IApplicationDbContext _context;
        AdoHelper _adoHelper;
        public EventDetailsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CreateAsync(EventDetails entity)
        {
            _context.EventDetails.Add(entity);
        }

        public void DeleteAsync(EventDetails entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.EventDetails.Remove(entity);
        }

        public async Task<IList<EventDetails>> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
                  EventDetails.Select(c => c)
                  .Skip(pageIndex)
                  .Take(pageSize)
                  .ToListAsync();
        }

        public IQueryable<EventDetails> GetAsync()
        {
            return _context.EventDetails.AsNoTracking().Include(u=>u.CategoryDetails);
        }


        public EventDetails GetEventById(long id)
        {
            return _context.EventDetails.Include(u => u.CategoryDetails).FirstOrDefault(u=>u.EventId== id);
        }

        private static void Map(IDataRecord record, EventDetailsDto dto)
        {
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Country = record[3] is DBNull ? "" : (string)record[3];
            dto.City = record[4] is DBNull ? "" : (string)record[4];
            dto.Municipality = record[5] is DBNull ? "" : (string)record[5];
            dto.Rating = record[6] is DBNull ? 0 : (int)record[6];
            dto.IsBin = !(record[7] is DBNull) && (bool)record[7];
            dto.EventStartDate = record[8] is DBNull ? "" : (string)record[8];

            dto.AllHit = record[9] is DBNull ? 0 : (int)record[9];
            dto.OverAllCount = record[10] is DBNull ? 0 : (int)record[10];
            dto.CategoryId = record[11] is DBNull ? 0 : (int)record[11];
            dto.EventId = record[12] is DBNull ? 0 : (long)record[12];
            dto.EventEndDate = record[13] is DBNull ? "" : (string)record[13];
            dto.Latitude = record[14] is DBNull ? "" : (string)record[14];
            dto.Longitude = record[15] is DBNull ? "" : (string)record[15];
            dto.Description = record[16] is DBNull ? "" : (string)record[16];
            dto.Owner = record[17] is DBNull ? "" : (string)record[17];
            dto.StartTime = record[18] is DBNull ? "" : (string)record[18];
            dto.EndTime = record[19] is DBNull ? "" : (string)record[19];
            dto.AmbassadorId = record[20] is DBNull ? "" : (string)record[20];
            dto.UserRating = record[21] is DBNull ? 0 : (int)record[21];
            dto.Ambassador = record[22] is DBNull ? "" : (string)record[22];



        }
        public async Task<EventDetailsDto> GetAsync(long id, string userId = null)
        {
            var args = new object[]
           {
                 "@Id",id,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@UserId",userId
           };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(CategoryDetailsStoredProcedureEnum.Mobile_EventDetails_SelectById.ToString(), args))
            {
                if (!await reader.ReadAsync())
                    throw new EntityNotFoundException(typeof(EventDetailsDto), "Failed to find 'Event' with id '" + id + "'.");

                var entity = new EventDetailsDto();
                Map(reader, entity);
                _adoHelper.Dispose();
                return entity;
            }
        }
    }
}
