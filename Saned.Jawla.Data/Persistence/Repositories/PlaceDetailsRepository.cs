using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class PlaceDetailsRepository : IPlaceDetailsRepository
    {
        internal enum CategoryDetailsPlaceStoredProcedure
        {
            Mobile_PlaceDetails_SelectById = 0,
        }

        private readonly IApplicationDbContext _context;
        AdoHelper _adoHelper;
        public PlaceDetailsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CreateAsync(HotelsDetails entity)
        {
            _context.HotelsDetails.Add(entity);
        }

        public void DeleteAsync(HotelsDetails entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.HotelsDetails.Remove(entity);
            
        }

        public async Task<IList<HotelsDetails>> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
                HotelsDetails.Select(c => c)
                .Skip(pageIndex)
                .Take(pageSize)
                .ToListAsync();
        }

        public IQueryable<HotelsDetails> GetAsync()
        {
            return _context.HotelsDetails.AsNoTracking().Include(u => u.CategoryDetails);
        }

        public HotelsDetails GetPlaceById(long id)
        {
        
            return _context.HotelsDetails.Include(u => u.CategoryDetails).FirstOrDefault(u => u.HotelId == id);   
       }

    public async Task<HotelDetailsDto> GetAsync(long id, string userId = null)
        {
            var args = new object[]
           {
                 "@Id",id,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@UserId",userId
           };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(CategoryDetailsPlaceStoredProcedure.Mobile_PlaceDetails_SelectById.ToString(), args))
            {
                if (!await reader.ReadAsync())
                    throw new EntityNotFoundException(typeof(EventDetailsDto), "Failed to find 'Palce' with id '" + id + "'.");

                var entity = new HotelDetailsDto();
                Map(reader, entity);
                _adoHelper.Dispose();
                return entity;
            }
        }

        private static void Map(IDataRecord record, HotelDetailsDto dto)
        {
            //--Id,Name,Category,Country City Municipality Rating IsBin AllHit OverAllCount [CategoryId] [HotelId] [Latitude] [Longitude] [Description] [Description] [AmbassadorId]
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Country = record[3] is DBNull ? "" : (string)record[3];
            dto.City = record[4] is DBNull ? "" : (string)record[4];
            dto.Municipality = record[5] is DBNull ? "" : (string)record[5];
            dto.Rating = record[6] is DBNull ? 0 : (int)record[6];
            dto.IsBin = !(record[7] is DBNull) && (bool)record[7];
            dto.AllHit = record[8] is DBNull ? 0 : (int)record[8];
            dto.OverAllCount = record[9] is DBNull ? 0 : (int)record[9];
            dto.CategoryId = record[10] is DBNull ? 0 : (int)record[10];
            dto.HotelId = record[11] is DBNull ? 0 : (long)record[11];
            dto.Latitude = record[12] is DBNull ? "" : (string)record[12];
            dto.Longitude = record[13] is DBNull ? "" : (string)record[13];
            dto.Description = record[14] is DBNull ? "" : (string)record[14];
            dto.WorkHours = record[15] is DBNull ? "" : (string)record[15];
            dto.AmbassadorId = record[16] is DBNull ? "" : (string)record[16];
            dto.UserRating = record[17] is DBNull ? 0 : (int)record[17];
            dto.Ambassador = record[18] is DBNull ? "" : (string)record[18];
        }






    }
}
