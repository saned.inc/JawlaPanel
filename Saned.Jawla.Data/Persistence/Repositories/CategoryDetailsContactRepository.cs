using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class CategoryDetailsContactRepository : ICategoryDetailsContactRepository
    {
        private readonly IApplicationDbContext _context;

        public CategoryDetailsContactRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<CategoryDetailsContact>> FindAllAsync(long categoryDetailsId, int type)
        {
            //int type = ContactTypeEnum.ProjectContact.GetHashCode();
            var item = _context.CategoryDetailsContact
                .Where(
                    u =>
                        u.CategoryDetailsId == categoryDetailsId &&
                        u.ContactTypeId == type).OrderBy(u=>u.Id);
            return await item.ToListAsync();


        }

        public void AddRange(List<CategoryDetailsContact> viewModelContacts)
        {
            if (viewModelContacts.Any())
               _context.CategoryDetailsContact.AddRange(viewModelContacts);
        }


        public void RemoveRange(long categoryId)
        {
            var contacts = _context.CategoryDetailsContact.Where(u => u.CategoryDetailsId == categoryId);
            if (contacts.Any())
                 _context.CategoryDetailsContact.RemoveRange(contacts);
        }
    }



}