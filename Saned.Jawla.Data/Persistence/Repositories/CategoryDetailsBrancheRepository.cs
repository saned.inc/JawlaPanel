using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public class CategoryDetailsBrancheRepository : ICategoryDetailsBrancheRepository
    {
        private readonly IApplicationDbContext _context;
        public CategoryDetailsBrancheRepository(IApplicationDbContext context)
        {
            _context = context;
        }
        public void CreateAsync(CategoryDetailsBranche entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.CategoryDetailsBranche.Add(entity);
        }

        public async Task<IList> FindAllAsync(int pageIndex, int pageSize, long categoryDetailsId)
        {
            return await _context.
                CategoryDetailsBranche
                .Where(u => u.CategoryDetailsId == categoryDetailsId).OrderByDescending(u => u.ModifiedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<IList> FindAllAsync(long categoryDetailsId)
        {
            var item = _context.
                CategoryDetailsBranche
                .Where(u => u.CategoryDetailsId == categoryDetailsId)
                 .Include(x => x.City)
                .Include(x => x.Country)
                .Include(x => x.Municipality)
                .Select(x =>
                new CategoryDetailsBrancheDto()
                {
                    City = x.City.ArabicName,
                    Country = x.Country.ArabicName,
                    Municipality = x.Municipality.ArabicName,
                    PhoneNumber = x.PhoneNumber,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                }
                );
            return await item.ToListAsync();


        }


        public async Task<List<CategoryDetailsBranche>> FindAllAsyncWithInclude(long categoryDetailsId)
        {
            return await _context.
                CategoryDetailsBranche
                .Where(u => u.CategoryDetailsId == categoryDetailsId)
                .Include(x => x.City)
                .Include(x => x.Country)
                .Include(x => x.Municipality).ToListAsync();
        }

        public void AddRange(List<CategoryDetailsBranche> branches)
        {
            _context.
                CategoryDetailsBranche.AddRange(branches);
        }

        public void RemoveRangeByCategoryDetailsId(long categoryDetailsId)
        {
            var branches = _context.
                CategoryDetailsBranche.Where(u => u.CategoryDetailsId == categoryDetailsId);

            _context.CategoryDetailsBranche.RemoveRange(branches);
        }
    }
}