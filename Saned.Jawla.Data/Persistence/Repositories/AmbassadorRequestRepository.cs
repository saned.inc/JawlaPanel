﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;
using System.Data.SqlClient;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public enum AmbassadorRequestStoredPprocedure
    {
        Mobile_AmbassadorRequests_SelectList,

    }
    public class AmbassadorRequestRepository : IAmbassadorRequestRepository, IDisposable
    {
        private readonly ApplicationDbContext _context;
        AdoHelper _adoHelper;
        public AmbassadorRequestRepository(ApplicationDbContext context, string connectionString)
        {
            _context = context;
            AdoHelper.ConnectionString = connectionString;

        }

        public void CreateAsync(AmbassadorRequest entity)
        {
            
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.AmbassadorRequests.Add(entity);

        }
        public void DeleteAsync(AmbassadorRequest entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.AmbassadorRequests.Remove(entity);
        }
        public IQueryable<AmbassadorRequest> GetAsync()
        {
            return  _context.AmbassadorRequests.AsNoTracking();
        }

   

        public async Task<IList> GetAllAsync()
        {
            return await _context.AmbassadorRequests.ToListAsync();
        }
        public async Task<AmbassadorRequest> GetAsync(long requestId)
        {
            if (requestId == 0) throw new ArgumentOutOfRangeException(nameof(requestId), requestId, "Must Sent");
            return await _context.
                AmbassadorRequests
                .Where(u => u.Id == requestId)
                .FirstOrDefaultAsync();
        }

        public async Task<AmbassadorRequest> FindAsyncByUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentOutOfRangeException(nameof(userId), userId, "Must Sent");
            return await _context.
                AmbassadorRequests
                .Where(u => u.UserId == userId)
                .FirstOrDefaultAsync();
        }
        public async Task<IList<AmbassadorRequest>> FindAllAsyncByStatus(int? status = null)
        {
            return await _context.
                 AmbassadorRequests
                   .Where(u => (status == null || u.Status == status))
                 .ToListAsync();
        }
        public async Task<IList<AmbassadorRequest>> FindAllAsyncByStatus(int pageIndex, int pageSize, int? status = null)
        {
            return await _context.
                 AmbassadorRequests
                 .Where(u => ( status == null  || u.Status == status))
                 .OrderBy(u => u.ModifiedDate)
                 .Skip((pageIndex - 1) * pageSize)
                 .Take(pageSize)
                 .ToListAsync();
        }

        public async Task<IList<AmbassadorRequest>> GetAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
                AmbassadorRequests
                .Where(u => u.Status == AmbassadorRequestStatusEnum.Approve.GetHashCode())
                  .Select(x => new AmbassadorRequest
                  {
                      UserId = x.UserId
                  })
                  .Skip((pageIndex - 1) * pageSize)
                  .Take(pageSize)
                  .ToListAsync();

        }
        public async Task<IList<AmbassadorRequestDto>> FindAllAsync(int pageNumber, int pageSize)
        {
            var args = new object[]
             {
                "@PageNumber", pageNumber,
                "@PageSize", pageSize,
                "@Status", AmbassadorRequestStatusEnum.Approve.GetHashCode()

             };
            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(AmbassadorRequestStoredPprocedure.Mobile_AmbassadorRequests_SelectList.ToString(), args))
            {
                var items = new List<AmbassadorRequestDto>();
                while (await reader.ReadAsync())
                {
                    var item = new AmbassadorRequestDto();
                    Map(reader, item);
                    items.Add(item);
                }
                _adoHelper.Dispose();
                return items;
            }

        }

        public async Task<IList<AmbassadorRequest>> FindAsyncByEmail(string email)
        {
            return await _context.
                  AmbassadorRequests
                  .Where(u => u.Email == email)
                 .ToListAsync();

        }
        public async Task<IList<AmbassadorRequest>> FindAsyncByPhone(string phoneNumber)
        {
            return await _context.
                     AmbassadorRequests
                     .Where(u => u.PhoneNumber == phoneNumber)
                    .ToListAsync();


        }
        private static void Map(IDataRecord record, AmbassadorRequestDto dto)
        {
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.PhotoUrl = record[2] is DBNull ? "" : (string)record[2];
            dto.OverAllCount = record[3] is DBNull ? 0 : (int)record[3];
        }

        public async Task<IdentityResult> ValidateAsync(List<string> errors, string email, string phoneNumber, string userId = null)
        {

            if (!string.IsNullOrEmpty(email))
            {
                var findAsyncByEmail = await FindAsyncByEmail(email);
                if (findAsyncByEmail != null && findAsyncByEmail.Count > 0)
                {
                    string errorMsg = "Email '" + email + "' is already related to request.";
                    errors.Add(errorMsg);
                }

            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                var findAsyncByphone = await FindAsyncByPhone(phoneNumber);
                if (findAsyncByphone != null && findAsyncByphone.Count > 0)
                {
                    string errorMsg = "PhoneNumber '" + phoneNumber + "' is already related to request.";
                    errors.Add(errorMsg);
                }
            }

            if (!string.IsNullOrEmpty(userId))
            {
                var findAsyncByEmail = await FindAsyncByUserId(userId);
                if (findAsyncByEmail != null)
                {
                    string errorMsg = "User Id'" + phoneNumber + "' is already related to request.";
                    errors.Add(errorMsg);
                }
            }


            return errors.Any()
                ? IdentityResult.Failed(errors.ToArray())
                : IdentityResult.Success;
        }

        public IQueryable<AmbassadorRequest> GetIQuerable()
        {
            return _context.AmbassadorRequests.AsQueryable();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _adoHelper.Dispose();
        }

        public async Task DeactivateCategoryDetailsByUserId(string userId)
        {
            var res =
         (await _context.Database.SqlQuery<object>(
               @"update categorydetails set status = 1 where AmbassadorId = @AmbassadorId",
                 new SqlParameter("AmbassadorId", SqlDbType.NVarChar) { Value = userId }
               ).FirstOrDefaultAsync());
        }
    }
}