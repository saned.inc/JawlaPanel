﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Saned.Jawla.Data.Core.Dtos;
using Saned.Jawla.Data.Core.Enum;
using Saned.Jawla.Data.Core.Models;
using Saned.Jawla.Data.Core.Repositories;
using Saned.Modules.DataLayer.Repository;

namespace Saned.Jawla.Data.Persistence.Repositories
{
    public enum CategoryDetailsStoredProcedureEnum
    {
        Mobile_CategoryDetails_SelectList,
        Mobile_EventDetails_SelectById,
        Mobile_CategoryDetails_SelectByUserId,
    }
    public class CategoryDetailsRepository : ICategoryDetailsRepository
    {

        private readonly IApplicationDbContext _context;
        AdoHelper _adoHelper;
        public CategoryDetailsRepository(IApplicationDbContext context, string connectionString)
        {
            _context = context;
            AdoHelper.ConnectionString = connectionString;
        }

        public CategoryDetails CreateAsync(CategoryDetails entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return _context.CategoryDetails.Add(entity);
        }

        public void DeleteAsync(CategoryDetails entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _context.CategoryDetails.Remove(entity);
        }

        public async Task<IList<CategoryDetails>> FindAllAsync(int pageIndex, int pageSize)
        {
            return await _context.
               CategoryDetails.Select(c => c)
               .Skip((pageIndex - 1) * pageSize)
               .Take(pageSize)
               .ToListAsync();
        }

        public async Task<IList> FindAsync(int? categoryId, int? subCategoryId, int? countryId = null, int? cityId = null, int? municipalityId = null)
        {
            var approve = CategoryDetailsStatusEnum.Approve.GetHashCode().ToString();

            List<int> categoryIds = new List<int>();
            if (categoryId.HasValue)
            {
                categoryIds = _context.Categories.Where(x => x.ParentId == categoryId.Value).Select(x => x.Id).ToList();
            }

            var category = _context.CategoryDetails.Where(
                c => (
                (!categoryId.HasValue || categoryIds.Contains(c.CategoryId))
                &&
                (!subCategoryId.HasValue || c.CategoryId == subCategoryId)
                &&
                c.Status == approve
                &&
                (c.CountryId == countryId || countryId == null)
                &&
                (c.CityId == cityId || cityId == null)
                &&
                (c.MunicipalityId == municipalityId || municipalityId == null)
                )).Select(s =>
                        new SelectItemDto()
                        {
                            Id = s.Id.ToString(),
                            Name = s.Name
                        });

            return await category.ToListAsync();
        }

        public async Task<CategoryDetails> FindAsync(long id)
        {
            return await _context.CategoryDetails.Where(c => c.Id == id).SingleOrDefaultAsync();
        }

        public async Task<IList> FindAsync(string ambassadorId, int pageIndex, int pageSize)
        {
            var approve = CategoryDetailsStatusEnum.Approve.GetHashCode().ToString();
            var category = _context.CategoryDetails.
                OrderByDescending(x => x.ModifiedDate)
                .Skip((pageIndex - 1) * pageSize)
               .Take(pageSize).
            Where(c => _context.VisitorCovers.Any(a=>a.UseId==ambassadorId&&a.CategoryDetailsId==c.Id) && c.Status == approve).Select(s =>
                          new AmbassadorSelectItemDto()
                          {
                              Id = s.Id,
                              Name = s.Name,
                              CategoryId = s.CategoryId

                          });
            return await category.ToListAsync();
        }

        public async Task<CategoryDetails> GetAsync(long categoryDetailsId)
        {
            return await _context.
              CategoryDetails
              .Where(u => u.Id == categoryDetailsId)
              .FirstOrDefaultAsync();
        }


        public async Task<CategoryDetails> GetById(long categoryDetailsId)
        {
            return await _context.
              CategoryDetails.Include(u => u.Hotels)
              .Include(u => u.CategoryDetailsContacts)
              .Include(u => u.CategoryDetailsFaqs)
              .Where(u => u.Id == categoryDetailsId)
              .FirstAsync();
        }



        public async Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize, int? countryId, int? cityId, int? municipalityId, int? categoryId, long? categoryDetailsId, string longitude, string latitude, int? parentCategoryId, DateTime? eventStatyDate = null)
        {
            object[] args;
            if (parentCategoryId.HasValue)
            {
                args = new object[]
            {
                 "@PageNumber", pageNumber,
                 "@PageSize", pageSize,
                 "@CountryId", countryId,
                 "@CityId", cityId,
                 "@MunicipalityId", municipalityId,
                 "@CategoryId", categoryId,
                 "@CategoryDetailsId",  categoryDetailsId,
                 "@Lang", longitude,
                 "@Lat", latitude,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@EventStartDate",eventStatyDate,
                 "@ParentCategoryId", parentCategoryId
            };
            }
            else
            {
                args = new object[]
          {
                 "@PageNumber", pageNumber,
                 "@PageSize", pageSize,
                 "@CountryId", countryId,
                 "@CityId", cityId,
                 "@MunicipalityId", municipalityId,
                 "@CategoryId", categoryId,
                 "@CategoryDetailsId",  categoryDetailsId,
                 "@Lang", longitude,
                 "@Lat", latitude,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@EventStartDate",eventStatyDate,
                 "@ParentCategoryId", DBNull.Value
          };
            }

            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(CategoryDetailsStoredProcedureEnum.Mobile_CategoryDetails_SelectList.ToString(), args))
            {
                var items = new List<CategoryDetailsDto>();
                while (await reader.ReadAsync())
                {
                    var item = new CategoryDetailsDto();
                    Map(reader, item);
                    items.Add(item);
                }
                _adoHelper.Dispose();
                return items;
            }

        }

        public async Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize, int? countryId, int? cityId, int? municipalityId, int? categoryId, long? categoryDetailsId, string longitude, string latitude, int? status, int? parentCategoryId, DateTime? eventStatyDate = null, string ambassadorId = null)
        {
            object[] args;
            if (parentCategoryId.HasValue)
            {
                args = new object[]
            {
                 "@PageNumber", pageNumber,
                 "@PageSize", pageSize,
                 "@CountryId", countryId,
                 "@CityId", cityId,
                 "@MunicipalityId", municipalityId,
                 "@CategoryId", categoryId,
                 "@CategoryDetailsId",  categoryDetailsId,
                 "@Lang", longitude,
                 "@Lat", latitude,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@EventStartDate",eventStatyDate,
                 "@Status",status,
                 "@AmbassadorId",ambassadorId,
                 "@ParentCategoryId", parentCategoryId
            };
            }
            else
            {
                args = new object[]
          {
                 "@PageNumber", pageNumber,
                 "@PageSize", pageSize,
                 "@CountryId", countryId,
                 "@CityId", cityId,
                 "@MunicipalityId", municipalityId,
                 "@CategoryId", categoryId,
                 "@CategoryDetailsId",  categoryDetailsId,
                 "@Lang", longitude,
                 "@Lat", latitude,
                 "@RelatedTypeId",RelateTypeEnum.CategoryDetails.GetHashCode(),
                 "@EventStartDate",eventStatyDate,
                 "@Status",status,
                 "@AmbassadorId",ambassadorId,
                 "@ParentCategoryId", DBNull.Value
          };
            }

            _adoHelper = new AdoHelper();
            using (var reader = await _adoHelper.ExecDataReaderProc(CategoryDetailsStoredProcedureEnum.Mobile_CategoryDetails_SelectList.ToString(), args))
            {
                var items = new List<CategoryDetailsDto>();
                while (await reader.ReadAsync())
                {
                    var item = new CategoryDetailsDto();
                    Map(reader, item);
                    item.Rating = item.Rating / 2;
                    items.Add(item);
                }
                _adoHelper.Dispose();
                return items;
            }

        }


        private static void Map(IDataRecord record, CategoryDetailsDto dto)
        {
            dto.Id = record[0] is DBNull ? 0 : (long)record[0];
            dto.Name = record[1] is DBNull ? "" : (string)record[1];
            dto.Category = record[2] is DBNull ? "" : (string)record[2];
            dto.Country = record[3] is DBNull ? "" : (string)record[3];
            dto.City = record[4] is DBNull ? "" : (string)record[4];
            dto.Municipality = record[5] is DBNull ? "" : (string)record[5];
            dto.Rating = record[6] is DBNull ? 0 : (int)record[6];
            dto.IsBin = !(record[7] is DBNull) && (bool)record[7];
            dto.EventStartDate = record[8] is DBNull ? "" : (string)record[8];

            dto.AllHit = record[9] is DBNull ? 0 : (int)record[9];
            dto.OverAllCount = record[10] is DBNull ? 0 : (int)record[10];
            dto.CategoryId = record[11] is DBNull ? 0 : (int)record[11];
            dto.ImageUrl = record[12] is DBNull ? "" : (string)record[12];

            dto.ParentCategoryId = record[13] is DBNull ? (int?)null : (int?)record[13];
        }

        public void RemoveImagesbyCategoryId(long id)
        {
            var photos = _context.Photos.Where(u => u.RelatedId == id.ToString());
            if (photos.Any())
                _context.Photos.RemoveRange(photos);
        }

        //public Task<IList<CategoryDetailsDto>> FindAllAsync(int pageNumber, int pageSize, int? countryId, int? cityId, int? municipalityId, int categoryDetailsId, out int totalCount)
        //{

        //    var categories = _context.CategoryDetails.Where(u => (countryId == null || u.CountryId == countryId) &&
        //                                   (cityId == null || u.CityId == cityId) &&
        //                                   (municipalityId == null || u.MunicipalityId == municipalityId) &&
        //                                   u.CategoryId== categoryDetailsId);




        //    totalCount = categories.Count();
        //    return await categories
        //       .Skip((pageNumber - 1) * pageSize)
        //       .Take(pageSize).Select(u=> new CategoryDetailsDto ()
        //        {

        //        })
        //       .ToListAsync();
        //}
    }
}


