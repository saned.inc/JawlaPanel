﻿--DROP PROC Mobile_AmbassadorRequests_SelectList
--GO
ALTER PROC Mobile_AmbassadorRequests_SelectList
@PageNumber [INT] = 1,
@PageSize [INT] = 10,
@Status INT 
AS 
SELECT
	Id,
	Name,
	[PhotoUrl],
	OverAllCount = COUNT(1) OVER()
FROM
	[AspNetUsers]
INNER JOIN
	[AmbassadorRequests]
ON
	[AspNetUsers].Id=[AmbassadorRequests].UserId
WHERE 
	[AspNetUsers].IsDeleted=0
AND
	[AmbassadorRequests].Status=@Status
ORDER BY
	[Id] 
	OFFSET @PageSize * (@PageNumber - 1) ROWS
FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)

