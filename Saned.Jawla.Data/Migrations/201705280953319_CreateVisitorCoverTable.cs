namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateVisitorCoverTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VisitorCovers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UseId = c.String(nullable: false, maxLength: 128),
                        CategoryDetailsId = c.Long(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VisitorCovers");
        }
    }
}
