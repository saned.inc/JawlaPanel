namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Drop3Table : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CategoryDetailsBranches", "CategoryDetailsId", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetailsBranches", "CityId", "dbo.Cities");
            DropForeignKey("dbo.CategoryDetailsBranches", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.CategoryDetailsBranches", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.CategoryDetailsContacts", "CategoryDetails_Id", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetailsContacts", "ContactType_Id", "dbo.CategoryContactTypes");
            DropForeignKey("dbo.CategoryDetailsFaqs", "CategoryDetailsId", "dbo.CategoryDetails");
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CategoryDetailsId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CountryId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CityId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "MunicipalityId" });
            DropIndex("dbo.CategoryDetailsContacts", new[] { "CategoryDetails_Id" });
            DropIndex("dbo.CategoryDetailsContacts", new[] { "ContactType_Id" });
            DropIndex("dbo.CategoryDetailsFaqs", new[] { "CategoryDetailsId" });
            AlterColumn("dbo.ApplicationContactInformations", "PhoneNumber", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "Email", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "FacebookUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "TwitterUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "InstagrameUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "YoutubeUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "GooglePlusUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "LinkedInUrl", c => c.String());
            DropTable("dbo.CategoryDetailsBranches");
            DropTable("dbo.CategoryDetailsContacts");
            DropTable("dbo.CategoryContactTypes");
            DropTable("dbo.CategoryDetailsFaqs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CategoryDetailsFaqs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        Answer = c.String(),
                        CategoryDetailsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryContactTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryDetailsContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactInfo = c.String(),
                        Icon = c.String(),
                        ContactTypeId = c.Long(nullable: false),
                        CategoryDetailsId = c.Int(nullable: false),
                        CategoryDetails_Id = c.Long(),
                        ContactType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryDetailsBranches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        CategoryDetailsId = c.Long(nullable: false),
                        CountryId = c.Int(),
                        CityId = c.Int(),
                        MunicipalityId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.ApplicationContactInformations", "LinkedInUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "GooglePlusUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "YoutubeUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "InstagrameUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "TwitterUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "FacebookUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "Email", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "PhoneNumber", c => c.String(maxLength: 100));
            CreateIndex("dbo.CategoryDetailsFaqs", "CategoryDetailsId");
            CreateIndex("dbo.CategoryDetailsContacts", "ContactType_Id");
            CreateIndex("dbo.CategoryDetailsContacts", "CategoryDetails_Id");
            CreateIndex("dbo.CategoryDetailsBranches", "MunicipalityId");
            CreateIndex("dbo.CategoryDetailsBranches", "CityId");
            CreateIndex("dbo.CategoryDetailsBranches", "CountryId");
            CreateIndex("dbo.CategoryDetailsBranches", "CategoryDetailsId");
            AddForeignKey("dbo.CategoryDetailsFaqs", "CategoryDetailsId", "dbo.CategoryDetails", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CategoryDetailsContacts", "ContactType_Id", "dbo.CategoryContactTypes", "Id");
            AddForeignKey("dbo.CategoryDetailsContacts", "CategoryDetails_Id", "dbo.CategoryDetails", "Id");
            AddForeignKey("dbo.CategoryDetailsBranches", "MunicipalityId", "dbo.Municipalities", "Id");
            AddForeignKey("dbo.CategoryDetailsBranches", "CountryId", "dbo.Countries", "Id");
            AddForeignKey("dbo.CategoryDetailsBranches", "CityId", "dbo.Cities", "Id");
            AddForeignKey("dbo.CategoryDetailsBranches", "CategoryDetailsId", "dbo.CategoryDetails", "Id", cascadeDelete: true);
        }
    }
}
