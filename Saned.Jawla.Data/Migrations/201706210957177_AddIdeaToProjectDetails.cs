namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIdeaToProjectDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectDetails", "Idea", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectDetails", "Idea");
        }
    }
}
