namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class drop_Photos_andCreateAgain : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Photos");
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RelatedId = c.String(),
                        RelatedTypeId = c.Int(nullable: false),
                        PhotoUrl = c.String(),
                        Lang = c.String(),
                        IsDefault = c.Boolean(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Photos");
        }
    }
}
