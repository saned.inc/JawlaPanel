namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePhotoProc : DbMigration
    {
        public override void Up()
        {
            Sql(@" CREATE PROCEDURE [dbo].[Photo_Delete]
    @Id [int]
AS
BEGIN
    DELETE [dbo].[Photos]
    WHERE ([Id] = @Id)
END
");


            Sql(@" 
CREATE PROCEDURE [Photo_DeleteByRelatedId]
@RelatedId  Nvarchar(Max),
@RelatedTypeId INT
AS
	DELETE [dbo].[Photos]
    WHERE (RelatedId = @RelatedId AND RelatedTypeId=@RelatedTypeId)
RETURN 0
");
        }
        
        public override void Down()
        {
        }
    }
}
