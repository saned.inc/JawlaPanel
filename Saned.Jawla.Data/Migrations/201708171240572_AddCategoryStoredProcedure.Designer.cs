// <auto-generated />
namespace Saned.Jawla.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCategoryStoredProcedure : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCategoryStoredProcedure));
        
        string IMigrationMetadata.Id
        {
            get { return "201708171240572_AddCategoryStoredProcedure"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
