﻿namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mobile_ProjectDetails_SelectById : DbMigration
    {
        public override void Up()
        {
            Sql(@"

 CREATE PROC Mobile_ProjectDetails_SelectById

		@RelatedTypeId INT,
		@Id    bigint ,
        @UserId Nvarchar(128)=NULL
		    as
		            SELECT 
					Id,Name,
					(SELECT Name FROM Categories WHERE Categories.Id=[CategoryDetails].CategoryId) AS 'Category',
					(SELECT ArabicName FROM Countries WHERE Countries.Id=[CategoryDetails].CountryId) AS 'Country',
					(SELECT ArabicName FROM Cities WHERE Cities.Id=[CategoryDetails].CityId) AS 'City',
					(SELECT ArabicName FROM Municipalities WHERE Municipalities.Id=[CategoryDetails].MunicipalityId) AS 'Municipality',
					(SELECT AVG(TotalDegree)  FROM[dbo].[RatingUsers]
					 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedType = @RelatedTypeId
					 ) AS 'Rating' ,
					IsBin,
                  (SELECT  COUNT(Id)  FROM [HitCountDetails] WHERE RelatedId=[CategoryDetails].Id AND RelatedTypeId=@RelatedTypeId)
				   AS 'AllHit'
,
	        OverAllCount = COUNT(1) OVER()  ,
			[CategoryDetails].[CategoryId] ,
			[ProjectDetails].[ProjectId],
			[Latitude],
			[Longitude],
            [Description],
[Owner],
[AmbassadorId],
(SELECT Top (1) TotalDegree  FROM[dbo].[RatingUsers]
					 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedType = @RelatedTypeId
					 AND UserId=@UserId
					 ) AS 'UserRating',

			ISNULL((SELECT Name  FROM AspNetUsers WHERE [CategoryDetails].AmbassadorId=Id),N'غير معروف') AS 'Ambassador'
	 
		                   
FROM [CategoryDetails] INNER JOIN [ProjectDetails]
ON [CategoryDetails].Id=[ProjectDetails].ProjectId
WHERE 	[ProjectDetails].ProjectId=@Id
");
        }
        
        public override void Down()
        {
        }
    }
}
