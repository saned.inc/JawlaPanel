namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeOrderByMobile_CategoryDetails_SelectList : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER PROCEDURE [dbo].[Mobile_CategoryDetails_SelectList] 
                    @PageNumber int = 1,
                    @PageSize int = 8,
                    @CountryId int = Null,
                    @CityId int = Null,
                    @MunicipalityId int = Null,
                    @CategoryId int = Null,
                    @CategoryDetailsId bigint= Null,
                    @Lang  Nvarchar(150)= Null,
                    @Lat Nvarchar(150)= Null,
					@RelatedTypeId INT,
                    @EventStartDate DateTime=NULL,
@Status INT=NULL
                AS
                    SELECT 
					Id,Name,
					(SELECT Name FROM Categories WHERE Categories.Id=[CategoryDetails].CategoryId) AS 'Category',
					(SELECT ArabicName FROM Countries WHERE Countries.Id=[CategoryDetails].CountryId) AS 'Country',
					(SELECT ArabicName FROM Cities WHERE Cities.Id=[CategoryDetails].CityId) AS 'City',
					(SELECT ArabicName FROM Municipalities WHERE Municipalities.Id=[CategoryDetails].MunicipalityId) AS 'Municipality',
					(SELECT AVG(TotalDegree)  FROM[dbo].[RatingUsers]
					 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedType = @RelatedTypeId
					 ) AS 'Rating' ,
					IsBin,
                    CONVERT(VARCHAR(10),[EventStartDate],110) AS EventStartDate

                    ,(SELECT  COUNT(Id)  FROM [HitCountDetails] WHERE RelatedId=[CategoryDetails].Id AND RelatedTypeId=@RelatedTypeId) AS 'AllHit'
,
	           OverAllCount = COUNT(1) OVER()  ,
			  [CategoryDetails].[CategoryId]   ,
			  (SELECT TOP (1) [PhotoUrl] FROM [dbo].[Photos]
			  	 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedTypeId = @RelatedTypeId
					 AND [IsDefault]=1
			  )   As 'ImageUrl'               
FROM [CategoryDetails]
                    
                    WHERE
                

                    [CategoryDetails].IsDeleted = 0 and(
                    [dbo].[CategoryDetails].[CategoryId] = ISNULL(@CategoryId,[dbo].[CategoryDetails].[CategoryId]) ANd
                    [dbo].[CategoryDetails].[CountryId] = ISNULL(@CountryId,[dbo].[CategoryDetails].[CountryId]) ANd
                    [dbo].[CategoryDetails].[CityId] = ISNULL(@CityId,[dbo].[CategoryDetails].[CityId]) ANd
                    [dbo].[CategoryDetails].[MunicipalityId] = ISNULL(@MunicipalityId,[dbo].[CategoryDetails].[MunicipalityId]) ANd
                    [dbo].[CategoryDetails].[MunicipalityId] = ISNULL(@MunicipalityId,[dbo].[CategoryDetails].[MunicipalityId]) ANd
                    [dbo].[CategoryDetails].[Id] = ISNULL(@CategoryDetailsId,[dbo].[CategoryDetails].Id)
					--(@Name is null or @Name = ''  or[dbo].[CategoryDetails].Name LIKE '%' + @Name + '%') 
                    -- ANd (@Lang is null or @Lang = ''  or[dbo].[CategoryDetails].Name LIKE '%' + @Lang + '%') ANd
                    --(@Lat is null or @Lat = ''  or[dbo].[CategoryDetails].Name LIKE '%' + @Lat + '%')
					
					) AND
                    ([EventStartDate]=@EventStartDate OR [EventStartDate] IS NULL Or @EventStartDate IS NULL )
AND CategoryDetails.Status=ISNULL(@Status,Status)                
ORDER BY [CategoryDetails].IsBin DESC, 
                    [CategoryDetails].ModifiedDate
                    OFFSET @PageSize *(@PageNumber - 1) ROWS
                FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)


");
        }
        public override void Down()
        {
        }
    }
}
