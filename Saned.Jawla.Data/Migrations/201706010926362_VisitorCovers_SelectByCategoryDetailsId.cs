namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VisitorCovers_SelectByCategoryDetailsId : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE PROC VisitorCovers_SelectByCategoryDetailsId 
@CategoryDetailsId BIGINT,
@RelatedTypeId BIGINT
AS
SELECT * FROM
Photos 
WHERE
[RelatedTypeId]=@RelatedTypeId
AND [RelatedId]
IN (SELECT Id FROM [VisitorCovers] WHERE [CategoryDetailsId]=@CategoryDetailsId )
");
        }
        
        public override void Down()
        {
        }
    }
}
