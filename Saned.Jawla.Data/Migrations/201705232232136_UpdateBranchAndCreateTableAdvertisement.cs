namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBranchAndCreateTableAdvertisement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhotoUrl = c.String(),
                        Url = c.String(),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsDeleted = c.Boolean(),
                        IsShow = c.Boolean(nullable: false),
                        SecondNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.CategoryDetailsBranches", "Latitude", c => c.String());
            AddColumn("dbo.CategoryDetailsBranches", "Longitude", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CategoryDetailsBranches", "Longitude");
            DropColumn("dbo.CategoryDetailsBranches", "Latitude");
            DropTable("dbo.Advertisements");
        }
    }
}
