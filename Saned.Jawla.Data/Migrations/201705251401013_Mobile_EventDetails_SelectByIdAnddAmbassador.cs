﻿namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mobile_EventDetails_SelectByIdAnddAmbassador : DbMigration
    {
        public override void Up()
        {
            Sql(@"
 ALTER PROC Mobile_EventDetails_SelectById

		@RelatedTypeId INT,
		@Id    bigint ,
        @UserId Nvarchar(128)=NULL
		    as
		            SELECT 
					Id,Name,
					(SELECT Name FROM Categories WHERE Categories.Id=[CategoryDetails].CategoryId) AS 'Category',
					(SELECT ArabicName FROM Countries WHERE Countries.Id=[CategoryDetails].CountryId) AS 'Country',
					(SELECT ArabicName FROM Cities WHERE Cities.Id=[CategoryDetails].CityId) AS 'City',
					(SELECT ArabicName FROM Municipalities WHERE Municipalities.Id=[CategoryDetails].MunicipalityId) AS 'Municipality',
					(SELECT AVG(TotalDegree)  FROM[dbo].[RatingUsers]
					 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedType = @RelatedTypeId
					 ) AS 'Rating' ,
					IsBin,
                    CONVERT(VARCHAR(10),[EventStartDate],110) AS EventStartDate
					,(SELECT  COUNT(Id)  FROM [HitCountDetails] WHERE RelatedId=[CategoryDetails].Id AND RelatedTypeId=@RelatedTypeId) AS 'AllHit'
,
	           OverAllCount = COUNT(1) OVER()  ,
			[CategoryDetails].[CategoryId] ,
			[EventDetails].[EventId],
			CONVERT(VARCHAR(10),[EndDate],110) AS EventEndDate,
			[Latitude],
			[Longitude],
            [Description],
[Owner],
[StartTime],
[EndTime],
[AmbassadorId],
(SELECT Top (1) TotalDegree  FROM[dbo].[RatingUsers]
					 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedType = @RelatedTypeId
					 AND UserId=@UserId
					 ) AS 'UserRating',

	ISNULL((SELECT Name  FROM AspNetUsers WHERE [CategoryDetails].AmbassadorId=Id),N'غير معروف') AS 'Ambassador'
			 
			                   
FROM [CategoryDetails] INNER JOIN [dbo].[EventDetails]
ON [CategoryDetails].Id=[dbo].[EventDetails].[EventId]

WHERE 	[EventDetails].[EventId]=@Id
");
        }
        
        public override void Down()
        {
        }
    }
}
