namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAdvertisementTableAddColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisements", "AmbassadorId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertisements", "AmbassadorId");
        }
    }
}
