namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateContactType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoryDetailsContacts", "SoicalType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CategoryDetailsContacts", "SoicalType");
        }
    }
}
