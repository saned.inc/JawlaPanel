namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParentIdToCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "ParentId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "ParentId");
        }
    }
}
