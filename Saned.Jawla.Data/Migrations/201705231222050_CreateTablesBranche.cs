namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTablesBranche : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryDetailsBranches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CountryId = c.Int(),
                        CityId = c.Int(),
                        MunicipalityId = c.Int(),
                        CategoryDetailsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetailsId, cascadeDelete: true)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityId)
                .Index(t => t.CountryId)
                .Index(t => t.CityId)
                .Index(t => t.MunicipalityId)
                .Index(t => t.CategoryDetailsId);
            
            CreateTable(
                "dbo.CategoryDetailsContacts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ContactInfo = c.String(),
                        Icon = c.String(),
                        ContactTypeId = c.Int(nullable: false),
                        CategoryDetailsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetailsId, cascadeDelete: true)
                .ForeignKey("dbo.CategoryContactTypes", t => t.ContactTypeId, cascadeDelete: true)
                .Index(t => t.ContactTypeId)
                .Index(t => t.CategoryDetailsId);
            
            CreateTable(
                "dbo.CategoryDetailsFaqs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        Answer = c.String(),
                        CategoryDetailsId = c.Long(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetailsId, cascadeDelete: true)
                .Index(t => t.CategoryDetailsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryDetailsFaqs", "CategoryDetailsId", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetailsContacts", "ContactTypeId", "dbo.CategoryContactTypes");
            DropForeignKey("dbo.CategoryDetailsContacts", "CategoryDetailsId", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetailsBranches", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.CategoryDetailsBranches", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.CategoryDetailsBranches", "CityId", "dbo.Cities");
            DropForeignKey("dbo.CategoryDetailsBranches", "CategoryDetailsId", "dbo.CategoryDetails");
            DropIndex("dbo.CategoryDetailsFaqs", new[] { "CategoryDetailsId" });
            DropIndex("dbo.CategoryDetailsContacts", new[] { "CategoryDetailsId" });
            DropIndex("dbo.CategoryDetailsContacts", new[] { "ContactTypeId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CategoryDetailsId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "MunicipalityId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CityId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CountryId" });
            DropTable("dbo.CategoryDetailsFaqs");
            DropTable("dbo.CategoryDetailsContacts");
            DropTable("dbo.CategoryDetailsBranches");
        }
    }
}
