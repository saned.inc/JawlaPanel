﻿using System.Collections.Generic;
using Saned.Jawla.Data.Core.Models;

namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Saned.Jawla.Data.Persistence.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        protected override void Seed(Saned.Jawla.Data.Persistence.ApplicationDbContext context)
        {



            if (!context.Categories.Any())
            {
                //context.Categories.AddRange(BuilCategory());
                context.SaveChanges();
            }
            if (!context.Countries.Any())
            {

                context.Countries.AddRange(BuilCountry());
                context.SaveChanges();
            }

            if (!context.CategoryContactTypes.Any())
            {
                context.CategoryContactTypes.AddRange(BuilCategoryContactType());
                context.SaveChanges();
            }

       
        }

        private static IEnumerable<Category> BuilCategory()
        {
            var list = new List<Category>
            {

                new Category
                {
                    Id = 1,
                    IsDeleted = false,
                    ModifiedDate = DateTime.Now,
                    Name = "مطاعم",
                    PhotoUrl = "img/1.png"
                },
                new Category
                {
                    Id = 2,
                    IsDeleted = false,
                    ModifiedDate = DateTime.Now,
                    Name = "أماكن سياحية",
                    PhotoUrl = "img/2.png"
                },
                   new Category
                {
                    Id = 5,
                    IsDeleted = false,
                    ModifiedDate = DateTime.Now,
                    Name = "أماكن تاريخية",
                    PhotoUrl = "img/3.png"
                }
                   ,
                   new Category
                {
                    Id = 6,
                    IsDeleted = false,
                    ModifiedDate = DateTime.Now,
                    Name = "فنادق",
                    PhotoUrl = "img/4.png"
                }
                   ,
                   new Category
                {
                    Id = 7,
                    IsDeleted = false,
                    ModifiedDate = DateTime.Now,
                    Name = "فعاليات",
                    PhotoUrl = "img/5.png"
                }
                    ,
                   new Category
                {
                    Id = 8,
                    IsDeleted = false,
                    ModifiedDate = DateTime.Now,
                    Name = "مشاريع مميزة",
                    PhotoUrl = "img/6.png"
                }
            };

            return list;
        }

        private static IEnumerable<Country> BuilCountry()
        {
            var list = new List<Country>
            {
                new Country
                {
                    IsDeleted = false,
                    ArabicName = "السعودية",
                    EnglishName = "Saudi Arabia",
                    Latitude = "23.885942",
                    Longitude = "45.079162"
                },
                new Country
                {
                    IsDeleted = false,
                    ArabicName = "مصر",
                    EnglishName = "Egypt",
                    Latitude = "26.820553",
                    Longitude = "30.802498"
                },
            };

            return list;
        }

        private static IEnumerable<CategoryContactType> BuilCategoryContactType()
        {
            var list = new List<CategoryContactType>
            {
                new CategoryContactType
                {
                   Id = 1,
                   TypeValue = "ProjectContact"
                },
                 new CategoryContactType
                {
                   Id = 2,
                   TypeValue = "OwerContact"
                },
            };

            return list;
        }
    }
}
