// <auto-generated />
namespace Saned.Jawla.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ChangeOrderByMobile_CategoryDetails_SelectList : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ChangeOrderByMobile_CategoryDetails_SelectList));
        
        string IMigrationMetadata.Id
        {
            get { return "201705231440020_ChangeOrderByMobile_CategoryDetails_SelectList"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
