namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryContactTypeTableCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryContactTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TypeValue = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.ApplicationContactInformations", "PhoneNumber", c => c.String(maxLength: 100));
            AlterColumn("dbo.ApplicationContactInformations", "Email", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "FacebookUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "TwitterUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "InstagrameUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "YoutubeUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "GooglePlusUrl", c => c.String(maxLength: 250));
            AlterColumn("dbo.ApplicationContactInformations", "LinkedInUrl", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ApplicationContactInformations", "LinkedInUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "GooglePlusUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "YoutubeUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "InstagrameUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "TwitterUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "FacebookUrl", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "Email", c => c.String());
            AlterColumn("dbo.ApplicationContactInformations", "PhoneNumber", c => c.String());
            DropTable("dbo.CategoryContactTypes");
        }
    }
}
