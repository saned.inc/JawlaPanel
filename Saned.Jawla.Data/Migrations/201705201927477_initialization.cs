namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AmbassadorRequests",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FullName = c.String(maxLength: 250),
                        Age = c.Int(nullable: false),
                        Address = c.String(maxLength: 500),
                        PhoneNumber = c.String(maxLength: 250),
                        Email = c.String(),
                        SoicalLinks = c.String(maxLength: 500),
                        PhotoUrl = c.String(maxLength: 500),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Status = c.Int(nullable: false),
                        UserId = c.String(),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationContactInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(maxLength: 100),
                        Email = c.String(maxLength: 250),
                        FacebookUrl = c.String(maxLength: 250),
                        TwitterUrl = c.String(maxLength: 250),
                        InstagrameUrl = c.String(maxLength: 250),
                        YoutubeUrl = c.String(maxLength: 250),
                        GooglePlusUrl = c.String(maxLength: 250),
                        LinkedInUrl = c.String(maxLength: 250),
                        AboutUs = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationContactUs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 150),
                        PhoneNumber = c.String(maxLength: 100),
                        Email = c.String(maxLength: 250),
                        Message = c.String(),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(maxLength: 150),
                        PhotoUrl = c.String(maxLength: 250),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryDetails",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        CountryId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        MunicipalityId = c.Int(nullable: false),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsDeleted = c.Boolean(),
                        IsBin = c.Boolean(),
                        Status = c.String(),
                        AmbassadorId = c.String(),
                        EventStartDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.CountryId)
                .Index(t => t.CityId)
                .Index(t => t.MunicipalityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArabicName = c.String(nullable: false, maxLength: 150),
                        EnglishName = c.String(nullable: false, maxLength: 150),
                        IsDeleted = c.Boolean(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArabicName = c.String(),
                        EnglishName = c.String(),
                        IsDeleted = c.Boolean(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventDetails",
                c => new
                    {
                        EventId = c.Long(nullable: false),
                        Owner = c.String(maxLength: 150),
                        EndDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.EventId)
                .ForeignKey("dbo.CategoryDetails", t => t.EventId)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.HotelsDetails",
                c => new
                    {
                        HotelId = c.Long(nullable: false),
                        WorkHours = c.String(),
                    })
                .PrimaryKey(t => t.HotelId)
                .ForeignKey("dbo.CategoryDetails", t => t.HotelId)
                .Index(t => t.HotelId);
            
            CreateTable(
                "dbo.Municipalities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArabicName = c.String(nullable: false, maxLength: 150),
                        EnglishName = c.String(nullable: false, maxLength: 150),
                        IsDeleted = c.Boolean(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.ProjectDetails",
                c => new
                    {
                        ProjectId = c.Long(nullable: false),
                        Owner = c.String(),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.CategoryDetails", t => t.ProjectId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.CategoryDetailsBranches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        CategoryDetailsId = c.Long(nullable: false),
                        CountryId = c.Int(),
                        CityId = c.Int(),
                        MunicipalityId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetailsId, cascadeDelete: true)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityId)
                .Index(t => t.CategoryDetailsId)
                .Index(t => t.CountryId)
                .Index(t => t.CityId)
                .Index(t => t.MunicipalityId);
            
            CreateTable(
                "dbo.CategoryDetailsContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactInfo = c.String(),
                        Icon = c.String(),
                        ContactTypeId = c.Long(nullable: false),
                        CategoryDetailsId = c.Int(nullable: false),
                        CategoryDetails_Id = c.Long(),
                        ContactType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetails_Id)
                .ForeignKey("dbo.CategoryContactTypes", t => t.ContactType_Id)
                .Index(t => t.CategoryDetails_Id)
                .Index(t => t.ContactType_Id);
            
            CreateTable(
                "dbo.CategoryContactTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryDetailsFaqs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        Answer = c.String(),
                        CategoryDetailsId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetailsId, cascadeDelete: true)
                .Index(t => t.CategoryDetailsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryDetailsFaqs", "CategoryDetailsId", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetailsContacts", "ContactType_Id", "dbo.CategoryContactTypes");
            DropForeignKey("dbo.CategoryDetailsContacts", "CategoryDetails_Id", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetailsBranches", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.CategoryDetailsBranches", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.CategoryDetailsBranches", "CityId", "dbo.Cities");
            DropForeignKey("dbo.CategoryDetailsBranches", "CategoryDetailsId", "dbo.CategoryDetails");
            DropForeignKey("dbo.ProjectDetails", "ProjectId", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetails", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.Municipalities", "CityId", "dbo.Cities");
            DropForeignKey("dbo.HotelsDetails", "HotelId", "dbo.CategoryDetails");
            DropForeignKey("dbo.EventDetails", "EventId", "dbo.CategoryDetails");
            DropForeignKey("dbo.CategoryDetails", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.CategoryDetails", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Cities", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.CategoryDetails", "CategoryId", "dbo.Categories");
            DropIndex("dbo.CategoryDetailsFaqs", new[] { "CategoryDetailsId" });
            DropIndex("dbo.CategoryDetailsContacts", new[] { "ContactType_Id" });
            DropIndex("dbo.CategoryDetailsContacts", new[] { "CategoryDetails_Id" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "MunicipalityId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CityId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CountryId" });
            DropIndex("dbo.CategoryDetailsBranches", new[] { "CategoryDetailsId" });
            DropIndex("dbo.ProjectDetails", new[] { "ProjectId" });
            DropIndex("dbo.Municipalities", new[] { "CityId" });
            DropIndex("dbo.HotelsDetails", new[] { "HotelId" });
            DropIndex("dbo.EventDetails", new[] { "EventId" });
            DropIndex("dbo.Cities", new[] { "CountryId" });
            DropIndex("dbo.CategoryDetails", new[] { "MunicipalityId" });
            DropIndex("dbo.CategoryDetails", new[] { "CityId" });
            DropIndex("dbo.CategoryDetails", new[] { "CountryId" });
            DropIndex("dbo.CategoryDetails", new[] { "CategoryId" });
            DropTable("dbo.CategoryDetailsFaqs");
            DropTable("dbo.CategoryContactTypes");
            DropTable("dbo.CategoryDetailsContacts");
            DropTable("dbo.CategoryDetailsBranches");
            DropTable("dbo.ProjectDetails");
            DropTable("dbo.Municipalities");
            DropTable("dbo.HotelsDetails");
            DropTable("dbo.EventDetails");
            DropTable("dbo.Countries");
            DropTable("dbo.Cities");
            DropTable("dbo.CategoryDetails");
            DropTable("dbo.Categories");
            DropTable("dbo.ApplicationContactUs");
            DropTable("dbo.ApplicationContactInformations");
            DropTable("dbo.AmbassadorRequests");
        }
    }
}
