namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForceCategoryDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EventDetails", "EventId", "dbo.CategoryDetails");
            DropForeignKey("dbo.HotelsDetails", "HotelId", "dbo.CategoryDetails");
            DropForeignKey("dbo.ProjectDetails", "ProjectId", "dbo.CategoryDetails");
            AddForeignKey("dbo.EventDetails", "EventId", "dbo.CategoryDetails", "Id", cascadeDelete: true);
            AddForeignKey("dbo.HotelsDetails", "HotelId", "dbo.CategoryDetails", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProjectDetails", "ProjectId", "dbo.CategoryDetails", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectDetails", "ProjectId", "dbo.CategoryDetails");
            DropForeignKey("dbo.HotelsDetails", "HotelId", "dbo.CategoryDetails");
            DropForeignKey("dbo.EventDetails", "EventId", "dbo.CategoryDetails");
            AddForeignKey("dbo.ProjectDetails", "ProjectId", "dbo.CategoryDetails", "Id");
            AddForeignKey("dbo.HotelsDetails", "HotelId", "dbo.CategoryDetails", "Id");
            AddForeignKey("dbo.EventDetails", "EventId", "dbo.CategoryDetails", "Id");
        }
    }
}
