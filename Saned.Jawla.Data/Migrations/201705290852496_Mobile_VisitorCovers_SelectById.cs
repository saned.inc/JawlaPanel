﻿namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mobile_VisitorCovers_SelectById : DbMigration
    {
        public override void Up()
        {
            Sql(@"

CREATE PROC Mobile_VisitorCovers_SelectById
@RelatedTypeId INT,
@Id bigint ,
@UserId Nvarchar(128)=NULL
AS
SELECT 
[CategoryDetailsId],
Name,
(SELECT Name FROM Categories WHERE Categories.Id=[CategoryDetails].CategoryId) AS 'Category',
(
SELECT AVG(TotalDegree)  FROM[dbo].[RatingUsers]
WHERE RelatedId = [CategoryDetails].Id 
AND RelatedType = @RelatedTypeId
) AS 'Rating' ,
(
SELECT  COUNT(Id)  FROM [HitCountDetails] 
WHERE RelatedId=[CategoryDetails].Id 
AND RelatedTypeId=@RelatedTypeId) AS 'AllHit',
[CategoryDetails].[CategoryId],
[AmbassadorId],
(SELECT Top (1) TotalDegree  FROM[dbo].[RatingUsers]
WHERE RelatedId = [CategoryDetails].Id 
AND RelatedType = @RelatedTypeId
AND UserId=@UserId
) AS 'UserRating',
ISNULL((SELECT Name  FROM AspNetUsers WHERE [CategoryDetails].AmbassadorId=Id),N'غير معروف') AS 'Ambassador'
FROM [CategoryDetails] INNER JOIN [dbo].[VisitorCovers]
ON [dbo].[VisitorCovers].[CategoryDetailsId]=[CategoryDetails].Id
AND [UseId]=@UserId
AND [dbo].[VisitorCovers].Id=@Id

");
        }
        
        public override void Down()
        {
        }
    }
}
