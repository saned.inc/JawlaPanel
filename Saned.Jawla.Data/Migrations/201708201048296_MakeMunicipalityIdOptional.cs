namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeMunicipalityIdOptional : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CategoryDetails", "MunicipalityId", "dbo.Municipalities");
            DropIndex("dbo.CategoryDetails", new[] { "MunicipalityId" });
            AlterColumn("dbo.CategoryDetails", "MunicipalityId", c => c.Int());
            CreateIndex("dbo.CategoryDetails", "MunicipalityId");
            AddForeignKey("dbo.CategoryDetails", "MunicipalityId", "dbo.Municipalities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryDetails", "MunicipalityId", "dbo.Municipalities");
            DropIndex("dbo.CategoryDetails", new[] { "MunicipalityId" });
            AlterColumn("dbo.CategoryDetails", "MunicipalityId", c => c.Int(nullable: false));
            CreateIndex("dbo.CategoryDetails", "MunicipalityId");
            AddForeignKey("dbo.CategoryDetails", "MunicipalityId", "dbo.Municipalities", "Id", cascadeDelete: true);
        }
    }
}
