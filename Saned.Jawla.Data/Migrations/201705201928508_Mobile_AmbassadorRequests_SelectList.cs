namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mobile_AmbassadorRequests_SelectList : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE PROC Mobile_AmbassadorRequests_SelectList
@PageNumber [INT] = 1,
@PageSize [INT] = 10,
@Status INT 
AS 
SELECT
	[AmbassadorRequests].Id,
	Name,
	[AspNetUsers].[PhotoUrl],
	OverAllCount = COUNT(1) OVER()
FROM
	[AspNetUsers]
INNER JOIN
	[AmbassadorRequests]
ON
	[AspNetUsers].Id=[AmbassadorRequests].UserId
WHERE 
	[AspNetUsers].IsDeleted=0
AND
	[AmbassadorRequests].Status=@Status
ORDER BY
	[Id] 
	OFFSET @PageSize * (@PageNumber - 1) ROWS
FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)

");
        }
        
        public override void Down()
        {
            Sql(@"DROP PROC Mobile_AmbassadorRequests_SelectList");
        }
    }
}
