namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateFavouriteTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Favourites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryDetailsId = c.Long(nullable: false),
                        UserId = c.String(),
                        IsDeleted = c.Boolean(),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryDetails", t => t.CategoryDetailsId, cascadeDelete: true)
                .Index(t => t.CategoryDetailsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Favourites", "CategoryDetailsId", "dbo.CategoryDetails");
            DropIndex("dbo.Favourites", new[] { "CategoryDetailsId" });
            DropTable("dbo.Favourites");
        }
    }
}
