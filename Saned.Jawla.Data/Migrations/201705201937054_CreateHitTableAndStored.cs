namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateHitTableAndStored : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE PROCEDURE [dbo].[HitCounts_Save]
@UserId NVARCHAR(128)=0,
@DeviceId NVARCHAR(MAX)=NULL,
@RelatedTypeId INT,
@RelatedId NVARCHAR(MAX),
@Hits INT =1 ,
@ModifiedDate DATETIME 
AS
IF EXISTS (
SELECT Id 
FROM 
[HitCountDetails]
WHERE
UserId=ISNULL(@UserId,UserId) AND
DeviceId=ISNULL(@DeviceId,DeviceId) AND
RelatedTypeId=ISNULL(@RelatedTypeId,RelatedTypeId) AND
RelatedId=ISNULL(@RelatedId,@RelatedId)
)
BEGIN
UPDATE [HitCountDetails] SET [Hits]=Hits+1
WHERE
UserId=ISNULL(@UserId,UserId) AND
DeviceId=ISNULL(@DeviceId,DeviceId) AND
RelatedTypeId=ISNULL(@RelatedTypeId,RelatedTypeId) AND
RelatedId=ISNULL(@RelatedId,RelatedId)

UPDATE  [dbo].[HitCounts] SET [Hits]=Hits+1
WHERE
RelatedTypeId=ISNULL(@RelatedTypeId,RelatedTypeId) AND
[RelatedId]=ISNULL(@RelatedId,RelatedId)
END

ELSE
BEGIN

INSERT INTO [dbo].[HitCountDetails]
([UserId],[DeviceId],[RelatedTypeId],[RelatedId],[Hits],[ModifiedDate])
VALUES
(@UserId,@DeviceId,@RelatedTypeId,@RelatedId,@Hits,@ModifiedDate)

IF EXISTS (SELECT Id FROM [HitCounts]
WHERE 
RelatedTypeId=ISNULL(@RelatedTypeId,RelatedTypeId) AND
[RelatedId]=ISNULL(@RelatedId,RelatedId)
)
BEGIN
UPDATE  [dbo].[HitCounts] SET [Hits]=Hits+1
WHERE
RelatedTypeId=ISNULL(@RelatedTypeId,RelatedTypeId) AND
[RelatedId]=ISNULL(@RelatedId,RelatedId)
END
ELSE
BEGIN
INSERT INTO [HitCounts]
([RelatedId],[RelatedTypeId],[Hits])
VALUES
(@RelatedId,@RelatedTypeId,@Hits)
END


END

GO
CREATE TABLE [dbo].[HitCountDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NULL,
	[DeviceId] [nvarchar](max) NULL,
	[RelatedTypeId] [int] NOT NULL,
	[RelatedId] [nvarchar](max) NOT NULL,
	[Hits] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
CREATE TABLE [dbo].[HitCounts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RelatedId] [nvarchar](max) NOT NULL,
	[RelatedTypeId] [int] NOT NULL,
	[Hits] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


");
        }
        
        public override void Down()
        {

            Sql(@"DROP PROC HitCounts_Save");
            DropTable("[dbo].[HitCountDetails]");
            DropTable("[dbo].[HitCounts]");

        }
    }
}
