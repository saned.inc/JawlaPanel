namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_Country_city_municipality : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CategoryDetailsBranches", "Latitude");
            DropColumn("dbo.CategoryDetailsBranches", "Longitude");
            DropTable("dbo.Advertisements");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhotoUrl = c.String(),
                        Url = c.String(),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsDeleted = c.Boolean(),
                        IsShow = c.Boolean(nullable: false),
                        SecondNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.CategoryDetailsBranches", "Longitude", c => c.String());
            AddColumn("dbo.CategoryDetailsBranches", "Latitude", c => c.String());
        }
    }
}
