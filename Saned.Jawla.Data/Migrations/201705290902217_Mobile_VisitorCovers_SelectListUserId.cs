namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mobile_VisitorCovers_SelectListUserId : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE PROCEDURE [dbo].Mobile_VisitorCovers_SelectListUserId 
                    @PageNumber int = 1,
                    @PageSize int = 8,
                    @UserId NVARCHAR(128),
                    @Status INT=NULL,
					@RelatedTypeId INT
                AS
                    SELECT 
					CategoryDetails.Id,Name,
					(SELECT Name FROM Categories WHERE Categories.Id=[CategoryDetails].CategoryId) AS 'Category',
					(SELECT ArabicName FROM Countries WHERE Countries.Id=[CategoryDetails].CountryId) AS 'Country',
					(SELECT ArabicName FROM Cities WHERE Cities.Id=[CategoryDetails].CityId) AS 'City',
					(SELECT ArabicName FROM Municipalities WHERE Municipalities.Id=[CategoryDetails].MunicipalityId) AS 'Municipality',
					(SELECT AVG(TotalDegree)  FROM[dbo].[RatingUsers]
					 WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedType = @RelatedTypeId
					 ) AS 'Rating' ,
					IsBin,
                    CONVERT(VARCHAR(10),[EventStartDate],110) AS EventStartDate

                    ,(SELECT  COUNT(Id)  FROM [HitCountDetails] WHERE RelatedId=[CategoryDetails].Id AND RelatedTypeId=@RelatedTypeId) AS 'AllHit'
,
	           OverAllCount = COUNT(1) OVER()  ,
			  [CategoryDetails].[CategoryId]   ,
			  (SELECT TOP (1) [PhotoUrl] FROM [dbo].[Photos] WHERE RelatedId = [CategoryDetails].Id 
					 AND RelatedTypeId = @RelatedTypeId
					 AND [IsDefault]=1
			  )   As 'ImageUrl'               
FROM [CategoryDetails] INNER JOIN [VisitorCovers]
ON [CategoryDetails].Id=[VisitorCovers].[CategoryDetailsId]
                    
                    WHERE
                

                     [CategoryDetails].IsDeleted = 0 AND
					 [CategoryDetails].Status=@Status AND
					 [VisitorCovers].[UseId]=@UserId
				
                                  
ORDER BY [CategoryDetails].IsBin DESC, 
                    [CategoryDetails].ModifiedDate
                    OFFSET @PageSize *(@PageNumber - 1) ROWS
                FETCH NEXT @PageSize ROWS ONLY OPTION(RECOMPILE)


");
        }
        
        public override void Down()
        {
        }
    }
}
