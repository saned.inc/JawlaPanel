namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryStoredProcedure : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                "dbo.Category_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 150),
                        PhotoUrl = p.String(maxLength: 250),
                        ModifiedDate = p.DateTime(storeType: "datetime2"),
                        IsDeleted = p.Boolean(),
                        ParentId = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Categories]([Name], [PhotoUrl], [ModifiedDate], [IsDeleted], [ParentId])
                      VALUES (@Name, @PhotoUrl, @ModifiedDate, @IsDeleted, @ParentId)
                      
                      DECLARE @Id int
                      SELECT @Id = [Id]
                      FROM [dbo].[Categories]
                      WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
                      
                      SELECT t0.[Id]
                      FROM [dbo].[Categories] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[Id] = @Id"
            );
            
            CreateStoredProcedure(
                "dbo.Category_Update",
                p => new
                    {
                        Id = p.Int(),
                        Name = p.String(maxLength: 150),
                        PhotoUrl = p.String(maxLength: 250),
                        ModifiedDate = p.DateTime(storeType: "datetime2"),
                        IsDeleted = p.Boolean(),
                        ParentId = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Categories]
                      SET [Name] = @Name, [PhotoUrl] = @PhotoUrl, [ModifiedDate] = @ModifiedDate, [IsDeleted] = @IsDeleted, [ParentId] = @ParentId
                      WHERE ([Id] = @Id)"
            );
            
            CreateStoredProcedure(
                "dbo.Category_Delete",
                p => new
                    {
                        Id = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Categories]
                      WHERE ([Id] = @Id)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Category_Delete");
            DropStoredProcedure("dbo.Category_Update");
            DropStoredProcedure("dbo.Category_Insert");
        }
    }
}
