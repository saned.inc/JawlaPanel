namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGetUserCoverSp : DbMigration
    {
        public override void Up()
        {
            Sql(@"Create PROCEDURE GetUserCover
@PageNumber [INT] = 1,
@PageSize [INT] = 10,
    @UserId NVARCHAR(128)
AS
BEGIN
 SELECT
[CategoryDetails].Id,
[CategoryDetails].Name,
(
SELECT 
Name 
FROM 
Categories
WHERE 
Categories.Id=[CategoryDetails].CategoryId
)
 AS 'CategoryName',
[VisitorCovers].Id as 'CoverId',
CONVERT(VARCHAR(10),[VisitorCovers].ModifiedDate,110) AS 'CoverModifiedDate',
(SELECT Top (1) TotalDegree  FROM[dbo].[RatingUsers]
 WHERE RelatedId = [CategoryDetails].Id 
AND RelatedType = 1
 AND UserId=@UserId
)
 AS 'UserRating',
(SELECT COUNT (Id) FROM [dbo].[Comments]
WHERE RelatedId = [CategoryDetails].Id 
AND RelatedId = 1
 AND UserId=@UserId)  as 'CommentCount',

 (SELECT COUNT (Id) FROM [Photos]
WHERE RelatedId = [CategoryDetails].Id 
AND [RelatedTypeId] =2
 AND [RelatedId]=@UserId)  as 'PhotoCount'

FROM 
[CategoryDetails]
INNER JOIN
[VisitorCovers]
ON [VisitorCovers].CategoryDetailsId=[CategoryDetails].Id

ORDER BY
	[Id] 
	OFFSET @PageSize * (@PageNumber - 1) ROWS
FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)
END
");
        }
        
        public override void Down()
        {
            Sql(@"DROP PROC GetUserCover");
        }
    }
}
