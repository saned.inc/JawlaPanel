namespace Saned.Jawla.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimeToEvent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventDetails", "StartTime", c => c.String(maxLength: 50));
            AddColumn("dbo.EventDetails", "EndTime", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventDetails", "EndTime");
            DropColumn("dbo.EventDetails", "StartTime");
        }
    }
}
